public function up()
{
    $this->createTable('content', array(
        'id'=>'pk',
        'title'=>'varchar(500) NOT NULL',
        'course_id'=>'int(11) NOT NULL',
        'parent_id'=>'int(11) NOT NULL DEFAULT '-1'',
        'detail'=>'text DEFAULT NULL',
        'show_order'=>'tinyint(3) NOT NULL DEFAULT '1'',
        'file_id'=>'int(11) DEFAULT NULL',
        'is_exercise'=>'tinyint(1) NOT NULL',
    ), '');

    $this->createIndex('idx_course_id', 'content', 'course_id', FALSE);

    $this->createTable('content_info', array(
        'course_id'=>'int(11) DEFAULT NULL',
        'course_name'=>'varchar(500) DEFAULT NULL',
        'number_of_content'=>'bigint(21) NOT NULL DEFAULT '0'',
    ), '');

    $this->createTable('coupon', array(
        'id'=>'pk',
        'code'=>'varchar(500) NOT NULL',
        'discount'=>'tinyint(3) NOT NULL',
        'expire_date'=>'date NOT NULL',
        'qty_use'=>'tinyint(5) NOT NULL DEFAULT '1'',
        'detail'=>'varchar(1000) DEFAULT NULL',
        'create_date'=>'datetime NOT NULL',
        'course_id'=>'int(11) NOT NULL DEFAULT '-1'',
        'user_id'=>'int(11) NOT NULL',
    ), '');

    $this->createIndex('idx_course_id', 'coupon', 'course_id', FALSE);

    $this->createIndex('idx_user_id', 'coupon', 'user_id', FALSE);

    $this->createTable('coupon_log', array(
        'coupon_id'=>'int(11) NOT NULL',
        'use_course_id'=>'int(11) NOT NULL',
        'used_time'=>'datetime NOT NULL',
        'use_user_id'=>'int(11) NOT NULL',
    ), '');

    $this->createTable('course', array(
        'id'=>'pk',
        'name'=>'varchar(500) NOT NULL',
        'name_th'=>'varchar(500) NOT NULL',
        'tag'=>'varchar(20) NOT NULL',
        'description'=>'text DEFAULT NULL',
        'about'=>'text NOT NULL',
        'price'=>'int(5) NOT NULL',
        'course_img'=>'varchar(1000) DEFAULT NULL',
        'course_status'=>'tinyint(1) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'commission'=>'int(11) NOT NULL',
        'duration'=>'tinyint(5) DEFAULT NULL',
        'is_public'=>'tinyint(1) NOT NULL DEFAULT '1'',
        'demo_video'=>'int(11) DEFAULT NULL',
    ), '');

    $this->createIndex('idx_user_id', 'course', 'user_id', FALSE);

    $this->createTable('exercise', array(
        'id'=>'pk',
        'content_id'=>'int(11) NOT NULL',
        'qty_show'=>'int(2) NOT NULL DEFAULT '10'',
        'is_random'=>'tinyint(1) NOT NULL',
    ), '');

    $this->createIndex('idx_content_id', 'exercise', 'content_id', FALSE);

    $this->createTable('exercise_answer_choice', array(
        'id'=>'pk',
        'answer'=>'varchar(1000) NOT NULL',
        'is_true'=>'tinyint(1) NOT NULL',
        'question_id'=>'int(11) NOT NULL',
    ), '');

    $this->createIndex('idx_question_id', 'exercise_answer_choice', 'question_id', FALSE);

    $this->createTable('exercise_choice_score', array(
        'id'=>'pk',
        'answer_id'=>'int(11) NOT NULL',
        'question_id'=>'int(11) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'do_time'=>'datetime NOT NULL',
        'is_true'=>'tinyint(1) NOT NULL',
    ), '');

    $this->createTable('exercise_question', array(
        'id'=>'pk',
        'question'=>'text NOT NULL',
        'show_order'=>'tinyint(2) NOT NULL DEFAULT '1'',
        'type'=>'tinyint(1) NOT NULL DEFAULT '1'',
        'exercise_id'=>'int(11) NOT NULL',
    ), '');

    $this->createIndex('idx_exercise_id', 'exercise_question', 'exercise_id', FALSE);

    $this->createTable('exercise_stat', array(
        'id'=>'pk',
        'exercise_id'=>'int(11) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'do_time'=>'datetime NOT NULL',
        'qty_question'=>'int(3) NOT NULL',
        'qty_correct'=>'int(3) NOT NULL',
        'question_list'=>'varchar(300) DEFAULT NULL',
    ), '');

    $this->createTable('exercise_text_answer', array(
        'id'=>'pk',
        'answer'=>'varchar(1000) NOT NULL',
        'question_id'=>'int(11) NOT NULL',
        'sequence'=>'int(2) NOT NULL DEFAULT '1'',
    ), '');

    $this->createIndex('idx_question_id', 'exercise_text_answer', 'question_id', FALSE);

    $this->createTable('exercise_text_score', array(
        'id'=>'pk',
        'question_id'=>'int(11) NOT NULL',
        'answer'=>'varchar(3000) DEFAULT NULL',
        'user_id'=>'int(11) NOT NULL',
        'do_time'=>'datetime NOT NULL',
        'sequence'=>'tinyint(3) NOT NULL DEFAULT '1'',
        'is_true'=>'tinyint(1) NOT NULL',
    ), '');

    $this->createTable('external_provider', array(
        'id'=>'pk',
        'user_id'=>'int(11) NOT NULL',
        'provider'=>'varchar(100) NOT NULL DEFAULT 'facebook'',
        'provider_id'=>'int(11) NOT NULL',
    ), '');

    $this->createIndex('idx_user_id', 'external_provider', 'user_id', FALSE);

    $this->createTable('file', array(
        'id'=>'pk',
        'name'=>'varchar(1000) NOT NULL',
        'original_name'=>'varchar(1000) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'type'=>'varchar(100) NOT NULL',
        'create_at'=>'datetime NOT NULL',
    ), '');

    $this->createIndex('idx_user_id', 'file', 'user_id', FALSE);

    $this->createTable('file_content', array(
        'id'=>'pk',
        'file_id'=>'int(11) NOT NULL',
        'content_id'=>'int(11) NOT NULL',
    ), '');

    $this->createIndex('idx_content_id', 'file_content', 'content_id', FALSE);

    $this->createIndex('idx_file_id', 'file_content', 'file_id', FALSE);

    $this->createTable('relate_course', array(
        'id'=>'pk',
        'course_id'=>'int(11) NOT NULL',
        'before_course'=>'varchar(100) DEFAULT NULL',
        'after_course'=>'varchar(100) DEFAULT NULL',
    ), '');

    $this->createTable('stat_user_study', array(
        'content_has_studied'=>'bigint(21) NOT NULL DEFAULT '0'',
        'user_id'=>'int(11) DEFAULT NULL',
        'course_id'=>'int(11) DEFAULT NULL',
        'number_of_content'=>'bigint(21) DEFAULT NULL',
    ), '');

    $this->createTable('tbl_migration', array(
        'version'=>'varchar(255) NOT NULL',
        'apply_time'=>'int(11) DEFAULT NULL',
    ), '');

    $this->addPrimaryKey('pk_tbl_migration', 'tbl_migration', 'version');

    $this->createTable('tbl_profiles', array(
        'user_id'=>'pk',
        'lastname'=>'varchar(50) NOT NULL',
        'firstname'=>'varchar(50) NOT NULL',
        'detail'=>'varchar(3000) DEFAULT NULL',
        'photo'=>'varchar(500) DEFAULT NULL',
    ), '');

    $this->createIndex('idx_user_id', 'tbl_profiles', 'user_id', FALSE);

    $this->createTable('tbl_profiles_fields', array(
        'id'=>'pk',
        'varname'=>'varchar(50) NOT NULL',
        'title'=>'varchar(255) NOT NULL',
        'field_type'=>'varchar(50) NOT NULL',
        'field_size'=>'varchar(15) NOT NULL DEFAULT '0'',
        'field_size_min'=>'varchar(15) NOT NULL DEFAULT '0'',
        'required'=>'int(1) NOT NULL',
        'match'=>'varchar(255) NOT NULL',
        'range'=>'varchar(255) NOT NULL',
        'error_message'=>'varchar(255) NOT NULL',
        'other_validator'=>'varchar(5000) NOT NULL',
        'default'=>'varchar(255) NOT NULL',
        'widget'=>'varchar(255) NOT NULL',
        'widgetparams'=>'varchar(5000) NOT NULL',
        'position'=>'int(3) NOT NULL',
        'visible'=>'int(1) NOT NULL',
    ), '');

    $this->createTable('tbl_users', array(
        'id'=>'pk',
        'username'=>'varchar(128) NOT NULL',
        'password'=>'varchar(128) NOT NULL',
        'email'=>'varchar(128) NOT NULL',
        'facebook_id'=>'varchar(500) DEFAULT NULL',
        'activkey'=>'varchar(128) NOT NULL',
        'create_at'=>'timestamp NOT NULL',
        'lastvisit'=>'timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'',
        'superuser'=>'int(1) NOT NULL',
        'status'=>'int(1) NOT NULL',
    ), '');

    $this->createTable('user', array(
        'id'=>'varchar(18) NOT NULL',
        'name'=>'varchar(500) NOT NULL',
        'lastname'=>'varchar(500) NOT NULL',
        'email'=>'varchar(500) NOT NULL',
        'username'=>'varchar(500) NOT NULL',
        'password'=>'varchar(500) NOT NULL',
        'address'=>'varchar(500) DEFAULT NULL',
    ), '');

    $this->addPrimaryKey('pk_user', 'user', 'id');

    $this->createTable('user_content_log', array(
        'id'=>'pk',
        'content_id'=>'int(11) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'study_time'=>'datetime NOT NULL',
        'course_id'=>'int(11) NOT NULL',
        'qty_times'=>'int(3) NOT NULL',
    ), '');

    $this->createIndex('idx_user_id', 'user_content_log', 'user_id', FALSE);

    $this->createIndex('idx_content_id', 'user_content_log', 'content_id', FALSE);

    $this->createTable('user_course', array(
        'id'=>'pk',
        'user_id'=>'int(11) NOT NULL',
        'course_id'=>'int(11) NOT NULL',
        'role'=>'tinyint(1) NOT NULL DEFAULT '1'',
        'create_date'=>'datetime DEFAULT NULL',
    ), '');

    $this->createIndex('idx_course_id', 'user_course', 'course_id', FALSE);

    $this->createIndex('idx_user_id', 'user_course', 'user_id', FALSE);

    $this->createTable('user_file', array(
        'id'=>'pk',
        'name'=>'varchar(1000) NOT NULL',
        'original_name'=>'varchar(2000) NOT NULL',
        'type'=>'varchar(100) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'create_at'=>'datetime NOT NULL',
    ), '');

    $this->createTable('user_permission', array(
        'id'=>'pk',
        'user_id'=>'int(11) NOT NULL',
        'can_create_course'=>'int(11) NOT NULL',
    ), '');

    $this->createIndex('idx_user_id', 'user_permission', 'user_id', FALSE);

    $this->addForeignKey('fk_content_course_course_id', 'content', 'course_id', 'course', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_coupon_course_course_id', 'coupon', 'course_id', 'course', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_coupon_tbl_users_user_id', 'coupon', 'user_id', 'tbl_users', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_course_tbl_users_user_id', 'course', 'user_id', 'tbl_users', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_exercise_content_content_id', 'exercise', 'content_id', 'content', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_exercise_answer_choice_exercise_question_question_id', 'exercise_answer_choice', 'question_id', 'exercise_question', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_exercise_question_exercise_exercise_id', 'exercise_question', 'exercise_id', 'exercise', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_exercise_text_answer_exercise_question_question_id', 'exercise_text_answer', 'question_id', 'exercise_question', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_external_provider_tbl_users_user_id', 'external_provider', 'user_id', 'tbl_users', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_file_tbl_users_user_id', 'file', 'user_id', 'tbl_users', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_file_content_content_content_id', 'file_content', 'content_id', 'content', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_file_content_file_file_id', 'file_content', 'file_id', 'file', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_tbl_profiles_tbl_users_user_id', 'tbl_profiles', 'user_id', 'tbl_users', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_user_content_log_tbl_users_user_id', 'user_content_log', 'user_id', 'tbl_users', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_user_content_log_content_content_id', 'user_content_log', 'content_id', 'content', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_user_course_course_course_id', 'user_course', 'course_id', 'course', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_user_course_tbl_profiles_user_id', 'user_course', 'user_id', 'tbl_profiles', 'user_id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_user_permission_tbl_users_user_id', 'user_permission', 'user_id', 'tbl_users', 'id', 'NO ACTION', 'NO ACTION');

}


public function down()
{
    $this->dropForeignKey('fk_content_course_course_id', 'content');

    $this->dropForeignKey('fk_coupon_course_course_id', 'coupon');

    $this->dropForeignKey('fk_coupon_tbl_users_user_id', 'coupon');

    $this->dropForeignKey('fk_course_tbl_users_user_id', 'course');

    $this->dropForeignKey('fk_exercise_content_content_id', 'exercise');

    $this->dropForeignKey('fk_exercise_answer_choice_exercise_question_question_id', 'exercise_answer_choice');

    $this->dropForeignKey('fk_exercise_question_exercise_exercise_id', 'exercise_question');

    $this->dropForeignKey('fk_exercise_text_answer_exercise_question_question_id', 'exercise_text_answer');

    $this->dropForeignKey('fk_external_provider_tbl_users_user_id', 'external_provider');

    $this->dropForeignKey('fk_file_tbl_users_user_id', 'file');

    $this->dropForeignKey('fk_file_content_content_content_id', 'file_content');

    $this->dropForeignKey('fk_file_content_file_file_id', 'file_content');

    $this->dropForeignKey('fk_tbl_profiles_tbl_users_user_id', 'tbl_profiles');

    $this->dropForeignKey('fk_user_content_log_tbl_users_user_id', 'user_content_log');

    $this->dropForeignKey('fk_user_content_log_content_content_id', 'user_content_log');

    $this->dropForeignKey('fk_user_course_course_course_id', 'user_course');

    $this->dropForeignKey('fk_user_course_tbl_profiles_user_id', 'user_course');

    $this->dropForeignKey('fk_user_permission_tbl_users_user_id', 'user_permission');

    $this->dropTable('content');
    $this->dropTable('content_info');
    $this->dropTable('coupon');
    $this->dropTable('coupon_log');
    $this->dropTable('course');
    $this->dropTable('exercise');
    $this->dropTable('exercise_answer_choice');
    $this->dropTable('exercise_choice_score');
    $this->dropTable('exercise_question');
    $this->dropTable('exercise_stat');
    $this->dropTable('exercise_text_answer');
    $this->dropTable('exercise_text_score');
    $this->dropTable('external_provider');
    $this->dropTable('file');
    $this->dropTable('file_content');
    $this->dropTable('relate_course');
    $this->dropTable('stat_user_study');
    $this->dropTable('tbl_migration');
    $this->dropTable('tbl_profiles');
    $this->dropTable('tbl_profiles_fields');
    $this->dropTable('tbl_users');
    $this->dropTable('user');
    $this->dropTable('user_content_log');
    $this->dropTable('user_course');
    $this->dropTable('user_file');
    $this->dropTable('user_permission');
}
