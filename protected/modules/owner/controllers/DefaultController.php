<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
        
        
        
        
        public function actionRegisterTeacher(){            
              $user_id = Yii::app()->user->id;             
              
              if(!empty($user_id)){
                  $this->redirect(Yii::app()->createUrl("owner/default/requestTobeTeacher",array("user_id"=>$user_id)));
              }
              
            $model = new RegistrationFormDoctor;
            $profile=new Profile;
            $profile->regMode = true;
              
            
            if(isset($_POST['RegistrationFormDoctor'])) {
            
                                     
					$model->attributes=$_POST['RegistrationFormDoctor'];
                                        
//                                        echo "<pre>";
//                                        print_r($model->attributes);                                        
//                                        exit;
                                         
                                        /*not in extension*/
//                                         $model->username=$model->email;
                                   
                                         
					$profile->attributes=((isset($_POST['Profile'])?$_POST['Profile']:array()));
//                                            if($model->validate()&&$profile->validate())
                                       
                                        
                                        if($model->validate())
					{
                                         
                                                
                                            
                                                $soucePassword = $model->password;
						$model->activkey=UserModule::encrypting(microtime().$model->password);
						$model->password=UserModule::encrypting($model->password);
						$model->verifyPassword=UserModule::encrypting($model->verifyPassword);
						$model->superuser=0;
						
                                                
                                                /*not in extension*/
                                               
						$model->status="1";
						if ($model->save()) {
							$profile->user_id=$model->id;
                                                        /* save with not validate */
                                                        
                                                        Yii::app()->user->setState('username',$model->email);                                                        
                                                        
                                                        $profile->save(false);
//							$profile->save();
//							                                                        
							$model_user_perrmission=  new UserPermission();
                                                        $model_user_perrmission->user_id=$model->id;
                                                        
                                                        if(!empty($_POST['is_request'])){
                                                            $model_user_perrmission->request_date = date("Y-m-d h:i:s");
                                                            $model_user_perrmission->is_request = 1;
                                                        }                                                        
                                                        
                                                        $model_user_perrmission->save(false);
                                                        
                                                        Yii::app()->user->id=$model->id;
                                                        $home_url=Yii::app()->createUrl("site/index");
                                                        $this->redirect($home_url);
                                                        

						}
					} else $profile->validate();
				}
            
              
              
            $this->render('registration',array('model'=>$model,'profile'=>$profile));
                 
        }
        
        public function actionRequestTobeTeacher($user_id){
            $user_session = Yii::app()->user->id;
            if(empty($user_session)){
                   $this->redirect(Yii::app()->createUrl("site/index"));
            }
            
            $model_user_perrmission = UserPermission::model()->find("user_id='$user_id'");
            
            if(isset($_POST['is_request'])){
                  $model_user_perrmission->is_request = 1;
                  $model_user_perrmission->request_date = date("Y-m-d h:i:s");
                  $model_user_perrmission->save(false);
            }
            
            
            
            
            $this->render("requestTobeTeacher",array("model_user_perrmission"=>$model_user_perrmission));
            
        }
        
        public function actionApproveTeacherRequest(){
             $is_super = Yii::app()->user->getState('superadmin');
             
             if(empty($is_super) || $is_super==0){
                    $this->redirect(Yii::app()->createUrl("site/index"));
             }
             
             $model_requests = UserPermission::model()->findAll("is_request='1' AND can_create_course='0'");
             
             $this->render("approveTeacherRequests",array("model_requests"=>$model_requests));
             
             
        }
        
        public function actionAcceptRequest(){
            $user_id = $_POST['user'];
            $is_super = Yii::app()->user->getState('superadmin');
              if(!empty($is_super) || $is_super!=0){
                  $model_permission = UserPermission::model()->find("user_id='$user_id'");
                  $model_permission->can_create_course = 1;
                  $model_permission->save(false);
                  echo "1";
              }else{
                  echo "0";
              }
            
            
            
            
            
        }
        
        
        
        
        
}