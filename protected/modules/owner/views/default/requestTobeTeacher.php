<script type="text/javascript">
    $(document).ready(function() {
        document.title = "Register "; 

    });
</script>


<div class="gray-row">
    <div class="container-full">
        <div class="form-box">
        <div class="w-container login-tab-box">
        <div class="w-form login-signup-form full-access">
   
            
            <div id="login-block" class="login-signup-panel login-tab-box" style="">
             <h1 class="head-form">TEACHER REGISTER</h1>
                
        
                 <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'Registration',                          
                            'enableClientValidation' => true,
                            'enableAjaxValidation' => true,
                            'htmlOptions' => array("role" => "form"),
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        ?> 
                
                
             
      
              
             
               <!--<b> เงื่อนไขการเป็นอาจารย์  </b><br/>-->
                <!--Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.-->
             
             
              
              <?php if($model_user_perrmission->can_create_course==0){ ?>
                
              <div class="checkbox">
                <label>
                   <input name="is_request" type="checkbox" class="" value="1" <?php if($model_user_perrmission->is_request==1){echo "checked='checked'";} ?>> <?php echo Yii::t("site","Accept condition. and send request to be a teacher?"); ?>
                </label>
                  
              </div>
              <?php }else{ ?>
                <div class="checkbox">
                <label>
                 <br/><b>  คุณเป็นครูแล้ว </b><br/>
                </label>
                  
              </div>
              <?php } ?>
              
              
              
         
               <button type="submit" class="btn btn-primary"> สมัครเป็นครู </button> 
               
             
                
                 <?php $this->endWidget(); ?>
                  </div>
            
        </div>
     </div>        
        
</div>

</div>
</div>    
    
