<script type="text/javascript">
    $(document).ready(function() {
        document.title = "Register "; 

    });
</script>

<?php $user_id = Yii::app()->user->id; ?>
<?php if (empty($user_id) || $user_id == "0") { ?>
<div class="gray-row">
    <div class="container-full">
        <div class="form-box">
        <div class="w-container login-tab-box">
        <div class="w-form login-signup-form full-access">
   
            
            <div id="login-block" class="login-signup-panel login-tab-box" style="">
             <h1 class="head-form">TEACHER REGISTER</h1>
                
        
                 <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'Registration',                          
                            'enableClientValidation' => true,
                            'enableAjaxValidation' => true,
                            'htmlOptions' => array("role" => "form"),
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        ?> 
                
                
                 <?php echo $form->errorSummary(array($model)); ?>
                
              <div class="form-group">
               <label for="exampleInputPassword1">Username*</label>
                <?php echo $form->textField($model, 'username',array("placeholder"=>"username","class"=>"form-control")); ?>
               <?php //echo $form->error($model, 'username'); ?>
              </div> 
             
              <div class="form-group">
               <label for="exampleInputPassword1">Email*</label>
                     <?php echo $form->textField($model, 'email',array("placeholder"=>"อีเมล","id"=>"exampleInputEmail1","class"=>"form-control")); ?>
               <?php //echo $form->error($model, 'username'); ?>
              </div> 
              
              
             <div class="form-group">
                <label for="exampleInputPassword1">Password*</label>
                <?php echo $form->PasswordField($model, 'password',array("placeholder"=>"รหัสผ่าน","id"=>"inputPassword3","class"=>"form-control")); ?>
              </div>
              
              <div class="form-group">
                <label for="exampleInputPassword1">Confirm Password*</label>
                <?php echo $form->PasswordField($model, 'verifyPassword',array("placeholder"=>"ยืนยันรหัสผ่าน","id"=>"inputPassword3","class"=>"form-control")); ?>
              </div>
              
<!--             
               <b> เงื่อนไขการเป็นอาจารย์  </b><br/>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.-->
             
             
              <div class="checkbox">
                <label>
                   <input name="is_request" type="checkbox" class="" value="1"> <?php echo Yii::t("site","Accept condition. and send request to be a teacher?"); ?>
                </label>
                  
              </div>
              
         
               <button type="submit" class="btn btn-primary"> สมัครเป็นครู </button> 
               
             
                
                 <?php $this->endWidget(); ?>
                  </div>
            
        </div>
     </div>        
        
</div>

<?php } ?>
</div>
</div>    
    
