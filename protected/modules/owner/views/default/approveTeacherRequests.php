  <style type="text/css" title="currentStyle">
          @import "js/datatable/css/demo_page.css";
          @import "js/datatable/css/demo_table.css";
      </style>
      <script type="text/javascript" language="javascript" src="js/datatable/js/jquery.js"></script>
      <script type="text/javascript" language="javascript" src="js/datatable/js/jquery.dataTables.js"></script>
      <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {

              $('#project-list').dataTable({
//                  "bServerSide": true,
//                   "bJQueryUI": true,
                   "aaSorting": [],
                   "sDom": '<"H"lfrp>t<"F"ip>',
                  "sPaginationType": "full_numbers",
                  "iDisplayLength": 25,
                  "aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]]
              });
          });
      </script>

  
      
      
<div class="w-hidden-small w-hidden-tiny hero-unit-section">
    <div class="w-container">
        <div class="w-row">


            <div class="w-col w-col-12">
                <h1> ยืนยันการร้องขอเป็นครู</h1>
                <hr/>
                
                       
            <table id="project-list" width="100%" style="">
            <thead>
                <tr>
                    <th class="text-center">  </th>
             
                    <th class="text-center"> ชื่อ - สกุล</th>
                    <th class="text-center"> username </th>
                    <th class="text-center"> email </th>
                    <th class="text-center"> วันที่ร้องขอ </th>
                    <th class="text-center">จัดการ</th>
                 
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($model_requests)){ 
                    $count_row =1;
                    foreach ($model_requests as $request){
                ?>
                    <tr id="request-<?php echo $request->user_id; ?>" >
                        <td> <?php echo $count_row; ?> </td>
                        <td> <?php echo $request->profile->firstname." ". $request->profile->lastname; ?> </td>
                        <td> <?php echo $request->user->username; ?> </td>
                        <td> <?php echo $request->user->email; ?> </td>                     
                        <td> <?php 
                            if(!empty($request->request_date)){
                                echo SiteHelper::dateFormat($request->request_date); 
                            }
                        
                        ?> 
                        
                        </td>      
                        <td class="text-center">
                            <?php if($request->can_create_course!=1){ ?>
                            <a class="request-list" onclick="acceptRequest(<?php echo $request->user_id; ?>)" href="#">ยอมรับ </a>
                 
                        </td>                         
                        
                    </tr>
                 <?php }
                    $count_row++;                
                    }
                    
                } 
                    ?>
            </tbody>
            </table>  
            </div> 
        </div>
    </div>
</div><br/>
      
      

<script type="text/javascript">

$(document).ready(function() {
    $(".request-list").on("click",function(event){
         event.preventDefault();
      
    });
 

  
});


 function acceptRequest(user){
           
        $.ajax({
                  url: '<?php echo Yii::app()->createUrl("owner/default/acceptRequest"); ?>',
                  type: 'POST',
                  data:{user:user},
                  dataType: 'html',                        
                  success: function(result) {  

                       if(result==1){                          
                           $("#request-"+user).hide();
                       }else{
                           alert("คุณไม่มีสิทธิ");
                       }
                   }
                });  
   }


  
 </script>
