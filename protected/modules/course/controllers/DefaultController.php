<?php
class DefaultController extends Controller
{
                   
	public function actionIndex()
	{
            
            
		$this->render('index');
	}
        
        # Show All Course >>> if login just select couse that person who logined not have
        public function actionCourseAll() {        
        
        SiteHelper::setMetaTag("description","couurse's all");
        $today = date("Y-m-d H:i:s");
            
        //create CListView Obj    
        $user_id = Yii::app()->user->id;
        $criteria = new CDbCriteria;
        $criteria->join="INNER JOIN content_info ON t.id=content_info.course_id";
         $criteria->condition = "(date_start <= '$today' OR date_start IS NULL) AND (date_end >= '$today' OR date_end IS NULL) AND is_public='1'";
        if(!empty($user_id)){
            //show only course which not bought before
//            $criteria->condition .=" AND LogBuffert.id NOT IN (SELECT course_id FROM user_course WHERE user_id='$user_id') 
//                                  AND t.id !=-1
//                                  AND t.id IN (SELECT course_id FROM content)";
            
            $criteria->condition .=" AND t.id !=-1
                                  AND t.id IN (SELECT course_id FROM content)";
        }
        
        $model_user_incourse= UserCourse::model()->findAll("user_id='$user_id'");
        
        
        $courses = new CActiveDataProvider('Course', array(
            'pagination' => array('pageSize' => "8"),
            'criteria' => $criteria,
        ));
        $this->render("course_all",array("courses"=>$courses,"model_user_incourse"=>$model_user_incourse));
    }
    
      # Show Course That User Has Applyed
      public function actionApplyCourse() {
            
        //create CListView Obj    
        $user_id = Yii::app()->user->id;
        $criteria = new CDbCriteria;
        $criteria->join="INNER JOIN content_info ON t.id=content_info.course_id";
        $criteria->condition="t.id IN (SELECT course_id FROM user_course WHERE user_id='$user_id' AND role='3') AND t.id !=-1 AND is_public='1'";
             
        $courses = new CActiveDataProvider('Course', array(
            'pagination' => array('pageSize' => "9"),
            'criteria' => $criteria,
        ));
        $this->render("course_all",array("courses"=>$courses));
    }
    
    
    
    public function newCoinModel(){
        $user_id = Yii::app()->user->id;
        $model_coin=new UserCoinBase();
        $model_coin->coin=0;
        $model_coin->create_date=date("Y-m-d H:i:s");
        $model_coin->update_date=date("Y-m-d H:i:s");
        $model_coin->user_id=$user_id;
        $model_coin->save();
        
        return $model_coin;
    }


    # for Person Who not Have not Course Or not Login to View detail of courese
    public function actionViewCourse($id,$affiliate=""){        
        SiteHelper::CheckReference();        
        Yii::app()->user->setReturnUrl("index.php?r=course/default/viewCourse&id=$id");    
       
        $demo_video="";
        $course=  Course::model()->findByPk($id);

        SiteHelper::setMetaTag("description","เรียน ".$course->name);
        SiteHelper::setMetaTag("keywords",$course->name);
        
        $course_contents_parent=  Content::model()->findAll("course_id='$id' AND parent_id='-1' AND is_approve ='1' ORDER BY show_order ASC");
        $relate_course=  RelateCourse::model()->find("course_id='$id'");
        $model_user="";
        $model_user_in_course="";
        $user_id = Yii::app()->user->id;
        $model_coin="";
        $user_own_course="";
        if(!empty($user_id)){
            $model_coin=  UserCoinBase::model()->find("user_id='$user_id'");
            
            $user_own_course=  UserCourse::model()->find("user_id='$user_id' AND course_id='$course->id'");                        
            if(empty($model_coin)){
                $model_coin=  $this->newCoinModel();
            }
        }        
        
        
        if(!empty($course->demo_video)){
            $demo_video=File::model()->findByPk($course->demo_video);
        }      
        
        
        $person= new UserCourse();
        $teachers=  UserCourse::model()->findAll($person->getRelatePeople($id, "1", "2", "criteria"));        
        $students= UserCourse::model()->findAll($person->getRelatePeople($id, "3","", "criteria","1","24")); 
        $qty_student=$person->getQtyStudent($id);        
        
        if(!empty($affiliate)){            
            setcookie('affiliate',$affiliate,time() + (86400 * 7)); // 86400 = 1 day         
        }        
     
        //sumary ภายใน course
        //$qty_count_content = Content::model()->count("course_id='$id' AND parent_id !='-1' AND is_public='1'");
     
        $model_payment = CoursePaymentBase::model()->find("user_id='$user_id' AND course_id='$id' AND is_active='1'");
        if(empty($model_payment)){
            $model_payment="";
        }
        
        
        $this->render("view_course",array("course"=>$course,"user_own_course"=>$user_own_course,
                                          "course_contents_parent"=>$course_contents_parent,
                                          "id"=>$id,"user_id"=>$user_id,"model_coin"=>$model_coin,
                                          "relate_course"=>$relate_course,"demo_video"=>$demo_video,
                                          "teachers"=>$teachers,"students"=>$students,"qty_student"=>$qty_student,
                                           "model_payment"=>$model_payment));
    }
    
    #click show Nore Student from >>> View course
    public function actionQueryStudentMore(){
                
        $already_query= rtrim($_POST["took"],',');
        $course_id=$_POST["course"];
        
        $sql="SELECT profile.firstname AS name,profile.lastname AS lastname,profile.photo AS photo,user_course.user_id AS user_id,tbl_users.username as username,tbl_users.facebook_id as facebook_id
            FROM user_course
                   INNER JOIN tbl_profiles AS profile on user_course.user_id= profile.user_id
                   INNER JOIN tbl_users on user_course.user_id = tbl_users.id
              WHERE course_id='$course_id' AND user_course.user_id NOT IN($already_query) AND user_course.role='3'
              LIMIT 24";      
                     
        
        $students= UserCourse::model()->findAllBySql($sql);        
        
              
        $this->renderPartial("_show_more_std",array("students"=>$students));    
        
    }
    
    # Buy Course >>> Payment page
    public function actionTakeCourse($id){
        $course=  Course::model()->findByPk($id);

        #add user to course   
        $user_id = Yii::app()->user->id;
        $model_user_in_course = new UserCourse();
        $model_user_in_course->user_id = $user_id;
        $model_user_in_course->course_id = $id;
        $model_user_in_course->create_date = date("Y-m-d H:i:s");
        $model_user_in_course->role = 3;
        $model_user_in_course->bought_price=$course->price;
       
        if(!empty($course->duration)){      
            $model_user_in_course->expire_date = date("Y-m-d H:i:s",strtotime("+$course->duration day"));
        }
        
        $model_user_in_course->save();

        
        
        $model_money_div_rate = CourseDivisionRateBase::model()->find("course_id='$id' ORDER BY id DESC");
        if(empty($model_money_div_rate)){
            $model_money_div_rate= CourseDivisionRateBase::model()->find("is_default='1'");
        }
        $course_price= $course->price;
        
        $model_weight_money= new CourseMoneyDivisionBase();
        
        if(!empty($_COOKIE['affiliate'])){
            $affiliate=$_COOKIE['affiliate'];
            $sql_affiliate_course = "SELECT * FROM user_affiliate WHERE affiliate_code='$affiliate' AND id IN (SELECT affiliate_id FROM user_affiliate_course WHERE course_id='$id')";
            $model_user_in_affiliate= UserAffiliate::model()->findBySql("affiliate_code=$affiliate");
            
            if(!empty($model_user_in_affiliate)){            
                $model_weight_money->affiliate_share_percent = $model_money_div_rate->affiliate_share_percent;
                $model_weight_money->affiliate_share_price=($course_price*$model_money_div_rate->affiliate_share_percent)/100;
                $course_price= $course_price-$model_weight_money->affiliate_share_price;
            }
        }
        
        $model_weight_money->teacher_share_percent=$model_money_div_rate->teacher_share_percent;
        $model_weight_money->company_share_percent=$model_money_div_rate->company_share_percent;
        $model_weight_money->teacher_share_price=($course_price*$model_money_div_rate->teacher_share_percent)/100;
        $model_weight_money->company_share_price=($course_price*$model_money_div_rate->teacher_share_percent)/100;
        
        
        $model_weight_money->course_id= $id;
        $model_weight_money->user_id=$user_id;
        $model_weight_money->create_date=date("Y-m-d H:i:s");        
        $model_weight_money->save();
        
        
        
        #minus money
//        $model_coin= UserCoinBase::model()->find("user_id='$user_id'");
//        $model_coin->update_date=date("Y-m-d H:i:s");
//        $model_coin->coin= $model_coin->coin-$course->price;            
//        $model_coin->save();
//        
        $success_url=Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$course->id,"chapterId"=>1));
        $this->redirect($success_url);

//        $this->render("take_course",array("course"=>$course));
    }
    
    
   #study course Page
    public function actionStudyCourse($courseId,$lesson_id="",$type="student"){
        #chapterId = Unit              
        
        $this->layout='//layouts/study';
        
        $user_id=Yii::app()->user->id;     
        $model_course=  Course::model()->findByPk($courseId);
        
        $today = date("Y-m-d H:i:s");
        
        $check_available_time = SiteHelper::CheckavailableCourseTime($model_course);
        
        
        if($check_available_time==FALSE){
            $this->redirect(Yii::app()->createUrl("course/default/notAvailableCourse",array("courseId"=>$courseId)));
        }
                
        #$chapterId = Unit              
        $can_study=CourseHelper::BeforeStudy($courseId);        
        if(!empty($can_study)){
            $this->redirect($can_study);
        }        
        //Query Unit that has lesson
        $criteria_chapter = new CDbCriteria;
        
        //is_approve='1' == is not new unit
        $criteria_chapter->condition="course_id='$courseId' AND parent_id = '-1' AND id IN (SELECT parent_id FROM content WHERE parent_id!='-1' AND course_id='$courseId') AND is_approve='1'";
        $criteria_chapter->order="show_order ASC";
        
        $chapters=Content::model()->findAll($criteria_chapter);       
        
        $rating=0;
        
        if(!empty($user_id)){
         $model_rating= CourseRatingBase::model()->find("user_id='$user_id' AND course_id ='$courseId'");
            if(!empty($model_rating)){
                 $rating=$model_rating->rating;
        }
        }
      
        
        if(empty($lesson_id)){
          
            $model_log = UserContentLog::model()->find("user_id='$user_id' AND course_id='$courseId' ORDER BY study_time DESC");
            if(!empty($model_log)){
                $model_lesson=  Content::model()->findByPk($model_log->content_id);           
                $lesson_id = $model_lesson->id;
            }else{
                $model_lesson= Content::model()->find("course_id='$courseId' AND parent_id != '-1' ORDER BY show_order ASC");
                $lesson_id = $model_lesson->id;                
            }
            
            
        }else{
            $model_lesson=  Content::model()->findByPk($lesson_id);                
        }
        
        #user log study
        CourseHelper::saveContentLog($model_lesson->id,$courseId);
        
        
        $is_exrecise=$model_lesson->is_exercise;
        
     
        
        SiteHelper::setTitle($model_course->name,$this);
        SiteHelper::setMetaTag("description",$model_lesson->title);
        SiteHelper::setMetaTag("keywords",$model_lesson->title);
        
        
        #check is lesson is exercise ?
        $model_question="";
        $model_excercise="";
        $model_excercise_stat="";
       
         if ($is_exrecise == 1) {
            
              $model_excercise = Exercise::model()->find("content_id='$model_lesson->id'");
              
              if(!empty($model_excercise))
              $model_excercise_stat=  ExerciseStat::model()->findAll("user_id='$user_id' AND exercise_id='$model_excercise->id' ORDER BY do_time ASC");
              
               if (!empty($model_excercise)) {
                    $criteria = new CDbCriteria;
                    $criteria->condition="exercise_id='$model_excercise->id'";
                    if(!empty($model_excercise->qty_show)){
                        $criteria->limit=$model_excercise->qty_show;
                 } 
                 
                 
                  if($model_excercise->is_random==1){
                        $criteria->order="RAND()";
                    }else{
                        $criteria->order="show_order ASC";
                    }
                    $model_question = ExerciseQuestion::model()->findAll($criteria);
            }
         }
        
        
         #calculate Progress
         $sql_progress="SELECT COUNT(user_content_log.id) AS progress 
                        FROM user_content_log 
                        WHERE user_id='$user_id' AND 
                              content_id IN (SELECT id FROM content WHERE parent_id !='-1' AND is_public ='1' AND course_id='$model_course->id')";
         
         $has_learn=  UserContentLog::model()->findBySql($sql_progress)->progress;
          
         if($model_lesson->is_code==1){
            $model_code= CodeEditorInContent::model()->find("content_id='$model_lesson->id'");
         }
         
         if(empty($model_code)){
             $model_code="";
         }
         
         
         if(empty($model_lesson->videoFile)){
             //set log to studird end
             $this->setFinishStudy($model_lesson->id,$model_course->id);
             
         }
                 
         $this->render("study_course",
            array("chapters"=>$chapters,                
                "model_course"=>$model_course,
                "user_id"=>$user_id,
                "lesson_id"=>$lesson_id,
                "model_lesson"=>$model_lesson,"is_exrecise"=>$is_exrecise,
                "model_excercise"=>$model_excercise,"type"=>$type,
                "model_code"=>$model_code,
                "model_excercise_stat"=>$model_excercise_stat,"model_question"=>$model_question,
                "has_learn"=>$has_learn,"nav_lesson"=>  $this->getLessonNavList($courseId),"rating"=>$rating
                                
                ));   

    }
    
    
    public function actionNotAvailableCourse($courseId) {
        $model_course=  Course::model()->findByPk($courseId);
        $this->render("notAvailableCourse",array("model_course"=>$model_course));
    }


    
    
    public function setFinishStudy($content_id,$course_id=""){
           $user_id=Yii::app()->user->id; 
         
           
           
           $model_log_content = UserContentLog::model()->find("content_id='$content_id' AND user_id='$user_id'");
             if(empty($model_log_content)){
                 $model_log_content = new UserContentLog();
             }
             $model_log_content->user_id = $user_id;
             $model_log_content->content_id= $content_id;
             $model_log_content->study_time = date("Y-m-d H:i:s");
          
             $model_log_content->qty_times += 1;
             $model_log_content->is_end=1;
             
             if(empty($course_id)){                 
                 $model_lesson=  Content::model()->findByPk($content_id);
                 $model_log_content->course_id=$model_lesson->course_id;                 
                 
             }else{
                    $model_log_content->course_id=$course_id;
             }
                          
             $model_log_content->save();
             
    }

    



    public function getLessonNavList($course_id){
        $model_content_all = Content::model()->findAll("course_id='$course_id' AND parent_id != '-1' ORDER BY show_order ASC");
        $nav_list=array();
        
        if(!empty($model_content_all)){
            foreach ($model_content_all as $content){
                $nav_list[]=$content->id;
            }
        }    
        return $nav_list;
    }

    

    public function actionUnitDetail($unit){
        $this->layout='//layouts/popup';
        $model_content=  Content::model()->findByPk($unit);
        $model_course=  Course::model()->findByPk($model_content->course_id);
        $this->render("unitDetail",array("model_content"=>$model_content,"model_course"=>$model_course));
    }







    #show content 
    public function actionGetContent($id){
        $content=Content::model()->findByPk($id);
        
        $this->renderPartial("_contentDisplay",
            array("content"=>$content,
                ));
    }
        
   
    #Query Question And Answer To Do Questionair
    public function actionDoExerciseChoice($id){

        // Yii::app()->clientScript->scriptMap['jquery.js'] = true;
        
        $model_content=  Content::model()->findByPk($id);
        $user_id = Yii::app()->user->id;
        $model_excercise=  Exercise::model()->find("content_id='$id'");
        $model_excercise_stat="";
                    
        if(!empty($model_excercise)){
            $criteria = new CDbCriteria;
            $criteria->condition="is_public='1' AND exercise_id='$model_excercise->id'";
            if(!empty($model_excercise->qty_show)){
                $criteria->limit=$model_excercise->qty_show;
            } 
            
            if($model_excercise->is_random==1){
                $criteria->order="RAND()";
            }else{
                $criteria->order="show_order ASC";
            }
            
            $model_question=  ExerciseQuestion::model()->findAll($criteria);
            
            //query Stat Doexercise 
            $model_excercise_stat=  ExerciseStat::model()->findAll("user_id='$user_id' AND exercise_id='$model_excercise->id' ORDER BY do_time ASC");
                           
        }
        
       $this->renderPartial("_form_exercise",array("model_content"=>$model_content,
                                                   "model_excercise"=>$model_excercise,
                                                   "model_question"=>$model_question,
                                                   "model_excercise_stat"=>$model_excercise_stat,
                                                   "user_id"=>$user_id));
    }
    
    
    //save answer choice << ajax
    public function actionSaveChoiceTest($exercise){
        if(!empty($_POST['form_params'])){    
     
            $now=date("Y-m-d H:i:s");
            $post=$_POST['form_params'];
            $count_quiz=0;
            $qty_correct=0;
            $list_question_str="";
                    
            //ควร fix ว่าให้ทำหมด ทุกข้อ ?
            foreach ($post as $p) {
                if ($p["type"] == "radio") {
                    $count_quiz++;
                    $model_score = new ExerciseChoiceScore();
                    $model_score->attributes = $p;
                    $model_score->is_true = $p["is_true"]["$model_score->answer_id"];
                    $model_score->do_time = $now;
                    $model_score->user_id = Yii::app()->user->id;
                    $model_score->save();
                    $list_question_str.=$model_score->question_id.",";

                    if ($model_score->is_true == 1) {
                        $qty_correct++;
                    }
                } 
                
                if($p["type"] == "text"){                    
                    //text type
                    $count_quiz++;
                    $question_id = $p["question_id"];
                    $list_question_str.=$question_id.",";
                    
                    $list_correct_answer = ExerciseTextAnswer::model()->findAll("question_id='$question_id'");
                    $list_true_text = array(); //save true answer list text
                    $start = 1;
                    $is_corrct = true;

                    foreach ($list_correct_answer as $correct_answer) {
                        $list_true_text[$start] = $correct_answer->answer;
                        $start++;
                    }   
                    

                    if (!empty($p["answer"])) {
                        foreach ($p["answer"] as $key => $answer) {
                            $model_score_text = new ExerciseTextScore();
                            $model_score_text->question_id = $question_id;
                            $model_score_text->answer = $answer;
                            $model_score_text->do_time = $now;
                            $model_score_text->user_id = Yii::app()->user->id;
                            $model_score_text->sequence = $key;
                         
                                                   
                            $correct = false;
                            // check --> answer correct?
                            if ($model_score_text->answer == $list_true_text[$key]) {
                                $correct = true;
                                $model_score_text->is_true = "1";
                            }
                            $is_corrct = $is_corrct && $correct;                            
                            $model_score_text->save();
                   
                        }
                    }
                    if ($is_corrct == true) {
                        $qty_correct++;
                    }                    
                   
                }
                
                if($p["type"] == "code"){ 
                  
                    $count_quiz++;
                    $question_id = $p["question_id"];
                    $code=$p['code'];
                    $answer=$p['answer'];
                    $list_question_str.=$question_id.",";                    
                    $model_answer = ExerciseCodeAnswer::model()->find("question_id='$question_id'");
//                                        
                    $model_score_code = new ExerciseCodeScore();
                    $model_score_code->question_id=$question_id;
                    $model_score_code->code=$code;
                    $model_score_code->answer=$answer;
                    $model_score_code->user_id = Yii::app()->user->id;
                    $model_score_code->do_time=$now;
                                        
                    $model_answer->answer=str_replace(" ","",$model_answer->answer); 
                    $answer=str_replace(" ","",$answer); 
                    
                    if($model_answer->answer==$answer){
                        $model_score_code->is_true=1;
                        $qty_correct++;
                    }                   
                    $model_score_code->save(); 
                }       
            }//foreach
            //            
            //save stat doExercise
            $model_exercise_stat=  new ExerciseStat();
            $model_exercise_stat->exercise_id=$exercise;
            $model_exercise_stat->user_id=Yii::app()->user->id;
            $model_exercise_stat->do_time=$now;
            $model_exercise_stat->qty_question=$count_quiz;
            $model_exercise_stat->qty_correct=$qty_correct;
            $list_question_str= rtrim($list_question_str, ",");
            $model_exercise_stat->question_list=$list_question_str;            
            $model_exercise_stat->save();
            
            if($qty_correct==$count_quiz){
                echo "<img src='images/button/great.png'><b> All Correct. </b>";
            }else{
                echo "<img src='images/button/write.png'><b> Correct $qty_correct Form $count_quiz </b>";
            }
        }                
    }
    
    public function actionHistoryExercise($id,$user_id,$exercise){
//    public function actionHistoryExercise(){

//        $id=$_POST['id'];
//        $user_id=$_POST['user_id'];
//        $exercise=$_POST['exercise'];

        $model_exercise=  Exercise::model()->findByPk($exercise);
        $model_content=  Content::model()->findByPk($model_exercise->content_id);
        
        $model_exercise_stat=  ExerciseStat::model()->find("id='$id' AND user_id='$user_id' AND exercise_id='$exercise'");
        $header="Exercise :: $model_content->title";
        $list_questions=  explode(",", $model_exercise_stat->question_list);
        $do_time=$model_exercise_stat->do_time;
        $html="";
                      
        $this->renderPartial('_histrory_exercise', 
                            array("do_time"=>$do_time,
                                   "header"=>$header,
                                   "list_questions"=>$list_questions,
                                   "id"=>$id,
                                   "user_id"=>$user_id,
                                   "exercise"=>$exercise,
                                   "model_content"=>$model_content,
                                   "html"=>$html), 
                                    false, true);  
    }
    
    public function syncCourse($id=""){
        
    }
    
    
     public function actionRateCourse($course_id){ 
        
        if(!empty($_POST['rating'])){
            $today = date("Y-m-d H:i:s");
            $user_id=Yii::app()->user->id;
            $model_rating= CourseRatingBase::model()->find("user_id='$user_id' AND course_id ='$course_id'");
            if(empty($model_rating)){
                $model_rating=new CourseRatingBase();
                $model_rating->create_date=$today;                
            }
            $model_rating->user_id=$user_id;
            $model_rating->course_id=$course_id;
            $model_rating->rating=$_POST['rating'];
            $model_rating->update_date=$today;
           
            
            $model_rating->save();    
        }
        
        $this->redirect(Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$course_id)));
        
       
    }
    
    public function actionLogBuffer() {
        $user_id = Yii::app()->user->id;
        $lesson_id= $_POST['lesson_id'];
        if (!empty($user_id) && !empty($lesson_id)) {

            $time=1;
            
            $model_log = VdoBufferRateBase::model()->find("user_id='$user_id' AND content_id='$lesson_id'");
            if (empty($model_log)) {
                $model_log = new VdoBufferRateBase();
            } else {
                $to_time = date("Y-m-d H:i:s");
                $from_time = $model_log->create_date;
                $minute = round(abs($to_time - $from_time) / 60, 2);
                
                if($minute>10){ //10 minute new record
                    $time=1;
                    $model_log = new VdoBufferRateBase();
                }else{
                    $time = $model_log->times+1;
                }
                
            }
            
            $model_log->content_id= $lesson_id;
            $model_log->user_id = $user_id;
            $model_log->times=$time;
            $model_log->create_date = date("Y-m-d H:i:s");
            
            $model_log->save(false);
            
        }
    }
    
    public function actionSetComplete(){
        $user_id = Yii::app()->user->id;
        $lesson_id= $_POST['lesson_id'];
        if (!empty($user_id) && !empty($lesson_id)) {
            $model_log= UserContentLogBase::model()->find("user_id='$user_id' AND content_id='$lesson_id'");
            if(!empty($model_log)){
                $model_log->is_end=1;               
                $model_log->study_time= date("Y-m-d H:i:s");
                $model_log->save(false);
            }
            
        }
        
    }

    

    public function actionSaveSuggestion(){
        $message = $_POST['message'];
        $content_id=$_POST['content_id'];
        $user_id = Yii::app()->user->id;
        
        $model_suggest = new ContentSuggestionBase();
        $model_suggest->content_id= $content_id;
        $model_suggest->user_id=$user_id;
        $model_suggest->message=$message;
        $model_suggest->create_date=date("Y-m-d H:i:s");
        $model_suggest->save(false);
    }
    
  
        
}