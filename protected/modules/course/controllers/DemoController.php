<?php
class DemoController extends Controller
{
    
               
    public function actionContent($content_id,$course){
        //check is real free?
        $this->layout='//layouts/popup';
       
        $model_content = Content::model()->findByPk($content_id);
        $model_course = Course::model()->findByPk($course);
        
        if($model_content->is_free!=1 || empty($model_content)){
            throw new CHttpException(404,'The requested page does not exist.');
        }
        
        $this->render("content",array("model_content"=>$model_content,"model_course"=>$model_course));
    }
  
        
}