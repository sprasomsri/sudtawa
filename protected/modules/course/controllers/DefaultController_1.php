<?php
class DefaultController extends Controller
{
    
        public $layout='//layouts/coursecreek';
        
	public function actionIndex()
	{
		$this->render('index');
	}
        
        # Show All Course >>> if login just select couse that person who logined not have
        public function actionCourseAll() {        
        
        SiteHelper::setMetaTag("description","couurse's all");
            
        //create CListView Obj    
        $user_id = Yii::app()->user->id;
        $criteria = new CDbCriteria;
        $criteria->join="INNER JOIN content_info ON t.id=content_info.course_id";
        if(!empty($user_id)){
            $criteria->condition="t.id NOT IN (SELECT course_id FROM user_course WHERE user_id='$user_id') 
                                  AND t.id !=-1 AND is_public='1'
                                  AND t.id IN (SELECT  course_id FROM content)";
        }
        
        $courses = new CActiveDataProvider('Course', array(
            'pagination' => array('pageSize' => "8"),
            'criteria' => $criteria,
        ));
        $this->render("course_all",array("courses"=>$courses));
    }
    
      # Show Course That User Has Applyed
      public function actionApplyCourse() {
            
        //create CListView Obj    
        $user_id = Yii::app()->user->id;
        $criteria = new CDbCriteria;
        $criteria->join="INNER JOIN content_info ON t.id=content_info.course_id";
        $criteria->condition="t.id IN (SELECT course_id FROM user_course WHERE user_id='$user_id' AND role='3') AND t.id !=-1 AND is_public='1'";
             
        $courses = new CActiveDataProvider('Course', array(
            'pagination' => array('pageSize' => "9"),
            'criteria' => $criteria,
        ));
        $this->render("course_all",array("courses"=>$courses));
    }
    
    
    
    public function newCoinModel(){
        $user_id = Yii::app()->user->id;
        $model_coin=new UserCoinBase();
        $model_coin->coin=0;
        $model_coin->create_date=date("Y-m-d H:i:s");
        $model_coin->update_date=date("Y-m-d H:i:s");
        $model_coin->user_id=$user_id;
        $model_coin->save();
        
        return $model_coin;
    }


    # for Person Who not Have not Course Or not Login to View detail of courese
    public function actionViewCourse($id){
        
        LinkHelper::SetPageHistory();
        Yii::app()->user->setReturnUrl("index.php?r=course/default/viewCourse&id=5");     
       
        $demo_video="";
        $course=  Course::model()->findByPk($id);
        
        SiteHelper::setTitle($course->name,$this);
        SiteHelper::setMetaTag("description","เรียน ".$course->name);
        SiteHelper::setMetaTag("keywords",$course->name);
        
        $course_contents_parent=  Content::model()->findAll("course_id='$id' AND parent_id='-1' ORDER BY show_order ASC");
        $relate_course=  RelateCourse::model()->find("course_id='$id'");
        $model_user="";
        $model_user_in_course="";
        $user_id = Yii::app()->user->id;
        $model_coin="";
        $user_own_course="";
        if(!empty($user_id)){
            $model_coin=  UserCoinBase::model()->find("user_id='$user_id'");
            
            $user_own_course=  UserCourse::model()->find("user_id='$user_id' AND course_id='$course->id'");                        
            if(empty($model_coin)){
                $model_coin=  $this->newCoinModel();
            }
        }        
        
        
        if(!empty($course->demo_video)){
            $demo_video=File::model()->findByPk($course->demo_video);
        }      
        
        
        $person= new UserCourse();
        $teachers=  UserCourse::model()->findAll($person->getRelatePeople($id, "1", "2", "criteria"));        
        $students= UserCourse::model()->findAll($person->getRelatePeople($id, "3","", "criteria","1","4")); 
        $qty_student=$person->getQtyStudent($id);
        
        
        
        
        $this->render("view_course",array("course"=>$course,"user_own_course"=>$user_own_course,
                                          "course_contents_parent"=>$course_contents_parent,
                                          "id"=>$id,"user_id"=>$user_id,"model_coin"=>$model_coin,
                                          "relate_course"=>$relate_course,"demo_video"=>$demo_video,
                                          "teachers"=>$teachers,"students"=>$students,"qty_student"=>$qty_student));
    }
    
    #click show Nore Student from >>> View course
    public function actionQueryStudentMore(){
        
               
                
        $already_query= rtrim($_POST["took"],',');
        $course_id=$_POST["course"];
        
        $sql="SELECT profile.firstname AS name,profile.lastname AS lastname,profile.photo AS photo,user_course.user_id AS user_id,tbl_users.username as username
            FROM user_course
                   INNER JOIN tbl_profiles AS profile on user_course.user_id= profile.user_id
                   INNER JOIN tbl_users on user_course.user_id = tbl_users.id
              WHERE course_id='$course_id' AND user_course.user_id NOT IN($already_query) AND user_course.role='3'";      
                
        
        $students= UserCourse::model()->findAllBySql($sql);        
        
              
        $this->renderPartial("_show_more_std",array("students"=>$students));    
        
    }
    
    # Buy Course >>> Payment page
    public function actionTakeCourse($id){
        $course=  Course::model()->findByPk($id);

        #add user to course   
        $user_id = Yii::app()->user->id;
        $model_user_in_course = new UserCourse();
        $model_user_in_course->user_id = $user_id;
        $model_user_in_course->course_id = $id;
        $model_user_in_course->create_date = date("Y-m-d H:i:s");
        $model_user_in_course->role = 3;
        $model_user_in_course->bought_price=$course->price;
        $model_user_in_course->save();

        
        #minus money
        $model_coin= UserCoinBase::model()->find("user_id='$user_id'");
        $model_coin->update_date=date("Y-m-d H:i:s");
        $model_coin->coin= $model_coin->coin-$course->price;            
        $model_coin->save();
        
        $success_url=Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$course->id,"chapterId"=>1));
        $this->redirect($success_url);

//        $this->render("take_course",array("course"=>$course));
    }
    
    
   #study course Page
    public function actionStudyCourse($courseId, $chapterId,$type="student"){
        $can_study=CourseHelper::BeforeStudy($courseId);
        $user_id=Yii::app()->user->id;
        if(!empty($can_study)){
            $this->redirect($can_study);
        }
        
        //Query Unit that has lesson
        $criteria_chapter = new CDbCriteria;
        $criteria_chapter->condition="course_id='$courseId' AND parent_id = '-1' AND id IN (SELECT parent_id FROM content WHERE parent_id!='-1' AND course_id='$courseId')";
        $criteria_chapter->order="show_order ASC";
        
        $chapters=Content::model()->findAll($criteria_chapter);
               
        //if first chapter is exercise
        $model_question="";
        $model_excercise="";
        $model_excercise_stat="";
        
        if(!empty($chapters)){        
            $is_exrecise = $chapters[$chapterId - 1]->childContents[0]->is_exercise;
            $id_exercise=$chapters[$chapterId - 1]->childContents[0]->id;
            if ($is_exrecise == 1) {


                $model_excercise = Exercise::model()->find("content_id='$id_exercise'");
                $model_excercise_stat=  ExerciseStat::model()->findAll("user_id='$user_id' AND exercise_id='$model_excercise->id' ORDER BY do_time ASC");

                    if (!empty($model_excercise)) {
                    $criteria = new CDbCriteria;
                    $criteria->condition="exercise_id='$model_excercise->id'";
                    if(!empty($model_excercise->qty_show)){
                        $criteria->limit=$model_excercise->qty_show;
                    } 

                    if($model_excercise->is_random==1){
                        $criteria->order="RAND()";
                    }else{
                        $criteria->order="show_order ASC";
                    }
                    $model_question = ExerciseQuestion::model()->findAll($criteria);
                }

            }
        }
        
       $this->render("study_course",
            array("chapters"=>$chapters,
                "chapterId"=>$chapterId,
                "courseId"=>$courseId,
                "model_question"=>$model_question,
                "model_excercise"=>$model_excercise,
                "model_excercise_stat"=>$model_excercise_stat,
                "user_id"=>$user_id,
                "type"=>$type,
                ));    
    }
    
    
    

    #show content 
    public function actionGetContent($id){
        $content=Content::model()->findByPk($id);
        
        $this->renderPartial("_contentDisplay",
            array("content"=>$content,
                ));
    }
        
   
    #Query Question And Answer To Do Questionair
    public function actionDoExerciseChoice($id){

        // Yii::app()->clientScript->scriptMap['jquery.js'] = true;
        
        $model_content=  Content::model()->findByPk($id);
        $user_id = Yii::app()->user->id;
        $model_excercise=  Exercise::model()->find("content_id='$id'");
        $model_excercise_stat="";
                    
        if(!empty($model_excercise)){
            $criteria = new CDbCriteria;
            $criteria->condition="is_public='1' AND exercise_id='$model_excercise->id'";
            if(!empty($model_excercise->qty_show)){
                $criteria->limit=$model_excercise->qty_show;
            } 
            
            if($model_excercise->is_random==1){
                $criteria->order="RAND()";
            }else{
                $criteria->order="show_order ASC";
            }
            
            $model_question=  ExerciseQuestion::model()->findAll($criteria);
            
            //query Stat Doexercise 
            $model_excercise_stat=  ExerciseStat::model()->findAll("user_id='$user_id' AND exercise_id='$model_excercise->id' ORDER BY do_time ASC");
                           
        }
        
       $this->renderPartial("_form_exercise",array("model_content"=>$model_content,
                                                   "model_excercise"=>$model_excercise,
                                                   "model_question"=>$model_question,
                                                   "model_excercise_stat"=>$model_excercise_stat,
                                                   "user_id"=>$user_id));
    }
    
    
    //save answer choice << ajax
    public function actionSaveChoiceTest($exercise){
        if(!empty($_POST['form_params'])){    
     
            $now=date("Y-m-d H:i:s");
            $post=$_POST['form_params'];
            $count_quiz=0;
            $qty_correct=0;
            $list_question_str="";
                    
            //ควร fix ว่าให้ทำหมด ทุกข้อ ?
            foreach ($post as $p) {
                if ($p["type"] == "radio") {
                    $count_quiz++;
                    $model_score = new ExerciseChoiceScore();
                    $model_score->attributes = $p;
                    $model_score->is_true = $p["is_true"]["$model_score->answer_id"];
                    $model_score->do_time = $now;
                    $model_score->user_id = Yii::app()->user->id;
                    $model_score->save();
                    $list_question_str.=$model_score->question_id.",";

                    if ($model_score->is_true == 1) {
                        $qty_correct++;
                    }
                } 
                
                if($p["type"] == "text"){                    
                    //text type
                    $count_quiz++;
                    $question_id = $p["question_id"];
                    $list_question_str.=$question_id.",";
                    
                    $list_correct_answer = ExerciseTextAnswer::model()->findAll("question_id='$question_id'");
                    $list_true_text = array(); //save true answer list text
                    $start = 1;
                    $is_corrct = true;

                    foreach ($list_correct_answer as $correct_answer) {
                        $list_true_text[$start] = $correct_answer->answer;
                        $start++;
                    }   
                    

                    if (!empty($p["answer"])) {
                        foreach ($p["answer"] as $key => $answer) {
                            $model_score_text = new ExerciseTextScore();
                            $model_score_text->question_id = $question_id;
                            $model_score_text->answer = $answer;
                            $model_score_text->do_time = $now;
                            $model_score_text->user_id = Yii::app()->user->id;
                            $model_score_text->sequence = $key;
                         
                                                   
                            $correct = false;
                            // check --> answer correct?
                            if ($model_score_text->answer == $list_true_text[$key]) {
                                $correct = true;
                                $model_score_text->is_true = "1";
                            }
                            $is_corrct = $is_corrct && $correct;                            
                            $model_score_text->save();
                   
                        }
                    }
                    if ($is_corrct == true) {
                        $qty_correct++;
                    }                    
                   
                }
                
                if($p["type"] == "code"){ 
                  
                    $count_quiz++;
                    $question_id = $p["question_id"];
                    $code=$p['code'];
                    $answer=$p['answer'];
                    $list_question_str.=$question_id.",";                    
                    $model_answer = ExerciseCodeAnswer::model()->find("question_id='$question_id'");
//                                        
                    $model_score_code = new ExerciseCodeScore();
                    $model_score_code->question_id=$question_id;
                    $model_score_code->code=$code;
                    $model_score_code->answer=$answer;
                    $model_score_code->user_id = Yii::app()->user->id;
                    $model_score_code->do_time=$now;
                                        
                    $model_answer->answer=str_replace(" ","",$model_answer->answer); 
                    $answer=str_replace(" ","",$answer); 
                    
                    if($model_answer->answer==$answer){
                        $model_score_code->is_true=1;
                        $qty_correct++;
                    }                   
                    $model_score_code->save(); 
                }       
            }//foreach
            //            
            //save stat doExercise
            $model_exercise_stat=  new ExerciseStat();
            $model_exercise_stat->exercise_id=$exercise;
            $model_exercise_stat->user_id=Yii::app()->user->id;
            $model_exercise_stat->do_time=$now;
            $model_exercise_stat->qty_question=$count_quiz;
            $model_exercise_stat->qty_correct=$qty_correct;
            $list_question_str= rtrim($list_question_str, ",");
            $model_exercise_stat->question_list=$list_question_str;            
            $model_exercise_stat->save();
            
            if($qty_correct==$count_quiz){
                echo "<img src='images/button/great.png'><b> All Correct. </b>";
            }else{
                echo "<img src='images/button/write.png'><b> Correct $qty_correct Form $count_quiz </b>";
            }
        }                
    }
    
    public function actionHistoryExercise($id,$user_id,$exercise){
//    public function actionHistoryExercise(){

//        $id=$_POST['id'];
//        $user_id=$_POST['user_id'];
//        $exercise=$_POST['exercise'];

        $model_exercise=  Exercise::model()->findByPk($exercise);
        $model_content=  Content::model()->findByPk($model_exercise->content_id);
        
        $model_exercise_stat=  ExerciseStat::model()->find("id='$id' AND user_id='$user_id' AND exercise_id='$exercise'");
        $header="Exercise :: $model_content->title";
        $list_questions=  explode(",", $model_exercise_stat->question_list);
        $do_time=$model_exercise_stat->do_time;
        $html="";
                      
        $this->renderPartial('_histrory_exercise', 
                            array("do_time"=>$do_time,
                                   "header"=>$header,
                                   "list_questions"=>$list_questions,
                                   "id"=>$id,
                                   "user_id"=>$user_id,
                                   "exercise"=>$exercise,
                                   "html"=>$html), 
                            false, true);  
    }
    
    public function syncCourse($id=""){
        
    }
    
    
        
}