<?php
class AffiliateController extends Controller
{
    public function actionIndex(){
     
        $user_id = Yii::app()->user->id;
        
        $model_affiliate = UserAffiliateBase::model()->find("user_id='$user_id'");
        
        if(!empty($model_affiliate)){
            $link_url = Yii::app()->createUrl("course/affiliate/formAffiliate");
            
        }else{
           $link_url = Yii::app()->createUrl("course/affiliate/showAffiliateCost");            
        }              
        $this->redirect($link_url);
    }
    
    public function actionFormAffiliate($is_admin=0){       
        $user_id = Yii::app()->user->id;
        $model_affiliate = UserAffiliateBase::model()->find("user_id='$user_id'");
        
        if(empty($model_affiliate)){
            $model_affiliate= new UserAffiliateBase();
        }
        
        
        if(isset($_POST['UserAffiliateBase'])){            
            $model_affiliate->attributes=$_POST['UserAffiliateBase'];
            $model_affiliate->user_id= $user_id;
            $model_affiliate->create_date= date("Y-m-d H:i:s");
            
            
            if($model_affiliate->validate()){
                $model_affiliate->save();
            }
        }        
        
        
        $this->render("form_affiliate",array("model_affiliate"=>$model_affiliate,"mode"=>"form","is_admin"=>$is_admin));        
       
    }
    
    public function actionShowAffiliateCost(){
        $user_id = Yii::app()->user->id;
        $model_affiliate = UserAffiliateBase::model()->find("user_id='$user_id'");
        
        if(empty($model_affiliate) || $model_affiliate->active_status==0){
            $this->redirect(Yii::app()->createUrl("course/affiliate/formAffiliate"));
        }else{    
        
            $criteria = new CDbCriteria;      
            $criteria->condition="affiliate_code='$model_affiliate->affiliate_code'";
            $criteria->order="create_date DESC";


            $division_stat = new CActiveDataProvider('CourseMoneyDivision', array(
                'pagination' => array('pageSize' => "10"),
                'criteria' => $criteria,
            ));
            

            $sql_sum = "select SUM(affiliate_share_price) AS affiliate_share_price FROM course_money_division WHERE affiliate_code='$model_affiliate->affiliate_code' group by affiliate_code";
            $model_sum_affiliate= CourseMoneyDivision::model()->findBySql($sql_sum);

                      
            $this->render("affiliate_cost",array("model_affiliate"=>$model_affiliate,"dataProvider"=>$division_stat,"mode"=>"payment","model_sum_affiliate"=>$model_sum_affiliate));
        }
        
    }
    
    
    public function actionAffiliateLink(){
        #find course
        #genlink        
        $user_id = Yii::app()->user->id;
        $model_affiliate = UserAffiliateBase::model()->find("user_id='$user_id'");
        
        if(empty($model_affiliate) || $model_affiliate->active_status==0){
            $this->redirect(Yii::app()->createUrl("course/affiliate/formAffiliate"));
        }else{    
            
            
            $criteria = new CDbCriteria; 
            $criteria->select="t.*,SUM(money.affiliate_share_price) as affiliate_share_price_sum";
            $criteria->condition="money.affiliate_code='$model_affiliate->affiliate_code'";
            $criteria->join = " LEFT JOIN user_affiliate as user ON (t.user_id = user.user_id)
                                LEFT JOIN course_money_division  as money ON (user.affiliate_code = money.affiliate_code)                                
                               ";
            $criteria->order="money.create_date DESC";
            $criteria->group= "t.course_id,money.course_id";


            $division_stat = new CActiveDataProvider('UserAffiliateCourse', array(
                'pagination' => array('pageSize' => "10"),
                'criteria' => $criteria,
            ));            
                    
            $this->render("affiliate_link",array("mode"=>"generate","model_affiliate"=>$model_affiliate,"dataProvider"=>$division_stat,"submode"=>"list"));
        }
    }    
      
    
    public function actionAffiliateSearchCourse(){  

        $cs = Yii::app()->clientScript;
        $cs->scriptMap['jquery-1.9.0.js'] = false;

        $user_id = Yii::app()->user->id;
        $model_affiliate = UserAffiliateBase::model()->find("user_id='$user_id'");
        
        if(empty($model_affiliate) || $model_affiliate->active_status==0){
            $this->redirect(Yii::app()->createUrl("course/affiliate/formAffiliate"));
        }
        
            $model_course= new Course('search');
            $model_course->unsetAttributes();            
           
                    
            $this->render("affiliate_search_course",array("mode"=>"generate","model_affiliate"=>$model_affiliate,
                                                          "model_course"=>$model_course,
                                                           "dataProvider"=>$model_course->getAffiliateCourse(),"submode"=>"search"));
        
    }

    
    public function actionGetlink($course_id,$code){
        $this->layout='//layouts/popup';
        
        $user_id = Yii::app()->user->id;
        $model_user_affiliate=UserAffiliateCourseBase::model()->find("user_id='$user_id' AND course_id ='$course_id'");
        
        if(empty($model_user_affiliate)){
            $model_affiliate = UserAffiliateBase::model()->find("user_id='$user_id'");
            $model_user_affiliate= new UserAffiliateCourseBase();
            $model_user_affiliate->course_id=$course_id;
            $model_user_affiliate->user_id=$user_id;
            $model_user_affiliate->affiliate_id=$model_affiliate->id;
            $model_user_affiliate->save();
        }
        
        
        $model_course= Course::model()->findByPk($course_id);         
        $this->render("show_link",array("course_id"=>$course_id,"code"=>$code,"model_course"=>$model_course));
    }
    
    
        
}