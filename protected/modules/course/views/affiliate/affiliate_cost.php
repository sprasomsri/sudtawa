<div class="w-container">       
    <div class="w-row">

        <div id="nav-content" class="w-col w-col-4">            
            <?php $this->renderPartial("_affiliate_menu", array("active_state" => $model_affiliate->active_status,"mode"=>$mode)); ?>     
        </div>

        <div class="w-col w-col-8 affiliate-content">

            <h3>Affiliate Cost</h3> <hr class="dark"/>
            
             <?php
                                    $this->widget('zii.widgets.grid.CGridView', array(
                                        'dataProvider' => $dataProvider,
                                        'columns' => array(
                                            array(
                                                'header' => Yii::t("site","No."),
                                                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)."."',
                                                'htmlOptions'=>array("style"=>"text-align:center;"),
                                            ),
//                               
//
                                            array(
                                                'type' => 'raw',
                                                'header'=>Yii::t("site","Create Date"),
                                                'value' => 'SiteHelper::dateFormat($data->create_date,"datetime")',                        
                                                'htmlOptions'=>array("style"=>"text-align:center;"),

                                            ),
                                            array(
                                                'type' => 'raw',
                                                'header'=>Yii::t("site","Course"),
                                                'value' => '$data->course->name',                        
                                                'htmlOptions'=>array("style"=>"text-align:center;"),

                                            ),
                                            
                                            array(
                                                'type' => 'raw',
                                                'header'=>Yii::t("site","Username"),
                                                'value' => '$data->user->username',                        
                                                'htmlOptions'=>array("style"=>"text-align:center;"),

                                            ),
                                            
                                            array(
                                                'type' => 'raw',
                                                'header'=>Yii::t("site","(%) Shared"),
                                                'value' => 'number_format($data->affiliate_share_percent)',                        
                                                'htmlOptions'=>array("style"=>"text-align:center;"),

                                            ),
                                            
                                            array(
                                                'type' => 'raw',
                                                'header'=>Yii::t("site","Price (Bath)"),
                                                'value' => 'number_format($data->affiliate_share_price)',                        
                                                'htmlOptions'=>array("style"=>"text-align:center;"),

                                            ),                                           

//
                                        ),
                                    ));
                                    ?>
            
            
            <div class="text-right sum-affiliate"><b>รวม:&nbsp;&nbsp;</b> <?php if(!empty($model_sum_affiliate)) echo $model_sum_affiliate->affiliate_share_price; ?> &nbsp;&nbsp; <u>บาท</u></div>
            
        </div>

    </div>
</div>

