<div class="bg-textcontent-vdo">

    <div class="background-lesson">
        <div class="lesson1-study">
            <h4 id="unit-19" class="major-lesson">   Affiliate  </h4>
        </div>
    </div>
    
    <?php if($is_admin==1){ ?>
    
     <div class="lesson-list"> 
        <div class="w-clearfix lesson-all">
            <img class="icon-lesson" src="images/affiliate_icon.png" width="50px">
            <a href="<?php echo Yii::app()->createUrl("admin/affiliate/index"); ?>" class="detail-lesson"> Affiliate List </a>            
        </div>
    </div>    
    
    <?php } ?>
    
    <div class="lesson-list"> 
        <div class="w-clearfix lesson-all <?php if($mode=="form"){ echo "lesson-active"; } ?>">
            <img class="icon-lesson" src="images/affiliate_icon.png" width="50px">
            <a href="<?php echo Yii::app()->createUrl("course/affiliate/formAffiliate"); ?>" class="detail-lesson">  Affiliate Information </a>            
        </div>
    </div>    
    
    <?php if($active_state==1){ ?>
     <div class="lesson-list"> 
        <div class="w-clearfix lesson-all <?php if($mode=="payment"){ echo "lesson-active"; } ?>">
           <img class="icon-lesson" src="images/affiliate_icon.png" width="50px">
            <a href="<?php echo Yii::app()->createUrl("course/affiliate/showAffiliateCost"); ?>" class="detail-lesson">  Affiliate Cost </a>            
        </div>
    </div>    
    
     <div class="lesson-list"> 
        <div class="w-clearfix lesson-all <?php if($mode=="generate"){ echo "lesson-active"; } ?>">
           <img class="icon-lesson" src="images/affiliate_icon.png" width="50px">
            <a href="<?php echo Yii::app()->createUrl("course/affiliate/affiliateLink"); ?>" class="detail-lesson">  Affiliate Generate </a>            
        </div>

    </div>
    
    <?php } ?>
</div>
<br/>