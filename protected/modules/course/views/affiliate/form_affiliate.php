<div class="w-container">       
    <div class="w-row">

        <div id="nav-content" class="w-col w-col-4">         
            <?php if(empty($is_admin)) {$is_admin=0;} ?>
            <?php $this->renderPartial("_affiliate_menu", array("active_state" => $model_affiliate->active_status,"mode"=>$mode,"is_admin"=>$is_admin)); ?>     
        </div>

        <div class="w-col w-col-8 affiliate-content">

            <h3>Form Affiliate</h3> <hr class="dark"/>


             <?php
            $form = $this->beginWidget('CActiveForm', array(
                 'id' => 'affiliate-form',
                 'htmlOptions' => array('enctype' => 'multipart/form-data'),
                 'clientOptions' => array(
                     'validateOnSubmit' => true,
                 ),
             ));
            
           ?>
            
           <?php echo $form->errorSummary($model_affiliate); ?>
            
            <div class="form-group">
                <label for="exampleInputEmail1">Bank Name</label>
                <?php echo $form->textField($model_affiliate, 'bank_name',array("class"=>"form-control")); ?>              
             </div>
            
            
            <div class="form-group">
                <label for="exampleInputEmail1">Bank Account</label>
                <?php echo $form->textField($model_affiliate, 'bankaccount',array("class"=>"form-control")); ?>              
             </div>
            
            
             <div class="form-group">
                <label for="exampleInputEmail1">Sub branch</label>
                <?php echo $form->textField($model_affiliate, 'subbranch',array("class"=>"form-control")); ?>              
             </div>
            
            
            <div class="form-group">
                <label for="exampleInputEmail1">Telephone</label>
                <?php echo $form->textField($model_affiliate, 'telephone',array("class"=>"form-control")); ?>              
             </div>
            
            <div class="form-group">
                <label for="exampleInputEmail1">Detail [How to promote?]</label>
                <?php echo $form->textArea($model_affiliate, 'detail',array("class"=>"form-control")); ?>    
              
             </div>
            
            
            <div class="form-group">
                <label for="exampleInputEmail1">Affiliate Code</label>
                <?php echo $form->textField($model_affiliate, 'affiliate_code',array("class"=>"form-control","disabled"=>"disbled")); ?>              
             </div>
            
            
            <?php 
            
                if($model_affiliate->active_status==1){
                    echo "<label class='affiliate-state'> State </label> <span class='label label-success'>Approve </span>";
                }
                
                if(!empty($model_affiliate->id) && $model_affiliate->active_status==0){
                    echo "<label class='affiliate-state'> State </label> <span class='label label-warning'>Processing </span>";
                }
            
            
            ?>
            
            
            
            <div class="form-group">
                <div>
                  <button type="submit" class="btn btn-default">Send Form</button>
                </div>
           </div>
            
            
            
            
            
            
            
           <?php $this->endWidget(); ?>
            
            
            
            
            
            
        </div>

    </div>
</div>

