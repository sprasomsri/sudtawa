<div class="w-container">       
    <div class="w-row">

        <div id="nav-content" class="w-col w-col-4">            
            <?php $this->renderPartial("_affiliate_menu", array("active_state" => $model_affiliate->active_status,"mode"=>$mode)); ?>     
        </div>

        <div class="w-col w-col-8 affiliate-content">

         <h3>Affiliate Generate</h3> <hr class="dark"/>
         
         
         
         <ul class="nav nav-tabs">
            <li class="<?php if($submode=="list"){echo "active";} ?>"><a href="<?php echo Yii::app()->createUrl("course/affiliate/affiliateLink"); ?>">Already Generated </a></li>
            <li class="<?php if($submode=="search"){echo "active";} ?>"><a href="<?php echo Yii::app()->createUrl("course/affiliate/affiliateSearchCourse"); ?>">Search Course</a></li>
        
         </ul>
         
            
             <?php
                                    $this->widget('zii.widgets.grid.CGridView', array(
                                        'dataProvider' => $dataProvider,
                                        'columns' => array(
                                            array(
                                                'header' => Yii::t("site","No."),
                                                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)."."',
                                                'htmlOptions'=>array("style"=>"text-align:center;"),
                                            ),
  
                                            array(
                                                'type' => 'raw',
                                                'header'=>Yii::t("site","Course Photo"),
                                                'value' => 'getCourseImage($data->course_id,$data->course->course_img)',                        
                                                'htmlOptions'=>array("style"=>"text-align:center;"),

                                            ),
                                          
                                            array(
                                                'type' => 'raw',
                                                'header'=>Yii::t("site","Course"),
                                                'value' => '$data->course->name',                        
                                                'htmlOptions'=>array("style"=>"text-align:center;"),

                                            ),
                                   
                                             array(
                                        
                                                'class'=>'CLinkColumn',
                                                'header'=>Yii::t("site","LINK"),
                                                'urlExpression'=>'Yii::app()->createUrl("course/affiliate/getlink",array("course_id"=>$data->course_id,"code"=>$data->affiliate->affiliate_code))',
                                                'label'=>'GET LINK',   
                                                 'linkHtmlOptions'=>array("class"=>"affiliate-link fancybox.iframe"),
                                                'htmlOptions'=>array("style"=>"text-align:center"),

                                            ), 
                                            
                                            
                                            
                                            

//
                                        ),
                                    ));
                                    ?>
         
         
         
        </div>

    </div>
</div>

<?php 
   function getCourseImage($course_id,$img=""){
        $course_nav_media = "<img src='images/default_course.png' height='50px'>";
       
        if(!empty($img)){
                 $course_nav_media = "<img src='course/original/$img' height='50px'>";
        }
        
        return $course_nav_media;
       
   } 
    

?>
<script>
    
    $(document).ready(function() {
	$(".affiliate-link").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: true,
		
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});
</script>