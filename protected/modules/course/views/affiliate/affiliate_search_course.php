<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>-->
<div class="w-container">       
    <div class="w-row">

        <div id="nav-content" class="w-col w-col-4">            
            <?php $this->renderPartial("_affiliate_menu", array("active_state" => $model_affiliate->active_status,"mode"=>$mode)); ?>     
        </div>

        <div class="w-col w-col-8 affiliate-content">

         <h3>Affiliate Generate</h3> <hr class="dark"/>
         
         
         
        <ul class="nav nav-tabs">
            <li class="<?php if($submode=="list"){echo "active";} ?>"><a href="<?php echo Yii::app()->createUrl("course/affiliate/affiliateLink"); ?>">Already Generated </a></li>
            <li class="<?php if($submode=="search"){echo "active";} ?>"><a href="<?php echo Yii::app()->createUrl("course/affiliate/affiliateSearchCourse"); ?>">Search Course</a></li>
        
         </ul>
         
            
          <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider' => $dataProvider,
//            'filter' => $model_course,
            'columns' => array(
                array(
                    'header' => 'No.',
                    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)."."',
                ),
                
                 array(
                    'type' => 'raw',
                    'header'=>Yii::t("site","Course Photo"),
                     'value' => 'getCourseImage($data->id,$data->course_img)',                        
                    'htmlOptions'=>array("style"=>"text-align:center;"),

                ),
                
                array(
                    'header'=>'Course Name',
                    'name' => 'name',
                    'value' => '$data->name',
                ),

               array(                                        
                    'class' => 'CLinkColumn',
                    'header' => Yii::t("site", "LINK"),
                    'urlExpression' => 'Yii::app()->createUrl("course/affiliate/getlink",array("course_id"=>$data->id,"code"=>$data->affiliate_code))',
                    'label' => 'GET Affiliate LINK',
                    'linkHtmlOptions' => array("class" => "affiliate-link fancybox.iframe"),
                    'htmlOptions' => array("style" => "text-align:center"),
                ),
            ),
        ));
?>
         
        </div>

    </div>
</div>

<?php 
   function getCourseImage($course_id,$img=""){
       
      
        $course_nav_media = "<img src='images/default_course.png' height='50px'>";
        $img_path = Yii::app()->basePath . "/../webroot/course/original/$img";
     
       
        if(!empty($img) && file_exists($img_path)){
                 $course_nav_media = "<img src='course/original/$img' height='50px'>";
        }
        
        return $course_nav_media;
       
   } 


?>



<script>
    
    $(document).ready(function() {
	$(".affiliate-link").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: true,
		
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});
</script>