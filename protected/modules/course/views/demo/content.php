  <style>
      p{
          width: 100%;
      }
      #demo-content{
        padding: 3%;
    }
    .demo-video{
        width: 70%;
        margin: 0 auto;
    }
  </style>
  
  

<div id="demo-content">  
<h4 class="hear-lesson-vdo text-center"><?php echo $model_content->title; ?></h4>
  <hr/>
 <?php  
   $videoFile = $model_content->videoFile;            
                    if (!empty($videoFile)) {
                        
                        $course_bg="";
                        if(!empty($model_course->theme_photo)){
                            $course_bg=SiteHelper::getImgBgOpacity($model_course->theme_photo);
                        }

                        $video_url = Yii::app()->baseUrl . "/files/$videoFile->user_id/video/$videoFile->name";
                        $video_path = Yii::app()->basePath . "/../webroot/files/$videoFile->user_id/video/$videoFile->name";
                         if (file_exists($video_path)) {
                             $play = VdoHelper::jwplayer($videoFile,$model_content->id);
                         ?>
  
                           <div class="media-preview study-vdo demo-video" style="<?php echo $course_bg; ?>"> 
                                    <?php echo $play; ?>                                
                            </div>
  
                         <?php 
                             
                         }
                         
                    }
                    
                    
                    echo "<hr/>".$model_content->detail;
 ?>
 </div>
