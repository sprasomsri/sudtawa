<div class="span12">
     <div class="chapter-title"> บทเรียน </div>
   
    <?php if(!empty($chapter->detail)){ ?>
    <a href="#" id="show-unit-detail" onclick="showunitdetail()" title="Unit Detail"><i class="icon-barcode"></i></a> 
    <?php } ?>
    
    <div id="unit-info" style="display: none">
        <?php echo $chapter->detail; ?>
    </div>
    <script type="text/javascript">
        function showunitdetail(){
            $("#unit-info").slideToggle();
        }
    </script>

    
</div>
<div class="row-fluid"> 
    <div class="span3">
        <!-- Left  Menu << Sub content--->
        <ul class="lesson-links">
            <?php 
             if(!empty($chapter->childContents)){
                foreach($chapter->childContents as $content){ ?>
                    <?php 
                    $class_lesson="link-type-info";
                    $class_lesson_icon="icon-video";
                    if($content->is_exercise==1){
                         $class_lesson="link-type-quiz";
                         $class_lesson_icon="icon-quiz";
                    }
                    
                    echo "<li class=\"$class_lesson\" id='lesson_$content->id'>";
                            $link=Yii::app()->createUrl("course/default/getContent/id/$content->id");
                            if($content->is_exercise==1){
                            $link=Yii::app()->createUrl("course/default/doExerciseChoice/id/$content->id");
                            }
                                         
                           echo CHtml::ajaxLink("<i class='$class_lesson_icon'></i>$content->title", $link,
                                                array('update'=>'#content-place-holder'),array("id"=>"contentsub_$content->id"));
                     echo "</li>";
                   ?>
                    
                    
             <?php }} ?>
        </ul>
    </div> <!-- span3 -->
    <div class="span9">
          <!-- content display--->
        <div id="content-place-holder">
           <?php if($chapter->childContents[0]->is_exercise!=1){ ?>
            <?php 
                $this->renderPartial('_contentDisplay', 
                array("content"=>$chapter->childContents[0])); ?>
           <?php }else{
                $this->renderPartial('_form_exercise', 
                array("model_content"=>$chapter->childContents[0], 
                      "model_question"=>$model_question,
                      "model_excercise"=>$model_excercise,
                      "courseId"=>$courseId,
                      "user_id"=>$user_id,
                      "model_excercise_stat"=>$model_excercise_stat));
                }
            ?>
        </div>
    </div> <!-- span9 -->
</div><!-- row-fluid -->