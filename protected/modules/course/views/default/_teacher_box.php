<?php 
$count=1;
foreach ($teachers as $teacher) { ?>
    <?php if($teacher->is_show==1){?>

        <?php 
        $teacher_name= $teacher->name . " " . $teacher->lastname; 
               if (!empty($teacher->photo) && file_exists(Yii::app()->basePath . "/../webroot/userphoto/original/$teacher->photo")) {
                       $teacher_phto = "userphoto/original/$teacher->photo";
                   } else {
                       $teacher_phto = "images/director.jpg";
                   }
        ?>


        <div class="img-teacher">
            <img class="img-circle teacher-photo" src="<?php echo $teacher_phto; ?>" alt="<?php echo $teacher->name; ?>">
        </div>
        <h1 class="name-teacher"><?php echo $teacher_name; ?></h1>
        <div class="instructor"><?php echo $teacher->position; ?></div>
        <p class="teacher-detail"> <?php echo $teacher->detail; ?></p>
        <div class="block-icon">
            <img src="images/teacher-fb-icon.png" alt="53fd8bcbea23e2c2725e123b_teacher-fb-icon.png">
            <img class="teacher-tw" src="images/teacher-tw-icon.png" alt="53fd8b890f661e6a06e8a092_teacher-tw-icon.png">
            <img class="teacher-google" src="images/teacher-google-icon.png" alt="53fd8e6b7eb4e36b068d2eff_teacher-google-icon.png">
            <img src="images/teacher-chat-icon.png" alt="53fd8e15ea23e2c2725e124b_teacher-chat-icon.png">
        </div>

    <?php } ?>
<?php $count++; } ?>
