<h1 class="text-center"><?php echo $model_course->name;?></h1> 
  <div class="w-container">       
      <div class="w-row">
          
        <div id="lesson-content" class="w-col w-col-8 course-content" >
          <h4 class="hear-lesson-vdo"><?php echo $model_lesson->title; ?></h4>
          
          
          <div class="w-embed w-video vdo-course">
              <?php
               $current_list=array_search($model_lesson->id, $nav_lesson);
               $lesson_redirect= Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$model_lesson->course_id,"lesson_id"=>$model_lesson->id));
               if(!empty($nav_lesson[$current_list+1])){
                   $lesson_redirect= Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$model_lesson->course_id,"lesson_id"=>$nav_lesson[$current_list+1]));
               }
              
              
                if($is_exrecise==0){
                    # check video in lesson
                    $videoFile = $model_lesson->videoFile;            
                    if (!empty($videoFile)) {
                       
                        $course_bg="";
                        if(!empty($model_course->theme_photo)){
                            $course_bg=SiteHelper::getImgBgOpacity($model_course->theme_photo);
                        }

                        $video_url = Yii::app()->baseUrl . "/files/$videoFile->user_id/video/$videoFile->name";
                        $video_path = Yii::app()->basePath . "/../webroot/files/$videoFile->user_id/video/$videoFile->name";
                         if (file_exists($video_path)) {
                            
                 ?>            
                             
                        
                         <script>
                            $(document).ready(function(){
//                                checkJwplayerStatus();                              
                            });
                            
                            function compleateLesson(){
                               $.ajax({                      
                                             url: "<?php echo Yii::app()->createUrl("course/default/setComplete"); ?>",                         
                                             dataType: 'html',
                                             type: 'POST',
                                                 data:{
                                                     'lesson_id':<?php echo $model_lesson->id ?>                          
                                                      },
                                             cache: false,
                                             success:function(result){ 
                                                 $("#course-state-<?php echo $model_lesson->id ?>").attr("src","images/end.png");
                                                 window.location = "<?php echo $lesson_redirect; ?>";
                                             }          

                                         });  
                            }
                                                    
                            function checkJwplayerStatus()
                                {               
                                    setInterval(function(){
                                        var jwstate = jwcallstate();
                                        if(jwstate=="BUFFERING"){

                                        $.ajax({                      
                                             url: "<?php echo Yii::app()->createUrl("course/default/LogBuffer"); ?>",                         
                                             dataType: 'html',
                                             type: 'POST',
                                                 data:{
                                                     'lesson_id':<?php echo $model_lesson->id ?>                          
                                                      },
                                             cache: false,
                                             success:function(result){ 
                                                 
                                             }          

                                         }); 

                                        }
                                    },3000); //3 second 
                                }
                          </script>
                          
                            
                             
                   <?php           

                            $play = VdoHelper::jwplayerStat($videoFile);
                           ?>
                            <div class="media-preview study-vdo" style="<?php echo $course_bg; ?>"> 
                                    <?php echo $play; ?>                                
                            </div>
                            
                          <?php 
                        }
                    }
                 
                    if(!empty($model_lesson->youtube_link)){
                            echo $play = VdoHelper::jwyoutube($model_lesson->youtube_link);
                            
                    }   
                    
                }
               ?>
              
                        
          </div>
          
          
          
          
          <div class="w-clearfix bg-explanation">
           
              <div class="btn-group hidden-xs nav-control" title="resize content">
                <a id="page-small" href="#" type="button" class="btn btn-default btn-sm active">  <span class="glyphicon glyphicon glyphicon-resize-small"></span> ขนาดเล็ก</a>
                <a id="page-full" href="#" type="button" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-resize-full"></span> ขนาดใหญ่ </a>
                <?php 
                $current_list=array_search($model_lesson->id, $nav_lesson);
                //current lesson
                                             
                ?>
                <?php if(!empty($nav_lesson[$current_list-1])){ ?>
                     <a id="page-small" href="<?php echo Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$model_lesson->course_id,"lesson_id"=>$nav_lesson[$current_list-1]));?>" type="button" class="btn btn-default btn-sm">  <span class="glyphicon glyphicon-backward"></span> บทเรียนก่อนหน้า</a>
                <?php  } ?>
               
                <?php if(!empty($nav_lesson[$current_list+1])){ ?>
                     <a id="page-small" href="<?php echo Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$model_lesson->course_id,"lesson_id"=>$nav_lesson[$current_list+1]));?>" type="button" class="btn btn-default btn-sm">  <span class="glyphicon glyphicon glyphicon-forward"></span> บทเรียนถัดไป</a>
                <?php  } 
                ?>
                
              
          
            </div>
           
            <p class="explanation">
                <?php
                if (!empty($model_lesson->detail)) {
                    echo $model_lesson->detail;
                }
                ?>
                
                
                <?php 
                    if(!empty($model_code)){
                ?>
                <link rel="stylesheet" type="text/css" href="css/code.css">
                <script src="plugin/ace/ace.js" type="text/javascript" charset="utf-8"></script>
                <?php 
                        if ($model_code->type == "php") {
                            $this->widget('CodeEditorPhp', array("model_code" => $model_code));
                        } else {
                            
                            if($model_code->type == "python"){
                                  $this->widget('CodeEditorSandBox', array("model_code" => $model_code));
                            }else{
                              
                                $this->widget('CodeEditor', array("model_code" => $model_code));
                            }
                        }
                    }
                
                ?>
                
                
                
                 <?php
                if ($is_exrecise == 1) {

                    $this->renderPartial('_form_exercise', array("model_question" => $model_question,
                                        "model_excercise" => $model_excercise,
                                        "courseId" => $model_course->id,
                                        "lesson_id"=>$model_lesson->id,
                                        "user_id" => $user_id,
                                        "model_excercise_stat" => $model_excercise_stat));
                }
                
                ?>

            </p>
          </div>          
          <?php $this->renderPartial('_suggestion',array("content_id"=>$model_lesson->id)); ?>  
      
        </div>          
          
        <!--- lesson menu --->
        <div id="nav-content" class="w-col w-col-4">
        <div class="bg-textcontent-vdo">
        <?php if(!empty($chapters)){ 
         $count_unit=1;
         $count_content=0;    
         foreach ($chapters as $order=>$unit){
             $class_lesson="hidden-lesoon";
             $class_unit="";
             $unit_detail_link=""; 
         
         
         if ($model_lesson->parent_id == $unit->id) {
                    $class_unit = "active-unit";
                    $class_lesson = "";
                    
         }         
         
         if (!empty($unit->detail)) {
                    $unit_link = Yii::app()->createUrl("course/default/unitDetail", array("unit" => $unit->id));
                    $unit_detail = "<a class='unit-detail' data-fancybox-type='iframe' href='$unit_link' title='unit-detail'>
                                       <span class='glyphicon glyphicon-info-sign'></span>
                                   </a>";
                } else {
                    $unit_detail = "";
            }
            
        ?>  
            
        <div class="background-lesson" onclick="<?php echo "showLesson($unit->id,event)";?>">
            <div class="lesson1-study"> 
              <h4 id="<?php echo "unit-$unit->id"; ?>" class="major-lesson <?php echo $class_unit;?>">
                  <?php echo $unit->title; ?>
              </h4>
             <?php  echo $unit_detail; ?>
            </div>
         </div>
            
                        
            
         <?php  if(!empty($unit->childContents)){ ?>
              <div class='lesson-list <?php echo $class_lesson; ?>' id='lesson-in-unit-<?php echo $unit->id ?>'>   
          <?php    
             $lessons=$unit->childContents;
               foreach ($lessons as $lesson){
                $active = "";
                $class_active="";
                $lesson_link=Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$model_course->id,"lesson_id"=>$lesson->id));
                   if ($lesson_id == $lesson->id) {                    
                       $lesson_link="#";
                       $class_active = "lesson-active";
                   }
                    
         ?>
                 
            
          <div class="w-clearfix lesson-all <?php echo $class_active; ?>">
          
             
            <img id="course-state-<?php echo $lesson->id; ?>" class="icon-lesson" src="images/<?php echo $lesson->getLessonLogBullet(); ?>">
            <a href='<?php echo $lesson_link ?>' class="detail-lesson"> <?php echo $lesson->title; ?></a> 
            
            <div class="sub-detail-lesson">
                
                <?php 
                $bullet_type="glyphicon-film";
                if($lesson->is_exercise==1){
                    $bullet_type= "glyphicon-pencil";
                }
                        
              ?>
                
              <span class="glyphicon <?php echo $bullet_type; ?> bullet-lesson"></span>             
              <div class="detail-lesson detail-lesson-sub">
                 <?php if(!empty($lesson->videoFile->duration)){
                     echo number_format(SiteHelper::convertTimeMinute($lesson->videoFile->duration))." นาที"; 
                 }else{
                     echo " - ";
                 }                 
                 
                 ?>
              </div>            
              
              
            </div>
          </div>
          
                     
        <?php  $count_content++;} ?>
          </div>        
        <?php } ?>
            
        <?php }} ?>
       
      </div>
            
            <div class="study-progress">
                <h4>เรียนมาแล้ว <span class="text-percent">[<?php echo number_format($percent_progress = ($has_learn / $count_content) * 100, 2); ?>%]</span></h4>

                <progress value="<?php echo $percent_progress; ?>" max="100" style="width: 100%;"> </progress>
                
                
                
          
      <hr/>
      <h4>ให้คะแนนคอร์สนี้ </h4>
    
      <form id="form-rating" action="<?php echo Yii::app()->createUrl("course/default/rateCourse",array("course_id"=>$model_course->id));?>" method="post">
   
     
            <input class="star {split:2}" type="radio" name="rating" value="0.5" <?php SiteHelper::checkRating($rating, "0.5"); ?>/>
            <input class="star {split:2}" type="radio" name="rating" value="1.0" <?php SiteHelper::checkRating($rating, "1"); ?>/>
            <input class="star {split:2}" type="radio" name="rating" value="1.5" <?php SiteHelper::checkRating($rating, "1.5"); ?>/>
            <input class="star {split:2}" type="radio" name="rating" value="2.0" <?php SiteHelper::checkRating($rating, "2"); ?>/>
            <input class="star {split:2}" type="radio" name="rating" value="2.5" <?php SiteHelper::checkRating($rating, "2.5"); ?>/>
            <input class="star {split:2}" type="radio" name="rating" value="3.0" <?php SiteHelper::checkRating($rating, "3"); ?>/>
            <input class="star {split:2}" type="radio" name="rating" value="3.5" <?php SiteHelper::checkRating($rating, "3.5"); ?>/>
            <input class="star {split:2}" type="radio" name="rating" value="4.0" <?php SiteHelper::checkRating($rating, "4.0"); ?>/>
            <input class="star {split:2}" type="radio" name="rating" value="4.5" <?php SiteHelper::checkRating($rating, "4,5"); ?>/>
            <input class="star {split:2}" type="radio" name="rating" value="5.0" <?php SiteHelper::checkRating($rating, "5"); ?>/>
            
            &nbsp; &nbsp;<input type="submit" value="Vote" name="submit-btn">
            
           </form>
       
         <br/><br/>
        
          <link rel="stylesheet" type="text/css" href="plugin/rating/jquery.rating.css">
          <script src="plugin/rating/jquery.rating.pack.js" type="text/javascript"></script>
          <script src="plugin/rating/jquery.form.js" type="text/javascript"></script>
          <script src="plugin/rating/jquery.MetaData.js" type="text/javascript"></script>      
                    
          </div>
      </div>
          
          
    </div>
  </div>
    
    

    <script src="js/site.js" type="text/javascript"></script> 
    <script>
//                         jQuery.noConflict(); 
        function showLesson(unit,event){
            event.preventDefault(event);
            $("#lesson-in-unit-"+unit).slideToggle();
        }
                        
        $(document).ready(function(){
            
                            
           $("#page-full").on("click",function(){
               $("#lesson-content").removeClass("w-col-8");
               $("#lesson-content").addClass("w-col-12");
                               
               $("#nav-content").removeClass("w-col-4");
               $("#nav-content").addClass("w-col-12");
                               
               $("#page-full").addClass("active");
               $("#page-small").removeClass("active");
                               
               $("#course-img").hide();
                               
           });
                                   
            $("#page-small").on("click",function(){
               $("#lesson-content").removeClass("w-col-12");
               $("#lesson-content").addClass("w-col-8");
                               
               $("#nav-content").removeClass("w-col-12");
               $("#nav-content").addClass("w-col-4");
                               
               $("#page-full").removeClass("active");
               $("#page-small").addClass("active");
               $("#course-img").show();
                               
                               
           });
                           
                           
           $(".unit-detail").fancybox({
               fitToView: true,
               width	: '80%',
               height	: 'auto',                   
               autoSize	: false,
               closeClick: true,
               openEffect: 'none',
               closeEffect: 'none',
            });
                                   
        });
                        
    </script>

    <script>
        $(document).ready(function(){     
            document.title = "<?php echo $model_lesson->title . "- " . $model_course->name; ?>"; 
        });
    </script>



