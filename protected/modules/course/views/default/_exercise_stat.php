<div id="loading" align="center" class="row-fluid load-more" style="display: none">
    <div class="span12">
        <img alt="" src="images/loading.gif">
        <p class="text-loading">กำลังโหลด</p>
    </div>
</div>
    <?php
    $dataProvider=getStatExercise($user_id,$exercise);
    if(!empty($dataProvider)){            
        $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider' => $dataProvider,
            'columns' => array(
               
                array(
                    'header' => Yii::t("site", "วัน - เวลา"),
                    'value' => 'SiteHelper::dateFormat($data->do_time)',

                ),
                array(
                   'header' => Yii::t("site", "จำนวนข้อที่ถูก"),
                    'value' => '$data->qty_correct',
                    'htmlOptions' => array("style" => "text-align:right;"),
                ),
                array(
                    'header' => Yii::t("site", "จำนวนคำถามทั้งหมด"),
                    'value' => '$data->qty_question',
                    'htmlOptions' => array("style" => "text-align:right;"),
                ),
    //
                array(
                    'type'=>"raw",
                    'header' => "ดูคำตอบ",
                    'value' => 'makeLink($data->id,$data->user_id,$data->exercise_id,$data->id)',
                    'htmlOptions' => array("style" => "text-align:right;"),
                ),

            ),
        ));
    }

    ?>

<?php 
function makeLink($id,$user_id,$exercise,$id){
    //ajax link
//    $url = Yii::app()->createUrl("course/default/historyExercise");
    $url = Yii::app()->createUrl("course/default/historyExercise",array("id"=>$id,"user_id"=>$user_id,"exercise"=>$exercise));
    $link = "<a href='#' id='view-history-exercise-$id' class='history-link' 
            onclick='viewhistory($id)' 
            data-id='$id' 
            data-user='$user_id' 
            data-url='$url' 
            data-exercise='$exercise'>
            ดูคำตอบ
            </a>";
    return $link;

}



function getStatExercise($user_id, $exercise) {
    $criteria = new CDbCriteria;
    $criteria->condition = "user_id ='$user_id' AND exercise_id='$exercise'";
    $criteria->order = "do_time ASC";
    $model_stat=  ExerciseStat::model()->findAll($criteria);
    
  
    return new CActiveDataProvider("ExerciseStat", array(
        'criteria' => $criteria,
        'pagination' => false,
    ));
}


?>