<div class="std-list">
<?php
$list_student = "";

foreach ($students as $student) {
    $list_student.=$student->user_id . ",";
    $student_pic_path = Yii::app()->basePath . "/../userphoto/$student->photo";

    if (!empty($student->photo) && file_exists($student_pic_path)) {
        $student_pic = "userphoto/$student->photo";
    } else {
        $student_pic = "images/user_default.jpg";
    }
    
    if(!empty($student->facebook_id)){            
            $student_pic= "https://graph.facebook.com/$student->facebook_id/picture";
        }
    
    ?>

    
    <span class="user_id_list" data-user="<?php echo $student->user_id ?>"> 
            <img class="enrolled-pic std-photo" src="<?php echo $student_pic; ?>"/>
    </span> 


<?php } ?>
</div>