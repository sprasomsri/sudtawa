<div id="student-list">
    <?php
    $list_student = "";
    foreach ($students as $student) {
        $list_student.=$student->user_id . ",";
        $student_pic_path = Yii::app()->basePath . "/../webroot/userphoto/$student->photo";

        if (!empty($student->photo) && file_exists($student_pic_path)) {
            $student_pic = "userphoto/$student->photo";
        } else {
            $student_pic = "images/user_default.jpg";
        }
        
        if(!empty($student->facebook_id)){            
            $student_pic= "https://graph.facebook.com/$student->facebook_id/picture";
        }
        
        
        ?>


        <span class="user_id_list" data-user="<?php echo $student->user_id ?>"> 
            <img class="enrolled-pic std-photo" src="<?php echo $student_pic; ?>"/>
        </span> 
<?php } ?>
</div>

<div id="loading" style="display: none;"> 
    <img src="images/loading.gif" width="20%"> 
</div> 

<input type="hidden" value="<?php echo $list_student ?>" id="list-std-have">
<?php $other_student = $qty_student - count($students); ?>
<input type="hidden" value="<?php echo $qty_student ?>" id="all_std">

<?php if ($other_student > 0) { ?>
    <div id="is-std-left"><br/>
        และ<a href="#" id="show-student-more"><u><?php echo Yii::t("site", "Others Member"); ?> 
                <span id="qty-std-left"><?php echo $other_student; ?> </span>
                <?php echo Yii::t("site", "person(s)") ?></u></a><?php echo Yii::t("site", "Study This Course") ?>
    </div>
<?php } ?>
    


<script type="text/javascript">   
<?php $action_more_std = Yii::app()->createUrl("course/default/queryStudentMore"); ?>
            $(document).ready(function(){
               $("#show-student-more").click(function(event){
                event.preventDefault();
                
               
                var took="";
                var qty_queryed=0;
                var all_std=$("#all_std").val();
                var one_time_query=24;
                         
                 //loop for select already was chosen
                $(".user_id_list").each(function(){
                    took+=$(this).data('user')+",";
                    qty_queryed=qty_queryed+1;
                });
                
               
                                           
                $.ajax({
                        url: '<?php echo $action_more_std; ?>',
                        data: {
                            "took" : took,
                            "course": <?php echo $course_id; ?>
                        },
                        dataType: 'html',
                        type: 'POST',
                        beforeSend:function(){
                            $("#loading").show();
                        },
                        success: function(html) {               
                                             
                                                $("#loading").hide();
                                                $("#student-list").append(html);
                                                var left = all_std - qty_queryed -one_time_query;                                             
                                                if (left <= 0) {
                                                    $("#is-std-left").hide();
                                                } else {
                                                    $("#qty-std-left").html(left);
                                                }


                                            }
                                        });

                                    });

                                });

</script>



