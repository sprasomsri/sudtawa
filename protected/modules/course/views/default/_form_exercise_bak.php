<div class="lesson-content-wrapper lesson-type-video">
    <div class="lesson-content-title">
        <span class="icon-green-quiz"></span>  
        <?php 
            echo $model_content->title;
           CourseHelper::saveContentLog($model_content->id,$model_content->course_id);
         ?>
        
    </div>
    
    <div id='exercise-content' class="stat-exercise">
        <?php 

            $style_lesson="display:block";
            if(!empty($model_excercise_stat)){
                $style_lesson="display:none";
                
                //show grideview do-exercise history.      
                $this->renderPartial("_exercise_stat",array("model_excercise_stat"=>$model_excercise_stat,"user_id"=>$user_id,"exercise"=>$model_excercise->id));
                ?>
                
               <!--- -ReNew Exercise button-->
                <div class="center-box">
                    <button class="btn btn-green-submit-box" id="repeat-exercise"><?php echo Yii::t("site","Review This Exercise"); ?></button>
               </div>        
                <?php 
            }       

        ?>
   </div>     
   
    
    
    <div class="lesson-content" style="<?php echo $style_lesson; ?>">
       
        <div class="video-wrapper">
            <?php 
            if(!empty($model_content->detail)){
                echo $model_content->detail;
            }            
            ?>
            
            <br/>
            <div id="result" class="alert alert-info" style="display: none;"> </div>
            <?php
            //use Ajax for submit and save data
             
            $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'do-test',
                    'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            
            
           <?php 
           $count=1;
           if(!empty($model_question)){
               foreach($model_question as $question){                 
                  echo "<input type='hidden' name='ExerciseChoiceScore[$count][question_id]' value='$question->id'>";                  
                  if($question->type==1 || $question->type==3){ //choice question?>                
                      
                     <div class="quiz-wrapper">
                        <div class="quiz-question">
                            <div class="question-title-wrapper row-fluid">
                                <div class="question-title-tag span2">ข้อ <?php echo $count;?></div>
                                <div class="question-title span10">
                                    <?php echo $question->question?>
                                </div>
                            </div>
                        </div>
                        <div class="quiz-answer">
                            <div class="btn-group-quiz"  data-toggle="buttons-radio">
                                <?php   $answers=$question->answer;  ?>
                                
                                <?php  echo "<input type='hidden' name='ExerciseChoiceScore[$count][type]' value='radio'>"; ?>                                
                                <?php if(!empty($answers)){                               
                                    foreach($answers as $answer){
                                        echo "<input type='radio' name='ExerciseChoiceScore[$count][answer_id]' value='$answer->id'>";
                                        echo $answer->answer;
                                        echo "<input type='hidden' name='ExerciseChoiceScore[$count][is_true][$answer->id]' value='$answer->is_true'>";
                                        echo "<br/><br/>";
                                    }
                              
                                }?>
                               
                            </div>
                        </div>                     
                    </div><!-- quiz-wrapper -->    
                      
                      
               <?php                          
               }
               
               
               if($question->type==2){ 
                     $question_text=strip_tags($question->question);
                     $sub_question=explode("__", $question_text);
                     $count_sub_question=0;                   
                     $answers=$question->answerText;              
                     echo "<input type='hidden' name='ExerciseChoiceScore[$count][type]' value='text'>";
                   
                   ?>                   
                    
                      <div class="quiz-wrapper">
                        <div class="quiz-question">
                            <div class="question-title-wrapper row-fluid">
                                <div class="question-title-tag span2">ข้อ <?php echo $count;?></div>
                                <div class="question-title span10">
                                    <?php echo $question->question?>
                                </div>
                            </div>
                        </div>
                        <div class="quiz-answer">
                            <div class="btn-group-quiz"  data-toggle="buttons-radio">
                                                             
                                <?php if(!empty($answers)){
                               
                                foreach($answers as $answer){
                                      $placeHolder="";
                                       if(!empty($sub_question[$count_sub_question])){
                                         $placeHolder=$sub_question[$count_sub_question];
                                        }
                                        
                                        echo "<input type='text' name='ExerciseChoiceScore[$count][answer][$answer->sequence]' placeHolder='$placeHolder'>";
                                        echo "<br/>";
                                     
                                        $count_sub_question++;                                 
                                }
                             
                                }?>
                               
                            </div>
                        </div>                     
                    </div><!-- quiz-wrapper -->                 
              <?php               
               }
               
                if($question->type==4){                
                     echo "<input type='hidden' name='ExerciseChoiceScore[$count][type]' value='code'>";
                   
                   ?>                   
                    
                      <div class="quiz-wrapper">
                        <div class="quiz-question">
                            <div class="question-title-wrapper row-fluid">
                                <div class="question-title-tag span2">ข้อ <?php echo $count;?></div>
                                <div class="question-title span10">
                                    <?php echo $question->question?>
                                </div>
                            </div>
                        </div>
                        <div class="quiz-answer">
                            <div class="btn-group-quiz"  data-toggle="buttons-radio">
                                                             
                                ANSWER CODE                            
                                <div id="codeEditor-<?php echo $count;?>" class="editor-box"></div>
                                 
                                <textarea name="<?php echo "ExerciseChoiceScore[$count][code]"; ?>" id="code-editor-<?php echo $count;?>" class="ace"></textarea>
                                <div id="loading-<?php echo $count;?>" class='loading' style='display:none;'><img src="images/bar-loading.gif" width="40px"> Writing code..</div>                                
                            </div>
                            <script type="text/javascript"> 
                                    $(document).ready(function(){
                                        var editor = ace.edit("codeEditor-<?php echo $count;?>");
                                        editor.setTheme("ace/theme/crimson_editor");
                                        editor.getSession().setMode("ace/mode/java");
                                        var textarea = $('textarea[id="code-editor-<?php echo $count; ?>"]').hide();
                                        
                                        
                                        $("#codeEditor-<?php echo $count;?>").on("mouseleave",function(){       
                                             var data=editor.getSession().getValue();
                                             textarea.val(data);
                                             $("#loading-<?php echo $count;?>").hide();
                                        });
                                        
                                        $("#codeEditor-<?php echo $count;?>").on("click",function(){       
                                             $("#loading-<?php echo $count;?>").show();
                                        });                         
                                    })                     
                            </script>                    
                            
                        </div>                     
                    </div><!-- quiz-wrapper -->           
                <?php }             
               $count++;          
             }
           }           
           ?>            
            
            <div class="row-fluid content row-course">                                    
                <div class="span3"></div>
                <div class="span9" align="right">
                    <div class="span8"></div>

                    <button type="submit" class="span3 btn btn-green-submit">ต่อไป</button>
                </div>                                    
            </div>
            
         <?php $this->endWidget(); ?>
            
       
            
    <?php $action_save=Yii::app()->createUrl("course/default/saveChoiceTest",array("exercise"=>$model_excercise->id));?>
   <script type="text/javascript">
             $( document ).ready(function() {
                $("#do-test").submit(function(event) {
                event.preventDefault();
               
                var form_params = $("#do-test").serializeJSON();

               
                $.ajax({
                  url: '<?php echo $action_save; ?>',
                  type: 'POST',
                  data:{
                        form_params:form_params
                       },
                  dataType: 'html',
                  success: function(message) {
                        $("#result").html(message);
                        $("#result").slideDown();
                        $("#do-test").hide();
                  }
                });
                });
                
                $("#repeat-exercise").on("click",function(event){
                    $(".lesson-content").show();
                    $(".stat-exercise").hide();
                    
                });
                
                //code Editor set
                
                
                
             });
            
    </script>
         
                   
        </div>
    </div><!-- lesson-content -->
</div> <!-- lesson-content-wrapper -->