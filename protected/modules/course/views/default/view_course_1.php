<!--- Add background --> 
<div class="section hero" style="">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-5 center">
            
            <?php
            if (!empty($demo_video)) {
                $video_url = Yii::app()->baseUrl . "/files/$demo_video->user_id/video/$demo_video->name";
                $video_path = Yii::app()->basePath . "/../files/$demo_video->user_id/video/$demo_video->name";

                if (file_exists($video_path)) {
                    $play = VdoHelper::jwplayer($video_url, '370', '300px');
                    $course_nav_media=$play;
                }
            } else {
                if (!empty($course->course_img)) {
                    $course_nav_media= "<img src='course/original/$course->course_img' width='100%' title='$course->name'>";
                } else {
                    $course_nav_media= "<img src='images/img-course-big.jpg' width='90%'>";
                }
            }
            ?> 
            
            <?php echo $course_nav_media; ?>
        </div>
        <div class="w-col w-col-7 hero-column">
          <h2 class="course-name"><?php echo $course->name; ?></h2>
          <p>
              <?php echo strip_tags($course->description); ?>
          
          </p>
           <?php
              if (!empty($user_id)) {
                  $href = Yii::app()->createUrl("course/default/takeCourse", array("id" => $course->id));
                  } else {
                  $href = Yii::app()->createUrl("user/login");
              }
              ?>
          
                      
           <a class="fancybox.ajax btn btn-warning btn-lg" id="operate11111111" href="<?php echo $href;?>">
              <img src="images/coin.png"> <b><?php echo number_format($course->price); ?> Coins</b>
          </a>
        
        </div>
      </div>      
    </div>
 
  </div>
<section class="section">
   <div class="w-container">
       <div class="w-col w-col-8 center">
           
            <?php $this->renderPartial("_course_content",array("course_contents_parent"=>$course_contents_parent))?>
           
       </div>
              
       <div class="w-col w-col-4">
           <?php 
           if (!empty($teachers))
            $this->renderPartial("_teacher_box",array("teachers"=>$teachers)); 
           ?>
           
           
           
           <?php 
                  if(!empty($relate_course))
                  $this->renderPartial("_relate_course",array("relate_course"=>$relate_course)); 
           ?>
           
           
            <?php 
                if(!empty($students))            
               $this->renderPartial("_student_box",array("students"=>$students,"qty_student"=>$qty_student,"course_id"=>$course->id));           
           ?>
           
       </div>
  </div>
</section>


   <script type="text/javascript">
    jQuery.noConflict(); 
    $(document).ready(function() {
      
        $("#operate11111111").fancybox({
		maxWidth	: 900,
		maxHeight	: 900,
		fitToView	: false,
		width		: '80%',
		height		: '70%',
		autoSize	: false,
		closeClick	: true,
		openEffect	: 'none',
		closeEffect	: 'none',
                
	});
      });
      
      
    </script>

