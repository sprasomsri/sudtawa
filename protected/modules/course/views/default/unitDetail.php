<div class="section">
    <div class="w-container">
        <h1 class="head-center"><?php echo $model_course->name; ?></h1>

        <div class="w-row">
            <div class="w-col w-col-3 content-column">
                <?php if(!empty($model_course->course_img)){
                    echo "<img src='course/$model_course->course_img' class='img-circle img-responsive'>";
                }
             ?>
            </div>

            <div class="w-col w-col-9 content-column">
                <h5><u>Course Detail</u></h5>
                <p><?php echo $model_course->description; ?></p>
                <hr class='gray-line'/>
            </div>

        </div>
        <br/>
        <div class="w-row">
            <div class="w-col w-col-3 content-column">
             
            </div>

            <div class="w-col w-col-9 content-column">
                  <div class="w-row">
                    <div class="w-col w-col-2 content-column"> 
                        <h5 class=''><u>Unit's Name</h5></u> 
                    </div> 
                    <div class="w-col w-col-10 content-column"> 
                       <?php echo $model_content->title; ?>
                    </div>
                  </div>
                
                <div class="w-row">
                    <div class="w-col w-col-2 content-column"> 
                        <h5 class=''><u>Unit's detail</h5></u> 
                    </div> 
                    <div class="w-col w-col-10 content-column"> 
                       <?php echo $model_content->detail; ?>
                    </div>
                  </div>
                
                <div class="w-row">
                    <div class="w-col w-col-2 content-column"> 
                        <h5 class=''><u>Unit's Lesson</h5></u> 
                    </div> 
                    <div class="w-col w-col-10 content-column"> 
                       <?php 
                            if(!empty($model_content->childContents)){
                                foreach ($model_content->childContents as $lesson){
                                    echo "-".$lesson->title."<br/>";
                                }
                            }              
                       ?>
                    </div>
                  </div>
                <hr class='gray-line'/> 
                <p class='text-right'> ©&nbsp;Streaming. All Rights Reserved.</p>
            </div>

        </div>
        
    </div>
</div>
            
            
