<?php 
   $current_list = array_search($model_lesson->id, $nav_lesson);
    $lesson_redirect = Yii::app()->createUrl("course/default/studyCourse", array("courseId" => $model_lesson->course_id, "lesson_id" => $model_lesson->id));
    if (!empty($nav_lesson[$current_list + 1])) {
        $lesson_redirect = Yii::app()->createUrl("course/default/studyCourse", array("courseId" => $model_lesson->course_id, "lesson_id" => $nav_lesson[$current_list + 1]));
    }
?>
<style type="text/css">
    p{ 
       /*overide case Ann has style border on tag p*/
       border: 0px !important;
       color: white !important;
    }
    p.explicate-course{
        color:#969696 !important;
    }
    
</style>

<div class="banner">
    <div class="vdo-play">
        <div class="w-row">
            <div class="w-col w-col-2 w-clearfix">
                <?php if(!empty($nav_lesson[$current_list-1])){ ?>
                     <a href="<?php echo Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$model_lesson->course_id,"lesson_id"=>$nav_lesson[$current_list-1]));?>" type="button"> 
                         <img class="arrow-back" src="images/back.png" alt="53fec1abaea2a5c70ee91674_back.png">
                     </a>
                <?php  } ?>
                
                
                
            </div>
            <div class="w-col w-col-8">
                <div class="arrow-course">
                    <a href="<?php echo Yii::app()->createUrl("course/default/courseAll"); ?>"><h1 class="back-courses">คอร์สทั้งหมด</h1></a>
                    <a href="<?php echo Yii::app()->createUrl("user/logout"); ?>"><h1 class="back-courses">ออกจากระบบ</h1></a>
                </div>
                <h1 class="name-vdo"><?php echo $model_lesson->title; ?></h1>
                
                
                <div class="w-embed w-video">
                    <?php   $videoFile = $model_lesson->videoFile;  
                        if(!empty($videoFile)){
                            $video_url = Yii::app()->baseUrl . "/files/$videoFile->user_id/video/$videoFile->name";
                            $video_path = Yii::app()->basePath . "/../webroot/files/$videoFile->user_id/video/$videoFile->name";
                         if (file_exists($video_path)) { ?>
                     
                    
                         <script>
                            $(document).ready(function(){
                                checkJwplayerStatus();                              
                            });
                            
                            function compleateLesson(){
                               $.ajax({                      
                                             url: "<?php echo Yii::app()->createUrl("course/default/setComplete"); ?>",                         
                                             dataType: 'html',
                                             type: 'POST',
                                                 data:{
                                                     'lesson_id':<?php echo $model_lesson->id ?>                          
                                                      },
                                             cache: false,
                                             success:function(result){                                                 
                                                 window.location = "<?php echo $lesson_redirect; ?>";
                                             }          

                                         });  
                            }
                                                    
                            function checkJwplayerStatus()
                                {               
                                    setInterval(function(){
                                        var jwstate = jwcallstate();
                                        if(jwstate=="BUFFERING"){

                                        $.ajax({                      
                                             url: "<?php echo Yii::app()->createUrl("course/default/LogBuffer"); ?>",                         
                                             dataType: 'html',
                                             type: 'POST',
                                                 data:{
                                                     'lesson_id':<?php echo $model_lesson->id ?>                          
                                                      },
                                             cache: false,
                                             success:function(result){ 
                                                 
                                             }          

                                         }); 

                                        }
                                    },3000); //3 second 
                                }
                          </script>
                          
                    
                    <?php 
                     
                            $play = VdoHelper::jwplayer($videoFile,$model_lesson->course_id);
//                            $play = VdoHelper::jwplayerStat($videoFile) //php file;
                             echo $play;
                    ?>                
                    
                    <?php }}
                    
                    if(!empty($model_lesson->youtube_link)){
                            echo $play = VdoHelper::jwyoutube($model_lesson->youtube_link);                            
                    }  
                    
                    ?>
                          
                    <?php
                        if (!empty($model_lesson->detail)) {
                            echo "<p class='lesson-text'>".$model_lesson->detail."</p>";
                        }
                    ?>
                    
                </div>
            </div>
            <div class="w-col w-col-2">
                 <?php if(!empty($nav_lesson[$current_list+1])){ ?>
                     <a href="<?php echo Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$model_lesson->course_id,"lesson_id"=>$nav_lesson[$current_list+1]));?>" type="button"> 
                          <img class="arrow-next-vdo" src="images/next.png" alt="53fec135aea2a5c70ee91671_next.png">
                     </a>
                <?php  } 
                ?>
                
                
              
            </div>
            <div></div>
        </div>
    </div>
</div>
<div class="course-vdo">
    <div>
        <h1 class="head-vdo"> <?php echo $model_course->name;?> </h1>
    </div>
 <?php if(!empty($chapters)){  
     $count_unit = count($chapters);
     foreach ($chapters as $unit){
          if($count_unit > 1){
              echo "<h2>".$unit->title."</h2>";
          }
           $lessons=$unit->childContents;
           if(!empty($lessons)){
               foreach ($lessons as $lesson){
                   $class_box = "";
                   if($model_lesson->id ==$lesson->id){
                       $class_box = "active-lesson";
                   }
                   
     ?>   
    <a href="<?php echo Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$model_course->id,"lesson_id"=>$lesson->id));?>">
    <div class="course-study <?php echo $class_box; ?>">
        <div class="w-row">
            <div class="w-col w-col-2">
                <div class="icon-vdo">
                    <img src="images/vdo.png" alt="53fd675cc4c606b2038a76ae_vdo.png">
                </div>
            </div>
            <div class="w-col w-col-10">
                <h1 class="head-course"> <?php echo $lesson->title; ?>  </h1>
                <p class="explicate-course"> <?php echo strip_tags($lesson->detail);?>
                    
                    <br/>
                     <?php if(!empty($lesson->videoFile->duration)){ 
                         $time_lesson = number_format(SiteHelper::convertTimeMinute($lesson->videoFile->duration));
                     
                            echo "วีดีโอ $time_lesson นาที";
                        }
                      ?>
                    </p>
            </div>
        </div>
    </div>
    </a>
 
    <?php }}}} ?>

</div>


<script src="js/site.js" type="text/javascript"></script> 

<script>
    $(document).ready(function(){     
        document.title = "<?php echo $model_lesson->title . "- " . $model_course->name; ?>"; 
    });
</script>



