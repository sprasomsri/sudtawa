<?php LinkHelper::SetPageHistory();
$this->setPageTitle('coursecreek Courses');
?>
<script>
    $(document).ready(function(){
       document.title = "courses"; 
    });
</script>

  <div class="gray-row">
    <div class="container-full">
      <h1 class="course-head">เลือกคอร์สที่คุณสนใจ</h1>
      <h3 class="text-study">แล้วสามารถเริ่มต้นการเรียนรู้ไปพร้อมๆ กับเราได้ทันทีเลือกคอร์สที่คุณสนใจ</h3>
      
      
      <div class="w-row"> 

          
           <?php
                $this->widget('zii.widgets.CListView', array(
                'id' => 'courselist',
                'dataProvider' => $courses,
                'itemView' => '_course_box',
                'template' => " {summary} \n <br/> {items}\n{pager}",
                //'viewData' => array('lang' => $lang),
                'pager' => array(
                    'class' => 'CLinkPager',
                ),
            ));
            ?> 
          
          </div> 
    </div>
   
    </div>
  </div>



