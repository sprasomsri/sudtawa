<div class="lesson-content-wrapper lesson-type-video">
    <div class="lesson-content-title">
        <span class="icon-green-quiz"></span>  
       
        
    </div>
    
    <div id='exercise-content' class="stat-exercise">
        <?php 

            $style_lesson="display:block";
            if(!empty($model_excercise_stat)){
                $style_lesson="display:none";
                
                //show grideview do-exercise history.      
                $this->renderPartial("_exercise_stat",array("model_excercise_stat"=>$model_excercise_stat,"user_id"=>$user_id,"exercise"=>$model_excercise->id));
                ?>
                
               <!--- -ReNew Exercise button-->
                <div class="center-box text-center">
                    <button class="btn btn-warning" id="repeat-exercise"><?php echo Yii::t("site","Review This Exercise"); ?></button>
               </div>        
                <?php 
            }       

        ?>
   </div>        
    
    
    <div class="lesson-content lesson-exercise" style="<?php echo $style_lesson; ?>">
       
        <div class="video-wrapper">
          
            
            <br/>
            <div id="result" class="alert alert-info" style="display: none;"> </div>
            <?php
            //use Ajax for submit and save data
             
            $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'do-test',
                    'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            
            
           <?php 
           $count=1;
           if(!empty($model_question)){
               echo "<hr class='seperate-content'/>";
               foreach($model_question as $question){                 
                  echo "<input type='hidden' name='ExerciseChoiceScore[$count][question_id]' value='$question->id'>";                  
                  if($question->type==1 || $question->type==3){ //choice question?>                
                      
                     <div class="form-group">
                       
                           
                                <label>ข้อ <?php echo $count;?> </label> 
                                <br/>
                                 <?php echo $question->question?>
                                
                          
                       
                        <div class="quiz-answer">
                          
                                <?php   $answers=$question->answer;  ?>
                                
                                <?php  echo "<input type='hidden' name='ExerciseChoiceScore[$count][type]' value='radio'>"; ?>                                
                                <?php if(!empty($answers)){                               
                                    foreach($answers as $answer){
                                        echo "<div class='radio'><label>
                                             <input type='radio' name='ExerciseChoiceScore[$count][answer_id]' value='$answer->id'>";
                                        echo $answer->answer;
                                        echo "<input type='hidden' name='ExerciseChoiceScore[$count][is_true][$answer->id]' value='$answer->is_true'>";
                                        echo "</label></div>";
                                    }
                              
                                }?>
                               
                            
                        </div>                     
                    </div><!-- quiz-wrapper -->    
                      
                      
               <?php                          
               }
               
               
               if($question->type==2){ 
                     $question_text=strip_tags($question->question);
                     $sub_question=explode("__", $question_text);
                     $count_sub_question=0;                   
                     $answers=$question->answerText;              
                     echo "<input type='hidden' name='ExerciseChoiceScore[$count][type]' value='text'>";
                   
                   ?>                   
                    
                      <div class="form-group">
                       
                           
                                <label>ข้อ <?php echo $count;?></label>
                                <br/>
                                    <?php echo $question->question?>
                                
                           
                       
                        <div class="quiz-answer">
                            <div class="btn-group-quiz"  data-toggle="buttons-radio">
                                                             
                                <?php if(!empty($answers)){
                               
                                foreach($answers as $answer){
                                      $placeHolder="";
                                       if(!empty($sub_question[$count_sub_question])){
                                         $placeHolder=$sub_question[$count_sub_question];
                                        }
                                        
                                        echo "<input type='text' class='form-control' name='ExerciseChoiceScore[$count][answer][$answer->sequence]' placeHolder='$placeHolder'>";
                                        echo "<br/>";
                                     
                                        $count_sub_question++;                                 
                                }
                             
                                }?>
                               
                            </div>
                        </div>                     
                    </div><!-- quiz-wrapper -->                 
              <?php               
               }
               
                if($question->type==4){  //code Editior               
                     echo "<input type='hidden' name='ExerciseChoiceScore[$count][type]' value='code'>";
                   
                   ?>                   
                    
                      <div class="form-group">
                        <div class="quiz-question">
                            <div class="question-title-wrapper row-fluid">
                                <label>ข้อ <?php echo $count;?></label>
                                <br/>
                                    <?php echo $question->question?>
                              
                            </div>
                        </div>
                        <div class="quiz-answer">
                            <div class="btn-group-quiz"  data-toggle="buttons-radio">                                                   
                                                
                                <!--<textarea name="<?php //echo "ExerciseChoiceScore[$count][code]"; ?>" id="code-editor-<?php //echo $count;?>" class="ace"></textarea>-->
                                <?php 
                                
                                if(empty($question->code_type)|| $question->code_type=="java"){
                                
                                
                                $this->widget('CodeEditor',array("mode"=>"text",
                                                                       "input_name"=>"ExerciseChoiceScore[$count][code]",
                                                                        "is_exercise"=>"1",
                                                                        "text_code"=>$question->initial_code,
                                                                         "is_show_answer"=>"0",
                                                                        "input_answer_name"=>"ExerciseChoiceScore[$count][answer]",
                                                                        "is_show_compile"=>"0")); 
                                }else{
                                    if($question->code_type=="python"){
                                          $this->widget('CodeEditorSandBox',array("default_code"=>$question->initial_code,
                                                                                "input_answer_name"=>"ExerciseChoiceScore[$count][answer]",
                                                                                "input_name"=>"ExerciseChoiceScore[$count][code]",
                                                                                "is_exercise"=>"1","is_show_answer"=>"0",)); 
                                    }
                                }   
                                
                                
                                ?>
                                 <p style="margin:2%; color: #FF9F40;"> กด ( <i class="icon-play"></i>) เพื่อเพิ่มคำตอบ </p> 
                            </div>
                           
                                   
                            
                        </div>                     
                    </div><!-- quiz-wrapper -->           
                <?php }             
               $count++;          
             }
           }  //foreach Question         
           ?>            
            
            <div> 
                <button type="submit" class="btn btn-warning"> Submit </button>   
                <?php
                    if(!empty($model_excercise_stat)){
                        
                        $url_history=Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$courseId,"lesson_id"=>$lesson_id));
                        echo "&nbsp;<a href='$url_history' type='button' class='btn-padding btn btn-default btn-inverse'>History Page</a>";
                    }
                ?>
                
                
            </div>
            
         <?php $this->endWidget(); ?>
            
                   
        </div>
    </div><!-- lesson-content -->
</div> <!-- lesson-content-wrapper -->


<link rel="stylesheet" type="text/css" href="css/code.css">
<script src="plugin/ace/ace.js" type="text/javascript" charset="utf-8"></script>


    <?php $action_save=Yii::app()->createUrl("course/default/saveChoiceTest",array("exercise"=>$model_excercise->id));?>
   <script type="text/javascript">
             $( document ).ready(function() {
                $("#do-test").submit(function(event) {
                event.preventDefault();
               
                var form_params = $("#do-test").serializeJSON();

               
                $.ajax({
                  url: '<?php echo $action_save; ?>',
                  type: 'POST',
                  data:{
                        form_params:form_params
                       },
                  dataType: 'html',
                  success: function(message) {
                        $("#result").html(message);
                        $("#result").slideDown();
                        $("#do-test").hide();
                  }
                });
                });
                
                $("#repeat-exercise").on("click",function(event){
                    $(".lesson-content").show();
                    $(".stat-exercise").hide();
                    
                });   
             });
            
    </script>
         