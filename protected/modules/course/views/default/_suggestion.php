<div class="suggestion">
    <h4>คำแนะนำ </h4>
       <p id="save-state" class="alert alert-success" style="display: none;">
           <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp; ได้รับคำแนะนำแล้วครับ ขอบคุณครับ
       </p>
    
    <div id="suggesstion-block" class="form-group">
        <textarea id="suggesstion" class="form-control" rows="3" placeHolder="รู้สึกอย่างไรกับบทเรียนนี้ครับ  ..."></textarea>
    </div>
       
    <input id="content-suggestion" type="hidden" name="content_id" value="<?php echo $content_id; ?>">    
    <br/><button id="submit-button" type="button" name="btn-submit" class="btn btn-warning btn-sm">ส่งคำแนะนำ</button>
</div>

<script type="text/javascript">
  $(document).ready(function(){
      $("#save-state").click(function(){
          $("#save-state").hide();
      });
            
      $("#submit-button").on("click",function(){         
         var message = $("#suggesstion").val();
         var content_id = $("#content-suggestion").val();
      
         
         if(message==""){
             $("#suggesstion-block").addClass("has-error");
             $("#save-state").hide();
             $("#suggesstion").attr("placeHolder","ช่วยระบุด้วยครับ");
         }else{           
          
            $.ajax({
                                        url: '<?php echo Yii::app()->createUrl("course/default/saveSuggestion"); ?>',
                                        type: 'POST',
                                        data:{                                    
                                              'message':message,
                                              'content_id':content_id,                                              
                                             },
                                        dataType: 'html',

                                        success: function(result) {
                                    
                                              $("#save-state").show();  
                                              $("#suggesstion").val("");
                                              $("#suggesstion-block").removeClass("has-error");
                                          }
                                      }); 
                     }        
         
      });

      
  });
  
</script>