<!--- Add background --> 
<script>
    $(document).ready(function(){
       document.title = "<?php echo $course->name;?>"; 
    });
</script>


<?php 
$course_bg="";
  
    if(!empty($course->theme_photo)){
    $course_bg=SiteHelper::getImgBgOpacity($course->theme_photo);
    }
    
?>

<div class="banner">
    <div class="block-header">
      <h1 class="name-course"><?php echo $course->name; ?></h1>
    </div>
    <div>
      <div class="show-course" style="<?php echo $course_bg; ?>">
        <div class="w-row">
          <div class="w-col w-col-6">
            <div class="block-img text-center">
               <?php            
             if (!empty($course->course_img)) {
                $course_nav_media = "<img class='course-img' src='course/original/$course->course_img' title='$course->name'>";
            } else {
                $course_nav_media = "<img class='course-img' src='images/default_course.png'>";
            }
            
            if(!empty($course->youtube_demo)){
                $course_nav_media= VdoHelper::jwyoutube($course->youtube_demo);
            }
            
            
            if (!empty($demo_video)) {
                $video_url = Yii::app()->baseUrl . "/files/$demo_video->user_id/video/$demo_video->name";
                $video_path = Yii::app()->basePath . "/../webroot/files/$demo_video->user_id/video/$demo_video->name";

                if (file_exists($video_path)) {
                    $play = VdoHelper::jwplayer($video_url, '390px', '300px');
                    $course_nav_media=$play;
                }
            }
            ?> 
            
            <?php echo $course_nav_media; ?>
            </div>
          </div>
          <div class="w-col w-col-6">
            <div>
              <p class="head-text">
                <?php echo strip_tags($course->description); ?>   
              
              </p>
              
                               <?php
                        $state="";                     
                        $state="UnAvailable";
                        $state_success=$state_text="";
                        $btn_id="operate";
                        $state_success= $course->price." <img src='images/coins.png' height='16px;'>";

                        if($course->price==0){
                             $state_success= "Free";
                        }            

                        $onclick="";
                        $block_class="";


                        if(!empty($user_own_course)){
                              $state="Available";
                              $state_success="คุณลงเรียนไว้แล้ว กดปุ่มเพื่อเรียนต่อได้เลย"; 
                              $href=Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$course->id,"chapterId"=>1));
                              $btn_name="<span class='glyphicon glyphicon-play-circle'></span> เรียน ";
                        }else{

                        //check more is have
                           if (!empty($user_id)) {               
                                 if($course->price==0){
                                     $href = Yii::app()->createUrl("course/default/takeCourse", array("id" => $course->id));
            //                         $state_success="Buying is Available"; 
                                     $state="Available";                       
                                     $btn_name="<span class='glyphicon glyphicon-pencil'></span>  สมัครเรียน";
                                     $onclick="return confirmBuyCourse($model_coin->coin,$course->price,$course->id)";                        

                                 }else{
                                     #not enough  >>> add money     
                                      $btn_name="Buy course" ; 
                                     if(!empty($model_payment) && $model_payment->approveCode_next==2){
                                          $btn_name="กำลังดำเนินการ" ; 
                                     }                         
                                     $href = Yii::app()->createUrl("payment/FormPayment",array("course"=>$course->id));                  

                                     $btn_id="Add-money";
            //                         $block_class="fancybox.iframe";

                                 }

                               } else {
                                 $href = Yii::app()->createUrl("user/login/login"); 
                                 $btn_name="เข้าสู่ระบบ"; 
//                                 $block_class="fancybox.ajax";
                             }
                        }
           ?> 
              
              
              
              <a class="access-btn" href="<?php echo $href; ?>"><?php echo $btn_name; ?></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="course-all">
    <div>
      <div class="w-row">
        <div class="w-col w-col-8">
          <div>
              
              
             <?php if (!empty($course_contents_parent)) { 
                 $count = 1; 
                      $count_content=0;
                      $sum_video_lengte = 0;
                      $count_unit= count($course_contents_parent);
                      
                    foreach ($course_contents_parent as $main_course) {
                    if($count_unit >1){
                        echo "<h3 class='text-left'>$main_course->title </h3>";
                    }
                    
                    $children_courses = Content::model()->findAll("parent_id='$main_course->id' ORDER BY show_order ASC");
                    if (!empty($children_courses)) {
                    foreach ($children_courses as $sub_course) {    
                        
             ?>    
            <div class="block-course">
              <div class="w-row">
                <div class="w-col w-col-2">
                  <div class="icon-vdo">
                    <img src="images/vdo.png" alt="53fd675cc4c606b2038a76ae_vdo.png" >
                  </div>
                </div>
                <div class="w-col w-col-10">
                  <h1 class="head-course"><?php echo $sub_course->title; ?></h1>
                  <p class="text-left">
                    <?php
                     echo strip_tags($sub_course->detail);
                     if (!empty($sub_course->videoFile->duration)) {
                            $time_video = SiteHelper::convertTimeMinute($sub_course->videoFile->duration);
                            $sum_video_lengte +=$time_video;
                            echo "<br/>".number_format($time_video) . " นาที</span>";
                     }
                    ?>
                  
                  </p>
                </div>
              </div>
            </div>
              
           <?php 
                    $count_content++;
                       }
                   }
                 $count++;
               }
           }
           ?>
              
              
          </div>
        </div>
        <div class="w-col w-col-4">
          <div class="blvck-pay">
            <div class="pay-course">
              <h1 class="name-course-pay"><?php echo $course->name; ?></h1>
              
              
              
              
              
              <h1 class="price-course">
                  <?php
                    if($course->price == 0){
                        echo "Free";
                    }else{
                        echo $course->price."บาท";
                    }
                  ?>
              
              
              </h1>
              
              
              
              <div class="explain-vdo">จำนวนบทเรียน <?php echo $count_content; ?> บทเรียน
                  <br>ความยาววีดีโอ <?php echo number_format(floor($sum_video_lengte)); ?> นาที
                
              </div>
              

              <?php if($state=="UnAvailable"){  #not login OR not enough money ?> 
                <a class="<?php echo $block_class; ?> unAvailable course-apply-button button-pay" href="<?php echo $href;?>" onclick="<?php echo $onclick; ?>" ><?php echo $btn_name; ?></a>
              
              
              <?php }else{ ?>
                 <a class="<?php echo $block_class; ?> unAvailable course-apply-button button-pay" href="<?php echo $href;?>" onclick="<?php echo $onclick; ?>" ><?php echo $btn_name; ?></a>
               <?php } ?>         
              
              
            </div>
          </div>
          <div class="blvck-pay">
            <div class="teacher-block">
                 <?php 
                    if (!empty($teachers))
                    $this->renderPartial("_teacher_box",array("teachers"=>$teachers)); 
                   ?>
                
              
            </div>
          </div>
        </div>
      </div>
       <br/><br/>
    </div>
  </div>



    <script>
        $(document).ready(function() {
	$(".demo-content").fancybox({
		maxWidth	: "80%",
		maxHeight	: '80%',
		fitToView	: true,
		width		: '60%',
		height		: '60%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});
    </script>


    <script type="text/javascript">  
               $(document).ready(function() {
                 $(".unAvailable").fancybox({
                         fitToView	: true,
                         width	: '80%',
                         height	: 'auto',
                         autoSize	: false,
                         closeClick	: false,
                         openEffect	: 'none',
                         closeEffect	: 'none',

                 });
               });  
    </script>  

 <script type="text/javascript">
                                function confirmBuyCourse(coin,price,course_id){
                                   var msg = confirm("Do you want to buy ?");
                                               if(msg == false) {
                                                  return false;
                                              }else{
                                                  $.ajax({                      
                                                      url: "<?php echo Yii::app()->createUrl("course/default/takeCourse"); ?>",                         
                                                      dataType: 'html',
                                                      type: 'POST',
                                                       data:{                                                 
                                                          'course_id' : course_id
                                                      },
                                                      cache: false,
                                                      success:function(html){                               
                                                            location.reload();
                                                      }          

                                                  });
                                              }
                              }
</script>
  
