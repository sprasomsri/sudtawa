<?php if (!empty($course_contents_parent)) { ?>
   
    <!-- chapter 1-->
    <?php $count = 1; ?>
    <?php foreach ($course_contents_parent as $main_course) { ?>

        <div class="w-row lesson-item">
            <div class="w-col w-col-1 lesson-number-icon">
                <img src="images/icon.png" width="33" alt="53182bf95f4401906700029d_icon.png">
                <div class="lesson-number"><?php echo $count; ?></div>
            </div>


            <div class="w-col w-col-11 lesson-content">
                <div class="lesson-name"><?php echo $main_course->title;?></div>

                <?php
                $children_courses = Content::model()->findAll("parent_id='$main_course->id' ORDER BY show_order ASC");
                if (!empty($children_courses)) {
                    ?>
             
              <ul class="w-list-unstyled lesson-list chapter-list">
            <?php foreach ($children_courses as $sub_course) { ?>

                  <li class="lesson-order">
                    <?php echo $sub_course->title; ?>   
                    
                    
                    <?php if(!empty($sub_course->videoFile->duration)){ ?>   
                    <span clas="content-time"style="display: inline-block; width: auto; text-align: right;  float: right;">    
                    
                     <?php 
                     echo number_format(SiteHelper::convertTimeMinute($sub_course->videoFile->duration))." นาที</span>";
                     ?>
                    </span>
                    <?php } ?>
                     
                      
                      
                  </li>

                    <?php } ?>
              </ul>
        <?php } ?>



            </div>
        </div>            

        <?php $count++;
    }
    ?>


    <!-- เนื้อหาของคอร์สนี้-->    
<?php } ?>

