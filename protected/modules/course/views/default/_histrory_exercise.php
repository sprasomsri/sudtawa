        <?php
        //create string
        $html="";
        $qty_correct=0;
        $question_count=1;
        if(!empty($list_questions)){
        	$score_max=count($list_questions);
            foreach ($list_questions as $question){
                $model_question= ExerciseQuestion::model()->findByPk($question);
                $model_question->question=  strip_tags($model_question->question);

                              
                    $answer_html="";
                    if($model_question->type==1 || $model_question->type=="3"){ //1 multi choice  3 two choice
                        $model_user_selected=  ExerciseChoiceScore::model()->find("do_time='$do_time' AND user_id='$user_id' AND question_id='$model_question->id'");
                        $answers=$model_question->answer;
                        if(!empty($answers)){
                           
                            foreach ($answers as $answer){
                                // $html.="<li>";
                                if($answer->id==$model_user_selected->answer_id){
                                	$selected_answer=$answer->answer;
                                  
                                }
                                
                                //true answer
                                if($answer->is_true==1){
                                    $true_answer=$answer->answer;
                                }
                                
                                
                                if($answer->id==$model_user_selected->answer_id){
                                    if(!empty($answer->hint)){
                                         $htnt_word="Hint";
                                         $hint=$answer->hint;
                                    }else{
                                         $htnt_word="Answer";
                                        $hint=$answer->answer;
                                    }
                                }                                   
                    
                            }                            
                            
                            $hint="
                                       <p class = 'hint' id='hint-p-$model_question->id' onclick='showHint($model_question->id)'> Hint</p> 
                                       <div class='question-result-text hint-answer' id='hint-div-$model_question->id' style='display:none'>
                                       $htnt_word: "
                                           . $hint."
                                       </div>
                                      ";
                            

                               if ($model_user_selected->is_true == "1") {
                                  
                                  $class_status="correct";
                                  $qty_correct++;
//                               

                              } else {
                                   $class_status="incorrect";
                                    
                              }
                                         $answer_html = " <div class='form-group $class_status'>
                                             <label>ข้อ " . $question_count . "</label>
						  <br/>" . $model_question->question . " <br/><br/>
                                                    <table class='answer'>
                                                           <tr>
                                                             <td class='label-ans'> Answer </td>   
                                                              <td class='answer-ans'>" .$selected_answer . "</td>
                                                              <td class='img-$class_status'> </td>
                                                            </tr>
                                                     </table>    
                                                    $hint 
                                                </div> ";
                                 $html.=$answer_html."<hr/>";
                                               
                                               
             
            }
                        
                     }
                     if($model_question->type==2){
                     
                       // type text
                            $model_user_selected_text=  ExerciseTextScore::model()->findAll("do_time='$do_time' AND user_id='$user_id' AND question_id='$model_question->id'");
                            $is_correct_all=true;
                            $list_answer_text="";
                            $list_hint="";
                            $hint="";
                            
                            
                            if(!empty($model_user_selected_text)){                            	

                                foreach($model_user_selected_text as $answer_text){
                                    
                                      if($answer_text->is_true==1){
                                      	  $is_correct_all=$is_correct_all && 1;
                                      	  $sub_correct_class="correct";                                      
                                       
                                      }else{
                                      	  $is_correct_all=$is_correct_all && 0;
                                      	  $sub_correct_class="incorrect";
                                         
                                      }  
                                        //list that use answered
	                                $list_answer_text.=$answer_text->answer."<br/>";
                                                 
                                }                     
                                                                                
                                 $answer_list=  ExerciseTextAnswer::model()->findAll("question_id=$model_question->id");     
                             
                                 if(!empty($answer_list)){
                                   
                                     $list_hint="";
                                     foreach($answer_list as $ans){
                                         if(!empty($ans->hint)){
                                            $list_hint.= "<li>".$ans->hint."</li>";
                                         }
                                         
                                     }
                                     
                                     if(!empty($list_hint)){
                                         //$list_hint.="<ul>".$list_choice."</ul>";
                                     }
                                     
                               
                                     if(!empty($list_hint)){
                                       $hint="
                                            <p class = 'hint' id='hint-p-$model_question->id' onclick='showHint($model_question->id)'> Hint</p> 
                                            <div class='question-result-text hint-answer' id='hint-div-$model_question->id' style='display:none'>
                                            Hint: <ul>"
                                                . $list_hint."</ul>
                                            </div>
                                      ";   
                                     }
                                     
                                 }                
                                

                                 if($is_correct_all==1){
                                    	$class_status="correct";                                        

                                    }else{
                                    	$class_status="incorrect";
                                        $true_answer="";                                       
                                    }
	   				               $answer_html=" <div class='form-group $class_status'>
						               			 <label>ข้อ ".$question_count."</label>
						              			
						                   			<br/>".$model_question->question."<br/><br/>
                                                                                          <table class='answer'>
                                                                                                <tr>
                                                                                                <td class='label-ans'> Answer </td>   
                                                                                                <td class='answer-ans'>".$list_answer_text."</td>
                                                                                                <td class='img-$class_status'> </td>
                                                                                                </tr>
                                                                                           </table>    
                                                                                  $hint        
                                                                                 
                                                                                 
						                   	    </div>


						                   	    ";

	   				                $html.=$answer_html."<hr/>";

                            }
                   }
                   
                   
                   if ($model_question->type == 4) { //code
                  
                    $model_user_code = ExerciseCodeScore::model()->find("do_time='$do_time' AND user_id='$user_id' AND question_id='$model_question->id'");
                    
                    if (!empty($model_user_code)) {
                        //code user answer save in db
                        $code = "<div id='codeEditor-$model_user_code->id' class='editor-box' style='width:80%; margin:0 auto;'>$model_user_code->code</div>";
                        $user_answer=$model_user_code->answer;
                        ?>
                          <script type="text/javascript"> 
                              $(document).ready(function(){
                              var editor = ace.edit("codeEditor-<?php echo $model_user_code->id; ?>");
                              editor.setTheme("ace/theme/crimson_editor");
                              editor.getSession().setMode("ace/mode/java");                   
                              })                     
                          </script> 
                        <?php
                        if ($model_user_code->is_true == 1) {
                            $class_status = "correct";
                            $qty_correct++;
                            $hint = "";
                        } else {
                            $model_true_answer=$model_question->answerCode;                            
                            $code_answer = "<div id='codeEditor-hint-$model_true_answer->id' class='editor-box'>$model_true_answer->code</div>";
                            $code_answer.="<b>Answer:</b>".$model_true_answer->answer; ?>
                          
                            <script type="text/javascript"> 
                              $(document).ready(function(){
                              var editor = ace.edit("codeEditor-hint-<?php echo $model_true_answer->id; ?>");
                              editor.setTheme("ace/theme/crimson_editor");
                              editor.getSession().setMode("ace/mode/java");                   
                              })                     
                          </script>
                            
                          
                          
                            <?php 
                            $class_status = "incorrect";
                            $hint = "<p class = 'hint' id='hint-p-$model_question->id' onclick='showHint($model_question->id)'> Hint</p> 
                                           <div class='question-result-text hint-answer' id='hint-div-$model_question->id' style='display:none'>
                                          Code: "
                                    . $code_answer . "
                                           </div>
                                          ";
                            
                            
                        }
                        
                        
                         $answer_html="
					            <div class='quiz-result-row $class_status code-exercise'>
					                <label>ข้อ ".$question_count."</label>
					                <div class='question-title span10'>
					                    <div class='question-result-text question-part'><i class='icon-question'></i>".$model_question->question."</div>
					                    <div class='question-result-text'><i class='icon-answer'></i>Your Code:".$code."</div>
                                                            <br/> Your Compile Result/(Answer): $user_answer
                                                            ".$hint."
                                                        </div>
					            </div>
   				               ";
   				               $html.=$answer_html."<hr/>";
                        
                        
                    }//empty $model_user_code
                }
                   
                    
                $question_count++;
                    
            }
        }
        ?>



           
<h5>ผลการทดสอบ <?php echo SiteHelper::dateFormat($do_time);?> </h5>
<hr class="seperate-content"/>
<div class="question-score text-right">
        <b> คะแนน: </b>
      <span class="score-earn"><?php echo $qty_correct; ?></span>
       /
      <span class="score-full"><?php echo $score_max; ?></span>
</div><br/><br/>
 
<div class="quiz-result">
     <?php echo $html; ?>           
</div>
<div class="text-center">
    <a class="btn btn-default" href="<?php echo Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$model_content->course_id,"lesson_id"=>$model_content->id));?>"> กลับหน้าแบบฝึกหัด </a> 
</div>


       


