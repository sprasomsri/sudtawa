<!--<script type="text/javascript">
    var current_lesson=<?php //echo $content->id;?>
    $(".link-type-quiz").removeClass("active");
    $(".icon-quiz").removeClass("active");
    $("#lesson_"+current_lesson).addClass('active');
    
</script>-->

<div class="lesson-content-wrapper lesson-type-video">
    <div class="lesson-content-title">
        <span class="icon-green-video"></span>
        <?php 
            echo $content->title; 
            CourseHelper::saveContentLog($content->id,$content->course_id);
         ?>
        
    </div>
    <div class="lesson-content">
        <div class="video-wrapper">

            
            <?php
            $videoFile = $content->videoFile;
            if (!empty($videoFile)) {
                
                $video_url = Yii::app()->baseUrl . "/files/$videoFile->user_id/video/$videoFile->name";
                $video_path = Yii::app()->basePath . "/../files/$videoFile->user_id/video/$videoFile->name";
                 if (file_exists($video_path)) {
                    $play = VdoHelper::jwplayer($video_url, '817px', '433px');
                    echo $play;
                  
                }
            }
            echo "<br /> <br />";
           
            if(!empty($content->codeBox)){            
                if($content->codeBox->position=="above")     
                $this->widget('CodeEditor',array("model_code"=>$content->codeBox));               
            }
                        
             if(!empty($content->detail)){
                echo $content->detail;
             }              
            
            if(!empty($content->codeBox)){          
                if($content->codeBox->position=="below")     
                $this->widget('CodeEditor',array("model_code"=>$content->codeBox));               
            }                
                    
            
            
            
            ?>
            
            
        </div>
    </div><!-- lesson-content -->
</div> <!-- lesson-content-wrapper -->