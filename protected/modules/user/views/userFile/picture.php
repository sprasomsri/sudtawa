<div class="bg-pagestudy">
    <div class="w-container">
      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">
              
                <?php $this->renderPartial("_manage_file_menu")?>
              
        
          </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">Picture</h1>
              <div class="text-right"><a class="btn btn-warning" href="<?php echo Yii::app()->createUrl("user/userFile/UploadPicture");?>"> Upload</a></div>
              <div class="w-row admin-row-user">
                
                    <?php
                    $this->widget('zii.widgets.CListView', array(
                      'id' => 'courselist',
                      'dataProvider' => $pictures,
                      'itemView' => '_picture_list',
                      'template' => " {summary}\n{pager} \n {items}",
                      //'viewData' => array('lang' => $lang),
                      'pager' => array(
                          'class' => 'CLinkPager',
                      ),
                  ));
                                                        
                   ?>
            
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript">
  $(document).ready(function() {
//                                                                  jQuery.noConflict(); 
   $(".fancybox-thumb").fancybox({
    prevEffect	: 'none',
    nextEffect	: 'none',
   helpers	: {
        title	: {
         type: 'outside'
        },
        thumbs	: {
        width	: 50,
        height	: 50
        }
        }
   });
});
    function confirmDelete() {
        var msg = confirm('Are you sure?');
        if (msg == false) {
            return false;
        }
    }

</script>    