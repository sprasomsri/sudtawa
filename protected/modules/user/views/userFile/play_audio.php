<?php 
 $audio_mp3_url = Yii::app()->baseUrl . "/files/$model_file->user_id/audio/$model_file->name";
 list($name,$type)=  explode(".", $model_file->name);
 $ogg_file=$name.".ogg";
 $audio_ogg_url = Yii::app()->baseUrl . "/files/$model_file->user_id/audio/$ogg_file";
 ?>
 <hr/>
<div style="width:auto; text-align: center; margin: 0 auto;"class="center-box">
    <audio controls>
        <source src="<?php echo $audio_mp3_url; ?>" type='audio/mpeg'> <!--- .mp3 for  for Internet Explorer, Chrome, and Safari -->
        <source src="<?php echo $audio_ogg_url; ?>" type='audio/ogg'> <!--- .ogg for   Firefox and Opera) -->
        <embed width='50%' src='$audio_mp3_url'>
    </audio>
    
    <hr />
    <?php 
    echo "File name: ".$model_file->original_name."<br/>";
    echo "UploadDate: ".SiteHelper::dateFormat($model_file->create_at);
    ?>
    
</div>


