<div class="bg-pagestudy">
    <div class="w-container">
      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">
              
                <?php $this->renderPartial("_manage_file_menu")?>
              
        
          </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">Audio</h1>
              <div class="text-right"><a class="btn btn-warning" href="<?php echo Yii::app()->createUrl("user/userFile/uploadAudio");?>"> Upload</a></div>
              <div class="w-row admin-row-user">
                
                       <?php
                                                        $this->widget('zii.widgets.CListView', array(
                                                            'id' => 'courselist',
                                                            'dataProvider' => $audios,
                                                            'itemView' => '_audio_list',
                                                            'template' => " {summary}\n{pager} \n {items}",
                                                            //'viewData' => array('lang' => $lang),
                                                            'pager' => array(
                                                                'class' => 'CLinkPager',
                                                            ),
                                                        ));
                        ?>
            
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

 <script type="text/javascript">
                                                        $(document).ready(function() {
//                                                            jQuery.noConflict();
                                                            $(".various").fancybox({
                                                                maxWidth: 1000,
                                                                maxHeight: 1000,
                                                                fitToView: false,
                                                                width: '50%',
                                                                height: '40%',
                                                                autoSize: false,
                                                                closeClick: false,
                                                                openEffect: 'none',
                                                                closeEffect: 'none'
                                                            });
                                                        });

                                                        function confirmDelete() {
                                                            var msg = confirm('Are you sure?');
                                                            if (msg == false) {
                                                                return false;
                                                            }
                                                        }
 </script>










