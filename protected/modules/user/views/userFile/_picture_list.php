<?php 
     $title=  "FileName:".$data->original_name."<br/> UploadDate: ".SiteHelper::dateFormat($data->create_at);
    if ($index % 3 == 0 || $index == 0) { ?>
    <?php $state = "new_div"; ?>
  
    <div class="w-row admin-row-user">
        <?php
        } else {
            $state = "not_new_div";
        }
        ?>
        
        <div class="w-col w-col-4">
            <div class="admin-student-box">
                <div>
                    <img class="admin-file-photo" src="<?php echo "files/$data->user_id/pictures/$data->name"; ?>">
                </div>
                <h5 class="admin-file-head"><?php echo $data->original_name; ?></h5>
                <p class="admin-file-date"><strong>UploadDate</strong>: <?php echo SiteHelper::dateFormat($data->create_at); ?></p>
                <div class="admin-info-btn">
                    <div class="w-row">
                        <div class="w-col w-col-5 text-left"></div>
                        <div class="w-col w-col-7 admin-teacher-btn-group">
                            <a class="button admin-info-btn admin-margin-btn fancybox-thumb" href="<?php echo "files/$data->user_id/pictures/$data->name"; ?>">VIEW</a>
                            <a class="button admin-info-btn admin-active-btn" onclick="return confirmDelete();" href="<?php echo Yii::app()->createUrl("user/userFile/deletePicture", array("id" => $data->id)); ?>">DELETE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php if ($state == "not_new_div" && (($index + 1) % 3 == 0)) { ?>

        </div>
<?php } ?>




