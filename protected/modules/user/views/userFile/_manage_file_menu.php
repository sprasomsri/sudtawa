<div class="menu-admin">
    <h4 class="course-name">ส่วนจัดการไฟล์&nbsp;</h4>
    <div class="text-center">
        <div class="list-menu">
            <div class="admin-submenu">
                <div><a href="<?php echo Yii::app()->createUrl("user/userFile/picture");?>" title="Manage Picture">Picture </a></div>
            </div>
            <div class="admin-submenu">
                <div><a href="<?php echo Yii::app()->createUrl("user/userFile/video");?>" title="Manage Video">Video</a></div>
            </div>
            <div class="admin-submenu">
                <div><a href="<?php echo Yii::app()->createUrl("user/userFile/audio");?>" title="Manage Au"> Audio </a></div>
            </div>
        </div>
    </div>
</div>