<div class="bg-pagestudy">
    <div class="w-container">
      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">
              
                <?php $this->renderPartial("_manage_file_menu")?>
              
        
          </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">Upload Audio</h1>
              
              <div class="w-row admin-row-user">
                
                    <div class="w-col w-col-12">
                     
                                               
                                                    <div class="form">
                                                            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
                                                            <script src="plugin/uploadify/uploadify_lib/jquery.uploadify.min.js" type="text/javascript"></script>
                                                            <link rel="stylesheet" type="text/css" href="plugin/uploadify/uploadify_lib/uploadify.css">

                                                            <script type="text/javascript">
                                                                $(function() {
                                                                    $('#file_upload').uploadify({
                                                                        'swf': 'plugin/uploadify/uploadify_lib/uploadify.swf',
                                                                        //                            'uploader' : '/plugin/uploadify/uploadify_lib/uploadify.php',
                                                                        'uploader': "<?php echo Yii::app()->createUrl("user/userFile/saveAudio",array("user_id"=>$user_id))?>",
                                                                        //options
                                                                        'auto': false,
                                                                        'multi': true,
                                                                        'buttonText': 'เพิ่มFile',
                                                                        'method': 'POST',
                                                                        //                            'fileSizeLimit' : '15MB',
                                                                        //                            'fileTypeDesc' : 'Image File', // file of type in browse.. Defualt  'All Files'
                                                                        'fileTypeExts': '*.mp3;', // defualt '*.*'
                                                                        'queueID': 'some_file_queue',
                                                                        'fileObjName': 'Filedata', // default Filedata
                                                                        //event
                                                                        
                                                                        
//                                                                         'onUploadStart' : function(file) {
//                                                                            queueSize = $(".uploadify-queue-item").size();
//                                                                            alert('queueSize' + queueSize);
//                                                                        },
                                                                        
                                                                        'onUploadSuccess': function(file, data, response) {
                                                                            
                                                                          
                                                                            
                                                                            //                                alert(file.name + ' was successfully uploaded with a response of ' + response + ':' + data);
                                                                            var file_name = $("#file_name_upload").val()

                                                                            if (file_name == "") {
                                                                                $("#file_name_upload").attr("value", data);
                                                                            } else {
                                                                                file_name = file_name + "," + data;
                                                                                $("#file_name_upload").attr("value", file_name);

                                                                            }
                                                                        }
                                                                    });
                                                                });

                                                            </script>
                                                            <style type="text/css">
                                                                .chkBoxlist {
                                                                    font: 13px Arial, Helvetica, Sans-serif;
                                                                }
                                                                #some_file_queue {
                                                                    background-color: #FFF;
                                                                    border-radius: 3px;
                                                                    box-shadow: 0 1px 3px rgba(0,0,0,0.25);
                                                                    height: 250px;
                                                                    margin-bottom: 10px;
                                                                    /*overflow: auto;*/
                                                                    padding: 5px 10px;
                                                                    width: 70%px !important;


                                                                }
                                                                .uploadify-queue-item{
                                                                    margin :0 auto;
                                                                }
                                                            </style>

                                                           
                                                            <div id="div_upload" style="margin-left:35px;">

                                                                <input id="file_name_upload" name="file_name_upload" type="hidden"/>
                                                                <div id="queue"></div>
                                                                <!-- <input id="file_upload" name="file_upload" type="file" multiple="true"> -->
                                                                <div class="row full cen">
                                                                    <input type="file" name="file_upload" id="file_upload" />
                                                                </div>

                                                                <div id="some_file_queue" style="width:100%" class="row full cen"></div>
                                                                <!--		<div id="total_file">จำนวนไฟล์ที่เลือกทั้งหมด 0 ไฟล์</div>-->
                                                                <div class="row full cen">
                                                                    <a class="btn btn-inverse btn-bg btn-padding" href="javascript:$('#file_upload').uploadify('upload', '*')">เริ่มอัพโหลดไฟล์</a>
                                                                    <!--		<a href="javascript:$('#file_upload').uploadify('cancel')">ยกเลิกไฟล์แรก</a> |-->
                                                                    <a class="btn btn-inverse btn-bg btn-padding" href="javascript:$('#file_upload').uploadify('cancel', '*')">ยกเลิกไฟล์ทั้งหมด</a>
                                                                    <!--		<a href="javascript:$('#file_upload').uploadify('disable', true)">Disable the Button</a> |
                                                                                    <a href="javascript:$('#file_upload').uploadify('disable', false)">Enable the Button</a>-->
                                                                </div>


                                                            </div>



                                                        </div><!-- form --> 
                    
                    </div>
            
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

