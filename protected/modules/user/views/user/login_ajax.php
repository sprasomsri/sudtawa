<script type="text/javascript">
    $(document).ready(function() {
       document.title = "Login - CourseCreek"; 

    });
</script>
<?php $user_id = Yii::app()->user->id; ?>
<?php if (empty($user_id) || $user_id == "0") { ?>


<div id="content-access" class="section hero section-course">
        <div class="w-container login-tab-box">
        <div class="w-form login-signup-form full-access">
        <?php $member_regis_link = Yii::app()->createUrl('/user/registration/registration') ?>   
        <div class="signup-login-toggle">
            <a id="regis_ajax" class="signup-tab inactive" href="#"><?php echo Yii::t("site", "Register"); ?></a><a id="signIn-tab" class="login-tab active" href="#"><?php echo Yii::t("site", "Login"); ?></a>
        </div> 
            
            <div id="login-block" class="login-signup-panel login-tab-box" style="">
                
              <?php 
                if(!empty($course_id)){
                $this->widget('FaceBookLogin',array("word"=>"เข้าสู่ระบบ ด้วย Facebook","course_id"=>$course_id)); 
                }else{
                    $this->widget('FaceBookLogin',array("word"=>"เข้าสู่ระบบ ด้วย Facebook")); 
                }
              ?>  
                
                
             <br/>                
                     <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'login_ajax',
                            'action' => Yii::app()->createUrl('user/login/validateLoginAjax'),
                            'enableClientValidation' => true,
                            'enableAjaxValidation' => true,
//                            'htmlOptions' => array("role" => "form"),
                            'clientOptions' => array(
                                'validateOnSubmit' => false,
                            ),
                        ));
                        ?> 
               
              <p class="text-center"> หรือเข้าสู่ระบบด้วยอีเมลล์ </p>   
             
              
              
              <div class="errorSummary" style="display: none">
                  Username หรือ Password ไม่ถูกต้อง
              </div>
              
              <div class="form-group">
               <label for="exampleInputPassword1">อีเมล*</label>            
               <input placeholder="อีเมล" id="username" class="form-control" name="username" type="text">
               <?php //echo $form->error($model, 'username'); ?>
              </div> 
              
              
               <div class="form-group">
                <label for="exampleInputPassword1">รหัสผ่าน*</label>
          
                <input type="password" name="รหัสผ่าน" class="form-control" id="password" placeholder="Password">
                <input type="hidden" name="is_box" value="<?php echo $inbox; ?>">
             </div>
              
              
              <?php if(!empty($course_id)){ ?>
                <input id='course-id' type="hidden" name='course_id' valuse='<?php echo $course_id; ?>'>   
                <input type="hidden" id="redirect_url" name="redirect_url" value="<?php echo Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$course_id));?>">
              <?php } ?>
              
              <div class="checkbox">
                <label>
                   <input id="remember" type="checkbox" name="rememberMe" class="" checked value="1"> <?php echo Yii::t("site","Remember me"); ?>
                </label>
                  
              </div>
              
               <button type="button" class="btn btn-primary" id="accept-form"> Login </button>      
                              
               <?php $this->endWidget(); ?>
                  
            </div>
            
        </div>
     </div>        
        
</div>





 <script type="text/javascript">
               jQuery.noConflict();
               $(document).ready(function(){
                  $("#accept-form").click(function(){                   
                   var username = $("#username").val();
                   var password = $("#password").val();   
                   var remember = $("#remember").val();
                   var course_id= $("#course-id").val();
                   var redirect_url = $("#redirect_url").val();               
               
                    $.ajax({
                        url:"<?php echo Yii::app()->createUrl("user/login/ValidateLoginAjax");?>",
                        type : "POST",
                        data: {username: username, password: password, remember: remember},
                        dataType: 'html',
                        beforeSend:function(){
                            $(".errorSummary").hide();       
                        },
                        success:function(result){                    
                            if(result == "success"){
                                $.fancybox.close();
                                if(redirect_url==""){
                                    location.reload();
                                }else{
                                     window.location.replace(redirect_url);
                                }
                                
//                               
                                
                                
                                $(".errorSummary").html(" ");  
                            }else{
                                $(".errorSummary").show();                                
                            }
                        }});
              });
              
              $("#regis_ajax").click(function(){
              $.ajax({
                        url:"<?php echo Yii::app()->createUrl("user/registration/registrationAjax");?>",                       
                        dataType: 'html',                       
                        success:function(result){
                            $("#content-access").html(result);
                           
                        }});           
              });
            
            
            });
               
               
              
              
              
</script>
<?php } ?>




