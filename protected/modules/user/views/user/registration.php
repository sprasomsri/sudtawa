<script type="text/javascript">
    $(document).ready(function() {
        document.title = "Register "; 

    });
</script>

<?php $user_id = Yii::app()->user->id; ?>
<?php if (empty($user_id) || $user_id == "0") { ?>
<div class="gray-row">
    <div class="container-full">
        <div class="form-box">
        <div class="w-container login-tab-box">
        <div class="w-form login-signup-form full-access">
   
            
            <div id="login-block" class="login-signup-panel login-tab-box" style="">
             <h1 class="head-form">  </h1>
                
        
                 <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'Registration',
                            'action' => Yii::app()->createUrl('/user/registration/registration'),
                            'enableClientValidation' => true,
                            'enableAjaxValidation' => true,
                            'htmlOptions' => array("role" => "form"),
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        ?> 
                
                
                 <?php echo $form->errorSummary(array($model)); ?>
                
              <div class="form-group">
               <label for="exampleInputPassword1">username*</label>
                <?php echo $form->textField($model, 'username',array("placeholder"=>"username","class"=>"form-control")); ?>
               <?php //echo $form->error($model, 'username'); ?>
              </div> 
             
              <div class="form-group">
               <label for="exampleInputPassword1">Email*</label>
                     <?php echo $form->textField($model, 'email',array("placeholder"=>"อีเมล","id"=>"exampleInputEmail1","class"=>"form-control")); ?>
               <?php //echo $form->error($model, 'username'); ?>
              </div> 
              
              
             <div class="form-group">
                <label for="exampleInputPassword1">รหัสผ่าน*</label>
                <?php echo $form->PasswordField($model, 'password',array("placeholder"=>"รหัสผ่าน","id"=>"inputPassword3","class"=>"form-control")); ?>
              </div>
              
              <div class="form-group">
                <label for="exampleInputPassword1">ยืนยันรหัสผ่าน*</label>
                <?php echo $form->PasswordField($model, 'verifyPassword',array("placeholder"=>"ยืนยันรหัสผ่าน","id"=>"inputPassword3","class"=>"form-control")); ?>
              </div>
              
              <div class="checkbox">
                <label>
                   <input name="rememberMe" type="checkbox" class="" checked value="1"> <?php echo Yii::t("site","Remember me"); ?>
                </label>
                  
              </div>
              
         
               <button type="submit" class="btn btn-primary"> สมัครสมาชิก </button> 
               
             
                
                 <?php $this->endWidget(); ?>
                  </div>
            
        </div>
     </div>        
        
</div>

<?php } ?>
</div>
</div>    
    
