<script type="text/javascript">
    $(document).ready(function() {
       document.title = "Login - CourseCreek"; 

    });
</script>

<?php $user_id = Yii::app()->user->id; ?>
<?php if (empty($user_id) || $user_id == "0") { ?>
<div class="gray-row">
    <div class="container-full">
        <div class="form-box">
        <div class="w-container login-tab-box">
        <div class="w-form login-signup-form full-access">
   
            
            <div id="login-block" class="login-signup-panel login-tab-box" style="">
             <h1 class="head-form"> Login </h1>
                
                 <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'login',
                            'action' => Yii::app()->createUrl('/user/login/login'),
                            'enableClientValidation' => true,
                            'enableAjaxValidation' => true,
                            'htmlOptions' => array("role" => "form"),
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        ?> 
                
                
                 <?php echo $form->errorSummary(array($model)); ?>
                
              <div class="form-group">
               <label for="exampleInputPassword1">Student Id:*</label>
               <?php echo $form->textField($model, 'username',array("placeholder"=>"อีเมล","id"=>"exampleInputEmail1","class"=>"form-control")); ?>
               <?php //echo $form->error($model, 'username'); ?>
              </div> 
              
              
             <div class="form-group">
                <label for="exampleInputPassword1">Password*</label>
                <?php echo $form->PasswordField($model, 'password',array("placeholder"=>"รหัสผ่าน","id"=>"inputPassword3","class"=>"form-control")); ?>
                <!--<input type="password" name="UserLogin[password]" class="form-control" id="exampleInputPassword1" placeholder="Password">-->
                <input type="hidden" name="is_box" value="<?php echo $inbox; ?>">
             </div>
              
              
              <div class="checkbox">
                <label>
                   <input name="rememberMe" type="checkbox" class="" checked value="1"> <?php echo Yii::t("site","Remember me"); ?>
                </label>
                  
              </div>
              
               <button type="submit" class="btn btn-primary"> เข้าาสู่ระบบ </button>
               
                <?php $reset_link = Yii::app()->createUrl("/user/recovery"); ?>
                <a href="<?php echo $reset_link ?>"><?php echo Yii::t("site","Reset Password"); ?> [Reset Password] </a>
                
                 <?php $this->endWidget(); ?>
                  </div>
            
        </div>
     </div>        
        
</div>

<?php } ?>
</div>
</div>    
    


