<?php $user_id = Yii::app()->user->id; ?>
<?php if (empty($user_id) || $user_id == "0") { ?>


<div id="content-access" class="section hero section-course">
        <div class="w-container login-tab-box">
        <div class="w-form login-signup-form full-access">
       <?php $member_regis_link = Yii::app()->createUrl('/user/login/login') ?>  
        <div class="signup-login-toggle">
            <a class="signup-tab active" href="#"><?php echo Yii::t("site", "Register"); ?></a><a class="login-tab inactive" href="#" id="login_ajax"><?php echo Yii::t("site", "Login"); ?></a>
        </div> 
            
            <div id="login-block" class="login-signup-panel login-tab-box" style="">
               <?php $this->widget('FaceBookLogin',array("word"=>"สมัครสมาชิกด้วย Facebook")); ?> <br/>
                
               <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'Registration',
                            'action' => Yii::app()->createUrl('/user/registration/submitRegistrationAjax'),
                            'enableClientValidation' => true,
                            'enableAjaxValidation' => true,
                            'htmlOptions' => array("role" => "form"),
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        ?> 
               
              <p class="text-center"> หรือสมัครสมาชิกด้วยอีเมลล์ </p>               
              
              <?php echo $form->errorSummary(array($model)); ?>
              
              
              <div class="form-group">
               <label for="exampleInputPassword1"> อีเมล</label>                         
               <input placeholder="อีเมล" id="email" class="form-control" name="email" type="text">
            
              </div> 
              
              
               <div class="form-group">
                <label for="exampleInputPassword1">รหัสผ่าน*</label>
                <input type="password" name="password" class="form-control" id="password" placeholder="รหัสผ่าน">
              </div>
              
              <div class="form-group">
                <label for="exampleInputPassword1">ยืนยันรหัสผ่าน*</label>
                 <input type="password" name="verifyPassword" class="form-control" id="passwordconfirm" placeholder="ยืนยันรหัสผ่าน">             
              </div>
              
                            
               <button type="submit" class="btn btn-primary"> Register </button> 
                              
               <?php $this->endWidget(); ?>
                  
            </div>
            
        </div>
     </div>              
</div>



<?php }else{ echo "you have already login"; } ?>

 <script type="text/javascript">
               jQuery.noConflict();
               $(document).ready(function(){
                    $("#login_ajax").click(function(){                                                 
                          
                                     
                         $.ajax({
                        url:"<?php echo Yii::app()->createUrl("user/login/loginAjax");?>",                       
                        dataType: 'html',                       
                        success:function(result){
                            
                            $("#content-access").html(result);
                           
                        }});           
             
                    });
                    
                    
               $("#accept-form").click(function(){                   
                   var username = $("#email").val();
                   var password = $("#password").val(); 
                   var password = $("#passwordconfirm").val();
                                   
                
                    $.ajax({
                        url:"<?php echo Yii::app()->createUrl("user/login/ValidateLoginAjax");?>",
                        type : "POST",
                        data: {username: username, password: password},
                        dataType: 'html',
                        beforeSend:function(){
                            $(".errorSummary").hide();       
                        },
                        success:function(result){                               
                            if(result == "success"){
                                $.fancybox.close();
                                location.reload();
                                $(".errorSummary").html("siiiii");  
                            }else{
                                $(".errorSummary").show();                                
                            }
                        }});
              });
                    
               });
  </script>    
