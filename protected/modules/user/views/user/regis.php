<script type="text/javascript">

    $(document).ready(function() {

        $("#regis_button").click(function(){
            $("#regis").submit();
        });
           $("body").removeClass("front").addClass("login");
        
    });
    
     
</script>
<?php $user_id = Yii::app()->user->id; ?>
<?php if (empty($user_id) || $user_id == "0") { ?>
<div class="body-login">
    <div id="maincontent">
        <div class="container-fluid">
            <div class="container">

                <!--warning-->
                <div class="row-fluid">                    
                    <div class="alert alert-info">
                        <i class="icon-warning-sign"></i> คุณยังไม่ได้เข้าสู่ระบบ กรุณาเข้าสู่ระบบหรือสมัครสมาชิกโดยกรอกแบบฟอร์มด้านล่างค่ะ
                    </div>                    
                </div>
                <!--warning-->

                <!--form-->
                <div class="row-fluid">
                    <div class="span2"></div>
                    
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'regis',
                            'action' => Yii::app()->createUrl('/user/registration/registration'),
                            'enableClientValidation' => true,
                            'enableAjaxValidation' => true,
                            'htmlOptions' => array("class" => "span8 login-form"),
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        ?>
                    
                  

                        <div class="span12">
                            <div class="row-fluid">
                                <div class="span2"></div>
                                <div class="span8">
                                    <div class="row-fluid btn-group row-course" id="btn-group-user" data-toggle="buttons-radio">
                                        
                                        <button class="btn btn-user btn-primary <?php if($mode!="login"){ echo "active"; }?>" type="button" id="member">สมัครสมาชิก</button>
<!--                                        <button class="btn btn-user btn-primary" type="button" id="login" >เข้าสู่ระบบ</button>-->

                                        <?php $login_link= Yii::app()->createUrl('/user/login/login'); ?>
                                        <input type="button" value="     เข้าสู่ระบบ     " class="btn btn-user btn-primary" id="login" onClick="window.location.href='<?php echo $login_link;?>'">
                                    </div> <!-- btn-group -->
                                </div>
                                <div class="span2"></div>
                            </div>
                            
                            
                            
                            <br><br>
                            <div class="row-fluid line-lightgrey-wrapper row-course">&nbsp;</div>
                            <br><br>
                            
                    
                            <div id="member-regis" class="row-course"> 
                                
                                <?php $this->renderPartial("/user/_regis",array("form"=>$form,"mode"=>$mode));?>
                                
                            </div> <!-- member -->

                        </div><!-- span8 -->

                     <?php $this->endWidget(); ?>
                    <div class="span2"></div>
                    </>
                </div><!--form-->    
            </div><!-- container -->
        </div><!-- container-fluid -->
    </div><!-- content -->   
    

<?php } ?>