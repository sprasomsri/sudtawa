<style type="text/css">
    .row_content{
        margin-left: 20px;
    }
</style>

    <script>
        $('#myTab a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        })
    </script>

    <div class="body-learning">
        <div id="maincontent">
            <div class="container-fluid">
                <div class="container">
                    <div class="tab-learning">
                        <div id="chapter-group">
                           
                        </div>
                        <div class="container-fluid tab-content">        
                            <div class="row-fluid tab-pane active" id="ajax-results">
                                <div class="span12">
                                    <div class="chapter-title">User Menu</div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span3">
                                        <?php $this->renderPartial("/user/_user_menu")?>
                                    </div> <!-- span3 -->
                                    <div class="span9">
                                        <div id="content-place-holder">
                                            <div class="lesson-content-wrapper lesson-type-video">
                                                <div class="lesson-content-title">
                                                    <span class="icon-green-video"></span>  
                                                   <?php echo Yii::t("site","Change Password"); ?>  </div>
                                                <div class="lesson-content">
                                                    <div class="video-wrapper">
                                            <!--<img src="images/lesson-video.png"/>-->
                                                    <?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
                                                    <div class="success">
                                                    <?php echo Yii::app()->user->getFlash('profileMessage'); ?>
                                                    </div>
                                                    <?php endif; ?>

                                                 
                                                    <div class="form">
                                              
                                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                                            'id'=>'changepassword-form',
                                                            'enableAjaxValidation'=>true,
                                                            'clientOptions'=>array(
                                                                    'validateOnSubmit'=>true,
                                                            ),
                                                    )); ?>



                                                          <div class="row-fluid content">
                                                                 <div class="span1"> </div>
                                                                
                                                                 <p class="note"><?php echo 'Fields with <span class="required">*</span> are required.'; ?></p>
                                                            </div>
                                                            
                                                       <div class="row-fluid content">
                                                             <div class="span1"> </div>
                                                            <?php echo $form->errorSummary($model); ?>
                                                        </div>
                                                     
                                                       
                                                        
                                                        <div class="row-fluid content">
                                                                <div class="span1"></div>
                                                                <div class="span2" align="left">   <?php echo $form->labelEx($model,'password'); ?></div>
                                                                <div class="span8" align="left">
                                                                     <?php echo $form->passwordField($model,'password'); ?>
                                                                     <?php echo UserModule::t("Minimal password length 4 symbols."); ?>
                                                                    <?php echo $form->error($model,'password'); ?>
                                                                </div>
                                                                <div class="span1"></div>
                                                        </div>
                                                              
                                                          
                                                        <div class="row-fluid content">
                                                                <div class="span1"></div>
                                                                <div class="span2" align="left">  <?php echo $form->labelEx($model,'verifyPassword'); ?></div>
                                                                <div class="span8" align="left">
                                                                     <?php echo $form->passwordField($model,'verifyPassword'); ?>
                                                                  
                                                                </div>
                                                                <div class="span1"></div>
                                                        </div>


                                                           <div class="row-fluid content">
                                                                <div class="span9"> </div>
                                                                <button type="submit" class="span3 btn btn-green-submit" name="btn-save" value='1'><?php echo Yii::t("site","Save"); ?></button>
                                                                                          
                                                          </div>
                                                        
                                                         <?php $this->endWidget(); ?>

                                                        </div><!-- form -->  
                                                    </div>
                                                </div><!-- lesson-content -->
                                            </div> <!-- lesson-content-wrapper -->        
                                         </div>
                                    </div> <!-- span9 -->
                                </div><!-- row-fluid -->                                        
                            </div>
                        </div> <!-- tab-content -->
                    </div> <!-- tab-learning -->
                </div> <!-- container -->
            </div> <!-- container-fluid -->
        </div> <!-- maincontent -->
    </div>
