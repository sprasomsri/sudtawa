<style type="text/css">
    .span-19{
        width:100%;
        margin-left: 0px;
    }
    .span-5 .last{
        width:100%;
        display: inline;
        clear:both;
    }
    .operations{
        width:100%;
        display: inline;
    }
    .operations li{        
        padding-right: 15px;
        padding-bottom: 10px;
        
    }
    
    .operations li:hover{        
    /*background-color: #D4D4D4;*/
        text-shadow: 0px -1px 1px blue;
        font-size: 16px;
        text-decoration: none;
    }
    
    .left-box{
        width: 20%;
        height: 100%;
        display: block;
        float: left;
    }
    
    .right-box{
        width:70%;
        height: 100%;
        display: block;
        float: right;
        
    }
    
    
    
    
    
    
    
</style>




<!--<div class="right-box">-->
    <div id="maincontent">
        <div class="container"> 
          <?php $this->renderPartial("/user/_user_menu")?>
            
            <div class="row">
                <div class="span7 row-course">            	
                    <div class="row-fluid row-course">

                        <h2><img src="images/course-detail-msg.png" class="h2-img" /><?php echo UserModule::t('Edit profile'); ?></h2>
                     
                      
                        
              
                        
       <?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
        <div class="success">
        <?php echo Yii::app()->user->getFlash('profileMessage'); ?>
        </div>
        <?php endif; ?>
        <div class="form">
        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'profile-form',
                'enableAjaxValidation'=>true,
                'htmlOptions' => array('enctype'=>'multipart/form-data'),
        )); ?>

                <p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

                <?php echo $form->errorSummary(array($model,$profile)); ?>

                                <?php 
                                       $profileFields=$profile->getFields();
                                       if ($profileFields) {
                                       foreach($profileFields as $field) {
                                                        ?>
                                        <div class="row">
                                                <?php echo $form->labelEx($profile,$field->varname);

                                                if ($widgetEdit = $field->widgetEdit($profile)) {
                                                        echo $widgetEdit;
                                                } elseif ($field->range) {
                                                        echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
                                                } elseif ($field->field_type=="TEXT") {
                                                        echo $form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
                                                } else {
                                                        echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
                                                }
                                                echo $form->error($profile,$field->varname); ?>
                                        </div>	
                                                        <?php
                                                        }
                                                }
                                ?>
                                        <div class="row">
                                                <?php echo $form->labelEx($model,'username'); ?>
                                                <?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>20)); ?>
                                                <?php echo $form->error($model,'username'); ?>
                                        </div>

                                        <div class="row">
                                                <?php echo $form->labelEx($model,'email'); ?>
                                                <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
                                                <?php echo $form->error($model,'email'); ?>
                                        </div>

                                        <div class="row buttons">
                                                <?php echo CHtml::submitButton($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save')); ?>
                                        </div>

                                <?php $this->endWidget(); ?>

                                </div><!-- form -->

                            </div>

                        </div>
                    </div>
                </div>

            </div>
<!--    </div>-->


        
        
        
        


