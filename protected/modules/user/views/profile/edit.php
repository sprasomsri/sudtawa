  <div class="gray-row">
    <div class="container-full">    
    
    <div class="w-container">       
    <div class="w-row">

        <div id="nav-content" class="w-col w-col-4">            
           <?php $this->renderPartial("/user/_user_menu")?>
        </div>

        <div class="w-col w-col-8 affiliate-content user-form">
            <h3>  User's Form. </h3>
            <?php if (Yii::app()->user->hasFlash('profileMessage')): ?>
                <div class="success alert alert-info">
                    <?php echo Yii::app()->user->getFlash('profileMessage'); ?>
                </div>
            <?php endif; ?>
            
            <?php $form=$this->beginWidget('CActiveForm', array(
                                                            'id'=>'profile-form',
                                                            'enableAjaxValidation'=>true,
                                                            'htmlOptions' => array('enctype'=>'multipart/form-data'),
                                                    )); ?>
                                                            <div class="row-fluid content">
                                                                 <div class="span1"> </div>
                                                                 <p class="note">
                                                                     <?php echo 'Fields with <span class="required">*</span> are required.'; 
                                                                      if($check_step->is_edit_profile==0 || empty($check_step)){
                                                                        echo "<br/><br/> กรุณาใส่ข้อมูลของท่านเพื่อเข้าใช้งาน";
                                                                        }                                                           
                                                                     ?>                            
                                                                 
                                                                 </p>
                                                            </div>
                                                            <?php echo $form->errorSummary(array($model,$profile)); ?>

                                                                            <?php //$profileFields=$profile->getFields();   ?>
                                                                             
                                                      
                                                                                    <div class="form-group">
                                                                                        <div class="span1"></div>
                                                                                        <div class="span2" align="left"> <?php echo $form->labelEx($profile,'firstname'); ?> </div>
                                                                                        <div class="span8" align="left">
                                                                                              <?php echo $form->textField($profile,'firstname',array("class"=>"form-control")); ?>
                                                                                              <?php echo $form->error($profile,'firstname'); ?>
                                                                                        </div>
                                                                                        <div class="span1"></div>
                                                                                    </div>
                                                        
                                                        
                                                                                    <div class=form-group">
                                                                                        <div class="span1"></div>
                                                                                        <div class="span2" align="left"> <?php echo $form->labelEx($profile,'lastname'); ?></div>
                                                                                        <div class="span8" align="left">
                                                                                              <?php echo $form->textField($profile,'lastname',array("class"=>"form-control")); ?>
                                                                                              <?php echo $form->error($profile,'lastname'); ?>
                                                                                        </div>
                                                                                        <div class="span1"></div>
                                                                                    </div>
                                                        
                                                                                <div class="form-group">
                                                                                        <div class="span1"></div>
                                                                                        <div class="span2" align="left"> <?php echo $form->labelEx($profile,'position'); ?></div>
                                                                                        <div class="span8" align="left">
                                                                                              <?php echo $form->textField($profile,'position',array("class"=>"form-control")); ?>
                                                                                              <?php echo $form->error($profile,'position'); ?>
                                                                                        </div>
                                                                                        <div class="span1"></div>
                                                                                    </div>
            
            
                                                        
                                                                                                               
                                                                                                                                            
                                                                                    <div class="form-group">
                                                                                        <div class="span1"></div>
                                                                                        <div class="span2" align="left"> <?php echo $form->labelEx($model,'username'); ?></div>
                                                                                        <div class="span8" align="left">
                                                                                              <?php echo $form->textField($model,'username',array("class"=>"form-control",'disabled'=>"disabled")); ?>
                                                                                              <?php echo $form->error($model,'username'); ?>
                                                                                              
                                                                                        </div>
                                                                                        <div class="span1"></div>
                                                                                    </div>
                                                            
                                                            
                                                                                     <div class="form-group">
                                                                                        <div class="span1"></div>
                                                                                        <div class="span2" align="left"> <label> E-mail </label></div>
                                                                                        <div class="span8" align="left">
                                                                                              <?php echo $form->textField($model,'email',array("class"=>"form-control")); ?>
                                                                                            <?php echo $form->error($model,'email'); ?>
                                                                                        </div>
                                                                                        <div class="span1"></div>
                                                                                    </div>
            
            
                                                                                <div class="form-group">
                                                                                        <div class="span1"></div>
                                                                                        <div class="span2" align="left"> <?php echo $form->labelEx($profile,'company'); ?></div>
                                                                                        <div class="span8" align="left">
                                                                                              <?php echo $form->textField($profile,'company',array("class"=>"form-control")); ?>
                                                                                              <?php echo $form->error($profile,'company'); ?>
                                                                                        </div>
                                                                                        <div class="span1"></div>
                                                                                    </div>
            
            
                                                                                    <div class="form-group">
                                                                                        <div class="span1"></div>
                                                                                        <div class="span2" align="left"> <?php echo $form->labelEx($profile,'tel'); ?></div>
                                                                                        <div class="span8" align="left">
                                                                                              <?php echo $form->textField($profile,'tel',array("class"=>"form-control")); ?>
                                                                                              <?php echo $form->error($profile,'tel'); ?>
                                                                                        </div>
                                                                                        <div class="span1"></div>
                                                                                    </div>
            
            

                                                                                             
                                                                                  <div class="form-group">
                                                                                        <div class="span1"></div>
                                                                                        <div class="span2" align="left"> <?php echo $form->labelEx($profile,'detail'); ?></div>
                                                                                        <div class="span8" align="left">
                                                                                               <?php echo $form->textArea($profile,'detail',array('style'=>"width:550px; height:200px; ")); ?>
                                                                                            <?php echo $form->error($profile,'detail'); ?>
                                                                                        </div>
                                                                                        <div class="span1"></div>
                                                                                    </div>
                                                            
                                                                                  
                                                                                  <div class="form-group">
                                                                                        <div class="span1"></div>
                                                                                        <div class="span2" align="left"> <?php echo $form->labelEx($profile,'photo'); ?></div>
                                                                                        <div class="span8" align="left">
                                                                                           <?php echo CHtml::activeFileField($profile, 'photo',array("class"=>"input-block-level")) ?>
                                                                                            <?php echo $form->error($profile,'photo'); ?>
                                                                                            
                                                                                               <div class="row_content">
                                                                                                <?php 
                                                                                                 
                                                                                                if(!empty($profile->photo)){ ?>
                                                                                                                                                                                                                     
                                                                                                            <a id="single_1" href="userphoto/original/<?php echo $profile->photo; ?>" title="   click to view real size ">
                                                                                                                   <img width="200" src="userphoto/original/<?php echo $profile->photo; ?>"/>
                                                                                                            </a> <br/>
                                                                                                            <a href="<?php echo Yii::app()->createUrl("user/profile/deletephoto");?>" onclick="return confirmDelete()">
                                                                                                                <i class="icon-remove"> </i> Remove Photo 
                                                                                                            </a>
                                                                                                       <script type="text/javascript">
                                                                                                          jQuery.noConflict(); 
                                                                                                           $(document).ready(function() {
                                                                                                                $("#single_1").fancybox({
                                                                                                                      helpers: {
                                                                                                                          title : {
                                                                                                                              type : 'float'
                                                                                                                          }
                                                                                                                      }
                                                                                                                  });
                                                                                                            });
                                                                                                       </script>
                                                                                                            
                                                                                                            
                                                                                                <?php }else{ ?>
                                                                                                       <?php $facebook_login= Yii::app()->user->getState('facebook'); ?>
                                                                                                       <?php if(empty($facebook_login)){ ?>                                                                                                        
                                                                                                            <img width="150px" src="images/user_default.png"/>
                                                                                                       <?php }else{ ?>
                                                                                                              <img src="https://graph.facebook.com/<?php echo $facebook_login; ?>/picture">
                                                                                                       <?php } ?>     
                                                                                                            
                                                                                                <?php } ?> 
                                                                                                </div>                                                                                           
                                                                                            
                                                                                        </div>
                                                                                      
                                                                                    </div>
                                                            
                                                                                                                                           

                                                                                   <div class="form-group content">
                                                                                       <div class="span9"> </div>
                                                                                            <?php echo $form->hiddenField($model,'id',array('value'=>$model->id)); ?>
                                                                                           <button type="submit" class="span3 btn btn-green-submit"><?php echo Yii::t("site","Save"); ?></button>
                                                                                            <?php //echo CHtml::submitButton(Yii::t("site","Save")); ?>
                                                                                    </div>

                                                                            <?php $this->endWidget(); ?>
            
        </div>
    </div>
  </div>
 </div>
</div> 
    

    <script type="text/javascript">
           function confirmDelete() {
              var msg = confirm('Are you sure?');
              if(msg == false) {
                  return false;
              }
          }                                               
    </script>  
    