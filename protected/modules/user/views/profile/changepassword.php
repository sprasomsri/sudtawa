  <div class="gray-row">
    <div class="container-full">
      
    <div class="w-container">       
    <div class="w-row">

        <div id="nav-content" class="w-col w-col-4">            
           <?php $this->renderPartial("/user/_user_menu")?>
        </div>

        <div class="w-col w-col-8 affiliate-content user-form">
            <h3>  User's Form. </h3>
            <?php if (Yii::app()->user->hasFlash('profileMessage')): ?>
                <div class="success alert alert-info">
                    <?php echo Yii::app()->user->getFlash('profileMessage'); ?>
                </div>
            <?php endif; ?>

            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'changepassword-form',
                'enableAjaxValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>


            <div class="row-fluid content">
                <div class="span1"> </div>

                <p class="note">  

                    <?php echo 'Fields with <span class="required">*</span> are required.'; 
                    
                    if($check_step->is_change_password==0 || empty($check_step)){
                        echo "<br/><br/> กรุณาเปลี่ยน Password เพื่อเข้าใช้งาน";
                    }
                        
                    
                    ?>      
                </p>
            </div>


            <div class="row-fluid content">
                <div class="span1"> </div>
            <?php echo $form->errorSummary($model); ?>
            </div>


            <div class="form-group content">
                <div class="span1"></div>
                <div class="span2" align="left"><label>Old Password</label>   </div>
                <div class="span8" align="left">
                <?php echo $form->passwordField($model, 'oldPassword',array("class"=>"form-control")); ?>
                <?php echo $form->error($model, 'oldPassword'); ?>
                </div>
                <div class="span1"></div>
            </div>

            <div class="form-group content">
                <div class="span1"></div>
                <div class="span2" align="left"><label>New Password</label></div>
                <div class="span8" align="left">
                    <?php echo $form->passwordField($model, 'password',array("class"=>"form-control")); ?> <br/>
                    <?php echo UserModule::t("Minimal password length 4 symbols."); ?>
                    <?php echo $form->error($model, 'password'); ?>
                </div>
                <div class="span1"></div>
            </div>


            <div class="form-groupcontent">
                <div class="span1"></div>
                <div class="span2" align="left"> <label>Confirm Password</label></div>
                <div class="span8" align="left">
                <?php echo $form->passwordField($model, 'verifyPassword',array("class"=>'form-control')); ?>
                <?php echo $form->error($model, 'verifyPassword'); ?>
                </div>
                <div class="span1"></div>
            </div>
            
            <br/>

            <div class="row-fluid content">
                <div class="span9"> </div>
                <button type="submit" class="span3 btn btn-green-submit"><?php echo Yii::t("site", "Save"); ?></button>

            </div>


            <?php $this->endWidget(); ?>
            
        </div>
    </div>
  </div>
  </div>
</div>
    
    
   