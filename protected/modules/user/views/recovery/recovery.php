  <div class="gray-row">
    <div class="container-full">    
           
<!--container-->
        <div class="user-form">

            
            <h3 class="center">Reset Password Form</h3> <br/>
           
            <?php if (Yii::app()->user->hasFlash('recoveryMessage')): ?>
                <div class="success">
                    RESET PASSWORD SUCCESS
                </div>
            <?php else: ?>

                <div class="form" style="text-align:center">
                    <?php echo CHtml::beginForm(); ?>

                    <?php echo CHtml::errorSummary($form); ?>

                    <div class="form-group">
                        <label> Email OR Username </label>
                        <?php echo CHtml::activeTextField($form, 'login_or_email', array("class" => "form-control")); ?>
                    </div>
                   


                    <div class="row submit">
                        <?php echo CHtml::submitButton("Reset Password",array("class"=>"btn btn-primary")); ?>
                    </div>

                    <?php echo CHtml::endForm(); ?>
                </div><!-- form -->
            <?php endif; ?>
        </div>
<!--container-->            
    </div>
</div>




