<?php
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	const ERROR_EMAIL_INVALID=3;
	const ERROR_STATUS_NOTACTIV=4;
	const ERROR_STATUS_BAN=5;
        private $facebook_id;
        /**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
      
		if (strpos($this->username,"@")) {
			$user=User::model()->notsafe()->findByAttributes(array('email'=>$this->username));
		} else {
			$user=User::model()->notsafe()->findByAttributes(array('username'=>$this->username));
		}
                          
          
		if($user===null)
                    	if (strpos($this->username,"@")) {
				$this->errorCode=self::ERROR_EMAIL_INVALID;
			} else {
				$this->errorCode=self::ERROR_USERNAME_INVALID;
			}
		else if(Yii::app()->getModule('user')->encrypting($this->password)!==$user->password)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else if($user->status==99)
			$this->errorCode=self::ERROR_STATUS_NOTACTIV;
		else if($user->status==-1)
			$this->errorCode=self::ERROR_STATUS_BAN;
		else {
                       
			$this->_id=$user->id;
                        Yii::app()->user->setState('username', $user->username);
                        Yii::app()->user->setState('photo', $user->profile->photo);        
                        Yii::app()->user->setState('superadmin', $user->superuser);
                     
                        $model_userpermission=  UserPermission::model()->find("user_id='$user->id'");
                        if(!empty($model_userpermission->can_create_course) && $model_userpermission->can_create_course==1){                         
                             Yii::app()->user->setState('owner',$model_userpermission->can_create_course);
                        }
                        
                              
			$this->username=$user->username;
			$this->errorCode=self::ERROR_NONE;
		}
		return !$this->errorCode;
	}
        
//        public function authenFaceBook(){
//               if (strpos($this->username,"@")) {
//			$user=User::model()->notsafe()->findByAttributes(array('email'=>$this->username));
//		} else {
//			$user=User::model()->notsafe()->findByAttributes(array('username'=>$this->username));
//		}
//                
//                if(!empty($user)){
//                    $this->_id=$user->id;
//                    $this->username=$user->username;
//                    $this->errorCode=self::ERROR_NONE;
//                    return !$this->errorCode;
//                }
//                
//                if($user->facebook_id != $this->facebook_id){
//                    $this->errorCode=self::ERROR_PASSWORD_INVALID;
//                }else{
//                    $this->_id=$user->id;
//                   
//			$this->username=$user->username;
//			$this->errorCode=self::ERROR_NONE;
//                }
//                return !$this->errorCode;
//        }

        /**
    * @return integer the ID of the user record
    */
	public function getId()
	{
		return $this->_id;
	}
        
        public function setUserId($user_id){
            $this->_id=$user_id;
        }
        
        
}