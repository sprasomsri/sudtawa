<?php

class DashboardController extends Controller
{
	public function actionIndex(){
            
            LinkHelper::SetPageHistory();
            $user_id=Yii::app()->user->id;
            
            if(empty($user_id)){
                $login_link=Yii::app()->createUrl("user/login/login",array('message'=>"กรุณาลอกอินเพื่อเข้าใช้ระบบ"));
                $this->redirect($login_link);
            }
            
                               
            #show progress only role 3
            $criteria = new CDbCriteria;
            $criteria->select="t.*";
            $criteria->group="t.course_id";
            $criteria->condition="t.user_id='$user_id' AND role='3' AND 
                                  (content_info.number_of_content!=stat_user_study.content_has_studied OR stat_user_study.course_id IS NULL)";
            $criteria->join= "LEFT JOIN content_info ON t.course_id=content_info.course_id 
                              LEFT JOIN stat_user_study ON t.course_id= stat_user_study.course_id";
                             
          
            
            $dataProvider= new CActiveDataProvider("UserCourse", array(
                'pagination' => array(
                    'pageSize' => 9,
                ),
                'criteria' => $criteria,
            ));
            
            $this->render("course_progress",array("dataProvider"=>$dataProvider));
                        
            
        }
        
        
        
        public function actionCompleateCourse(){
            LinkHelper::SetPageHistory();
            $user_id=Yii::app()->user->id;
            
            if(empty($user_id)){
                $login_link=Yii::app()->createUrl("user/login/login",array('message'=>"กรุณาลอกอินเพื่อเข้าใช้ระบบ"));
                $this->redirect($login_link);
            }
            
           
                    
            #show progress only role 3
            $criteria = new CDbCriteria;
            $criteria->select="t.*";
            $criteria->condition="t.user_id='$user_id' AND role='3' AND 
                                  content_info.number_of_content=stat_user_study.content_has_studied";
            $criteria->join= "LEFT JOIN content_info ON t.course_id=content_info.course_id 
                              LEFT JOIN stat_user_study ON t.course_id= stat_user_study.course_id";
            $criteria->group="t.course_id";
                             
          
            
            $dataProvider= new CActiveDataProvider("UserCourse", array(
                'pagination' => array(
                    'pageSize' => 9,
                ),
                'criteria' => $criteria,
            ));
            
            $this->render("course_progress",array("dataProvider"=>$dataProvider));
                                  
        }
}