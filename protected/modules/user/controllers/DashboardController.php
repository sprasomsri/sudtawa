<?php

class DashboardController extends Controller
{
    
       // public $layout='//layouts/coursecreek';
    
	public function actionIndex(){
            
            LinkHelper::SetPageHistory();
            $user_id=Yii::app()->user->id;
            
            
            
            if(empty($user_id)){
                $login_link=Yii::app()->createUrl("user/login/login",array('message'=>"กรุณาลอกอินเพื่อเข้าใช้ระบบ"));
                $this->redirect($login_link);
            }
                                           
            #show progress only role 3
            $criteria = new CDbCriteria;
            $criteria->select="t.*";
            $criteria->group="t.course_id";
            $criteria->condition="t.content_has_studied != t.number_of_content AND user_id='$user_id'";           
        
           
               
            $dataProvider= new CActiveDataProvider("StatUserStudy", array(
                'pagination' => array(
                    'pageSize' => 5,
                ),
                'criteria' => $criteria,
            ));
            
            $menu="DashBoard";
            
            $this->render("course_progress",array("dataProvider"=>$dataProvider,"menu"=>$menu));
                        
            
        }
        
        
        
        public function actionCompleateCourse(){
            LinkHelper::SetPageHistory();
            $user_id=Yii::app()->user->id;
            
            if(empty($user_id)){
                $login_link=Yii::app()->createUrl("user/login/login",array('message'=>"กรุณาลอกอินเพื่อเข้าใช้ระบบ"));
                $this->redirect($login_link);
            }
            
           
                    
            #show progress only role 3

            $criteria = new CDbCriteria;
            $criteria->select="t.*";
            $criteria->group="t.course_id";
            $criteria->condition="t.content_has_studied = t.number_of_content AND user_id='$user_id'";    
                                      
             
            $dataProvider= new CActiveDataProvider("StatUserStudy", array(
                'pagination' => array(
                    'pageSize' => 5,
                ),
                'criteria' => $criteria,
            ));
            $menu="CompleateCourse";
            
            $this->render("course_progress",array("dataProvider"=>$dataProvider,"menu"=>$menu));
                                  
        }
}