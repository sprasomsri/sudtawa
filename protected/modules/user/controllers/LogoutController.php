<?php

class LogoutController extends Controller
{
	public $defaultAction = 'logout';
	
	/**
	 * Logout the current user and redirect to returnLogoutUrl.
	 */
	public function actionLogout() {

            Yii::app()->user->logout();
            $facebook=SiteHelper::getFacbook();
            $user = $facebook->getUser();
             if(!empty($user)){
                $facebook->LogoutFaceBook();
            }


            $target = Yii::app()->createUrl("site/index");
            $this->redirect($target);
            
         
    }

}