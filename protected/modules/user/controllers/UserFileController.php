<?php

class UserFileController extends Controller
{
    public $layout='//layouts/main_backend';  
    public function actionIndex(){
        
        $this->render("index");
    }
    
    
    public function CheckUserid(){
         $user_id = Yii::app()->user->id;
        if(empty($user_id)){
             $destination=Yii::app()->createUrl("site/error");
             $this->redirect($destination);
        }
    }


       
    public function actionPicture(){
        $this->CheckUserid();      
        $user_id = Yii::app()->user->id;         
        $criteria = new CDbCriteria;
        $criteria->select="t.*";

        $criteria->condition="user_id='$user_id' AND type= 'image'";
        $criteria->order="id DESC";

        $pictures = new CActiveDataProvider('File', array(
            'pagination' => array('pageSize' => "9"),
            'criteria' => $criteria,
        ));
         
         
       $this->render("picture",array("pictures"=>$pictures,"user_id"=>$user_id));
        
    }
    
    public function actionUploadPicture(){      
        $this->CheckUserid();
        $user_id = Yii::app()->user->id;     
        $this->render("form_picture",array("user_id"=>$user_id));
        
    }
    
    
    public function actionSavePicture($user_id) {

        if (!empty($_FILES)) {            

            $random = substr(str_shuffle('abcdefgunhijklarngearmnopqestuvwxyz5040563263'), 0, 15);
            $targetFolder = Yii::app()->basePath . "/../webroot/files/$user_id/pictures/";


            if (!is_dir($targetFolder)) {
               mkdir($targetFolder, 0775, true);
            }

            $old_file_name = strtolower($_FILES['Filedata']['name']);

            $new_file_name = $random . "courrsecreek_" . $old_file_name;

            $tempFile = trim($_FILES['Filedata']['tmp_name']);
//            $targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
//            $targetFile = rtrim($targetPath, '/') . '/' . strtolower($new_file_name);
            $targetFile = Yii::app()->basePath . "/../webroot/files/$user_id/pictures/" . $new_file_name;

            // Validate the file type
            $fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // File extensions
            $fileParts = pathinfo($_FILES['Filedata']['name']);

          
            
            if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
                move_uploaded_file($tempFile, $targetFile);//                
                $this->saveFileInfo("image", $new_file_name, $_FILES['Filedata']['name'], $user_id);
//                
                echo 'Success';
                echo $new_file_name;
            } else {
                echo 'Invalid file type.';
            }
        }
    }
    
    public function actionDeletePicture($id){        
        $this->CheckUserid();        
        $model_file=  File::model()->findByPk($id);
        $user_id = Yii::app()->user->id;
        $targetFolder = Yii::app()->basePath . "/../webroot/files/$user_id/pictures/$model_file->name";
        
        if (file_exists($targetFolder)) {
            unlink($targetFolder);
            
        }
        $model_file->delete();
        $destination=Yii::app()->createUrl("user/userFile/picture");
        $this->redirect($destination); 
    }


    
    
    public function actionAudio(){
        //photo Click show iframe
        $this->CheckUserid();      
        $user_id = Yii::app()->user->id;         
        $criteria = new CDbCriteria;
        $criteria->select="t.*";

        $criteria->condition="user_id='$user_id' AND type= 'audio'";
        $criteria->order="id DESC";

        $audios = new CActiveDataProvider('File', array(
            'pagination' => array('pageSize' => "9"),
            'criteria' => $criteria,
        ));
         
         
       $this->render("audio",array("audios"=>$audios,"user_id"=>$user_id));
        
    }
    
    
    
    public function actionPlayAudio($id){
         $this->layout = "//layouts/blank";
         $this->CheckUserid();
         $model_file= File::model()->findByPk($id);
         $this->render("play_audio",array("model_file"=>$model_file));    
    }


    
    public function actionUploadAudio(){
        $this->CheckUserid();
        $user_id = Yii::app()->user->id;
        $this->render("form_audio",array("user_id"=>$user_id));
      
        
    }
    
     public function actionSaveAudio($user_id){         
          if (!empty($_FILES)) {
            $random = substr(str_shuffle('abcdefgunhijklarngearmnopqestuvwxyz5040563263'), 0, 15);
            $targetFolder = Yii::app()->basePath . "/../webroot/files/$user_id/audio/";

            if (!is_dir($targetFolder)) {
               mkdir($targetFolder, 0775, true);
            }

            $old_file_name = strtolower($_FILES['Filedata']['name']);

            $new_file_name = "courrsecreek_" . $random.$old_file_name;
            
            $reserveString = array(",", " ", "(", ")", "<", ">");
            $replace = array("", "", "", "", "", "");
            $new_file_name = str_replace($reserveString, $replace, $new_file_name);



            $tempFile = trim($_FILES['Filedata']['tmp_name']);
            $targetFile_mp3 = Yii::app()->basePath . "/../webroot/files/$user_id/audio/" . $new_file_name;
            
            
            

            // Validate the file type
            $fileTypes = array('mp3'); // File extensions
            $fileParts = pathinfo($_FILES['Filedata']['name']);

           
            
            if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
                move_uploaded_file($tempFile, $targetFile_mp3);
                
               //convert ogg file
               list($file_name,$file_type)=  explode(".", $new_file_name);
                $file_ogg=$file_name.".ogg";
                $target_path_ogg_file = Yii::app()->basePath . "/../webroot/files/$user_id/audio/$file_ogg";
                $convert_ogg ="ffmpeg -i $targetFile_mp3 -acodec libvorbis -aq 60 -map_meta_data 0:0 $target_path_ogg_file &";
                exec($convert_ogg);
                            
                $this->saveFileInfo("audio", $new_file_name, $_FILES['Filedata']['name'], $user_id);
                
                echo 'Success';
                echo $new_file_name;
            } else {
                echo 'Invalid file type.';
            }
        }        
    }  
    
    
    public function actionDeleteAudio($id){
        $this->CheckUserid();        
        $model_file=  File::model()->findByPk($id);
        $user_id = Yii::app()->user->id;
        $target_file_mp3 = Yii::app()->basePath . "/../webroot/files/$user_id/audio/$model_file->name";
        
        list($name,$type)=  explode(".", $model_file->name);
        $ogg_file=$name.".ogg";
        $target_file_ogg = Yii::app()->basePath . "/../webroot/files/$user_id/audio/$ogg_file";
        
        if (file_exists($target_file_ogg)) {
            unlink($target_file_ogg);
            
        }       
        
        if (file_exists($target_file_mp3)) {
            unlink($target_file_mp3);
            
        }
        $model_file->delete();
        $destination=Yii::app()->createUrl("user/userFile/audio");
        $this->redirect($destination); 
        
    }

 
    public function actionVideo() {
        $this->CheckUserid();
        $user_id = Yii::app()->user->id;
        $criteria = new CDbCriteria;
        $criteria->select = "t.*";

        $criteria->condition = "user_id='$user_id' AND type= 'video'";
        $criteria->order = "id DESC";

        $videos = new CActiveDataProvider('File', array(
            'pagination' => array('pageSize' => "9"),
            'criteria' => $criteria,
        ));


        $this->render("video", array("videos" => $videos, "user_id" => $user_id));
    }
    
    
    public function actionUploadVideo(){
        $this->CheckUserid(); 
        $user_id = Yii::app()->user->id;
        $this->render("form_video",array("user_id"=>$user_id));
        
    }
    
    public function actionSaveVideo($user_id) {
        
          if (!empty($_FILES)) {          
            $targetFolder = Yii::app()->basePath . "/../webroot/files/$user_id/video/";


            if (!is_dir($targetFolder)) {
               mkdir($targetFolder, 0775, true);
            }

            $file_original = strtolower($_FILES['Filedata']['name']);
                       
            $tempFile = trim($_FILES['Filedata']['tmp_name']);
            $targetFileOriginal = Yii::app()->basePath . "/../webroot/files/$user_id/video/" . $file_original;

            // Validate the file type
             $fileTypes = array('webm','mp4','3gpp','mov','avi','wmv','flv','mpeg');// File extensions
             $fileParts = pathinfo($_FILES['Filedata']['name']);          
            
            if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
                move_uploaded_file($tempFile, $targetFileOriginal);
                
                $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz1234567890'), 0, 5);
                $new_file_name = "coursecreek" . $random .$file_original;
                
                $reserveString=array(","," ","(",")","<",">");
                $replace=array("","","","","","");
                $new_file_name=  str_replace($reserveString,$replace, $new_file_name);        
                
                  
                # Generate thumbnail -------------------------------------------
                    $thumbnail_name=  VdoHelper::generateVideohTumbnailname($new_file_name,$fileParts['extension']);
                    $thumbnail_vdo = Yii::app()->basePath . "/../webroot/files/$user_id/video/".$thumbnail_name;
                    VdoHelper::generateVideoThumbnail($targetFileOriginal, $thumbnail_vdo);
                    
                    
                # encode Video -------------------------------------------------
                   $encode_file_save = Yii::app()->basePath . "/../webroot/files/$user_id/video/".$new_file_name;
                   VdoHelper::encodeVideoH264($targetFileOriginal, $encode_file_save); 
                   
                   $this->saveFileInfo("video", $new_file_name, $_FILES['Filedata']['name'], $user_id);    
                
                echo 'Success Upload';
                echo $new_file_name;
            } else {
                echo 'Invalid file type.';
            }
        }
        
    }
       
    
    public function actionViewVideo($id){
        $this->layout = "//layouts/blank";
        $model_file= File::model()->findByPk($id);
        $this->render("view_video",array("model_file"=>$model_file));       
        
    }
    
    
    public function actionDeleteVideo($id) {
        $model_file = File::model()->findByPk($id);
        $vdo_path = Yii::app()->basePath . "/../webroot/files/$model_file->user_id/video/" . $model_file->name;
        $vdo_thumbnail_path = Yii::app()->basePath . "/../webroot/files/$model_file->user_id/video/" . VdoHelper::getMp4TumbnailMp4($model_file->name);

        if (file_exists($vdo_path)) {
            unlink($vdo_path);
        }

        if (file_exists($vdo_thumbnail_path)) {
            unlink($vdo_thumbnail_path);
        }
         $model_file->delete();
        
        $destination = Yii::app()->createUrl("user/userFile/video");
        $this->redirect($destination);
       
    }
    
    
    public function saveFileInfo($type, $name, $original_name, $user_id) {
        $model_user_file = new File();
        $model_user_file->user_id = $user_id;
        $model_user_file->name = $name;
        $model_user_file->original_name = $original_name;
        $model_user_file->type = $type;
        $model_user_file->create_at = date("Y-m-d H:i:s");
        $model_user_file->save();
    }
    
    
    
    
	
}