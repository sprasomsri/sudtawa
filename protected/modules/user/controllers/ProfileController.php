<?php

class ProfileController extends Controller
{
	public $defaultAction = 'profile';
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;
	/**
	 * Shows a particular model.
	 */
	public function actionProfile()
	{
            
            $this->redirect(Yii::app()->createUrl("user/profile/edit"));
            
            
		$model = $this->loadUser();
	    $this->render('profile',array(
	    	'model'=>$model,
			'profile'=>$model->profile,
	    ));
	}


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionEdit()
	{
        $user_id=Yii::app()->user->id;
	$model = User::model()->findByPk($user_id);
        $user_name=$model->username;
        $email=$model->email;
        $profile=  ProfilesEdit::model()->find("user_id='$user_id'");
        $old_photo=$profile->photo;
        
        $check_step = LogStudentStepBase::model()->find("user_id='$user_id'");
        if (empty($check_step)) {
            $check_step = new LogStudentStepBase();
        }
                
		
		// ajax validator
		if(isset($_POST['ajax']) && $_POST['ajax']==='profile-form')
		{
			echo UActiveForm::validate(array($model,$profile));
			Yii::app()->end();
		}
		
		if(isset($_POST['User'])||isset($_POST['ProfilesEdit']))
		{
		$model->attributes=$_POST['User'];
                
      
//                $model->email=email;
//                $model->username=$user_name;
		$profile->attributes=$_POST['ProfilesEdit'];
                
                
//                echo "<pre>";
//                print_r($_POST['ProfilesEdit']);
//                echo "<hr/>";
//                print_r($profile->attributes);
//                echo "</pre>";
//                exit;
                
                
                $profile->user_id=$user_id;
                        
                 $vaild=true;
                 $user_photo = CUploadedFile::getInstance($profile, 'photo');        
                
                       
                 if(!empty($user_photo)){
                 list($name,$type)=  explode(".", $user_photo);
                 $photo_array=array("jpg","png","gif","JPG","GIF","PNG");
                 
                 
                 if(!in_array($type, $photo_array)){
                     $vaild=false;
                      $profile->addError('photo', ' **image type must be >>>jpg,png,gif ');
                 }else{
                      $random = substr(str_shuffle('abcoursecreekdefghijklmnopqrstuvwxyz0128883456789'), 0, 100);
                      $user_photo->saveAs(Yii::app()->basePath . '/../webroot/userphoto/original/' . "$random" . "_" . CUploadedFile::getInstance($profile, 'photo'));
                      $profile->photo = $random."_".$user_photo;
                      
                         //save small for tambail
                        $file_to_resize = "userphoto/original/$profile->photo";
                        $file_to_resize = Yii::app()->simpleImage->load($file_to_resize);
                        $file_to_resize->resizeToWidth(106);
                        $file_to_resize->save("userphoto/$profile->photo");


                          if(!empty($old_photo)){
                             #small pic
                             $old_file_small= Yii::app()->basePath."/../webroot/userphoto/".$old_photo;
                             if (file_exists($old_file_small)) {
                                 unlink($old_file_small);
                             }

                             #big pic 
                             $old_file_origin= Yii::app()->basePath."/../webroot/userphoto/original/".$old_photo;
                             if (file_exists($old_file_origin)) {
                                 unlink($old_file_origin);
                             }

                          }
                     
                   }
                 }else{
                     $profile->photo=$old_photo;
                 }
                 
                                  
                 if($model->validate() && $vaild==true && $profile->validate()) {
				$model->save();
				$profile->save(false);
                                
                                
                               
                                 $check_step->is_edit_profile = 1;                                 
                                 $check_step->save();
                                                
                                                 
                                 if($check_step->is_change_password==0){
                                      $success_link=Yii::app()->createUrl("user/profile/changepassword");
                                 }else{                                                     
                                   
                                      $model->status=1;
                                      $model->save(false);                                         
                                      $success_link=Yii::app()->createUrl("user/profile/edit");
                                  }                            
                                                                
                                Yii::app()->user->updateSession();
				Yii::app()->user->setFlash('profileMessage',UserModule::t("Changes is saved."));
				$this->redirect($success_link);
			} 
		}

		$this->render('edit',array(
			'model'=>$model,
			'profile'=>$profile,
                        'check_step'=>$check_step
		));
	}
	
	/**
	 * Change password
	 */
        public function actionDeletePhoto() {
        $user_id = Yii::app()->user->id;
        $model_profile = Profile::model()->find("user_id='$user_id'");
        $old_photo=$model_profile->photo;
        
        if (!empty($old_photo)) {
            #small pic
            $old_file_small = Yii::app()->basePath . "/../userphoto/" . $old_photo;
            if (file_exists($old_file_small)) {
                unlink($old_file_small);
            }

            #big pic 
            $old_file_origin = Yii::app()->basePath . "/../userphoto/original/" . $old_photo;
            if (file_exists($old_file_origin)) {
                unlink($old_file_origin);
            }
        }
        
        $model_profile->photo="";
        $model_profile->save(false);
        $success_link=Yii::app()->createUrl("user/profile/edit");
        $this->redirect($success_link);
    }
    
    public function actionConnectSocialNetwork($message=""){
        $user_id = Yii::app()->user->id;
     
        $model_provider=  ExternalProvider::model()->find("user_id='$user_id' AND provider='facebook'");
                
        $facebook = SiteHelper::getFacbook();
         
         $user = $facebook->getUser();
             
         if (!empty($user) && empty($model_provider)) {
            try {
                // Proceed knowing you have a logged in user who's authenticated.
                $user_profile = $facebook->api('/me');
                $provider_id=$user_profile['id'];
                $email=$user_profile['email'];
                              
                $model_provider_check=  ExternalProvider::model()->find("provider_id='$provider_id' AND email='$email'");
                $model_user=  UsersFaceBook::model()->find("facebook_id='$user'");
                if(empty($model_provider_check) && empty($model_user)){
                    $model_provider=new ExternalProvider();
                    $model_provider->user_id=$user_id;
                    $model_provider->user_provide_name=$user_profile['name'];
                    $model_provider->provider_id=$user_profile['id'];
                    $model_provider->email=$user_profile['email'];
                    $model_provider->provider="facebook";
                    $model_provider->create_date=Date("Y-m-d H:i:s");
                    $model_provider->save();
                    $facebook->LogoutFaceBook();
                }else{
                    $message="Sorry !!! This Facebook has already been in system.";
                    $facebook->LogoutFaceBook();
                }
                $facebook->LogoutFaceBook();
                $success_link=Yii::app()->createUrl("user/profile/connectSocialNetwork/message/$message");
                $this->redirect($success_link);
                
            } catch (FacebookApiException $e) {
                error_log($e);
                $user = null;
            }
        }
         
                
        $this->render("connect_social_network",array("model_provider"=>$model_provider,"facebook"=>$facebook,"user"=>$user,'message'=>$message));
    }
    
 

    public function actionUnlinksocial($id){
        $model_provider=  ExternalProvider::model()->findByPk($id);
        $model_provider->delete();
        
        $success_link=Yii::app()->createUrl("user/profile/connectSocialNetwork");
        $this->redirect($success_link);
        
    }

    public function actionCreatepassword() {
        $user_id = Yii::app()->user->id;
        $model = new UserCreatePassword();
        if (Yii::app()->user->id) {
            // ajax validator
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'changepassword-form') {
                echo UActiveForm::validate($model);
                Yii::app()->end();
            }

            if (isset($_POST['UserCreatePassword'])) {
                $model->attributes = $_POST['UserCreatePassword'];
                $model->password=$_POST['UserCreatePassword']['password'];

                if ($model->validate()) {                 
                    $new_password = User::model()->notsafe()->findbyPk(Yii::app()->user->id);
                    $new_password->password = UserModule::encrypting($model->password);
                    $new_password->activkey = UserModule::encrypting(microtime() . $model->password);
                    $new_password->save();
                    Yii::app()->user->setFlash('profileMessage', UserModule::t("New password is saved."));
                    $success_link=Yii::app()->createUrl("user/profile/changepassword");
                    $this->redirect(array("profile"));
                }
            }
            $this->render("form_create_password", array("model" => $model));
        }
        
    }

    public function actionChangepassword() {
      
                $user_id = Yii::app()->user->id;
              
                
		$model = new UserChangePassword;
		if (Yii::app()->user->id) {
			
			// ajax validator
			if(isset($_POST['ajax']) && $_POST['ajax']==='changepassword-form')
			{
				echo UActiveForm::validate($model);
				Yii::app()->end();
			}
                        
                        $check_step = LogStudentStepBase::model()->find("user_id='$user_id'");                                  
                         if (empty($check_step)) {
                            $check_step = new LogStudentStepBase();
                        }
                        
			
			if(isset($_POST['UserChangePassword'])) {
					$model->attributes=$_POST['UserChangePassword'];
					if($model->validate()) {
						$new_password = User::model()->notsafe()->findbyPk(Yii::app()->user->id);
						$new_password->password = UserModule::encrypting($model->password);
						$new_password->activkey=UserModule::encrypting(microtime().$model->password);
                                                $new_password->save(false);//model user                             
                                              
                                                
						Yii::app()->user->setFlash('profileMessage',UserModule::t("New password is saved."));
						
                                                 
                                                 
                                                 $check_step->is_change_password = 1;                                 
                                                 $check_step->save();
                                                
                                                 
                                                 if($check_step->is_edit_profile==0){
                                                      $success_link=Yii::app()->createUrl("user/profile/edit");
                                                 }else{                                                     
                                                     
                                                    $model_user = Users::model()->findByPk($user_id); 
                                                    $model_user->status=1;
                                                    $model_user->save(false);                                         
                                                    $success_link=Yii::app()->createUrl("user/profile/changepassword");
                                                 }
                                                 $this->redirect($success_link);
                                                 
                                                 
                                               
                                                 
                                                 
                                                 
					}
			}
			$this->render('changepassword',array('model'=>$model,"check_step"=>$check_step));
	    }
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the primary key value. Defaults to null, meaning using the 'id' GET variable
	 */
	public function loadUser()
	{
		if($this->_model===null)
		{
			if(Yii::app()->user->id)
				$this->_model=Yii::app()->controller->module->user();
			if($this->_model===null)
				$this->redirect(Yii::app()->controller->module->loginUrl);
		}
		return $this->_model;
	}
}