<?php

class RegistrationController extends Controller
{
	public $defaultAction = 'registration';
	//public $layout='//layouts/coursecreek';
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}
	/**
	 * Registration user
	 */
	public function actionRegistration() {  
      
            SiteHelper::setMetaTag("description","Register");
            SiteHelper::setMetaTag("keyworg","Register");
                 
            
            $model = new RegistrationFormDoctor;
            $profile=new Profile;
            $profile->regMode = true;
            
			// ajax validator
			if(isset($_POST['ajax']) && $_POST['ajax']==='regis')
			{
				echo UActiveForm::validate(array($model,$profile));
				Yii::app()->end();
			}
			
		    if (Yii::app()->user->id) {
		    	//$this->redirect(Yii::app()->controller->module->profileUrl);
                         $home_url=Yii::app()->createUrl("site/index");
                         $this->redirect($home_url);
                        
		    } else {
		    	if(isset($_POST['RegistrationFormDoctor'])) {
                                     
					$model->attributes=$_POST['RegistrationFormDoctor'];
                                        
//                                        echo "<pre>";
//                                        print_r($model->attributes);                                        
//                                        exit;
                                         
                                        /*not in extension*/
//                                         $model->username=$model->email;
                                   
                                         
					$profile->attributes=((isset($_POST['Profile'])?$_POST['Profile']:array()));
//                                            if($model->validate()&&$profile->validate())
                                       
                                        
                                        if($model->validate())
					{
                                         
                                                
                                            
                                                $soucePassword = $model->password;
						$model->activkey=UserModule::encrypting(microtime().$model->password);
						$model->password=UserModule::encrypting($model->password);
						$model->verifyPassword=UserModule::encrypting($model->verifyPassword);
						$model->superuser=0;
						$model->status=((Yii::app()->controller->module->activeAfterRegister)?User::STATUS_ACTIVE:User::STATUS_NOACTIVE);
                                                
                                                /*not in extension*/
                                               
						$model->status="1";
						if ($model->save()) {
							$profile->user_id=$model->id;
                                                        /* save with not validate */
                                                        
                                                        Yii::app()->user->setState('username',$model->email);                                                        
                                                        
                                                        $profile->save(false);
//							$profile->save();
//							
							$model_user_perrmission=  new UserPermission();
                                                        $model_user_perrmission->user_id=$model->id;
                                                        $model_user_perrmission->save();
                                                        
                                                        Yii::app()->user->id=$model->id;
                                                        $home_url=Yii::app()->createUrl("site/index");
                                                        $this->redirect($home_url);
                                                        

						}
					} else $profile->validate();
				}
			    $this->render('/user/registration',array('model'=>$model,'profile'=>$profile));
                         // $this->render('/user/login',array("mode"=>"regis"));       
		    }
	}
        
        
        
        public function actionRegistrationAjax() {  
            $this->layout='//layouts/popup';
            SiteHelper::setMetaTag("description","Register");
            SiteHelper::setMetaTag("keyworg","Register");
                 
            
            $model = new RegistrationFormDoctor();
            $profile=new Profile;
            $profile->regMode = true;
            
			// ajax validator
			if(isset($_POST['ajax']) && $_POST['ajax']==='regis')
			{
				echo UActiveForm::validate(array($model,$profile));
				Yii::app()->end();
			}
			
		    if (Yii::app()->user->id) {		    
                         $home_url=Yii::app()->createUrl("site/index");
                         $this->redirect($home_url);
                        
		    } else {		    	
			 $this->render('/user/registration_ajax',array('model'=>$model,'profile'=>$profile));
                         
		    }
	}
        
        
        public function actionSubmitRegister(){
            
        }
        
}