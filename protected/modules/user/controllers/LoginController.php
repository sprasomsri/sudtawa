<?php
class LoginController extends Controller
{
	public $defaultAction = 'login';
        //public $layout='//layouts/coursecreek';
	/**
	 * Displays the login page
	 */
	public function actionLogin($message="",$inbox=FALSE)
	{       
                            
                if($inbox==TRUE){
                    $this->layout='//layouts/popup';
                }
            
		if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			// collect user input data
			if(isset($_POST['UserLogin']))
			{                            
                                        
				$model->attributes=$_POST['UserLogin'];
                                
                                if(!empty($_POST['rememberMe']))
                                $model->rememberMe=$_POST['rememberMe'];
				// validate user input and redirect to previous page if valid
                                
                             
                            if($model->validate()) {
                                $this->CheckApprove();
                                                              
                            if (Yii::app()->user->returnUrl == '/index.php') {
                                 $this->redirect(Yii::app()->controller->module->returnUrl);
                            } else {
                                $user_id = Yii::app()->user->id;
                                $is_have_course=  UserCourse::model()->Count("user_id='$user_id'");
                                
                                
                                if(!empty($_POST['is_box']) && $_POST['is_box']=="1"){
                                    $inbox=TRUE;
                                }
                                
                                if($is_have_course==0){
                                    
                                     if($inbox==TRUE && !empty(Yii::app()->user->returnUrl)){                                        
                                        $this->redirect(Yii::app()->user->returnUrl);
                                    }else{                                        
                                        $home_url=Yii::app()->createUrl("site/index");
                                        $this->redirect($home_url);
                                    }
                                }else{
//                                    if(!empty(Yii::app()->user->returnUrl)){
//                                        $this->redirect(Yii::app()->user->returnUrl);
//                                    }else{
                                        $dashboard_url=Yii::app()->createUrl("user/dashboard/");
                                        $this->redirect($dashboard_url);
//                                    }
                                }
                            }
                        }
                                
			}
			// display the login form
                        SiteHelper::setMetaTag("description","coursecreek's login");
                        SiteHelper::setMetaTag("keyworg","coursecreek's login");
                 
                                              
                       $this->render('/user/login',array("mode"=>"login","model"=>$model,"message"=>$message,"inbox"=>$inbox));
                        
		} else
			$this->redirect(Yii::app()->controller->module->returnUrl);
	}
        
        
        public function actionLoginAjax($inbox=TRUE,$course_id=""){
            
            if($inbox==TRUE){
                    $this->layout='//layouts/popup';
             }
            
		if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			
			// display the login form
                        SiteHelper::setMetaTag("description","coursecreek's login");
                        SiteHelper::setMetaTag("keyworg","coursecreek's login");
                 
                                              
                       $this->render('/user/login_ajax',array("mode"=>"login","model"=>$model,"inbox"=>$inbox,"course_id"=>"$course_id"));
                        
		} 
        }
        
        
        public function actionValidateLoginAjax(){           
            if(!empty($_POST['username']) || !empty($_POST['password'])){
                $model=new UserLogin;
                $model->username=$_POST['username'];
                $model->password=$_POST['password'];
                                       
                if(!empty($_POST['remember'])){
                    $model->rememberMe=$_POST['remember'];
                }
                
                if($model->validate()) {
                  $this->lastViset();
                   echo "success";
                }else{
                    echo "fail";
                }
               
            }else{                
                 echo "fail";
            }            
        }

        
            private function CheckApprove() {
                $model_user = Users::model()->findByPk(Yii::app()->user->id);
                $model_user->lastvisit = date("Y-m-d H:i:s");
		$model_user->save();
                
                
                
                if($model_user->is_student==1 && $model_user->status==0){
                    $check_step = LogStudentStepBase::model()->find("user_id='$model_user->id'");
                    if(empty($check_step)){
                        $check_step = new LogStudentStepBase();
                        $check_step->user_id = $model_user->id;
                        $check_step->is_change_password = 0;
                        $check_step->is_edit_profile=0;
                        $check_step->save();
                    }
                    
                    if($check_step->is_change_password==0 || $check_step->is_edit_profile==0){
                            if($check_step->is_change_password==0){
                                $this->redirect(Yii::app()->createUrl("user/profile/changepassword"));
                            }
                            
                            if($check_step->is_edit_profile==0){
                                  $this->redirect(Yii::app()->createUrl("user/profile/edit"));
                            }
                            
                            
                        }
                    }
                
                
             
	}
        
        
        

}