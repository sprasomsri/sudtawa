<?php

class PermissionController extends Controller
{
    public function actionIndex()
    {
        Yii::app()->clientScript->registerCoreScript('jquery');
        $is_super_admin= Yii::app()->user->getState("superadmin");
        
        if($is_super_admin!=1){
//             throw new CHttpException(404,'The requested page does not exist.');
            echo "can not access"; exit;
        }
        
        
        $criteria = new CDbCriteria;
        $criteria->select="t.*";
        $criteria->order="id DESC";
                
        
        if(isset($_POST['btn-submit'])){
          if(!empty($_POST['email'])){
              $criteria->condition="username like '%".$_POST['email']."%'";
          }
        }
        
        $users = new CActiveDataProvider('Users', array(
            'pagination' => array('pageSize' => "10"),
            'criteria' => $criteria,
        ));
              
        $this->render("index",array("users"=>$users));
        
     }
     
     
     
     public function actionSetSuper(){
         $user_id=$_POST['user_id'];
         $model_user = Users::model()->findByPk($user_id);
         if($model_user->superuser==1){
             $model_user->superuser=0;
         }else{
              $model_user->superuser=1;
              $model_user->is_inside=1;
              $model_user_permission = UserPermission::model()->find("user_id='$user_id'");
              
              if(empty($model_user_permission)){
                  $model_user_permission= new UserPermission();
                  $model_user_permission->user_id=$user_id;
              }              
              
              $model_user_permission->can_create_course=1;
              $model_user_permission->save(false);
              
         }
          
         $model_user->save(false);        
         echo $model_user->superuser;
         
         
     }
     
     public function actionSetTeacher(){
         $user_id = $_POST['user_id'];
         $model_user_permission = UserPermission::model()->find("user_id='$user_id'");
         $model_user = Users::model()->findByPk($user_id);
         
         if(empty($model_user_permission)){
                  $model_user_permission= new UserPermission();
                  $model_user_permission->user_id=$user_id;
                  $model_user_permission->can_create_course=1;
                  $model_user_permission->save(false);
                  
              
                  $model_user->is_inside=1;
                  $model_user->save(false); 
                  
                  
         }else{
             
             if($model_user_permission->can_create_course==0){
                  $model_user_permission->can_create_course=1;
                  $model_user_permission->save(false);
                  $model_user->is_inside=1;
                  $model_user->save(false); 
                                 
             }else{
                  $model_user_permission->can_create_course=0;
                  $model_user_permission->save(false);                  
             }   
             
         }
         
         echo $model_user_permission->can_create_course;
     }
     
     
     public function actionSetInside(){
         $user_id = $_POST['user_id'];
         $model_user = Users::model()->findByPk($user_id);
         if($model_user->is_inside==1){
             $model_user->is_inside=0;
         }else{
              $model_user->is_inside=1;
         }
         $model_user->save(false);
         echo  $model_user->is_inside;
         
     }
     
     public function actionStdAccount(){

         
         $sql_student = "SELECT * FROM tbl_users WHERE is_student='1' AND id NOT IN (SELECT user_id FROM user_permission WHERE can_create_course='1')";
         
         $model_list_student = User::model()->findAllBySql($sql_student);
         

         $this->render("stdAccount",array("model_list_student"=>$model_list_student));
         
     }
     
     public function actionOperateStudent($student_id=""){
         
         
         if(!empty($student_id)){
                $model_regist = UsersStudent::model()->findByPk($student_id);
                $profile=  ProfilesEdit::model()->find("user_id='$student_id'");
                $old_password =$model_regist->password; 
                $mode = "edit";
         }
         
         if(empty($student_id) || empty($model_regist)){
                $model_regist = new UsersStudent;
                $profile = new ProfilesEdit;
                $old_password = "";
                $mode = "add";
         }
         
          if(isset($_POST['UsersStudent'])) {
            $model_regist->attributes = $_POST['UsersStudent'];
            
               
            if($model_regist->password!=$old_password){
               
                $soucePassword = $model_regist->password;
                $model_regist->activkey=UserModule::encrypting(microtime().$model_regist->password);
                $model_regist->password=UserModule::encrypting($model_regist->password);
             
            }
            
            
            if($model_regist->validate()){
                $model_regist->is_student = 1;                
                $model_regist->save(false);
                
                if($mode=="add"){
                    $model_regist->status = 0;
                }
                
                if(isset($_POST['ProfilesEdit'])){
                    $profile->attributes=$_POST['ProfilesEdit'];
                    $profile->user_id = $model_regist->id;
                    $profile->save();
                }
                
                $model_permission = UserPermission::model()->find("user_id='$model_regist->id'");
                if(empty($model_permission)){
                    $model_permission = new UserPermission;
                    $model_permission->user_id = $model_regist->id;
                    $model_permission->can_create_course = 0;
                    $model_permission->save(false);
                }
               
                
                
                $this->redirect(Yii::app()->createUrl("admin/permission/StdAccount")); 
                
                
            }
         }   
         
          $this->render("form_student",array("model_regist"=>$model_regist,"profile"=> $profile));
         
     }
     
     public function actionSetActive($student_id,$is_active){
         if($is_active==99){
             $set_active =1;
         }else{
             $set_active=99;
         }
         
         $user_id = $student_id;
         $model_user = Users::model()->findByPk($student_id);
         $model_user->status = $set_active;
         $model_user->save(false);
         $this->redirect(Yii::app()->createUrl("admin/permission/StdAccount")); 
         
         
         
     }

 
}