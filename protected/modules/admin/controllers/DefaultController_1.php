<?php

class DefaultController extends Controller
{
        public $layout='//layouts/backend';
    
	public function actionIndex()
	{
		$this->render('index');
	}
        
        #for check Admin who can edit Course
        public function checkOwner($course_id){
             $user_id = Yii::app()->user->id;
             $model_user_course=  UserCourse::model()->find("course_id='$course_id' AND user_id='$user_id'");
             if(!empty($model_user_course)){
             if($model_user_course->role!=="1" && $model_user_course->role!="2"){
                 
                 if(!empty($return_url)){
                     //get return url 
                     $destination=Yii::app()->controller->module->returnUrl;
                 }else{
                     $destination=Yii::app()->createUrl("/admin/default/ownCourse");
                                     
                 }
                 
                 echo "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
                 echo "<script>alert('คุณไม่ได้เป็นเจ้าของ course นี้!!!')
                          window.location='$destination';
                          </script>";
             }
           }else{
                 echo "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
                 $destination=Yii::app()->createUrl("site/error");
                 echo "<script>alert('ไม่สามารถเข้า URL นี้')
                          window.location='$destination';
                          </script>";
           }
        }
        
        #Query Owner Course 
        public function actionOwnCourse() {
            
        $this->layout='//layouts/main';    
            
        $user_id = Yii::app()->user->id;
        
        $criteria = new CDbCriteria;
        $criteria->select="t.*";
        $criteria->join ="INNER JOIN user_course ON t.id=user_course.course_id";
        $criteria->group="t.id";
        $criteria->condition="(user_course.role='1' OR user_course.role='2') AND user_course.user_id='$user_id'";
        $criteria->order="id DESC";
        
        $courses = new CActiveDataProvider('Course', array(
            'pagination' => array('pageSize' => "7"),
            'criteria' => $criteria,
        ));

        $cout_course=  Course::model()->Count($criteria);
       $this->render('own_course',array('courses' => $courses,'mode'=>"admin","cout_course"=>$cout_course));
     }
    
     
    #Add And Edit Course Information (course detail)
    public function actionOperateCourse($id = "") {
          
        $this->layout='//layouts/main_backend';  //no script

        if (!empty($id)) {
            $this->checkOwner($id);
            $model_course = Course::model()->findByPk($id);
            $mode="edit";
            $old_img=$model_course->course_img;            
            if(!empty($model_course->demo_video)){
                $demoVideo=  File::model()->findByPk($model_course->demo_video);
            }
            
        } else {
            $model_course = new Course();
            $mode="new";
          
        }              
        $list_all_teacher = SiteHelper::getTeacherList();
        $selected_teacher = array();

        if (!empty($model_course->id)) {
            $selected_teacher = SiteHelper::getTeacherList($model_course->id,"getuser_id");                       
        }
        
        if(empty($demoVideo)){
            $demoVideo="";
        }
        
          $user_id = Yii::app()->user->id;
          
        
        if (isset($_POST['Course'])) {         
            
            $model_course->attributes = $_POST['Course'];      
            $model_course->is_public= $_POST['Course']['is_public'];
             $model_course->is_affiliate= $_POST['Course']['is_affiliate'];
            $list_teacher=$_POST['teacherList'];
            $model_course->user_id=$user_id;
                   
            if ($model_course->validate()) { 
               
                $img_file = CUploadedFile::getInstance($model_course, 'course_img');               
                if (!empty($img_file)) {
                    $random = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz0123456789'), 0, 100);
                    
                    //save original                   
                    $img_file->saveAs(Yii::app()->basePath . '/../webroot/course/original/' . "$random" . "_" . CUploadedFile::getInstance($model_course, 'course_img'));
                    
                    //resize img
                    $file_to_resize = "course/original/".$random."_".$img_file;                   
                    $file_to_resize = Yii::app()->simpleImage->load($file_to_resize);
                    // $file_to_resize->resize(350,160);
                    $file_to_resize->resizeToWidth(350);
                    $file_to_resize->save("course/".$random."_".$img_file);
                    
                    $model_course->course_img = $random."_".$img_file;
                    
                        
                    //Delete Old Photo                    
                    if($mode=="edit" && !empty($old_img)){
                         $old_img_path= Yii::app()->basePath."/../webroot/course/original/".$old_img;
                         if (file_exists($old_img_path)) {
                             unlink($old_img_path);
                         }                                
                    }
            }
                  
                $model_course->save();
                
         if(!empty($list_teacher)){
             $just_save_teacher=array(); 
               foreach ($list_teacher as $teacher){
                   if(in_array($teacher, $selected_teacher)){
                       //do nothing
                       $just_save_teacher[$teacher]=$teacher;
                   }else{                   
                       $model_user_course=new UserCourse();
                       $model_user_course->course_id=$model_course->id;
                       $model_user_course->user_id=$teacher;
                       $model_user_course->role=2;
                       $model_user_course->create_date=date("Y-m-d H:i:s");
                       $model_user_course->save();
                       $just_save_teacher[$teacher]=$teacher;
                  }
               }
               //delete person who used to in list
               $used_to_list_teacher= array_diff_key($selected_teacher,$just_save_teacher);
               
               if(!empty($used_to_list_teacher)){
                                   
                    foreach($used_to_list_teacher as $old_teacher){                      
                        $used_to_list=  UserCourse::model()->find("user_id='$old_teacher' AND course_id='$model_course->id' AND role='2'");
                        if(!empty($used_to_list)){
                            $used_to_list->delete();
                        }
                   }
               }               
           }                
                //add owner of course
                $user_id = Yii::app()->user->id;
                $model_course_owner=  UserCourse::model()->find("user_id='$user_id' AND course_id='$model_course->id' AND role ='1'");
                if(empty($model_course_owner)){
                    $model_course_owner=new UserCourse();
                    $model_course_owner->user_id=$user_id;
                    $model_course_owner->course_id=$model_course->id;
                    $model_course_owner->role="1";
                    $model_course_owner->save();
                }                
                
                $success_link = Yii::app()->createUrl("/admin/default/manageContent/course/$model_course->id");
                $this->redirect($success_link);
            }
        }     
        $this->render("form_course", array("model_course" => $model_course,
                                            "user_id"=>$user_id,
                                            "list_all_teacher"=>$list_all_teacher,
                                            "selected_teacher"=>$selected_teacher,
                                            "demoVideo"=>$demoVideo));
    }
    
    #show page for manage content
    public function actionManageContent($course){
//       $this->layout='//layouts/backend';  //no script
       $this->checkOwner($course);
       Yii::app()->user->setState('content_id',""); //destory content it may has form add/edit new content
       $model_course = Course::model()->findByPk($course);
       $model_parent_content=  Content::model()->findAll("parent_id='-1' AND course_id='$course' ORDER BY show_order ASC");
       $this->render("manage_content",array("model_parent_content"=>$model_parent_content,
                                            "course_id"=>$course,"model_course"=>$model_course));                   
    }
    
    
    #Add Edit vdo content | Unit parent_id -1 >>> lesson !=1|  
     public function actionOperateVideoContent($id = "", $course, $type = "unit",$code_type="") {
    
        $this->layout='//layouts/main_backend';  //no script
        
        $this->checkOwner($course);
        if (!empty($id)) {
            $model_content = Content::model()->findByPk($id);
     
            
            $model_vdo=  FileVdo::model()->findByPk($model_content->file_id);          
            
            if(empty($model_vdo)){
                $model_vdo=new FileVdo();
            }
            
            $old_vdo=$model_vdo->name;
            $mode = "edit";
        } else {
            $model_content = new Content();
            $model_vdo=  new FileVdo();
            $mode = "add";
        }
        
        $model_code="";
        if($type=="code"){
             if(!empty($id)){
                $model_code=  CodeEditorInContent::model()->find("content_id='$id'");
                $code_type= $model_code->type;
            }
            if(empty($model_code)){
                $model_code= new CodeEditorInContent();
            }
        }
        
        
        $model_course = Course::model()->findByPk($course);
        $user_id = Yii::app()->user->id;
        
 
        $this->render("form_content", array("model_content" => $model_content, 
                                            "type" => $type,
                                            "model_course"=>$model_course,
                                            "model_vdo"=>$model_vdo,
                                            "model_code"=>$model_code,
                                            "code_type"=>$code_type
                                            ));
    }
    
    public function getContentOrder($course){
        $coun_content=  Content::model()->Count("course_id='$course'");
        $new_order=$coun_content+1;
        return $new_order;        
    }
    
    
     
   #save content ajax
   #save content ajax
   public function actionSaveContent($id = "", $course, $type = "unit") {
              
       $contentId = Yii::app()->user->getState('content_id');
        if (!empty($id) || !empty($contentId)) {
            if(empty($id)){ $id=$contentId; }
            
            $model_content = Content::model()->findByPk($id);
            $model_vdo = FileVdo::model()->findByPk($model_content->file_id);

            if (empty($model_vdo)) {
                $model_vdo = new FileVdo();
            }

            $old_vdo = $model_vdo->name;
            $mode = "edit";
        } else {
                      
            $model_content = new Content();
            $model_vdo = new FileVdo();
            $mode = "add";
        }
        
        $model_code="";
        if($type=="code"){
            if(!empty($id)){
                $model_code=  CodeEditorInContent::model()->find("content_id='$id'");
            }
            if(empty($model_code)){
                $model_code= new CodeEditorInContent();
            }
            $model_content->is_code=1;       
            $model_content->is_exercise=0;           
        }      

        $model_course = Course::model()->findByPk($course);
        $user_id = Yii::app()->user->id;
  
        if (isset($_POST['Content'])) {
           
            $model_content->attributes = $_POST['Content'];
            $model_content->course_id = $course;

            if(!empty($_POST['Content']['is_free'])){
                $model_content->is_free= (int)$_POST['Content']['is_free'];
            }
            
            if($mode=="add"){
                $qty_content= Content::model()->count("course_id='$course'");
                $model_content->show_order=$qty_content+1;
            }
            
            
            if ($type == "unit") {
                $model_content->parent_id = (int) "-1";
            }
          
            if ($model_content->validate()) {
                $model_content->save();   
                $code_post="";
                if(!empty($_POST['code'])){
                    $code_post=$_POST['code'];
                }
                if(!empty($code_post)){
                    $model_code->content_id=$model_content->id;
                    $model_code->user_id=Yii::app()->user->id;
                    
                    if(!empty($_POST['code_position']) && !empty($_POST['code_type'])){
                        $code_post= str_replace(array("<", ">"),array("&lt;", "&gt;"), htmlspecialchars($code_post, ENT_NOQUOTES, "UTF-8")); 
                    }         
                    
                    $model_code->code=$code_post;
                    
                    $model_code->type=$_POST['code_type'];
//                    $model_code->position=$_POST['code_position'];
                    $model_code->save(false);
                }         
                
                Yii::app()->user->setState('content_id',$model_content->id);
                
                $url=Yii::app()->createUrl("admin/default/uploadVideo",array("contentId"=>$model_content->id));
                $json = array("id" => $model_content->id, "is_validate" => "1","type"=>$type,"url"=>$url);
                $json=json_encode($json);
                
                echo $json;
            } else {
               
                $errors = $model_content->getErrors();
                
                if (!empty($errors)) {
                    $html = "<ul>";
                    foreach ($errors as $error_field) {
                        foreach($error_field as $error){
                         $html.="<li>" . $error . "</li>";
                        }
                    }
                    $html.="</div>";
                    $json=array("error"=>$html,"is_validate"=>"0");
                    $json=json_encode($json);
                    
                    echo $json;
                    
                    
                }
            }
        }
    }
    
   
//    public function actionSaveVideoContent(){
//          $content_id=Yii::app()->user->getState('content_id');
//    }

    

    public function actionSaveVideoContent(){
    
       if(empty($content_id)){
           $content_id=Yii::app()->user->getState('content_id');
       }
       
      
        $user_id = Yii::app()->user->id;
        
        
         if (!empty($_FILES) && !empty($content_id)) {
          
                 
            $model_content = Content::model()->findByPk($content_id);
      
            $model_vdo = FileVdo::model()->findByPk($model_content->file_id);
            
           
            
            if(empty($model_vdo)){
                $model_vdo=new FileVdo();
            }else{
                 $old_vdo=$model_vdo->name;
            }
            
            
            $targetFolder = Yii::app()->basePath . "/../webroot/files/$user_id/video/";
            if (!is_dir($targetFolder)) {
               mkdir($targetFolder, 0775, true);
            }
            
            $file_original = strtolower($_FILES['Filedata']['name']);
            $tempFile = trim($_FILES['Filedata']['tmp_name']);
            $targetFileOriginal = Yii::app()->basePath . "/../webroot/files/$user_id/video/" . $file_original;
             
            $fileTypes = array('webm','mp4','3gpp','mov','avi','wmv','flv','mpeg');// File extensions
            $fileParts = pathinfo($_FILES['Filedata']['name']);
            
            //save file 
            if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
                move_uploaded_file($tempFile, $targetFileOriginal);
                
                $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz1234567890'), 0, 5);
                $new_file_name = "coursecreek" . $random .$file_original;
                $time = exec("ffmpeg -i ".$targetFileOriginal." 2>&1 | grep Duration | cut -d ' ' -f 4 | sed s/,//");
                
                $reserveString=array(","," ","(",")","<",">");
                $replace=array("","","","","","");
                
                
                $new_file_name=  str_replace($reserveString,$replace, $new_file_name);        
                
                  
                # Generate thumbnail -------------------------------------------
                    $thumbnail_name=  VdoHelper::generateVideohTumbnailname($new_file_name,$fileParts['extension']);
                    $thumbnail_vdo = Yii::app()->basePath . "/../webroot/files/$user_id/video/".$thumbnail_name;
                    VdoHelper::generateVideoThumbnail($targetFileOriginal, $thumbnail_vdo);
                    
                    
                # encode Video -------------------------------------------------
                   $encode_file_save = Yii::app()->basePath . "/../webroot/files/$user_id/video/".$new_file_name;
                   VdoHelper::encodeVideoH264($targetFileOriginal, $encode_file_save);  //encode and unlink original vdo
                   
               $model_vdo->duration= $time;
               $model_vdo->user_id = $user_id;
               $model_vdo->type = "video";                
               $model_vdo->original_name = $file_original;
               $model_vdo->create_at=date("Y-m-d H:i:s");    
               $model_vdo->name = $new_file_name;
               
             
               
               
               if($model_vdo->save(false)){
                   //unlink and save content
//                  $model_vdo->save(); 
                  $model_content->file_id = $model_vdo->id;
                  
                  
                  if (!empty($old_vdo)) {

                            $old_file_path = Yii::app()->basePath . "/../webroot/files/$user_id/video/" . $old_vdo;
                            if (file_exists($old_file_path) && !empty($old_vdo)) {
                                unlink($old_file_path);
                            }
                  }                  
                  $model_content->save();
                   
               }               
                  
                echo 'Success Upload';
                echo $file_original;
            } else {
                echo 'Invalid file type.';
            }
            
            
         }else{
             echo "nofile or not assign content id";
         }
             
    }

    public function actionUploadVideo($contentId="") {
         //return json
       if(empty($contentId)){
             $contentId = Yii::app()->user->getState('content_id');
       }         
        $model_content = Content::model()->findByPk($contentId);
        $model_vdo = FileVdo::model()->findByPk($model_content->file_id);
      
         $user_id = Yii::app()->user->id;
         
        if(empty($model_vdo)){
            $model_vdo=new FileVdo();
        }else{
             $old_vdo=$model_vdo->name;
        }
        
        $user_id = Yii::app()->user->id;
        $old_file_id = $model_content->file_id;

       if (!empty($_FILES)) {           
           
            list($type_file,$type_vdo)=  explode("/", $_FILES['uploadedFile']['type']); 
            $file_type_avi = array("webm", "mp4", "3gpp", "mov", "avi", "wmv", "flv", "mpeg");
            if ($type_file !== "video" || (!in_array($type_vdo, $file_type_avi))) {
                
                $json = array("message" => "worng type should be >> webm 3gpp mov avi wmv flv OR mpeg","is_error"=>"1");
                $json=json_encode($json);
                echo $json;
                 
            }else{  
                     
                $target_path = Yii::app()->basePath . "/../webroot/files/$user_id/video";
                if (!is_dir($target_path)) {
                    mkdir($target_path, 0755, true);
                }

                $reserveString=array(","," ","(",")","<",">");
                $replace=array("","","","","","");
                $file_save=  str_replace($reserveString,$replace, $_FILES['uploadedFile']['name']);       
              
                
               $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263'), 0, 30);
               $new_name = $random . "_".$file_save;
               
               $path_save_original = Yii::app()->basePath . "/../webroot/files/$user_id/video/".$new_name;
                
                
                if (move_uploaded_file($_FILES['uploadedFile']['tmp_name'], $path_save_original)) {                     
                    $model_vdo->user_id = $user_id;
                    $model_vdo->type = "video";                
                    $model_vdo->original_name = $_FILES['uploadedFile']['name'];
                    $model_vdo->create_at=date("Y-m-d H:i:s");
                    
                     #file length ----------------------------------------------------
                    $file=$path_save_original;
                    $time = exec("ffmpeg -i ".$file." 2>&1 | grep Duration | cut -d ' ' -f 4 | sed s/,//");
                    $model_vdo->duration= $time;
                    

                      # Generate thumbnail -------------------------------------------                 
                        $thumbnail_name=  VdoHelper::generateVideohTumbnailname($new_name,$type_vdo);
                        $thumbnail_vdo = Yii::app()->basePath . "/../webroot/files/$user_id/video/$thumbnail_name";
                        VdoHelper::generateVideoThumbnail($path_save_original, $thumbnail_vdo);
    //                         
                     # encode Video -------------------------------------------------
                       $encode_file_save = Yii::app()->basePath . "/../webroot/files/$user_id/video/".$new_name;
                      // VdoHelper::encodeVideoH264($path_save_original, $encode_file_save);         


                    $model_vdo->name=$new_name;               


                    //validATE and save
                    if($model_vdo->validate()){
                         $model_vdo->save();
                          $message= "The file " . basename($_FILES['uploadedFile']['name']) . " has been uploaded";

                          if (!empty($old_vdo)) {

                            $old_file_path = Yii::app()->basePath . "/../webroot/files/$user_id/video/" . $old_vdo;
                            if (file_exists($old_file_path) && !empty($old_vdo)) {
                                unlink($old_file_path);
                            }
                        }
                          $model_content->file_id = $model_vdo->id;
                           $model_content->save();

                          $json = array("message" => $message,"is_error"=>"0");
                          $json=json_encode($json);
                          echo $json;                    


                    }else{                 

                       $errors=$model_vdo->getErrors();

                        $html = "<ul>";
                        foreach ($errors as $error_field) {
                            foreach($error_field as $error){
                             $html.="<li>" . $error . "</li>";
                            }
                        }
                        $html.="</ul>";
                        $json = array("message" => $html,"is_error"=>"1");  
                        $json=json_encode($json);
                        echo $json;  

                    }

                //unlink old file
            } else {

                    $json = array("message" => "uploading the fail, please try again!","is_error"=>"1");
                    $json=json_encode($json);
                    echo $json;
                }
            }
       }
            
    } 
    
   
    #check duplicat video
    public function checkDuplicateVideoFile($file_path, $user_id,$file_name,$random) {
     if (file_exists($file_path)) {
            $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263'), 0, 400);
            $fipath_temp=Yii::app()->basePath . "/../webroot/files/$user_id/video/" . $random . "_" .$file_name;
            #Recursive
            $this->checkDuplicateVideoFile($file_path,$random);
            
        } else {
            return $random."_".$file_name;
        }
    }    
    
    
    #Delete Unit And Lesson Type Video
    public function actionDeleteVideoContent($id){
        $model_content=  Content::model()->findByPk($id);
        $course_id=$model_content->course_id;
        
        $model_vdo=  FileVdo::model()->findByPk($model_content->file_id);
        if (!empty($model_vdo)) {
            $file_del = Yii::app()->basePath . "/../webroot/files/$model_vdo->user_id/video/" . $model_vdo->name;
            if (file_exists($file_del)) {
                unlink($file_del);
            }
             $model_vdo->delete();
        }
        
        $model_content->delete();
        $success_link = Yii::app()->createUrl("admin/default/manageContent/course/$course_id");
        $this->redirect($success_link);
           
    }
    
    #Show List of User(Student) in Course 
    public function actionStudentList($course){
         $this->layout='//layouts/main_backend';  //no script
         
        $this->checkOwner($course);
        $model_course = Course::model()->findByPk($course);
        $model_user_course=new UserCourse;
        $mode="student";         
        $this->render("student_list",array("model_course"=>$model_course,"mode"=>$mode,
                                          "dataProvider"=>$model_user_course->getRelatePeople($course, "3")));
    }
    
    
    #Show List of User(Teacher) in Course 
    public function actionTeacherList($course){
        $this->checkOwner($course);
        $model_course = Course::model()->findByPk($course);
        $model_user_course=new UserCourse;
        $mode="teacher";
                
        $this->render("teacher_list",array("model_course"=>$model_course,"mode"=>$mode,
                                          "dataProvider"=>$model_user_course->getRelatePeople($course,"1","2")));
    }
    
    #Add Std in course
    public function actionAddPersonToCourse($course,$type="3"){
//        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->checkOwner($course);
        $model_course = Course::model()->findByPk($course);
        $data_for_search=  SiteHelper::getPersonNotInCourse($course);
       
        $model_person = new UserQuery('search');
        $model_person->unsetAttributes();
        
        //Add more user
        if (isset($_POST['person']) && !empty($_POST['person'])) {
            $student_form = $_POST['person'];
           
            $model_user = User::model()->find("email='$student_form'");
            if (!empty($model_user)) {
                $user_id_to_save = $model_user->id;
            } else {
                list($name, $lastname) = explode(" ", $student_form);
              
                $model_user_profile = Profile::model()->find("firstname='$name' AND lastname='$lastname'");
                              
                if (!empty($model_user_profile)) {
                    $user_id_to_save = $model_user_profile->user_id;
                }
            }
         
            if (!empty($user_id_to_save)) {
              
                $model_user_course = UserCourse::model()->find("user_id='$user_id_to_save' AND course_id='$course'");
                    
                if (empty($model_user_course)) {
                    $model_user_course = new UserCourse();
                    $model_user_course->user_id = $user_id_to_save;
                    $model_user_course->course_id = $course;
                    $model_user_course->create_date = date("Y-m-d H:i:s");
                    $model_user_course->role=$type;
                    $model_user_course->save();
                         
                }
            }
            //search in grideview
            if (isset($_GET['UserQuery'])) {
                $model_person->attributes = $_GET['UserQuery'];
            }
        }  
        $this->render("form_person",array("model_course"=>$model_course,
                                            "data_for_search"=>$data_for_search,
                                            "model_person"=>$model_person,
                                            "type"=>$type,
                                            "dataProvider_people"=>$model_person->getPersonInCourse($course,$type)));
    }
    
    
    public function actionDeleteStudent($user_id,$course){
        $model_user=  UserCourse::model()->model()->find("user_id='$user_id' AND course_id='$course'");
        $model_user->delete();
        $success_link=Yii::app()->createUrl("admin/default/addPersonToCourse",array("course"=>$course));
        $this->redirect($success_link);        
    }

    #sort lesson and Unit
    public function actionSortContent() {
        $parent = "";
        $round = 1;
        $vaildate=true;
        if (!empty($_POST)) {
            foreach ($_POST as $content) {

                foreach ($content as $sort_order => $id_content) {
                    $model = Content::model()->findByPk($id_content);

                    #if make child above parent
                    if ($round == 1 && $model->parent_id != "-1") { 
                        $fail_link = Yii::app()->createUrl("/admin/default/manageContent/course/$model->course_id");
                        echo "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
                        echo "<script>alert('ขออภัยบทเรียนต้องอยู่ภายใต้บทเรียน');
                      window.location='$fail_link';
                      </script>";
                        $vaildate=false;
                                        
                    }

                    if ($model->parent_id == "-1") {
                        $model->show_order = $sort_order;
                        $parent = $id_content;
                    } else {
                        $model->parent_id = $parent;
                        $model->show_order = $sort_order;
                    }
                    if($vaildate==true){
                        $model->save();
                    }

                    $round++;
                }
            }
        }
        
    }
    
    #Video State
    public function actionVideoStat(){     
        
        $this->render("videoStat",array());
    }
    
    #show coupon list in each round
     public function actionCoupon($course) {
        $this->checkOwner($course);
        $model_course = Course::model()->findByPk($course);
        $user_id = Yii::app()->user->id;
        $model_user=  Profile::model()->findByPk($user_id);
        
        $model = new Coupon();
        $model->unsetAttributes();
        if (isset($_GET['Coupon']))
            $model->attributes = $_GET['Coupon'];

        $this->render('coupon', array('dataProvider' => $model->getRoundCoupon($course, $user_id),
                                      'model' => $model,
                                      'model_course'=>$model_course,
                                      'model_user'=>$model_user));
    }
    
    #Query coupon Inside group << use create_date for group
    public function actionViewCoupon($date, $course) {
        $this->checkOwner($course);
        $model_course = Course::model()->findByPk($course);
        $user_id = Yii::app()->user->id;

        $model = new Coupon();
        $model->unsetAttributes();
        if (isset($_GET['Coupon']))
            $model->attributes = $_GET['Coupon'];

        $this->render('view_coupon', array('dataProvider' => $model->getCouponInGroup($date, $course,$user_id),
            'model' => $model,
            'date'=>$date,
            'model_course' => $model_course));
    }
    

    #coupong form
    public function actionFormCoupon($course){
        $this->checkOwner($course);
        $model_course = Course::model()->findByPk($course);
        $model_coupon= new Coupon();
      
        
        if(isset($_POST['Coupon'])){
             $model_coupon->attributes = $_POST['Coupon'];            
             
             if(!empty($model_coupon->expire_date)){
                list($day,$month,$year)=  explode("/", $model_coupon->expire_date);
                $model_coupon->expire_date=$year."-".$month."-".$day;
             }
             $user_id=Yii::app()->user->id;
             $today=date("Y-m-d H:i:s");
             $model_coupon->create_date=$today;
             $model_coupon->user_id=$user_id;
             $model_coupon->code="1";
             
             $discount=$model_coupon->discount;
             $qty_use=$model_coupon->qty_use;
             $detail=$model_coupon->detail;
             $qty_gen=$_POST['quanlity'];
             $prefix=$_POST['prefix'];
                       
                      
             if($model_coupon->validate()){
                 
                 for($i=1;$i<=$qty_gen;$i++){
                    
                     $model_coupon= new Coupon();
                    
                     $model_coupon->create_date=$today;
                     $model_coupon->user_id=$user_id;
                     $model_coupon->expire_date=$year."-".$month."-".$day;
                     $model_coupon->discount=$discount;
                     $model_coupon->course_id=$course;
                     $model_coupon->qty_use=$qty_use;  
                     $model_coupon->detail=$detail;
                     
                     if(empty($prefix)){
                         $prefix=(int)date("m")+$i;
                     }
                     
                     $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263ABCDEFGHIJKLMNOPQRSTUVWXYZ@#$%&'), 0, 5);
                     $random2 = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263ABCDEFGHIJKLMNOPQRSTUVWXYZ@#$%&'), 0, 3);
                     $datesecond=date("s")+$i;
                     $code=$user_id.$random.$i.$datesecond.$random2;
                           //user_id-Random5digit-order-second-random
                     $code = substr(str_shuffle($code), 0, 15);
                     $code =$prefix.$code;
                  
                     
                     $model_coupon->code=$code;
                     $model_coupon->save();
                    
                     
                 }
                     
                 $success_link = Yii::app()->createUrl("admin/default/directionCoupong/course/$course/date/+$today+");
                 $this->redirect($success_link);
             }
        }
        
        $this->render("form_coupong",array("model_course"=>$model_course,
                                           "model_coupon"=>$model_coupon));
        
    }
    
    #after create coupon show option link for select
    public function actionDirectionCoupong($course,$date){
         $this->checkOwner($course);
         $model_course = Course::model()->findByPk($course);
         $this->render("direction_coupon",array("model_course"=>$model_course,"date"=>$date));
        
    }
    
    
    #print coupong in same round <<< use $date to grooup data
    public function actionPrintCoupon($date,$course) {
        $this->layout = "//layouts/print";
        $this->checkOwner($course);
      
        
        $model_course= Course::model()->findByPk($course);
        $criteria = new CDbCriteria;
        $criteria->condition = "create_date='$date'";
        
        $qty_coupon=  Coupon::model()->Count($criteria);
        

        $dataProvider = new CActiveDataProvider('Coupon', array(
            'pagination'=>false,
            'criteria' => $criteria,
        ));


        $this->render("print_coupon", array("dataProvider"=>$dataProvider,
                                            "model_course"=>$model_course,
                                            "qty_coupon"=>$qty_coupon));
    }
    
    #before course And AfterCourse
    public function actionRelateCourse($course,$message=""){
          $this->layout='//layouts/main_backend';  //no script
        $model_course=  Course::model()->findByPk($course);
        
        $model_course_all = Course::model()->findAll("id!='$course' AND id!='-1'");
     
        $all_course=array();
        if(!empty($model_course_all)){
            foreach ($model_course_all as $course_list){
                $all_course[$course_list->id]=$course_list->name;
           }
        }
        
        $relateCourse=  RelateCourse::model()->find("course_id='$course'");
        $before_course=array();
        $after_course=array();
        
        if(!empty($relateCourse)){
            
            if(!empty($relateCourse->before_course)){
                $before_course= explode(",", $relateCourse->before_course);
            }
           
            if(!empty($relateCourse->after_course)){
                $after_course= explode(",", $relateCourse->after_course);
            }
            
        }
        
        
        if(isset($_POST['beforeList'])||isset($_POST['afterList'])){
            
         
            
             $before_course=$_POST['beforeList'];
             $after_course=$_POST['afterList'];
            
             if(empty($relateCourse)){
                 $relateCourse=new RelateCourse();
             }
             
             $before_course_list="";
             $after_course_list="";
             
             if(!empty($before_course)){
                foreach($before_course as $before_course_id){
                     $before_course_list.=$before_course_id.",";
                 }
                 $before_course_list=rtrim($before_course_list,',');
             }
             
             if(!empty($after_course)){
                 foreach ($after_course as $after_course_id){
                     $after_course_list.= $after_course_id.",";
                 }
                 $after_course_list=  rtrim($after_course_list,',');
             }
             
             
            $relateCourse->course_id=$course;
            $relateCourse->before_course=$before_course_list;
            $relateCourse->after_course=$after_course_list;
            $relateCourse->save();        
            
            $success_url=  LinkHelper::yiiLink("admin/default/relateCourse/course/$course/message/Data has been saved.");
            $this->redirect($success_url);
            
                      
        }
               
        
        $this->render("form_relate_course",array("model_course"=>$model_course,
                                                "all_course"=>$all_course,
                                                "before_course"=>$before_course,
                                                "after_course"=>$after_course,
                                                "message"=>$message));
        
    }
    
    #Exercise 1st step
#Exercise 1st step
    public function actionOperateExercise($id="",$course,$type="lesson"){
        $this->layout='//layouts/main_backend'; //no script
        
        $model_course=  Course::model()->findByPk($course);
        $model_content=  Content::model()->findByPk($id);
        $model_exercise= Exercise::model()->find("content_id='$id'");
        
        $qty_question=0;
        if(!empty($model_exercise)){
        $qty_question= count($model_exercise->question);
        }
        
        
        if(empty($model_content)){
            $model_content=new Content();
        }
        
        if(empty($model_exercise)){
            $model_exercise=new Exercise();
        }
        
        if(isset($_POST["Content"])||isset($_POST["Exercise"])){
              $model_content->attributes = $_POST['Content'];
              $model_exercise->attributes = $_POST['Exercise'];
                   
              $model_content->is_exercise="1";
              $model_content->course_id=$course;
              if(empty($id)){
              $model_content->show_order=$this->getContentOrder($course);
              }
               
             if($model_content->validate()){
                   $model_content->save();
                    
                   $model_exercise->content_id=$model_content->id;
                   $model_exercise->save();

                   $success_url=  LinkHelper::yiiLink("admin/default/showQuestion/exercise/$model_exercise->id/course/$course");                    
                   $this->redirect($success_url);
               }               
        }
                
       $this->render("form_exercise",array("model_content"=>$model_content,
                                           "model_course"=>$model_course,
                                           "model_exercise"=>$model_exercise,
                                           "qty_question"=>$qty_question));      
    }    
    
    //order Question
    public function actionShowQuestion($exercise,$course){
       $model_course=  Course::model()->findByPk($course);
       $model_all_question=  ExerciseQuestion::model()->findAll("exercise_id='$exercise' ORDER BY show_order ASC,id ASC");
       $model_exercise=  Exercise::model()->findByPk($exercise);
       $model_content=  Content::model()->findByPk($model_exercise->content_id);
       
       $this->render("show_question",array("model_course"=>$model_course,
                                            "model_all_question"=>$model_all_question,
                                            "model_content"=>$model_content,
                                            "model_exercise"=>$model_exercise));
       
    }
    
        
    //add Edit Question and answer
    #type  >>  1 multichoice ,2 Text, 3 TwoFalse 
    
    public function actionOperateQuestion($exercise,$course,$type="1",$question="",$message=""){
       $this->layout='//layouts/main_backend'; //no script
       $model_course=  Course::model()->findByPk($course);
       $model_all_question=  ExerciseQuestion::model()->findAll("exercise_id='$exercise' ORDER BY show_order ASC,id ASC");
       $qty_question=count($model_all_question);
       
               
        if(!empty($question)){
            $mode="edit";
            $model_question=  ExerciseQuestion::model()->findByPk($question);
            $answer_all= ExerciseAnswerChoice::model()->findAll("question_id='$model_question->id' ORDER BY is_true DESC,id ASC");                     
        
            if(!empty($answer_all)){
                $i=0;
                foreach ($answer_all as $answer){
                   $list_answer[$i]=$answer;
                   $i++;                  
                }
            }
            
        }else{
            
             $mode="new";
             $model_question= new ExerciseQuestion();
            
             if($type=="3"){ //true false question
                 for($i=0;$i<=1;$i++){
                       $list_answer[$i]=new ExerciseAnswerChoice();
                 }  
             }else{
                for($i=0;$i<=3;$i++){
                       $list_answer[$i]=new ExerciseAnswerChoice();
                 }          
             }
        }
        
        if(isset($_POST['ExerciseQuestion'])||isset($_POST['ExerciseAnswerChoice'])){
            $is_pass=true;
            
            $model_question->attributes = $_POST['ExerciseQuestion'];
            $model_question->exercise_id=$exercise;
            $model_question->type=$type;
            
            $answer_post=$_POST['ExerciseAnswerChoice'];
            $count_answer=0;
          
 
            foreach($answer_post as $key=>$answer){
                      
                if(empty($list_answer[$key])){
                    $list_answer[$key]=new ExerciseAnswerChoice();
                }
                
                 $list_answer[$key]->attributes=$answer;               
                
                     
                if(!empty($list_answer[$key]->answer)){
                    $count_answer++;
               }
            }
            
            
            if(empty($list_answer[0]->answer)){
               $list_answer[0]->addError("[0]answer", ' **Please Set correct answer.');
               $is_pass=false;
            }
            

            
            if($count_answer<2){
               $list_answer[0]->addError("[0]answer", ' **Answer Must have least 2 choices.');
               $is_pass=false;
            }
            
            if($model_question->validate() && $is_pass==true){
                $model_question->save();
                foreach($list_answer as $key=>$answer){
                    if(!empty($answer->answer)){
                        $answer->question_id=$model_question->id;
                        if(empty($answer->is_true)) $answer->is_true=0;
                        $answer->save();
                    }
                }
               $message="Data has Been save"; 
               $success_url=  Yii::app()->createUrl("/admin/default/OperateQuestion",array("exercise"=>$exercise,"course"=>$course,"question"=>$model_question->id,"type"=>$model_question->type,"message"=>$message));
               $this->redirect($success_url);
                             
         
            }            
            //answer array of obj            
        }
        
        $this->render("form_question",array("model_all_question"=>$model_all_question,
                                            "model_question"=>$model_question,
                                            "model_course"=>$model_course,
                                            "exercise"=>$exercise,
                                            "list_answer"=>$list_answer,
                                            "type"=>$type,
                                            "message"=>$message,"qty_question"=>$qty_question,
                                            "mode"=>$mode));
        
        
        
    }
    
    
    public function actionOperateQuestionCode($exercise,$course,$type_code="java",$question="",$message="",$type=""){
         $this->layout='//layouts/main_backend'; //no script
          $model_course=  Course::model()->findByPk($course);
         $model_all_question=  ExerciseQuestion::model()->findAll("exercise_id='$exercise' ORDER BY show_order ASC,id ASC");
         $qty_question=count($model_all_question);
         $list_answer=array();
          if(!empty($question)){
              $mode="edit";
              $model_question=  ExerciseQuestion::model()->findByPk($question);
              $model_answer= ExerciseCodeAnswer::model()->find("question_id='$model_question->id'");
              
    

         }else{
              $mode="new";
              $model_question= new ExerciseQuestion();
              $model_answer= new ExerciseCodeAnswer();      
         }
         
          if(isset($_POST['ExerciseQuestion'])){              
                
                $model_question->attributes = $_POST['ExerciseQuestion'];
                $model_question->exercise_id=$exercise;
                $model_question->type=4;
                $model_question->exercise_id=$exercise;
                $model_question->code_type = $type_code;
                
                if(!empty($_POST['code_default'])){                  
                    $model_question->initial_code= $_POST['code_default'];
                }
                
                
//                $model_answer->attributes=$_POST['ExerciseCodeAnswer'];
                $model_answer->code=$_POST['code'];
                $model_answer->answer=$_POST['answer'];
                $model_answer->question_id="0"; //set default first
                           
                if($model_question->validate() && $model_answer->validate()){
                    $model_question->save();
                    $model_answer->question_id=$model_question->id;
                    $model_answer->save();
                    
                    $message="Data has been saved";
//                    $success_url=  LinkHelper::yiiLink("admin/default/operateQuestionCode/exercise/$exercise/course/$course/question/$model_question->id/message/$message");
                    $success_url = Yii::app()->createUrl("admin/default/operateQuestionCode",array("exercise"=>$exercise,"course"=>$course,"type_code"=>$type_code,"question"=>$model_question->id,"message"=>$message));
                    $this->redirect($success_url);
                }
          }
          
         $this->render("form_question_code",array("model_course"=>$model_course,
                                                   "exercise"=>$exercise,
                                                   "model_all_question"=>$model_all_question,
                                                   "qty_question"=>$qty_question,
                                                   "model_question"=>$model_question,
                                                   "model_answer"=>$model_answer,
                                                   "message"=>$message,
                                                   "type_code"=>$type_code,
                                                   "mode"=>$mode));
    }
    
    
    
    #Delete Lesson type lesson
    public function actionDeleteExerciseContent($id){
        $model_content=  Content::model()->findByPk($id);
        $course_id=$model_content->course_id;
        $model_exercise= Exercise::model()->find("content_id='$id'");
        if(!empty($model_exercise)){
            $model_questions=  ExerciseQuestion::model()->findAll("exercise_id='$model_exercise->id'");
            if(!empty($model_questions)){
                foreach ($model_questions as $question){
                    
                    if($question->type==2){
                        $model_answers= ExerciseTextAnswer::model()->findAll("question_id='$question->id'");
                        if(!empty($model_answers)){
                            foreach ($model_answers as $ans){
                                $ans->delete();
                            }
                        }
                        
                    }else{
                        $model_answers=  ExerciseAnswerChoice::model()->findAll("question_id='$question->id'");
                        if(!empty($model_answers)){
                            foreach ($model_answers as $ans){
                                $ans->delete();
                            }
                        }
                    }
                    $question->delete();
                    
                }
            }
            $model_exercise->delete();
        }
        $model_content->delete();     
       
        $success_link = LinkHelper::yiiLink("/admin/default/manageContent/course/$course_id");
        $this->redirect($success_link);
    }
    
    #Delete Question in Exercise
    public function actionDeleteQuestion($id,$course){
        $model_question=  ExerciseQuestion::model()->findByPk($id);
        $exrecise_id=$model_question->exercise_id;
        
        $model_choice=  ExerciseAnswerChoice::model()->findAll("question_id='$id'");
        if(!empty($model_choice)){
            foreach ($model_choice as $choice){
                $choice->delete();
            }
        } 
        
        $model_text= ExerciseTextAnswer::model()->findAll("question_id='$id'");
        if(!empty($model_text)){
            foreach ($model_text as $text){
                $text->delete();
            }
        }         
         
        
        $model_question->delete();
      
       
        
        
        //$success_link=LinkHelper::yiiLink("/admin/default/OperateQuestion/exercise/$exrecise_id/course/$course");
        $success_link=LinkHelper::yiiLink("admin/default/showQuestion/exercise/$exrecise_id/course/$course");
       
        $this->redirect($success_link);
    }
    
    #Delete choice in Exercise >>> Question
    public function actionDeleteAnswerChoice($id,$course,$excercise){
        $model_answer= ExerciseAnswerChoice::model()->findByPk($id);
        $question_id=$model_answer->question_id;
        $count_answer_all=  ExerciseAnswerChoice::model()->count("question_id='$question_id'");
        if($count_answer_all>2){
            $model_answer->delete();
            $success_link=  LinkHelper::yiiLink("/admin/default/OperateQuestion/exercise/$excercise/course/$course/question/$question_id/message/Update success");
            $this->redirect($success_link);
        }else{
            $unsuccess_link=  LinkHelper::yiiLink("/admin/default/OperateQuestion/exercise/$excercise/course/$course/question/$question_id/message/Can't not delete answer must have at least 2 choices./type/error");
            $this->redirect($unsuccess_link);
        }
    }    
    
    #sort able jquery
    public function actionSortQuestion() {

        if (!empty($_POST)) {
            foreach ($_POST as $question_list) {

                foreach ($question_list as $sort_order => $question_id) {
                    $model = ExerciseQuestion::model()->findByPk($question_id);

                    $model->show_order = $sort_order+1;
                    $model->save();
                }
            }
        }
    }
    
    #show Stat in each Question
    public function actionShowStatInQuestion($exercise,$course,$question){
        $model_course=  Course::model()->findByPk($course);
        $model_question=  ExerciseQuestion::model()->findByPk($question);
                        
        $this->render("stat_in_question",array("model_question"=>$model_question,"model_course"=>$model_course,"exercise"=>$exercise));        
    }
    
    
    #show Stat in each Question Type Text
     public function actionShowStatInQuestionText($exercise,$course,$question){
          $model_course=  Course::model()->findByPk($course);
          $model_question=ExerciseQuestion::model()->findByPk($question);
          $model_exercise =  Exercise::model()->findByPk($exercise);
          
          
          $this->render("stat_in_question_text",array("model_question"=>$model_question,
                                                      "model_course"=>$model_course,
                                                      "exercise"=>$exercise,
                                                       "model_exercise"=>$model_exercise));
     } 
    
    
    
    #Redo    
    #show Qty Answer Correct SCORE FROM all User 
    public function actionShowStatScorePhase($content,$exercise,$course){
       
        $model_course=  Course::model()->findByPk($course);
        $model_exercise= Exercise::model()->findByPk($exercise);
        $model_content= Content::model()->findByPk($content);
        
        $this->render("show_stat_score_phase",array("model_course"=>$model_course,
                                                    "model_exercise"=>$model_exercise,
                                                    "model_content"=>$model_content));       
    }    
    
   #upload video
    public function actionUploadContentVideo($contentId,$type="vdo") {
        if (Yii::app()->request->isAjaxRequest) {
            $content = $this->getContent($contentId);
            $content->type = 2;
            $content->save();

            $contentDir = ResourcePath::getContentBasePath() . $contentId;
            $tmpDir = $contentDir . '/tmp';
            if (is_dir($tmpDir)) {
                //PHPHelper::rrmdir($contentDir);
                shell_exec('rm -rf ' . $tmpDir);
            }
            mkdir($tmpDir, 0755, true);

            $target = 'video.' . PHPHelper::getFileExtension($_FILES['uploadedFile']['name']);
            if ((!is_uploaded_file($_FILES['uploadedFile']['tmp_name'])) or !copy($_FILES['uploadedFile']['tmp_name'], $tmpDir . '/' . $target)) {
                echo "Error copy files";
            } else {
                VideoUtil::encode($tmpDir . '/' . $target, $contentDir . '/');
            }
        }
    }
    
    
    #Exercise Question >>> type text 
    public function actionOperateQuestionText($exercise,$course,$question="",$message="",$type=""){
         $this->layout='//layouts/main_backend'; //no script
        
         $model_course=  Course::model()->findByPk($course);
         $model_all_question=  ExerciseQuestion::model()->findAll("exercise_id='$exercise' ORDER BY show_order ASC,id ASC");
         $qty_question=count($model_all_question);
         $list_answer=array();
          if(!empty($question)){
              $mode="edit";
              $model_question=  ExerciseQuestion::model()->findByPk($question);
              $answer_all= ExerciseTextAnswer::model()->findAll("question_id='$model_question->id' ORDER BY sequence ASC");
              
              if(!empty($answer_all)){
                  $i=0;
                  foreach ($answer_all as $answer){
                      $list_answer[$i]=$answer;
                      $i++;
                  }
              }

         }else{
              $mode="new";
              $model_question= new ExerciseQuestion();
              
              for($i=0;$i<=3;$i++){
                    $list_answer[$i]=new ExerciseTextAnswer();
              }        
          }
          
          
           if(isset($_POST['ExerciseQuestion'])||isset($_POST['ExerciseTextAnswer'])){
               
                           
                $is_pass=true;
                $model_question->attributes = $_POST['ExerciseQuestion'];
                $model_question->exercise_id=$exercise;
                $model_question->type=2;
                $model_question->exercise_id=$exercise;
                $answer_post=$_POST['ExerciseTextAnswer'];
                $count_answer=0;
               
               if(!empty($answer_post)){
                foreach($answer_post as $key=>$answer){
                     if(empty($list_answer[$key])){
                         $list_answer[$key]=new ExerciseTextAnswer();
                     }

                     $list_answer[$key]->attributes=$answer;
                     if(!empty($list_answer[$key]->answer)){
                         $count_answer++;
                     }  

                 }
               }
           
           if($count_answer<1){
               $is_pass=false;
               $list_answer[0]->addError("[0]answer", ' **Answer Must have least 1 answer.');
           }
           
           if($model_question->validate() && $is_pass==true){
                $model_question->save();
                $count_order=1;
                foreach($list_answer as $key=>$answer){
                    $list_answer[$key]->question_id=$model_question->id;
                    $list_answer[$key]->sequence=$count_order;
                    $list_answer[$key]->save();
                    $count_order++;
                }
                 $message="Data has been saved";
                 $success_url=  LinkHelper::yiiLink("/admin/default/OperateQuestionText/exercise/$exercise/course/$course/question/$model_question->id/message/$message");
                 $this->redirect($success_url);
                
              }           
           }
              
          $this->render("form_question_text",array("model_course"=>$model_course,
                                                   "exercise"=>$exercise,
                                                   "model_all_question"=>$model_all_question,
                                                   "qty_question"=>$qty_question,
                                                   "model_question"=>$model_question,
                                                   "list_answer"=>$list_answer,
                                                   "message"=>$message,
                                                   "mode"=>$mode));
         
             
    }
    
    public function actionUserDetail($user_id,$course){
        $this->layout = "//layouts/blank";
        $user=  UsersFaceBook::model()->findByPk($user_id);
        
        $user_course=  UserCourse::model()->find("user_id='$user_id' AND course_id='$course'");
        $model_course = Course::model()->findByPk($course);
        
        $end_contents= Content::model()->findAllBySql("SELECT * FROM content WHERE course_id = '$course' AND id IN (SELECT content_id FROM user_content_log WHERE is_end='1' AND user_id='$user_id')");
        $stuying_contents= Content::model()->findAllBySql("SELECT * FROM content WHERE course_id = '$course' AND id IN (SELECT content_id FROM user_content_log WHERE is_end='0' AND user_id='$user_id')");
           
        $this->render("user_detail",array("user"=>$user,"course"=>$course,"user_course"=>$user_course,
                                          "model_course"=>$model_course,"end_contents"=>$end_contents,
                                          "stuying_contents"=>$stuying_contents));        
    }
    
    public function actionDeleteUserFromCourse($user_id,$course){
         $model_user_course=  UserCourse::model()->find("user_id='$user_id' AND course_id='$course'");
         $model_user_course->delete();
         $success_link= Yii::app()->createUrl("admin/default/studentList",array("course"=>$course));
         $this->redirect($success_link);       
    }
    
    public function actionListVideo($user_id){
        $this->layout = "//layouts/blank";        
        $criteria = new CDbCriteria;
        $criteria->select = "t.*";

        $criteria->condition = "user_id='$user_id' AND type= 'video'";
        $criteria->order = "id DESC";
        $model_user_files=  File::model()->findAll($criteria);       
        
        $this->render("_list_video",array("model_user_files"=>$model_user_files));
    }
    
    public function actionListPhoto(){
        $this->layout = "//layouts/blank";   
        
        $user_id = Yii::app()->user->id;
        $target_path = Yii::app()->basePath . "/../webroot/images/courseBg";
        $file_address = "images/courseBg";
        
        $files="";

        if (is_dir($target_path)) {
            $files = CFileHelper::findFiles($file_address, array('fileTypes' => array('gif', 'png', 'jpg', 'jpeg')));
           
        }
        
        
         $this->render("_list_photo",array("files"=>$files));
    }

    

    public function actionViewLessonStat($id,$qty){
    
        $model_lesson=  Content::model()->findByPk($id);
        $model_course=  Course::model()->findByPk($model_lesson->course_id);
        
        $model_stat_table= new UserContentLog();
        $model_stat_table->unsetAttributes();
        
        if (isset($_GET['UserContentLog']))
            $model->attributes = $_GET['UserContentLog'];
        
        $this->render('view_lesson_stat',array(
            'dataProvider'=>$model_stat_table->getStatInLesson($id),
            'model_stat_table'=>$model_stat_table,
            'model_lesson'=>$model_lesson,
            'model_course'=>$model_course,
            'qty'=>$qty,            
            
        ));        
    }
    
    #show All Student Progess bar
//    public function actionStatProgressOfStudy($course){
//         $key_word="";
//         $model_course=  Course::model()->findByPk($course);
//         $model_users=  new StatUserStudy();         
//                  
//        if(!empty($_POST["studentname"])){
//           $key_word=$_POST["studentname"];
//        }
//        
//       
//        $this->render("stat_progress_of_study",
//                       array("model_course"=>$model_course,
//                             "dataProvider"=>$model_users->getListUserProgressInCourse($course, $key_word),
//        ));    
//        
//    }
    
    
    #show All Student Progess bar
    public function actionStatProgressOfStudy($course){
         $key_word="";
         $model_course=  Course::model()->findByPk($course);
         $model_users=  new UserCourse(); 
         $qty_content=  ContentInfo::model()->find("course_id='$course'")->number_of_content;     
         $data_for_search=  SiteHelper::getPersonInCourse($course,3);
         
         
                  
        if(!empty($_POST["studentname"])){
           $key_word=$_POST["studentname"];
        }       
       
        $this->render("stat_progress_of_study",
                       array("model_course"=>$model_course,
                             "qty_content"=>$qty_content,
                             "data_for_search"=>$data_for_search,
                             "dataProvider"=>$model_users->getListUserInformationInCourse($course, $key_word),
        ));    
        
    }
    
    
    public function actionStatInLesson($courseId){
        $model_course=  Course::model()->findByPk($courseId);
        
        $condition= "stat_study_in_lesson.course_id='$courseId'";
        $sql="SELECT stat_study_in_lesson.* FROM stat_study_in_lesson WHERE $condition";
        $start="";
        $to="";
        
         if(isset($_POST['submit'])){

            $start=$_POST['date_start'];
            $to=$_POST['date_end'];
          
            
            if(!empty($start)){
                $date_start=  SiteHelper::convertDatedb($start);
                $date_start = $date_start." 00:00:00";
                $condition.=" AND user_content_log.study_time >= '$date_start'";
                
            }
            
            if(!empty($to)){
                 $date_end=  SiteHelper::convertDatedb($to);
                 $date_end = $date_end." 23:59:59";
                 $condition.=" AND user_content_log.study_time <= '$date_end'";  
            }
            
            if(!empty($start) || !empty($to)){
             $sql="SELECT SUM(user_content_log.qty_times) AS total_time_view,stat_study_in_lesson.title AS title 
                    FROM stat_study_in_lesson 
                    LEFT JOIN user_content_log on stat_study_in_lesson.content_id=user_content_log.content_id
                    WHERE $condition
                    GROUP BY user_content_log.content_id";
              }
         }
         
         
        $model_contents_stat=  StatStudyInLesson::model()->findAllBySql($sql);
        
        $lesson_name="";
        $qty_lesson_view="";
        $has_stat=TRUE;
        
        if(!empty($model_contents_stat)){           

            foreach ($model_contents_stat as $content){      
                 $lesson_name .= " '".$content->title."',";
                 
                 if(empty($content->total_time_view)){
                     $content->total_time_view=0;
                 }                 
                 
                 $qty_lesson_view .= " ".$content->total_time_view.",";       
            }
            
            $lesson_name=  rtrim($lesson_name,",");
            $qty_lesson_view= rtrim($qty_lesson_view, ",");
            $message = $model_course->name;
          
        }else{
           $has_stat=FALSE;
           $message = "no stat now";
        }
        
        
        
        
       $this->render("stat_in_lesson",array("model_course"=>$model_course,
                                            "has_stat"=>$has_stat,
                                            "message"=>$message,"start"=>$start,"to"=>$to,
                                            "lesson_name"=>$lesson_name,
                                            "qty_lesson_view"=>$qty_lesson_view));

        
        
    }
    
    public function actionNumberStudentAccess($courseId,$type="day"){
        $model_course=  Course::model()->findByPk($courseId);
        $is_have_stat=TRUE;
        
        //default 10 day ago
        //10 day ago
         $ten_days = time( ) - 86400 * 10; 
         $days_ago = date("Y-m-d", $ten_days);         
         $start=date("d/m/Y", $ten_days);           
       
         $today=date("Y-m-d H:i:s");
         $to=date("d/m/Y");
         $bar_x="";
         $bar_y="";
        
         $colum_name="date";

         $sql="SELECT day(create_date) as day,month(create_date) as month, year(create_date) as year,COUNT(id) AS quantity 
               FROM user_course 
               WHERE course_id='$courseId' AND role='3' AND create_date >= '$days_ago' AND create_date <= '$today' 
               GROUP BY DATE(create_date) ORDER BY create_date ASC,id ASC";
         
         $model_stat=Yii::app()->db->createCommand($sql)->queryAll();
         $message="on 30 days ago";        
     
         
        if(isset($_POST['submit'])){
           
            $message="";
            
            if(!empty($_POST['view_type'])){
                $type=$_POST['view_type'];
            }
            
            
            
            $start=$_POST['date_start'];
            $to=$_POST['date_end'];
            $course_id=$_POST['courseId'];       
            
            #3 = student
            $condition= "course_id='$course_id' AND role='3'";
            
            if(!empty($start)){
                $date_start=  SiteHelper::convertDatedb($start);
                $date_start = $date_start." 00:00:00";
                $condition.=" AND create_date >= '$date_start'";
                
            }
            
            if(!empty($to)){
                 $date_end=  SiteHelper::convertDatedb($to);
                 $date_end = $date_end." 23:59:59";
                 $condition.=" AND create_date <= '$date_end'";  
            }
            
             if($type=="day"){                  
              $group =" DATE(create_date)";    
              $colum_name="date";
            }
           
                        
            if($type=="month"){  
                
              $group="YEAR(create_date), MONTH(create_date)";    
              $colum_name="month - year";
              
            }
            
            if($type=="year"){
                $group ="YEAR(create_date)";  
                $colum_name= "year";
            }
            
            
            $sql="SELECT day(create_date) as day,month(create_date) as month, year(create_date) as year,COUNT(id) AS quantity
                  FROM user_course
                  WHERE $condition
                  GROUP BY $group
                  ORDER BY create_date ASC,id ASC";                    
         
            
            $model_stat=Yii::app()->db->createCommand($sql)->queryAll();                
            
            if(empty($model_stat)){     
                $is_have_stat=FALSE;                
            }            
           
        }// isset post
        
      
        
         if (!empty($model_stat)) {
            
            $grap = "['" . $colum_name . "', 'Quantity'],";           

            foreach ($model_stat as $stat) {
                $month_name = SiteHelper::getShortMonthName($stat['month']);

                if ($type == "day") {
                    $col1 = $stat['day'] . "-" . $month_name . "-" . $stat['year'];
                }

                if ($type == "month") {
                    $col1 = $month_name . "-" . $stat['year'];
                }

                if ($type == "year") {
                    
                    //count is 1 row add more
                    
                    $col1 = $stat['year'];
                }

                $quantity = $stat['quantity'];
                
                $grap.= "['$col1', $quantity],";
                
                $bar_x.= " '".$col1."',";
                $bar_y.= " $quantity,";
                
            }

            $grap = rtrim($grap, ",");
            $bar_x= rtrim($bar_x, ",");
            $bar_y= rtrim($bar_y, ",");
            
            
        }else{
             $is_have_stat=FALSE;
             $grap="";
        }      
        
        $this->render("stat_number_student",array("model_course"=>$model_course,
                                                  "message"=>$message,
                                                  "type"=>$type,"start"=>$start,"to"=>$to,
                                                  "is_have_stat"=>$is_have_stat,
                                                  "bar_x"=>$bar_x,"bar_y"=>$bar_y,
                                                   "grap"=>$grap));
        
    }

    public function actionNumberCoinIncourse($courseId,$type="day"){
          $model_course=  Course::model()->findByPk($courseId);
        $is_have_stat=TRUE;
        
        //default 10 day ago
        //30 day ago
         $ten_days = time( ) - 86400 * 30; 
         $days_ago = date("Y-m-d", $ten_days);        
         
      
         $start=date("d/m/Y", $ten_days);           
       
         $today=date("Y-m-d H:i:s");
         $to=date("d/m/Y");
        
         $colum_name="date";

         $sql="SELECT day(create_date) as day,month(create_date) as month, year(create_date) as year,SUM(bought_price) AS quantity 
               FROM user_course 
               WHERE course_id='$courseId' AND role='3' AND create_date >= '$days_ago' AND create_date <= '$today' 
               GROUP BY DATE(create_date) ORDER BY create_date ASC,id ASC";
         
        
         
         $model_stat=Yii::app()->db->createCommand($sql)->queryAll();
         $message="on 30 days ago";
         $bar_x="";
         $bar_y="";
         
         
          $sql_money_source = "SELECT SUM(amt) as amt,method AS method,method_name AS method_name
                            FROM course_payment
                            WHERE course_id='$courseId' AND approveCode_next = '00'
                            AND update_date >= '$days_ago' AND update_date <= '$today'
                            GROUP BY method, course_id
                            ";
          
          $model_source=Yii::app()->db->createCommand($sql_money_source)->queryAll();
       
         
         
        if(isset($_POST['submit'])){           
            $message="";
            if(!empty($_POST['view_type'])){
                $type=$_POST['view_type'];
            }            
            
            $start=$_POST['date_start'];
            $to=$_POST['date_end'];
            $course_id=$_POST['courseId'];       
            
            #3 = student
            $condition= "course_id='$course_id' AND role='3'";
            $condition_source = "course_id='$courseId' AND approveCode_next = '00'";
            
            if(!empty($start)){
                $date_start=  SiteHelper::convertDatedb($start);
                $date_start = $date_start." 00:00:00";
                $condition.=" AND create_date >= '$date_start'";
                $condition_source .= "AND update_date >= '$date_start'";
                
                
            }
            
            if(!empty($to)){
                 $date_end=  SiteHelper::convertDatedb($to);
                 $date_end = $date_end." 23:59:59";
                 $condition.=" AND create_date <= '$date_end'"; 
                 $condition_source .= "AND update_date <= '$date_end'";
            }
            
             if($type=="day"){                  
              $group =" DATE(create_date)";    
              $colum_name="date";
            }
           
                        
            if($type=="month"){  
                
              $group="YEAR(create_date), MONTH(create_date)";    
              $colum_name="month - year";
              
            }
            
            if($type=="year"){
                $group ="YEAR(create_date)";  
                $colum_name= "year";
            }
            
            
            $sql="SELECT day(create_date) as day,month(create_date) as month, year(create_date) as year,SUM(bought_price) AS quantity
                  FROM user_course
                  WHERE $condition
                  GROUP BY $group
                  ORDER BY create_date ASC,id ASC";                    
            
            
            $model_stat=Yii::app()->db->createCommand($sql)->queryAll();     
            
            
            
            
           $sql_money_source = "SELECT SUM(amt) as amt,method AS method,method_name AS method_name
                            FROM course_payment
                            WHERE $condition_source                           
                            GROUP BY method, course_id
                            ";
          
           $model_source=Yii::app()->db->createCommand($sql_money_source)->queryAll();
            
            
            if(empty($model_stat)){     
                $is_have_stat=FALSE;                
            }            
           
        }// isset post
        
        $total_coin = 0;
         if ($is_have_stat == TRUE && !empty($model_stat)) {
            $grap = "['" . $colum_name . "', 'Quantity Money'],";
            $bar_x="";
            $bar_y="";

            foreach ($model_stat as $stat) {
                $month_name = SiteHelper::getShortMonthName($stat['month']);

                if ($type == "day") {
                    $col1 = $stat['day'] . "-" . $month_name . "-" . $stat['year'];
                }

                if ($type == "month") {
                    $col1 = $month_name . "-" . $stat['year'];
                }

                if ($type == "year") {
                    
                    //count is 1 row add more
                    
                    $col1 = $stat['year'];
                }

                $quantity = $stat['quantity'];
                
                $grap.= "['$col1', $quantity],";
                
                $bar_x.= " '".$col1."',";
                $bar_y.= " $quantity,";
                $total_coin += $quantity; 
                
            }

            $grap = rtrim($grap, ",");
            $bar_x= rtrim($bar_x, ",");
            $bar_y= rtrim($bar_y, ",");
            
            
        }else{
             $is_have_stat=FALSE;
             $grap="";
        }           

        if(!empty($model_source)){
            $bar_x_source="";
            $bar_y_source = "";
            
            foreach($model_source as $source){
                $bar_x_source.= " '".$source['method_name']."',";
                $bar_y_source.= " ".$source['amt'].",";                
                
            }
            $bar_x_source= rtrim($bar_x_source, ",");
            $bar_y_source= rtrim($bar_y_source, ",");            
              
        }

        
        
        $this->render("stat_number_coin",array("model_course"=>$model_course,
                                                  "message"=>$message,
                                                  "type"=>$type,"start"=>$start,"to"=>$to,
                                                  "is_have_stat"=>$is_have_stat,
                                                  "bar_x"=>$bar_x,"bar_y"=>$bar_y,
                                                  "grap"=>$grap,"total_coin"=>$total_coin,
                                                  "bar_x_source"=>$bar_x_source,"bar_y_source"=>$bar_y_source));        
    }

    
    
     public function actionChangePublic(){
        $id=$_POST['id'];
        $state=$_POST['state'];
        
        if(!empty($_POST['type'])){
            $type=$_POST['type'];
        }else{
             $type="lesson";
        } 
        
        if($state==1){ $new_state=0; }else{ $new_state=1;}        
        
        if($type=="lesson"){     
            $model_content=  Content::model()->findByPk($id);
            $model_content->is_public=$new_state;
            $model_content->save();
        }        
        
        if($type=="exercise"){
            $model_question=  ExerciseQuestion::model()->findByPk($id);
            $model_question->is_public=$new_state;
            $model_question->save();
        }        
    }
    
    public function actionClearStatExercise($course){
       $sql_exercise="SELECT * FROM exercise WHERE id IS NOT NULL AND content_id IN (SELECT id AS content_id FROM content WHERE is_exercise='1' AND course_id='$course')";
       $model_exercise=Yii::app()->db->createCommand($sql_exercise)->queryAll();      
       
       if(!empty($model_exercise)){
           foreach ($model_exercise as $exercise){                  
               $exercise_id=$exercise['id'];
               $model_question= ExerciseQuestion::model()->findAll("exercise_id='$exercise_id'");
               if(!empty($model_question)){
                   foreach ($model_question as $question){                       
                       $model_choice_score=  ExerciseChoiceScore::model()->deleteAll("question_id='$question->id'");
                       $model_code_score= ExerciseCodeScore::model()->deleteAll("question_id='$question->id'");
                       $model_text_score= ExerciseTextScore::model()->deleteAll("question_id='$question->id'");
                   }          
               }
                $model_stat= ExerciseStat::model()->deleteAll("exercise_id='$exercise_id'");             
           }
       }
       
        $success_link=Yii::app()->createUrl("admin/default/manageContent",array("course"=>$course));
        $this->redirect($success_link);     
    }
    
    
    public function actionTestphp(){
        $this->render("test_php");
    }
    
    public function actionCourseDivision($courseId){
        $this->checkOwner($courseId);
        
        $model_course =  Course::model()->findByPk($courseId);
        
        $model_money_div_rate = CourseDivisionRateBase::model()->find("course_id='$courseId' ORDER BY id DESC");
        if(empty($model_money_div_rate)){
            $model_money_div_rate= CourseDivisionRateBase::model()->find("is_default='1'");
        }
        
        $criteria = new CDbCriteria;      
        $criteria->condition="course_id='$courseId'";
        $criteria->order="id DESC";
        
        
        $division_stat = new CActiveDataProvider('CourseMoneyDivision', array(
            'pagination' => array('pageSize' => "10"),
            'criteria' => $criteria,
        ));        
        $this->render("course_division",array("model_money_div_rate"=>$model_money_div_rate,"dataProvider"=>$division_stat,"model_course"=>$model_course));
        
    }
    
    public function actionSetHead($id){
       $model_userCourse= UserCourse::model()->findByPk($id);  
        
       $sql_update_head="UPDATE user_course SET is_head=0 WHERE is_head='1' AND course_id='$model_userCourse->course_id'";
       $command= Yii::app()->db->createCommand($sql_update_head);
       $rowCount=$command->execute();
       
       $model_userCourse->is_head=1;
       $model_userCourse->save(false);
      
               
    }
    
    public function actionSetShow($id){
        $model_userCourse= UserCourse::model()->findByPk($id);  
        if($model_userCourse->is_show==1){
            $model_userCourse->is_show=0;
        }else{
            $model_userCourse->is_show=1;
        }
        $model_userCourse->save(false);
        echo $model_userCourse->is_show;
        
        
    }
    
      public function actionContentComment($courseId){
          $criteria = new CDbCriteria;
          $criteria->select = "t.*";
          $criteria->condition = "t.content_id IN (SELECT id FROM content WHERE course_id='$courseId')";
          $criteria->order="t.create_date DESC";
          
          
          if(isset($_POST['btn-submit'])){
              
              if(!empty($_POST['content_id'])){
                  $content_id= $_POST['content_id'];
                   $criteria->condition.= " AND t.content_id=$content_id";
              }
          }
          
          $model_suggestions = new CActiveDataProvider("ContentSuggestion", array(
                                    'pagination' => array(
                                        'pageSize' => 9,
                                    ),
                                    'criteria' => $criteria,
                                ));
          $model_course= Course::model()->findByPk($courseId);
          $model_content= Content::model()->findAll("course_id='$courseId' AND parent_id !='-1'");
       
         $this->render("content_comment",array("model_suggestions"=>$model_suggestions,"model_course"=>$model_course,"model_content"=>$model_content));        
        
    }
    
    
    public function actionStatBufferRate($courseId){
         $model_course= Course::model()->findByPk($courseId);
         $model_content= Content::model()->findAll("course_id='$courseId' AND parent_id !='-1'");
         $head= "สถิติการ Buffer";
         $content_id = "";
         
         
        //30 day ago
         $ten_days = time( ) - 86400 * 30; 
         $days_ago = date("Y-m-d", $ten_days);  
         $start=date("d/m/Y", $ten_days); 
         
       
         $today=date("Y-m-d H:i:s");
         $to=date("d/m/Y");
         
         $date_start = SiteHelper::convertDatedb($start);
         $date_start = $date_start." 00:00:00";
         
         $date_end=  SiteHelper::convertDatedb($to);
         $date_end = $date_end." 23:59:59";
             
         
         $sql="SELECT MAX(times) as max_time,AVG(times) as avg_time, MIN(times) as min_time,DATE(create_date) as date
               FROM vdo_buffer_rate 
               WHERE content_id IN (SELECT id FROM content WHERE course_id='$courseId') AND create_date >= '$date_start' AND create_date <= '$date_end'
               GROUP BY DATE(create_date) ORDER BY create_date ASC,id ASC";
          
         
         $sql_total="SELECT SUM(times) as sum_time, MAX(times) as max_time,AVG(times) as avg_time, MIN(times) as min_time,DATE(create_date) as date
               FROM vdo_buffer_rate 
               WHERE content_id IN (SELECT id FROM content WHERE course_id='$courseId') AND create_date >= '$date_start' AND create_date <= '$date_end'";
         
         
       
          $sql_grap_person="SELECT times as time,COUNT(id) as qty_person
               FROM vdo_buffer_rate 
               WHERE content_id IN (SELECT id FROM content WHERE course_id='$courseId') AND create_date >= '$date_start' AND create_date <= '$date_end'
               GROUP BY times ORDER BY COUNT(id) ASC";
         
         
         if(isset($_POST['submit'])){             
      
           $condition =  "content_id IN (SELECT id FROM content WHERE course_id='$courseId')";
             
         
           
             
            if(!empty($_POST['date_start'])){
                $start= $_POST['date_start'];
                $date_start=  SiteHelper::convertDatedb($start);
                $date_start = $date_start." 00:00:00";
                $condition.=" AND create_date >= '$date_start'";
                
            }
            
            if(!empty($_POST['date_end'])){
                 $to = $_POST['date_end'];
                 $date_end=  SiteHelper::convertDatedb($to);
                 $date_end = $date_end." 23:59:59";
                 $condition.=" AND create_date <= '$date_end'";  
            }
            
            if(!empty($_POST['content_id'])){
                $content_id = $_POST['content_id'];
                $condition.=" AND content_id = '$content_id'";
                $content_select = Content::model()->findByPk($content_id)->title;
                $head .= "- บทเรียน $content_select";
            }
            
             $sql="SELECT MAX(times) as max_time,AVG(times) as avg_time, MIN(times) as min_time,DATE(create_date) as date
               FROM vdo_buffer_rate 
               WHERE $condition
               GROUP BY DATE(create_date) ORDER BY create_date ASC,id ASC"; 
             
             
              $sql_total="SELECT SUM(times) as sum_time, MAX(times) as max_time,AVG(times) as avg_time, MIN(times) as min_time,DATE(create_date) as date
               FROM vdo_buffer_rate 
               WHERE $condition";
              
              
              $sql_grap_person="SELECT times as time,COUNT(id) as qty_person
               FROM vdo_buffer_rate 
               WHERE $condition
               GROUP BY times ORDER BY COUNT(id) ASC";
            
         }
         
         
        $model_stats=Yii::app()->db->createCommand($sql)->queryAll();  
        $model_stat_person = Yii::app()->db->createCommand($sql_grap_person)->queryAll();  
        $model_sumary = Yii::app()->db->createCommand($sql_total)->queryRow();  
        
        
        
        
        $grap = "";
        $grap_person ="";
        
        if(!empty($model_stats)){
            $grap = "['Date', 'MAX BufferTime', 'AvG BufferTime' ,'AvG BufferTime LINE' , 'MIN BufferTime'],";
            foreach ($model_stats as $stat){
              
               $stat['max_time']*=3;
               $stat['avg_time']*=3;
               $stat['min_time']*=3;      
                
               $grap.= "['".$stat['date']."',".$stat['max_time'].",".$stat['avg_time'].",".$stat['avg_time'].",".$stat['min_time']."],";
            }
            
            $grap= rtrim($grap, ",");
        }
        
        
        
        
        if(!empty($model_stat_person)){            
           
            $grap_person = "['จำนวนคน', 'เวลา buffer'],";
            foreach ($model_stat_person as $person){
               $person['time'] *=3;
               
               $grap_person .= "['".$person['qty_person']."',".$person['time']."],";
                 
                 
                
            }
            $grap_person= rtrim($grap_person, ",");           
        }
        
        
        
        $this->render("statBufferRate",array("model_course"=>$model_course,"model_sumary"=>$model_sumary,
                                            "grap"=>$grap,"start"=>$start,
                                            "grap_person"=>$grap_person,
                                            "to"=>$to,"model_content"=>$model_content,
                                            "content_id"=>$content_id,
                                            "head"=>$head));       
         
        
    }
    
    
    
    
}