<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
        
        #for check Admin who can edit Course
        public function checkOwner($course_id){
             $user_id = Yii::app()->user->id;
             $model_user_course=  UserCourse::model()->find("course_id='$course_id' AND user_id='$user_id'");
             if(!empty($model_user_course)){
             if($model_user_course->role!="1"){
                 
                 if(!empty($return_url)){
                     //get return url 
                     $destination=Yii::app()->controller->module->returnUrl;
                 }else{
                     Yii::app()->createUrl($link);
                     $destination=Yii::app()->createUrl("/admin/default/ownCourse");
                                     
                 }
                 
                 echo "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
                 echo "<script>alert('คุณไม่ได้เป็นเจ้าของ course นี้!!!')
                          window.location='$destination';
                          </script>";
             }
           }else{
                 echo "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
                 $destination=Yii::app()->createUrl("/site/error");
                 echo "<script>alert('ไม่สามารถเข้า URL นี้')
                          window.location='$destination';
                          </script>";
           }
        }
        
        #Query Owner Course 
        public function actionOwnCourse() {
        $user_id = Yii::app()->user->id;

        $criteria = new CDbCriteria;
        $criteria->select="t.*";
        $criteria->join ="INNER JOIN user_course ON t.id=user_course.course_id";
        $criteria->group="t.id";
        $criteria->condition="user_course.role='1' AND user_course.user_id='$user_id'";
        $criteria->order="id DESC";

        $courses = new CActiveDataProvider('Course', array(
            'pagination' => array('pageSize' => "8"),
            'criteria' => $criteria,
        ));

       $this->render('own_course',array('courses' => $courses,'mode'=>"admin"));
     }
    
    
    #Add And Edit Course Information (course detail)
    public function actionOperateCourse($id = "") {
        if (!empty($id)) {
            $this->checkOwner($id);
            $model_course = Course::model()->findByPk($id);
            $mode="edit";
            $old_img=$model_course->course_img;
        } else {
            $model_course = new Course();
            $mode="new";
        }
        
        $teacher->user_id=array();
        $list_all_teacher = SiteHelper::getTeacherList();
        $selected_teacher = array();

        if (!empty($model_course->id)) {
            $selected_teacher = SiteHelper::getTeacherList($model_course->id,"getuser_id");
                       
        }
        

        if (isset($_POST['Course'])) {
            $model_course->attributes = $_POST['Course'];
            
           
            $list_teacher=$_POST['teacherList'];
           
            $user_id = Yii::app()->user->id;
            $model_course->user_id=$user_id;
                   
            if ($model_course->validate()) {
                
                $img_file = CUploadedFile::getInstance($model_course, 'course_img');
               
                if (!empty($img_file)) {
                    $random = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz0123456789'), 0, 100);
                    $img_file->saveAs(Yii::app()->basePath . '/../course/' . "$random" . "_" . CUploadedFile::getInstance($model_course, 'course_img'));
                    
                    $model_course->course_img = "course/".$random."_".$img_file;
                     
                    //resize img
                    $file_to_resize = $model_course->course_img;
                    $file_to_resize = Yii::app()->simpleImage->load($file_to_resize);
                    $file_to_resize->resize(350,160);
                    $file_to_resize->save($model_course->course_img);
                    
                        
                    //Delete Old Photo 
                    if($mode=="edit" || !empty($old_file)){
                         $old_file= Yii::app()->basePath."/../course/original/".$old_img;
                         if (file_exists($old_file)) {
                             unlink($old_file);
                         }
                                
                    }
            }
                  
                $model_course->save();
                
         if(!empty($list_teacher)){
             $just_save_teacher=array(); 
               foreach ($list_teacher as $teacher){
                   if(in_array($teacher, $selected_teacher)){
                       //do nothing
                       $just_save_teacher[$teacher]=$teacher;
                   }else{
                   
                       $model_user_course=new UserCourse();
                       $model_user_course->course_id=$model_course->id;
                       $model_user_course->user_id=$teacher;
                       $model_user_course->role=2;
                       $model_user_course->save();
                       $just_save_teacher[$teacher]=$teacher;
                  }
               }
               //delete person who used to in list
               $used_to_list_teacher= array_diff_key($selected_teacher,$just_save_teacher);
               
               if(!empty($used_to_list_teacher)){
                                   
                    foreach($used_to_list_teacher as $old_teacher){
                      
                        $used_to_list=  UserCourse::model()->find("user_id='$old_teacher' AND course_id='$model_course->id' AND role='2'");
                        if(!empty($used_to_list)){
                            $used_to_list->delete();
                        }
                   }
               }
               
               
               
           }
                
                
              
                //add owner of course
                $user_id = Yii::app()->user->id;
                $model_course_owner=  UserCourse::model()->find("user_id='$user_id' AND course_id='$model_course->id' AND role ='1'");
                if(empty($model_course_owner)){
                    $model_course_owner=new UserCourse();
                    $model_course_owner->user_id=$user_id;
                    $model_course_owner->course_id=$model_course->id;
                    $model_course_owner->role="1";
                    $model_course_owner->save();
                }
                
                
                $success_link = Yii::app()->createUrl("/admin/default/manageContent/course/$model_course->id");
                $this->redirect($success_link);
            }
        }
        
   
        
        $this->render("form_course", array("model_course" => $model_course,
                                            "list_all_teacher"=>$list_all_teacher,
                                            "selected_teacher"=>$selected_teacher));
    }
    
    #show page for manage content
    public function actionManageContent($course){
       $this->checkOwner($course);
       $model_course = Course::model()->findByPk($course);
       $model_parent_content=  Content::model()->findAll("parent_id='-1' AND course_id='$course' ORDER BY show_order ASC");
       $this->render("manage_content",array("model_parent_content"=>$model_parent_content,
                                            "course_id"=>$course,"model_course"=>$model_course));              
    }
    
    
    #Add Edit vdo content | Unit parent_id -1 >>> lesson !=1|  
    public function actionOperateVideoContent($id = "", $course, $type = "unit") {
        if (!empty($id)) {
            $model_content = Content::model()->findByPk($id);
            $model_vdo=  File::model()->findByPk($model_content->file_id);
            
            if(empty($model_vdo)){
                $model_vdo=new File();
            }
            
            $old_vdo=$model_vdo->name;
            $mode = "edit";
        } else {
            $model_content = new Content();
            $model_vdo=  new file();
            $mode = "add";
        }
        
        $model_course = Course::model()->findByPk($course);
        $user_id = Yii::app()->user->id;

        if (isset($_POST['Content']) || isset($_POST['File'])) {
            $model_content->course_id = $course;
            $model_content->attributes = $_POST['Content'];

            if ($type == "unit") {
                $model_content->parent_id = (int) "-1";
            }
            
            $new_upload=CUploadedFile::getInstance($model_vdo,'original_name');
            $video_vaild=false;
            
            if (!empty($new_upload)) {

                $model_vdo->name = CUploadedFile::getInstance($model_vdo, 'original_name');
                $model_vdo->original_name = $model_vdo->name;
                
                $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263'), 0, 400);
                $path = Yii::app()->basePath . "/../files/$user_id";

                if (!is_dir($path)) {
                    mkdir($path);
                }

                $fipath_temp=Yii::app()->basePath . "/../files/$user_id/" . $random . "_" .CUploadedFile::getInstance($model_vdo, 'original_name');
                $random=$this->checkDuplicateVideoFile($fipath_temp,$user_id,$new_upload,$random);
                
               

                $model_vdo->user_id = $user_id;
                $model_vdo->type = "vdo";   
                if($model_vdo->validate()){
                   
                    $video_vaild=true;                    
                }

                //delete old file
                if ($mode == "edit") {
                    $old_file_path = Yii::app()->basePath . "/../files/$user_id/".$old_vdo;
                    if (file_exists($old_file_path)&&(!empty($old_vdo))) {
                        $old_file_path = Yii::app()->basePath . "/../files/$user_id/".$old_vdo;
                        if (file_exists($old_file_path)) {
                            unlink($old_file_path);
                        }
                    }
                }
            }else{
               // อาจจะ อัพ แต่เนื้อหาไม่อัพบทเรียน
                $video_vaild=true;
            }
            
            if ($model_content->validate() && $video_vaild==true) {
                
                //save video
                $model_vdo->name->saveAs(Yii::app()->basePath . "/../files/$user_id/" . $random . "_" .CUploadedFile::getInstance($model_vdo, 'original_name'));
                $model_vdo->name = $random . "_" . $model_vdo->name;
                 $model_vdo->save();
                 
                 
                if(!empty($model_vdo->id)){
                    $model_content->file_id=$model_vdo->id;
                }
                $model_content->save();
                $success_link = Yii::app()->createUrl("/admin/default/manageContent/course/$course");
                $this->redirect($success_link);
            }
        }
        
        
       $this->render("form_content", array("model_content" => $model_content, 
                                            "type" => $type,
                                            "model_course"=>$model_course,
                                            "model_vdo"=>$model_vdo,
                                            ));
    }
    
   
    #check duplicat video
    public function checkDuplicateVideoFile($file_path, $user_id,$file_name,$random) {
     if (file_exists($file_path)) {
            $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263'), 0, 400);
            $fipath_temp=Yii::app()->basePath . "/../files/$user_id/" . $random . "_" .$file_name;
            #Recursive
            $this->checkDuplicateVideoFile($file_path,$random);
            
        } else {
            return $random;
        }
    }
    
    
    
    #Delete Unit And Lesson Type Video
    public function actionDeleteVideoContent($id){
        $model_content=  Content::model()->findByPk($id);
        $course_id=$model_content->course_id;
        
        $model_vdo=  File::model()->findByPk($model_content->file_id);
        if (!empty($model_vdo)) {
            $file_del = Yii::app()->basePath . "/../files/$model_vdo->user_id/" . $model_vdo->name;
            if (file_exists($file_del)) {
                unlink($file_del);
            }
             $model_vdo->delete();
        }
        
       
        $model_content->delete();
        $success_link = Yii::app()->createUrl("admin/default/manageContent/course/$course_id");
        $this->redirect($success_link);
           
    }
    
    #Show List of User(Student) in Course 
    public function actionStudentList($course){
        $this->checkOwner($course);
        $model_course = Course::model()->findByPk($course);
        $model_user_course=new UserCourse;
                
        $this->render("people_list",array("model_course"=>$model_course,
                                          "dataProvider"=>$model_user_course->getRelatePeople($course, "3")));
    }
    
    #Show List of User(Teacher) in Course 
    public function actionTeacherList($course){
        $this->checkOwner($course);
        $model_course = Course::model()->findByPk($course);
        $model_user_course=new UserCourse;
                
        $this->render("people_list",array("model_course"=>$model_course,
                                          "dataProvider"=>$model_user_course->getRelatePeople($course,"1","2")));
    }
    
    
    #sort lesson and Unit
    public function actionSortContent() {
        $parent = "";
        $round = 1;
        $vaildate=true;
        if (!empty($_POST)) {
            foreach ($_POST as $content) {

                foreach ($content as $sort_order => $id_content) {
                    $model = Content::model()->findByPk($id_content);

                    #if make child above parent
                    if ($round == 1 && $model->parent_id != "-1") { 
                        $fail_link = Yii::app()->createUrl("/admin/default/manageContent/course/$model->course_id");
                        echo "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
                        echo "<script>alert('ขออภัยบทเรียนต้องอยู่ภายใต้บทเรียน');
                      window.location='$fail_link';
                      </script>";
                        $vaildate=false;
                                        
                    }

                    if ($model->parent_id == "-1") {
                        $model->show_order = $sort_order;
                        $parent = $id_content;
                    } else {
                        $model->parent_id = $parent;
                        $model->show_order = $sort_order;
                    }
                    if($vaildate==true){
                        $model->save();
                    }

                    $round++;
                }
            }
        }
        
    }
    
    #Video State
    public function actionVideoStat(){
              
        
        $this->render("videoStat",array());
    }
    
    #show coupon list in each round
     public function actionCoupon($course) {
        $this->checkOwner($course);
        $model_course = Course::model()->findByPk($course);
        $user_id = Yii::app()->user->id;
        $model_user=  Profile::model()->findByPk($user_id);
        
        $model = new Coupon();
        $model->unsetAttributes();
        if (isset($_GET['Coupon']))
            $model->attributes = $_GET['Coupon'];

        $this->render('coupon', array('dataProvider' => $model->getRoundCoupon($course, $user_id),
                                      'model' => $model,
                                      'model_course'=>$model_course,
                                      'model_user'=>$model_user));
    }
    
    #Query coupon Inside group << use create_date for group
    public function actionViewCoupon($date, $course) {
        $this->checkOwner($course);
        $model_course = Course::model()->findByPk($course);
        $user_id = Yii::app()->user->id;

        $model = new Coupon();
        $model->unsetAttributes();
        if (isset($_GET['Coupon']))
            $model->attributes = $_GET['Coupon'];

        $this->render('view_coupon', array('dataProvider' => $model->getCouponInGroup($date, $course,$user_id),
            'model' => $model,
            'date'=>$date,
            'model_course' => $model_course));
    }
    

    #coupong form
    public function actionFormCoupon($course){
        $this->checkOwner($course);
        $model_course = Course::model()->findByPk($course);
        $model_coupon= new Coupon();
      
        
        if(isset($_POST['Coupon'])){
             $model_coupon->attributes = $_POST['Coupon'];            
             
             if(!empty($model_coupon->expire_date)){
                list($day,$month,$year)=  explode("/", $model_coupon->expire_date);
                $model_coupon->expire_date=$year."-".$month."-".$day;
             }
             $user_id=Yii::app()->user->id;
             $today=date("Y-m-d H:i:s");
             $model_coupon->create_date=$today;
             $model_coupon->user_id=$user_id;
             $model_coupon->code="1";
             
             $discount=$model_coupon->discount;
             $qty_use=$model_coupon->qty_use;
             $detail=$model_coupon->detail;
             $qty_gen=$_POST['quanlity'];
             $prefix=$_POST['prefix'];
                       
                      
             if($model_coupon->validate()){
                 
                 for($i=1;$i<=$qty_gen;$i++){
                    
                     $model_coupon= new Coupon();
                    
                     $model_coupon->create_date=$today;
                     $model_coupon->user_id=$user_id;
                     $model_coupon->expire_date=$year."-".$month."-".$day;
                     $model_coupon->discount=$discount;
                     $model_coupon->course_id=$course;
                     $model_coupon->qty_use=$qty_use;  
                     $model_coupon->detail=$detail;
                     
                     if(empty($prefix)){
                         $prefix=(int)date("m")+$i;
                     }
                     
                     $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263ABCDEFGHIJKLMNOPQRSTUVWXYZ@#$%&'), 0, 5);
                     $random2 = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263ABCDEFGHIJKLMNOPQRSTUVWXYZ@#$%&'), 0, 3);
                     $datesecond=date("s")+$i;
                     $code=$user_id.$random.$i.$datesecond.$random2;
                           //user_id-Random5digit-order-second-random
                     $code = substr(str_shuffle($code), 0, 15);
                     $code =$prefix.$code;
                  
                     
                     $model_coupon->code=$code;
                     $model_coupon->save();
                    
                     
                 }
                     
                 $success_link = Yii::app()->createUrl("admin/default/directionCoupong/course/$course/date/+$today+");
                 $this->redirect($success_link);
             }
        }
        
        $this->render("form_coupong",array("model_course"=>$model_course,
                                           "model_coupon"=>$model_coupon));
        
    }
    
    #after create coupon show option link for select
    public function actionDirectionCoupong($course,$date){
         $this->checkOwner($course);
         $model_course = Course::model()->findByPk($course);
         $this->render("direction_coupon",array("model_course"=>$model_course,"date"=>$date));
        
    }
    
    
    #print coupong in same round <<< use $date to grooup data
    public function actionPrintCoupon($date,$course) {
        $this->layout = "//layouts/print";
        $this->checkOwner($course);
      
        
        $model_course= Course::model()->findByPk($course);
        $criteria = new CDbCriteria;
        $criteria->condition = "create_date='$date'";
        

        $dataProvider = new CActiveDataProvider('Coupon', array(
            'pagination'=>false,
            'criteria' => $criteria,
        ));


        $this->render("print_coupon", array("dataProvider"=>$dataProvider,"model_course"=>$model_course));
    }
    
    #before course And AfterCourse
    public function actionRelateCourse($course,$message=""){
        $model_course=  Course::model()->findByPk($course);
        
        $model_course_all = Course::model()->findAll("id!='$course' AND id!='-1'");
     
        $all_course=array();
        if(!empty($model_course_all)){
            foreach ($model_course_all as $course_list){
                $all_course[$course_list->id]=$course_list->name;
           }
        }
        
        $relateCourse=  RelateCourse::model()->find("course_id='$course'");
        $before_course=array();
        $after_course=array();
        
        if(!empty($relateCourse)){
            
            if(!empty($relateCourse->before_course)){
                $before_course= explode(",", $relateCourse->before_course);
            }
           
            if(!empty($relateCourse->after_course)){
                $after_course= explode(",", $relateCourse->after_course);
            }
            
        }
        
        
        if(isset($_POST['beforeList'])||isset($_POST['afterList'])){
            
         
            
             $before_course=$_POST['beforeList'];
             $after_course=$_POST['afterList'];
            
             if(empty($relateCourse)){
                 $relateCourse=new RelateCourse();
             }
             
             $before_course_list="";
             $after_course_list="";
             
             if(!empty($before_course)){
                foreach($before_course as $before_course_id){
                     $before_course_list.=$before_course_id.",";
                 }
                 $before_course_list=rtrim($before_course_list,',');
             }
             
             if(!empty($after_course)){
                 foreach ($after_course as $after_course_id){
                     $after_course_list.= $after_course_id.",";
                 }
                 $after_course_list=  rtrim($after_course_list,',');
             }
             
             
            $relateCourse->course_id=$course;
            $relateCourse->before_course=$before_course_list;
            $relateCourse->after_course=$after_course_list;
            $relateCourse->save();        
            
            $success_url=  LinkHelper::yiiLink("/admin/default/relateCourse/course/1/message/Data has been saved.");
            $this->redirect($success_url);
            
                      
        }
               
        
        $this->render("form_relate_course",array("model_course"=>$model_course,
                                                "all_course"=>$all_course,
                                                "before_course"=>$before_course,
                                                "after_course"=>$after_course,
                                                "message"=>$message));
        
    }
    
    #Exercise 1st step
    public function actionOperateExercise($id="",$course,$type="lesson"){
        $model_course=  Course::model()->findByPk($course);
        $model_content=  Content::model()->findByPk($id);
        $model_exercise= Exercise::model()->find("content_id=$id");
        
        $qty_question=0;
        if(!empty($model_exercise)){
        $qty_question= count($model_exercise->question);
        }
        
        
        if(empty($model_content)){
            $model_content=new Content();
        }
        
        if(empty($model_exercise)){
            $model_exercise=new Exercise();
        }
        
        if(isset($_POST["Content"])||$_POST["Exercise"]){
              $model_content->attributes = $_POST['Content'];
              $model_exercise->attributes = $_POST['Exercise'];
                   
              $model_content->is_exercise="1";
              $model_content->course_id=$course;
              if($model_content->validate()){
                   $model_content->save();
                    if(empty($model_exercise)){
                       $model_exercise->content_id=$model_content->id;
                    }
                   $model_exercise->save();
                    if($qty_question==0){
                         $success_url=  LinkHelper::yiiLink("admin/default/OperateQuestion/exercise/$model_exercise->id/course/$course");
                    }else{
                         $success_url=  LinkHelper::yiiLink("admin/default/showQuestion/exercise/$model_exercise->id/course/$course");
                    }
                   $this->redirect($success_url);
               }
              
                            
        }
                
       $this->render("form_exercise",array("model_content"=>$model_content,
                                           "all_unit"=>$all_unit,
                                           "model_course"=>$model_course,
                                           "model_exercise"=>$model_exercise,
                                           "qty_question"=>$qty_question));      
    }
    
    
    //order Question
    public function actionShowQuestion($exercise,$course){
       $model_course=  Course::model()->findByPk($course);
       $model_all_question=  ExerciseQuestion::model()->findAll("exercise_id='$exercise' ORDER BY show_order ASC,id ASC");
       $model_exercise=  Exercise::model()->findByPk($exercise);
       $model_content=  Content::model()->findByPk($model_exercise->content_id);
       
       $this->render("show_question",array("model_course"=>$model_course,
                                            "model_all_question"=>$model_all_question,
                                            "model_content"=>$model_content,
                                            "model_exercise"=>$model_exercise));
       
    }
    
    
    
    //add Edit Question and answer
    public function actionOperateQuestion($exercise,$course,$question="",$message="",$type=""){
       $model_course=  Course::model()->findByPk($course);
       $model_all_question=  ExerciseQuestion::model()->findAll("exercise_id='$exercise' ORDER BY show_order ASC,id ASC");
       $qty_question=count($model_all_question);
       
        
        
        if(!empty($question)){
            $mode="edit";
            $model_question=  ExerciseQuestion::model()->findByPk($question);
            $answer_all= ExerciseAnswerChoice::model()->findAll("question_id='$model_question->id' ORDER BY is_true DESC,id ASC");
                     
        
            if(!empty($answer_all)){
                $i=0;
                foreach ($answer_all as $answer){
                   $list_answer[$i]=$answer;
                   $i++;                  
                }
            }
          
                        
                for($j=$i;$j<=$i;$j++){
                    $list_answer[$j]=new ExerciseAnswerChoice();
                }
            
            
        }else{
             $mode="new";
            $model_question= new ExerciseQuestion();
              for($i=0;$i<=4;$i++){
                    $list_answer[$i]=new ExerciseAnswerChoice();
              }          
            
        }
        
        if(isset($_POST['ExerciseQuestion'])||isset($_POST['ExerciseAnswerChoice'])){
            $is_pass=true;
            
            $model_question->attributes = $_POST['ExerciseQuestion'];
            $model_question->exercise_id=$exercise;
            
            $answer_post=$_POST['ExerciseAnswerChoice'];
            $count_answer=0;
          
 
            foreach($answer_post as $key=>$answer){
                      
                if(empty($list_answer[$key])){
                    $list_answer[$key]=new ExerciseAnswerChoice();
                }
                
                 $list_answer[$key]->attributes=$answer;
               
                
                     
                if(!empty($list_answer[$key]->answer)){
                    $count_answer++;
               }
            }
            
            
            if(empty($list_answer[0]->answer)){
               $list_answer[0]->addError("[0]answer", ' **Please Set correct answer.');
               $is_pass=false;
            }
            

            
            if($count_answer<2){
               $list_answer[0]->addError("[0]answer", ' **Answer Must have least 2 choices.');
               $is_pass=false;
            }
            
            if($model_question->validate() && $is_pass==true){
                $model_question->save();
                foreach($list_answer as $key=>$answer){
                    if(!empty($answer->answer)){
                        $answer->question_id=$model_question->id;
                        $answer->save();
                    }
                }
               $message="Data has Been save"; 
                 $success_url=  LinkHelper::yiiLink("/admin/default/OperateQuestion/exercise/$exercise/course/$course/question/$model_question->id/message/$message");
               //$success_url=  LinkHelper::yiiLink("admin/default/showQuestion/exercise/4/course/1/");
               $this->redirect($success_url);
                             
         
            }
            
            //answer array of obj
            
        }
        
        $this->render("form_question",array("model_all_question"=>$model_all_question,
                                            "model_question"=>$model_question,
                                            "model_course"=>$model_course,
                                            "exercise"=>$exercise,
                                            "list_answer"=>$list_answer,
                                            "message"=>$message,"qty_question"=>$qty_question,
                                            "mode"=>$mode));
        
        
        
    }
    
    #Delete Lesson type lesson
    public function actionDeleteExerciseContent($id){
        $model_content=  Content::model()->findByPk($id);
        $course_id=$model_content->course_id;
        $model_exercise= Exercise::model()->find("content_id='$id'");
        if(!empty($model_exercise)){
            $model_questions=  ExerciseQuestion::model()->findAll("exercise_id='$model_exercise->id'");
            if(!empty($model_questions)){
                foreach ($model_questions as $question){
                    $model_answers=  ExerciseAnswerChoice::model()->findAll("question_id='$question->id'");
                    if(!empty($model_answers)){
                        foreach ($model_answers as $ans){
                            $ans->delete();
                        }
                    }
                    $question->delete();
                }
            }
            $model_exercise->delete();
        }
        $model_content->delete();     
       
        $success_link = LinkHelper::yiiLink("/admin/default/manageContent/course/$course_id");
        $this->redirect($success_link);
    }
    
    #Delete Question in Exercise
    public function actionDeleteQuestion($id,$course){
       
        $model_question=  ExerciseQuestion::model()->findByPk($id);
        $model_question->delete();
        $exrecise_id=$model_question->exercise_id;
        //$success_link=LinkHelper::yiiLink("/admin/default/OperateQuestion/exercise/$exrecise_id/course/$course");
        $success_link=LinkHelper::yiiLink("admin/default/showQuestion/exercise/$exrecise_id/course/$course");
       
        $this->redirect($success_link);
    }
    
    #Delete choice in Exercise >>> Question
    public function actionDeleteAnswerChoice($id,$course,$excercise){
        $model_answer= ExerciseAnswerChoice::model()->findByPk($id);
        $question_id=$model_answer->question_id;
        $count_answer_all=  ExerciseAnswerChoice::model()->count("question_id='$question_id'");
        if($count_answer_all>2){
            $model_answer->delete();
            $success_link=  LinkHelper::yiiLink("/admin/default/OperateQuestion/exercise/$excercise/course/$course/question/$question_id/message/Update success");
            $this->redirect($success_link);
        }else{
            $unsuccess_link=  LinkHelper::yiiLink("/admin/default/OperateQuestion/exercise/$excercise/course/$course/question/$question_id/message/Can't not delete answer must have at least 2 choices./type/error");
            $this->redirect($unsuccess_link);
        }
    }
    
    
    public function actionSortQuestion() {

        if (!empty($_POST)) {
            foreach ($_POST as $question_list) {

                foreach ($question_list as $sort_order => $question_id) {
                    $model = ExerciseQuestion::model()->findByPk($question_id);

                    $model->show_order = $sort_order+1;
                    $model->save();
                }
            }
        }
    }
    
    #show Stat in each Question
    public function actionShowStatInQuestion($exercise,$course,$question){
        $model_course=  Course::model()->findByPk($course);
        $model_question=  ExerciseQuestion::model()->findByPk($question);
                        
        $this->render("stat_in_question",array("model_question"=>$model_question,"model_course"=>$model_course,"exercise"=>$exercise));        
    }
    
    #show Qty Answer Correct SCORE FROM all User 
    public function actionShowStatScorePhase($content,$exercise,$course){
    
        $model_course=  Course::model()->findByPk($course);
        $model_exercise= Exercise::model()->findByPk($exercise);
        $model_content= Content::model()->findByPk($content);
        
        $this->render("show_stat_score_phase",array("model_course"=>$model_course,
                                                    "model_exercise"=>$model_exercise,
                                                    "model_content"=>$model_content));       
    }
    
    
   
    
        
    
    
        
}