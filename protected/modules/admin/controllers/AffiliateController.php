<?php
class AffiliateController extends Controller
{
    
    public function actionIndex(){    
       
        $criteria = new CDbCriteria;
        $criteria->order = "create_date DESC";

       

        $dataProvider = new CActiveDataProvider('UserAffiliate', array(
            'pagination' => array('pageSize' => "10"),
            'criteria' => $criteria,
        ));
        
        $this->render("index",array("dataProvider"=>$dataProvider));       
    }
    
    
    
    //ajax
    public function actionChangeState(){
        $state= $_POST['state'];
        $id= $_POST['id'];
               
        $model_affiliate = UserAffiliate::model()->findByPk($id);
        
        if($state==1){
           $model_affiliate->active_status=0;
           $model_affiliate->affiliate_code= "";
                     
        }else{
            $model_affiliate->active_status=1;
            $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263'), 0, 5);
            $model_affiliate->affiliate_code=$random.$model_affiliate->user_id;            
        }
        
        $model_affiliate->save(false);
        
        if($model_affiliate->active_status==1){
            echo "images/correct.gif";
        }else{
            echo "images/incorrect.gif";
        }
        
        
    }
    
        
    
    
    
    
}