<div class="w-container">       
    <div class="w-row">

        <div id="nav-content" class="w-col w-col-4">            
            <?php $this->renderPartial("_affiliate_menu", array("mode"=>"list")); ?>     
        </div>

        <div class="w-col w-col-8 affiliate-content">

            <h3>Affiliate List</h3> <hr class="dark"/>


              <?php
                                    $this->widget('zii.widgets.grid.CGridView', array(
                                        'dataProvider' => $dataProvider,
                                        'columns' => array(
                                            array(
                                                'header' => Yii::t("site","No."),
                                                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)."."',
                                                'htmlOptions'=>array("style"=>"text-align:center;"),
                                            ),
  
                                           
                                            
                                             array(
                                        
                                                'class'=>'CLinkColumn',
                                                'header'=>Yii::t("site","username"),
                                                'urlExpression'=>'Yii::app()->createUrl("admin/affiliate/userDetail",array("course_id"=>$data->user_id))',
                                                'labelExpression' => '$data->user->username',                                                
                                                'htmlOptions'=>array("style"=>"text-align:center"),

                                            ), 
                                            
                                            
                                            array(
                                                'name' => 'create_date',
                                                'value' => 'SiteHelper::dateFormat($data->create_date)',
                                                'htmlOptions'=>array("style"=>"text-align:center;"),
                                            ),                     
                                          
                                            
                                              array(
                                                'header'=>Yii::t("site","active status"),
                                                'type' => 'raw',
                                                'value' => 'CHtml::image(($data->active_status==1)? "images/correct.gif" : "images/incorrect.gif"
                                                            , "On", array("onclick" => "return confirmApprove($data->active_status,$data->id)"
                                                            ,"id"=>"onoff_".$data->id))',
                                                   'htmlOptions'=>array("style"=>"text-align:center;"),
                                               ),
                                            
                                        ),
                                        
                                         'template' => "{pager}\n{items}\n{pager}",
                                        'pager' => array(
                                            'class' => 'CLinkPager',
                                            'header' => 'Page: ',
                                            'firstPageLabel' => 'firstPageLabel',
                                            'prevPageLabel' => 'prevPageLabel',
                                            'nextPageLabel' => 'nextPageLabel',
                                            'lastPageLabel' => 'lastPageLabel',
                                        )
                                    ));
                                    ?>
            
            
        </div>

    </div>
</div>

<script>
    function confirmApprove(active,id){
        if(active==1){
            var msg = confirm('Would you like to UnApprove ?');
       
        }else{
             var msg = confirm('Would you like to Appove ?');            
        }
        
        if(msg == false) {          
            return false;
        }else{
            
              $.ajax({
                  url: '<?php echo Yii::app()->createUrl("admin/affiliate/changeState"); ?>',
                  type: 'POST',
                  data:{
                        'state':active,
                        'id':id,
                       
                       },
                  dataType: 'html',
  
                  success: function(result) {
                    alert(result);              
                    $("#onoff_"+id).attr("src",result);
                    
                  }
                });
        }
    }
</script>
