  <div class="bg-pagestudy">
    <div class="w-container">
      <div class="admin-page">
        <div class="w-form">
            
            
            
            
         <?php
                    //use Ajax for submit and save data
                  $form = $this->beginWidget('CActiveForm', array(
                       'id' => 'course-content',
                       'htmlOptions' => array(
                           'enctype' => 'multipart/form-data',
                       ),
                       'clientOptions' => array(
                           'validateOnSubmit' => false,
                       ),
                   ));
           ?>
            
            
            <h3 class="super-header">User Permission</h3>
            <label for="email">Find User With Keyword E-mail</label>
            <div class="w-row">
              <div class="w-col w-col-9">
                <input class="w-input"  placeholder="Enter email address" name="email" required="required">
              </div>
              <div class="w-col w-col-3"><button type="submit" value="search" name="btn-submit" class="button admin-info-btn admin-big-more btn-warning" href="#">Search</button>
              </div>
            </div>
          
 <?php $this->endWidget(); ?>   
            
            
            
            
          <div class="w-form-done">
            <p>Thank you! Your submission has been received!</p>
          </div>
          <div class="w-form-fail">
            <p>Oops! Something went wrong while submitting the form :(</p>
          </div>
        </div>
      </div>
      <div class="super-div-content">
          
          <?php
          $this->widget('zii.widgets.CListView', array(
              'id' => 'user_list_permission',
              'dataProvider' => $users,
              'itemView' => '_user_list',
              'template' => " {summary}\n{pager} \n {items}",
              //'viewData' => array('lang' => $lang),
              'pager' => array(
                  'class' => 'CLinkPager',
              ),
          ));
          ?>     

  
      </div>
    </div>
  </div>



<script>
$( "a.super-booton-state" ).click(function(event) {
  event.preventDefault(); 
});


function setsuper(user_id){
     $.ajax({                      
        url: "<?php echo Yii::app()->createUrl("admin/permission/setSuper"); ?>",                         
        dataType: 'html',
        type: 'POST',
        data:{
              'user_id':user_id                          
        },
        cache: false,
        success:function(result){ 
            if(result==1){
                $("#super-"+user_id).removeClass("btn-info");
                $("#teacher-"+user_id).removeClass("btn-info");
                $("#inside-"+user_id).removeClass("btn-info");
                
                $("#super-"+user_id).addClass("btn-primary");
                $("#teacher-"+user_id).addClass("btn-primary");
                $("#inside-"+user_id).addClass("btn-primary");
                
                $("#super-"+user_id).html("<span class='glyphicon glyphicon-ok'></span> Set Super");
                $("#teacher-"+user_id).html("<span class='glyphicon glyphicon-ok'></span> Set Teacher");
                $("#inside-"+user_id).html("<span class='glyphicon glyphicon-ok'></span> Set Inside");
              
                
            }else{                
                $("#super-"+user_id).addClass("btn-info");      
                $("#super-"+user_id).removeClass("btn-primary");               
                $("#super-"+user_id).html("Set Super");
              
            }
         }          
     }); 
}

function setTeacher(user_id){
 $.ajax({                      
        url: "<?php echo Yii::app()->createUrl("admin/permission/setTeacher"); ?>",                         
        dataType: 'html',
        type: 'POST',
        data:{
              'user_id':user_id                          
        },
        cache: false,
        success:function(result){ 
            if(result==1){               
                $("#teacher-"+user_id).removeClass("btn-info");
                $("#inside-"+user_id).removeClass("btn-info");                
              
                $("#teacher-"+user_id).addClass("btn-primary");
                $("#inside-"+user_id).addClass("btn-primary");                
              
                $("#teacher-"+user_id).html("<span class='glyphicon glyphicon-ok'></span> Set Teacher");
                $("#inside-"+user_id).html("<span class='glyphicon glyphicon-ok'></span> Set Inside");
              
                
            }else{
                $("#teacher-"+user_id).removeClass("btn-primary"); 
                $("#teacher-"+user_id).addClass("btn-info");  
                $("#teacher-"+user_id).html("Set Teacher");
              
            }
         }          
     }); 
}

function setInside(user_id){

     $.ajax({                      
        url: "<?php echo Yii::app()->createUrl("admin/permission/setInside"); ?>",                         
        dataType: 'html',
        type: 'POST',
        data:{
              'user_id':user_id                          
        },
        cache: false,
        success:function(result){ 
                         
             if(result==1){               
                $("#inside-"+user_id).removeClass("btn-info");
                $("#inside-"+user_id).addClass("btn-primary");
                $("#inside-"+user_id).html("<span class='glyphicon glyphicon-ok'></span> Set Inside");
              
                
            }else{
                $("#inside-"+user_id).removeClass("btn-primary"); 
                $("#inside-"+user_id).addClass("btn-info");  
                $("#inside-"+user_id).html("Set Inside");
              
            }
         }          
     });
}

</script>