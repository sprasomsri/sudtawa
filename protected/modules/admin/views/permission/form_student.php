<div class="bg-pagestudy">
    <div class="w-container">
      <div class="admin-page">
        <div class="w-form">
            

            
            <h3 class="super-header">Student Form</h3>
           
            
               <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'RegistrationStudent',                          
                            'enableClientValidation' => true,
                            'enableAjaxValidation' => true,
                            'htmlOptions' => array("role" => "form"),
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                 ?> 
            
            
                 <?php echo $form->errorSummary(array($model_regist,$profile)); ?>
              
               <div class="form-group">
               <label for="exampleInputPassword1">username*</label>         
               <?php echo $form->textField($model_regist, 'username',array("placeholder"=>"username","class"=>"form-control")); ?>
               <?php //echo $form->error($model, 'username'); ?>
              </div> 
               
              
<!--              <div class="form-group">
               <label for="exampleInputPassword1">อีเมล</label>        
                   
               <?php //echo $form->textField($model_regist, 'email',array("placeholder"=>"อีเมล","id"=>"exampleInputEmail1","class"=>"form-control")); ?>
               <?php //echo $form->error($model, 'username'); ?>
              </div> -->
              
              
               <div class="form-group">
                <label for="exampleInputPassword1">รหัสผ่าน*</label>
                <?php echo $form->PasswordField($model_regist, 'password',array("placeholder"=>"รหัสผ่าน","id"=>"inputPassword3","class"=>"form-control")); ?>
              </div>
              
  
            
             <div class="form-group">
                <label for="exampleInputPassword1"> ชื่อ</label>
                     <?php echo $form->textField($profile,'firstname',array("class"=>"form-control")); ?>
              </div>
            
            
              <div class="form-group">
                  <label for="exampleInputPassword1"> นามสกุล </label>
                     <?php echo $form->textField($profile,'lastname',array("class"=>"form-control")); ?>
              </div>
            
              <button type="submit" class="btn btn-warning"><?php echo Yii::t("site","Save"); ?></button>
            

            
             <?php $this->endWidget(); ?>
          


        
        </div>
      </div>
     
    </div>
  </div>

