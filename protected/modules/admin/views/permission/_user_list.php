<div class="w-row super-row-user">
   
    <?php 
    
        $class_teacher = "btn-info";
        $class_super = "btn-info";
        $class_inside = "btn-info";
        
        $word_teacher = "Set Teacher";
        $word_superuser = "Set Super";
        $word_inside = "Set Inside";
        
        if($data->permission->can_create_course==1){
            $class_teacher="btn-primary";
            $word_teacher= "<span class='glyphicon glyphicon-ok'></span> ".$word_teacher;
        } 
        
        if($data->superuser==1){
             $class_super = "btn-primary";
             $word_superuser= "<span class='glyphicon glyphicon-ok'></span> ".$word_superuser;
        }
        
        if($data->is_inside==1){
             $class_inside = "btn-primary";
              $word_inside= "<span class='glyphicon glyphicon-ok'></span> ".$word_inside;
        }
    
    
    ?>
    
    
    <div class="w-col w-col-6 super-userinfo">
        <div><strong><?php echo $data->username; ?>&nbsp;&nbsp;&nbsp;
                <?php if(!empty($data->profile->firstname)){ ?>
                (<?php echo $data->profile->firstname." ".$data->profile->lastname; ?>)
                <?php } ?>
                
                <br/></strong>Register date : <?php echo SiteHelper::dateFormat($data->create_at); ?></div>
    </div>
    <div class="w-col w-col-2 text-center">
        <a id="super-<?php echo $data->id;?>" class="button super-booton-state btn <?php echo $class_super; ?>" href="#" onclick="setsuper(<?php echo $data->id; ?>)"><?php echo $word_superuser; ?></a>
    </div>
    <div class="w-col w-col-2 text-center">
        <a id="teacher-<?php echo $data->id;?>" class="button super-booton-state btn <?php echo $class_teacher; ?>" href="#" onclick="setTeacher(<?php echo $data->id; ?>)"><?php echo $word_teacher; ?></a>
    </div>
    <div class="w-col w-col-2 text-center">
        <a id="inside-<?php echo $data->id;?>" class="button super-booton-state btn <?php echo $class_inside; ?>" href="#" onclick="setInside(<?php echo $data->id; ?>)"><?php echo $word_inside; ?></a>
    </div>
</div>

