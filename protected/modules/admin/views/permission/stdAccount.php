  <style type="text/css" title="currentStyle">
          @import "js/datatable/css/demo_page.css";
          @import "js/datatable/css/demo_table.css";
      </style>
      <script type="text/javascript" language="javascript" src="js/datatable/js/jquery.js"></script>
      <script type="text/javascript" language="javascript" src="js/datatable/js/jquery.dataTables.js"></script>
      <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {

              $('#project-list').dataTable({
//                  "bServerSide": true,
//                   "bJQueryUI": true,
                   "aaSorting": [],
                   "sDom": '<"H"lfrp>t<"F"ip>',
                  "sPaginationType": "full_numbers",
                  "iDisplayLength": 25,
                  "aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]]
              });
          });
      </script>
      
      
<div class="bg-pagestudy">
    <div class="w-container">
      <div class="admin-page">
        <div class="w-form">
            
            <p class="text-right">
                <a class="btn btn-warning" href="<?php echo Yii::app()->createUrl("admin/permission/OperateStudent");?>"> เพิ่มนักเรียน </a>
            </p>
            
            <h3 class="text-left"> รายการนักเรียน </h2>
            <p> 1000 ลำดับล่าสุด </p>
         
            
            
            <table id="project-list" width="100%" style="">
            <thead>
                <tr>
                    <th class="text-center">  </th>
                    <th class="text-center"> username  (รหัสนักศึกษา)</th>
                    <th class="text-center"> ชื่อ - สกุล</th>
                    <th class="text-center">E-mail</th>
                    <th class="text-center">จัดการ</th>
                 
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($model_list_student)){ 
                    $count_row =1;
                    foreach ($model_list_student as $student){
                        $class_user="";
                         if ($student->status == 99) {
                            $class_user = "block-user";
                        }
                ?>  
                    <tr class="<?php echo $class_user; ?>">
                        <td class="text-center"> <?php echo $count_row;?> </td>
                        <td> <?php echo $student->username;?> </td>
                        <td> 
                            <?php 
                                if(!empty($student->profile)){
                                echo $student->profile->firstname."&nbsp;&nbsp;".$student->profile->lastname;
                                }
                            
                            ?> 
                        
                        </td>
                        <td> <?php echo $student->email;?> </td>
                        <td class="text-center"> 
                            <a href="<?php echo Yii::app()->createUrl("admin/permission/OperateStudent",array("student_id"=>$student->id));?>"> แก้ไข </a> | 
                            <?php 
                            
                            $active_message= "";
                            if($student->status == 1){
                                $active_message = "ถอนสิทธิการใช้งาน";
                            }  
                            if($student->status == 99){
                                $active_message = "อนุญาติให้ใช้งาน";
                            }
                            
                            ?>
                            <a href="<?php echo Yii::app()->createUrl("admin/permission/setActive",array("student_id"=>$student->id,"is_active"=>$student->status));?>"> <?php echo $active_message; ?> </a>
                        
                        </td>
                       
                        
                    </tr>
                <?php 
                    $count_row++;
                
                    }} ?>
            </tbody>
            </table>
            
        </div>
      </div>
     
    </div>
  </div>

