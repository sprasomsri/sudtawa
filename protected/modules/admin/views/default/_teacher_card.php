<?php if ($index % 3 == 0 || $index == 0) { ?>
    <?php $state = "new_div"; ?>
    <div class="w-row admin-row-user"> 
        <?php
    } else {
        $state = "not_new_div";
    }
    ?>
        
        <div class="w-col w-col-4">
            <div class="admin-student-box">
                <div class="amdin-profile-photo">
                    
               <?php 
                $img_source="images/user_default.jpg";
                if(!empty($data->photo)){
                      $img_source="userphoto/".$data->photo;    
                }else{
                    if(!empty($data->facebook_id)){
                       $img_source="https://graph.facebook.com/$data->facebook_id/picture"; 
                    }
                }                          
                ?>
                    
                    
                    <img class="admin-img-profile" src="<?php echo $img_source; ?>" >
                </div>
                <div class="w-row admin-person-info">
                    <div class="w-col w-col-3">
                        <div class="admin-left-label"><strong>อีเมล</strong>
                        </div>
                    </div>
                    <div class="w-col w-col-9">
                        <div><?php echo $data->username; ?></div>
                    </div>
                </div>
                <div class="w-row admin-person-info">
                    <div class="w-col w-col-3">
                        <div class="admin-left-label"><strong>  ชื่อ </strong>
                        </div>
                    </div>
                    <div class="w-col w-col-9">
                        <div><?php echo $data->name." ".$data->lastname; ?> </div>
                    </div>
                </div>
                <div class="admin-info-btn">
                    <div class="w-row">
                        <div class="w-col w-col-5 text-left">
                            <!--admin-inactive-teacher-->
                            <?php  
                            $class_head="";
                            $class_show="";
                            
                            if($data->is_head==0){
                                $class_head = "admin-inactive-teacher";
                            }
                            
                            if($data->is_show==0){
                                $class_show = "admin-inactive-teacher";
                            }
                            
                            ?>
                            <div id="head-<?php echo $data->id?>" class="admin-inline-box head-state <?php echo $class_head; ?>">ครูหลัก</div>
                            
                            <div class="admin-inline-box admin-inactive-teacher">|</div>
                            
                            <div id="show-<?php echo $data->id?>" class="admin-inline-box <?php echo $class_show; ?>">แสดง</div>
                        </div>
                                       
                        
                        <div class="w-col w-col-7 admin-teacher-btn-group">
                            <a id="<?php echo "teacher-".$data->id; ?>" onclick='setHeader(<?php echo $data->id; ?>)' class="button admin-info-btn admin-margin-btn" href="#">ครูหลัก</a>
                            <a id="<?php echo "show-teacher-".$data->id; ?>" onclick='setShow(<?php echo $data->id; ?>)' class="button admin-info-btn admin-active-btn" href="#">แสดง</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

<?php if ($state == "not_new_div" && (($index + 1) % 3 == 0)) { ?>
    </div>
<?php } ?>

