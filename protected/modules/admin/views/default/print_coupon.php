<script type="text/javascript">
    $(document).ready(function(doc) {
        $('body').removeClass('front').addClass('allcourses');
    })
</script>
<div id="maincontent">
    <div class="container-fluid">
        <div class="container">

            <?php
            $this->widget('zii.widgets.CListView', array(
                'id' => 'courselist',
                'dataProvider' => $dataProvider,
                'itemView' => '_coupon_box',
                'template' => " {summary}\n{pager} \n {items}",
                'viewData' => array('course_img' => $model_course->course_img,'course_name'=>$model_course->name),
                'pager' => array(
                    'class' => 'CLinkPager',
                ),
            ));
            ?>

            <?php
            if ($qty_coupon % 3 != 0) {

                echo "</div>"; // close div reow if qty block can't % with 3 
            }
            ?>
        </div>
    </div>
</div>
