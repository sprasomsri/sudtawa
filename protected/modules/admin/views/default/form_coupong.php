<div class="body-admin">
    <div id="maincontent">

        <div class="container">
            <div class="row-fluid">

  
                <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id)); ?>

                <div class="span9">
                    <div class="container-fluid">
                        <div class="admin-bg-body row-course">
                            <div class="container-fluid">

                               <?php
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'form-coupon',
                                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                                    'clientOptions' => array(
                                        'validateOnSubmit' => true,
                                    ),
                                 ));
                                ?>
                                  
                               
                                    <!--Course Detail-->
                                    <div class="row-fluid row-course">
                                        <h2><img src="images/course-detail-msg.png" class="h2-img" /> <?php echo Yii::t("site","Form");?>-<?php echo Yii::t("site","Coupon");?></h2>
                                        <h4><?php echo $model_course->name."-".$model_course->name_th;?></h4>
                                        <div class="row-fluid line-lightgrey-wrapper">&nbsp;</div> 
                                    </div>

                                    <?php echo $form->errorSummary($model_coupon); ?>
                                   
                                    
                                    
                                    <div class="row-fluid content">
                                        <div class="span1"></div>
                                        <div class="span2" align="left"><?php echo Yii::t("site","Discount");?></div>
                                        <div class="span8" align="left">
                                              <?php echo $form->textField($model_coupon,'discount'); ?> (%)
                                        </div>
                                        <div class="span1"></div>
                                    </div>                            

                                    <div class="row-fluid content">
                                        <div class="span1"></div>
                                        <div class="span2" align="left"><?php echo Yii::t("site","Quality Use");?></div>
                                        <div class="span8" align="left">
                                            <?php echo $form->textField($model_coupon, 'qty_use'); ?>
                                            <span class="note">1 คูปอง ใช้ได้กี่ User </span>
                                        </div> 
                                        <div class="span1"></div>
                                    </div>
                                    
                                    
                                     <div class="row-fluid content">
                                        <div class="span1"></div>
                                        <div class="span2" align="left"><?php echo Yii::t("site","Quality Coupon");?></div>
                                        <div class="span8" align="left">
                                           <input type="text" name="quanlity" value="1" />
                                        </div>
                                        <div class="span1"></div>
                                    </div>
                                    
                                    <div class="row-fluid content">
                                        <div class="span1"></div>
                                        <div class="span2" align="left"><?php echo Yii::t("site","Prefix");?></div>
                                        <div class="span8" align="left">
                                           <input type="text" name="prefix" placeHolder="sci" />
                                        </div>
                                        <div class="span1"></div>
                                    </div>
                                    
                                    
                                    
                                    <div class="row-fluid content">
                                        <div class="span1"></div>
                                        <div class="span2" align="left"><?php echo Yii::t("site","Expire Date");?></div>
                                        <div class="span8" align="left">
                                            <?php
                                            Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                                            $this->widget('CJuiDateTimePicker', array(
                                            'model' => $model_coupon, //Model object
                                            'attribute' => 'expire_date', //attribute name
                                            'mode' => 'date', //use "time","date" or "datetime" (default)
                                            'options' => array(), // jquery plugin options
                                            ));
                                            ?>
                                        </div>
                                        <div class="span1"></div>
                                    </div>
                                    
                                    
                                    <div class="row-fluid content">
                                        <div class="span1"></div>
                                        <div class="span2" align="left"><?php echo Yii::t("site","Coupon Description");?></div>
                                        <div class="span8" align="left">
                                            <?php echo $form->textArea($model_coupon, 'detail',array("rows"=>"5","style"=>"width:504px")); ?>
                                        </div>
                                        <div class="span1"></div>
                                    </div> 

                                  

                                   

                                    <div class=" row-course">&nbsp;</div>
                                    <!--submit button-->
                                    <div class="row-fluid content row-course">                                    
                                        <div class="span3"></div>
                                        <div class="span9" align="right">
                                            <div class="span8"></div>
                                       
                                            
                                           
                                            
                                            
                                            <button type="submit" class="span3 btn btn-green-submit"><?php echo Yii::t("site","Next");?></button>
                                        </div>                                    
                                    </div>
                                    <!--submit button-->

                               <?php $this->endWidget(); ?>

                            </div><!--container-fluid-->
                        </div><!--admin-bg-body row-course-->
                    </div><!--/row-fluid-->
                </div><!--/span9-->

            </div><!--row-fluid-->
        </div><!--container-->

    </div><!--/maincontent-->        
</div> <!-- body-allcourses -->
