<div class="bg-pagestudy">
    <div class="w-container">

      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
          </div>
            
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">แบบฝึกหัด</h1>
              <?php if(!empty($model_question)){ ?>
              <p>หน้าคำถาม</p>
              <?php } ?>
              
              
               <!----- Right content------>           
               <?php
               $form = $this->beginWidget('CActiveForm', array(
                   'id' => 'add-course',
                   'htmlOptions' => array('enctype' => 'multipart/form-data'),
                   'clientOptions' => array(
                       'validateOnSubmit' => true,
                   ),
               ));
               ?>
               
                <?php echo $form->errorSummary($model_content); ?>
               
               <div class="form-group">                
                <label> ชื่อแบบฝึกหัด</label>
                 <?php echo $form->textField($model_content, 'title',array("class"=>"form-control")); ?>
               </div>

                <div class="form-group">                
                <label> คำอธิบาย</label>                 
                 <?php
                                        
                                        $this->widget('application.widgets.redactorjs.Redactor', array('model' => $model_content, 'attribute' => 'detail',
                                            'toolbar' => "mini",
                                            "htmlOptions" => array("class"=>"form-control","style" => "height:300px; width:100%;"),
                                            'editorOptions' => array(
                                                //action for img upload
                                                'imageUpload' => Yii::app()->createAbsoluteUrl('post/img'),
                                                'imageGetJson' => Yii::app()->createAbsoluteUrl('post/listimages'),
                                                'fileUpload' => Yii::app()->createAbsoluteUrl('post/uploadFile'),
                                                'videoUpload' => Yii::app()->createAbsoluteUrl('post/uploadSelfVideo'),
                                                'videoGetJson' => Yii::app()->createAbsoluteUrl('post/listVideo'),
                                                'audioUpload' => Yii::app()->createAbsoluteUrl('post/uploadAudio'),
                                                'audioGetJson' => Yii::app()->createAbsoluteUrl('post/listAudio'),
                                            ),));
                                        ?>  
               </div>
               
                <div class="form-group">                
                <label> จำนวนคำถามที่แสดง</label>                 
                  <?php
                                        if($qty_question==0){
                                            $qty_question="10";
                                        }
                                        $array_order = array();
                                        for ($i = $qty_question; $i >= 1; $i--) {
                                            $array_order[$i] = $i;
                                        }
                                   ?>                         
                                        
                   <?php echo CHTml::activeDropDownList($model_exercise, 'qty_show',$array_order,array("class"=>"form-control")); ?>
               </div>
               
               
               <div class="form-group">                
                <label> ภายในบท</label>                 
                   <?php                    
                    echo  CHtml::activeDropDownList($model_content,'parent_id',
                                        CHtml::listData(Content::model()->findAll("parent_id='-1' AND course_id='$model_course->id' ORDER BY show_order ASC"),'id', 'title'),array("class"=>"form-control"));
                                         
                                       
                     ;?>
               </div>
               
                 <div class="form-group">                
                <label> แสดงแบบสุ่ม ?</label>                 
                    <?php echo $form->checkBox($model_exercise, 'is_random'); ?>
               </div>



               <?php $back_url = Yii::app()->createUrl("/admin/default/manageContent/course/$model_course->id") ?>
               <button type="submit" class="btn btn-warning">ต่อไป</button>
                  
               <input type="button" value="ย้อนกลับ" class="btn btn-default"
                      onClick="window.location.href = '<?php echo $back_url; ?>'">

                    
               <?php $this->endWidget(); ?>
      
    
                     
               <!-----END Right content------>
            </div>
          </div>
        </div>
      </div>



    </div>
  </div>


 