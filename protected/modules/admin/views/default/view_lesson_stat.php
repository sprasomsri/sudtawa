<div class="body-admin">
    <div id="maincontent">

        <div class="container">
            <div class="row-fluid">


                <?php $this->renderPartial("_admin_menu", array("course_name" => $model_course->name, "course_id" => $model_course->id)); ?>

                <div class="span9">
                    <div class="container-fluid">
                        <div class="admin-bg-body row-course">
                            <div class="container-fluid">
                                <h2> <?php echo Yii::t("site","Lesson Stat View ")?> </h2>
                                <p> <i class="icon-film"></i> <b>Lesson name: </b> <?php echo $model_lesson->title; ?></p>
                                <p> <i class="icon-book"></i> <b>Course name: </b> <?php echo $model_lesson->course->name; ?></p>
                                <p> <i class="icon-list"></i> <b>Number of student: </b> <?php echo $model_lesson->course->getQtyStudent(); ?> person(s)</p>
                                <p> <i class="icon-facetime-video"></i > <b>Number of views: </b> <?php echo $qty; ?> views(s)</p>
                                
                                 <?php
                                    $this->widget('zii.widgets.grid.CGridView', array(
                                        'dataProvider' => $dataProvider,
                                        'columns' => array(
                                            array(
                                                'header' => Yii::t("site","No."),
                                                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)."."',
                                                'htmlOptions'=>array("style"=>"text-align:center;"),
                                            ),                              
//
                                            array(
                                                'type' => 'raw',
                                                'header'=> 'student_name',
                                                'value' => '$data->user_name', 
                                                'value' => '$data->user_name." ".$data->user_lastname',
                                                'htmlOptions'=>array("style"=>"text-align:right;"),

                                            ),
                                            
                                             array(
                                                'type' => 'raw',
                                                'header'=> 'user email',                                                  
                                                'value' => 'CHtml::link(CHtml::encode($data->user_email), "mailto:".CHtml::encode($data->user_email))',
                                                'htmlOptions'=>array("style"=>"text-align:center;"),
                                            ),
                                            
                                            
                                            array(
                                                'type' => 'raw',
                                                'header'=> 'lastVisit',
                                                'value' => 'SiteHelper::dateFormat($data->study_time)',                        
                                                'htmlOptions'=>array("style"=>"text-align:center;"),

                                            ),
                                            
                                            
                                            array(
                                                'type' => 'raw',
                                                'header'=> 'จำนวนการดู',
                                                'value' => '$data->qty_times',                        
                                                'htmlOptions'=>array("style"=>"text-align:right;"),

                                            ),
                                                                                        
                                            
                                            

                                        ),
                                    ));
                                    ?>
                            </div><!--container-fluid-->
                        </div><!--admin-bg-body row-course-->
                    </div><!--/row-fluid-->
                </div><!--/span9-->

            </div><!--row-fluid-->
        </div><!--container-->

    </div><!--/maincontent-->        
</div> <!-- body-allcourses -->