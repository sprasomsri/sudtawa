<?php 
$has_study=  SiteHelper::getStatHasStudy($data->user_id,$data->course_id);
$percent=($has_study/$qty_content)*100;
$href= Yii::app()->createUrl("admin/default/userDetail", array("user_id" => $data->user_id,"course"=>$data->course_id));

?>

<div class="w-row">
    <a href="<?php echo $href; ?>" data-fancybox-type="iframe" class="various">
    <?php if(empty($data->photo)){ 
        $data->photo="images/user_default.jpg";     
    }else{
         $data->photo="userphoto/$data->photo";
    }
    
     if (!empty($data->facebook_id)) {
         $data->photo = "https://graph.facebook.com/$data->facebook_id/picture";
     }    
    
    ?>
   
    <div class="w-col w-col-3" bgcolor="red"> <img src="<?php echo $data->photo ?>">
    <h4 > Student: <?php echo $data->name."&nbsp;&nbsp;".$data->lastname; ?></h4>
        <p class="detail-progress"> <?php echo $data->email; ?>  <span> <?php echo $has_study ?>/<b><?php echo $qty_content ?></b><span></p>
    
    </div>
    <div class="w-col w-col-9">        
            <?php $this->widget('CourseProgressBox', array("course_id"=>$data->course_id,"user_id"=>$data->user_id,"show_photo"=>"0","mode"=>"user_info")); ?> 
       
    </div>
    </a>
</div>




