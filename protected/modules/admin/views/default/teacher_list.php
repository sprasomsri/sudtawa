<div class="bg-pagestudy">
    <div class="w-container">
      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
          </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">ครู</h1>
               <!----- Right content------>
               <div class="w-row admin-row-user">
                  <?php
                                $this->widget('zii.widgets.CListView', array(
                                    'id' => 'user_list',
                                    'dataProvider' => $dataProvider,
                                    'itemView' => '_teacher_card',
                                    'template' => " {summary}\n{pager} \n {items}",
                                    'viewData' => array('model_course' => $model_course),
                                    'pager' => array(
                                        'class' => 'CLinkPager',
                                    ),
                                ));
                   ?>                  
               </div>  
               <!-----END Right content------>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript">
function setHeader(id){                                      
                                  
     $.ajax({
        url: '<?php echo Yii::app()->createUrl("admin/default/setHead"); ?>',
        type: 'GET',
        data:{                                    
              'id':id
             },
        dataType: 'html',

        success: function(result) {
          $(".head-state").addClass("admin-inactive-teacher");
          $("#head-"+id).removeClass("admin-inactive-teacher"); 


        }
      });                                                             
}
                                
                                
function setShow(id){
     $.ajax({
        url: '<?php echo Yii::app()->createUrl("admin/default/SetShow"); ?>',
        type: 'GET',
        data:{                                    
              'id':id                                            
             },
        dataType: 'html',

        success: function(result) {
                                          
         label_show = "On show";  
                                         
          if(result==0){ 
                                              
              $("#show-"+id).addClass("admin-inactive-teacher");               
                                             
          }else{
              $("#show-"+id).removeClass("admin-inactive-teacher");              
            
          }
       }
      });

}
</script>
          