 <ul class="nav nav-tabs">
<?php 
    $is_active=false;
    $order=1;
    if(!empty($model_all_question)){
        foreach ($model_all_question as $question){
            $class="";
            if($question->id==$question_id){
                $class="active";
                $is_active=true;
            }
            if($question->type==1 || $question->type=="3"){
                $link=  Yii::app()->createUrl("admin/default/operateQuestion",array("exercise"=>$exercise,"course"=>$course_id,"type"=>$question->type,"question"=>$question->id));
            }else{
                 $link=  Yii::app()->createUrl("admin/default/operateQuestionText",array("exercise"=>$exercise,"course"=>$course_id,"question"=>$question->id));
            }
            echo "<li class='$class'>";
            echo "<a href='$link' title='$question->question'>";
            echo "ข้อที่. ".$order;
            echo "</a></li>";
            $order++;
  
        }
    }
    if($is_active==false){
        $class="active"; 
        echo "<li class='$class new-ques'>";
        echo "<a href='#' title='New Question'>";
        echo  "<b>+</b>";
        echo "</a></li>";
    }
?>
     
     
</ul>