<?php if ($index % 3 == 0 || $index == 0) { ?>
    <?php $state = "new_div"; ?>
    <div class="w-row admin-row-user"> 
    <?php
    } else {
        $state = "not_new_div";
    }
    ?>
    <!-- show person card -->   
    <?php 
    $href="#";
    if($data->role==3){   
        $href= Yii::app()->createUrl("admin/default/userDetail", array("user_id" => $data->user_id,"course"=>$model_course->id));        
    }
    ?>
          <?php 
                $student_pic_path = Yii::app()->basePath . "/../userphoto/".$data->photo;

                if (!empty($data->photo) && file_exists($student_pic_path)) {
                    $student_pic = "userphoto/".$data->photo;
                } else {
                    $student_pic = "images/user_default.jpg";
                }

                if(!empty($data->facebook_id)){            
                   $student_pic= "https://graph.facebook.com/$data->facebook_id/picture";
               }
                
            ?>   
        
        
        
        <div class="w-col w-col-4">
            <div class="admin-student-box">
                <div class="amdin-profile-photo">
                    <img class="admin-img-profile" src="<?php echo $student_pic; ?>">
                </div>
                <div class="w-row admin-person-info">
                    <div class="w-col w-col-3">
                        <div class="admin-left-label"><strong>อีเมล</strong>
                        </div>
                    </div>
                    <div class="w-col w-col-9">
                        <div><?php echo $data->email; ?></div>
                    </div>
                </div>
                <div class="w-row admin-person-info">
                    <div class="w-col w-col-3">
                        <div class="admin-left-label"><strong>วันที่เข้า</strong>
                        </div>
                    </div>
                    <div class="w-col w-col-9">
                        <div><?php echo SiteHelper::dateFormat($data->create_date); ?></div>
                    </div>
                </div>
                <div class="admin-info-btn">
                    <a data-fancybox-type="iframe" class="button admin-info-btn various" href="<?php echo $href; ?>">
                        ดูความก้าวหน้า
                    </a>
                </div>
            </div>
        </div>    

    
<?php if ($state == "not_new_div" && (($index + 1) % 3 == 0)) { ?>
    </div>
<?php } ?>


