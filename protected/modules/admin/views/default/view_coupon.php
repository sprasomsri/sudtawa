<div class="body-admin">
    <div id="maincontent">

        <div class="container">
            <div class="row-fluid">


                <?php $this->renderPartial("_admin_menu", array("course_name" => $model_course->name, "course_id" => $model_course->id)); ?>

                <div class="span9">
                    <div class="container-fluid">
                        <div class="admin-bg-body row-course">
                            <div class="container-fluid">
                                <h2> <?php echo Yii::t("site","Coupon Date")?>: <?php echo SiteHelper::dateFormat($date); ?></h2>
                                
                                
                              
                                
                                <?php
                                    $this->widget('zii.widgets.grid.CGridView', array(
                                        'dataProvider' => $dataProvider,
                                        'columns' => array(
                                            array(
                                                'header' => Yii::t("site","No."),
                                                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)."."',
                                                'htmlOptions'=>array("style"=>"text-align:center;"),
                                            ),
                               

                                            array(
                                                'type' => 'raw',
                                                'header'=>Yii::t("site","code"),
                                                'value' => '$data->code',                        
                                                'htmlOptions'=>array("style"=>"text-align:center;"),

                                            ),
                                                                                        
                                            
                                            array(
                                                'header'=>Yii::t("site","Discount"),
                                                'name' => 'discount',
                                                'value' => '$data->discount."%"',
                                                'htmlOptions'=>array("style"=>"text-align:right;"),
                                            ),
                                            
//                                            array(
//                                                'header'=>Yii::t("site","Quantity Use"),
//                                                'value' => '$data->qty',
//                                                'htmlOptions'=>array("style"=>"text-align:right;"),
//                                            ),
//                                            
                                                         
                                        ),
                                    ));
                                    ?>
                            </div><!--container-fluid-->
                        </div><!--admin-bg-body row-course-->
                    </div><!--/row-fluid-->
                </div><!--/span9-->

            </div><!--row-fluid-->
        </div><!--container-->

    </div><!--/maincontent-->        
</div> <!-- body-allcourses -->