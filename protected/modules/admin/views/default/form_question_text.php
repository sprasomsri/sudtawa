<div class="bg-pagestudy">
    <div class="w-container">
      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
          </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">คำถาม - TEXT</h1>
               <!----- Right content------>               
                <a href="<?php echo LinkHelper::yiiLink("admin/default/showQuestion/exercise/$exercise/course/$model_course->id")?>" title=' <?php echo Yii::t("site","Question Page");?>' class="pull-right">
                                <img src="images/button/question.png" width='20px' /> <?php echo Yii::t("site","Question Page");?>     
               </a>               
               <?php $this->renderPartial("_question_list",array("model_all_question"=>$model_all_question,
                                                                                 "exercise"=>$exercise,"course_id"=>$model_course->id,
                                                                                 "question_id"=>$model_question->id)); ?>               
                             
               <?php
               if (!empty($message)) {                  
                       echo "<div class='alert alert-success' role='alert'> <span class='glyphicon glyphicon-floppy-saved'></span>&nbsp;&nbsp;  $message</div>";
               }
               ?>       
               <div class="form-group">                
                <label>คำถาม</label>                 
                 
                 <?php
                     $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'form-question',
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                ));
                  ?>
                  <?php echo $form->errorSummary($model_question); ?>
                
                <?php
                $this->widget('application.widgets.redactorjs.Redactor', array('model' => $model_question, 'attribute' => 'question',
                    'toolbar' => "mini",
                    "htmlOptions" => array("style" => "height:300px; width:100%;","class"=>"form-control"),
                    'editorOptions' => array(
                        //action for img upload
                        'imageUpload' => Yii::app()->createAbsoluteUrl('post/img'),
                        'imageGetJson' => Yii::app()->createAbsoluteUrl('post/listimages'),
                        'fileUpload' => Yii::app()->createAbsoluteUrl('post/uploadFile'),
                        'videoUpload' => Yii::app()->createAbsoluteUrl('post/uploadSelfVideo'),
                        'videoGetJson' => Yii::app()->createAbsoluteUrl('post/listVideo'),
                        'audioUpload' => Yii::app()->createAbsoluteUrl('post/uploadAudio'),
                        'audioGetJson' => Yii::app()->createAbsoluteUrl('post/listAudio'),
                    ),));
                ?>    
               <br/> ใช้  __ เพื่อแมปกับคำตอบ 
               </div>       
               <div class="form-group">                
                <label>ลำดับ</label> 
                
                   <?php 
                        if ($mode == "new") {
                            $show_order = $qty_question + 1;
                        } else {
                            $show_order = $model_question->show_order;
                        }                                    
                    ?>
                 <?php echo $form->textField($model_question, 'show_order',array("value"=>$show_order,"class"=>"form-control")); ?> 
                </div>
               
               <hr/>
               
               <?php
               if (!empty($list_answer)) {
                   echo $form->errorSummary($list_answer);
                   $answer_count = 1;
                   foreach ($list_answer as $key => $answer) {
                       ?>



                       <div class="form-group">

                           <label class="ans-count" data-order="<?php echo $answer_count; ?>">
                             <?php echo Yii::t("site", "Answer"); ?> : <?php echo $answer_count; ?>
                           </label>
                           
                           
                               <?php
                               if (!empty($answer->answer) && $key != 0) {
//                                           $link_del_ans=  LinkHelper::yiiLink("/admin/default/deleteAnswerChoice/id/$answer->id/course/$model_course->id/excercise/$exercise");
//                                           $img_del_link="<a href='$link_del_ans' title='delete answer $answer->answer' class='del-ans'>
//                                                            <img src='images/button/delete.png'>
//                                                         </a>";
                               }
                               echo $form->textField($answer, "[$key]answer",array("class"=>"form-control"));
                               echo $form->error($answer, "[$key]answer");
//                                        echo $img_del_link;
                               echo $form->hiddenField($answer, "[$key]sequence", array("value" => $answer_count));
                               echo "<br/>";
                               echo $form->textField($answer, "[$key]hint", array("class" => "hint form-control", "placeholder" => "Hint"));
                               ?> 
                           
                       </div>

                       <?php
                       $answer_count++;
                   }
               }
               ?>
               
               
               <div id="more-answer-show"> </div>                              
               <a href="#" id="more-answer" title="add more answer"> <img src="images/button/add_more.png"> </a>
                                
               <br/><br/>
               <button type="submit" class="btn btn-warning"><?php echo Yii::t("site", "Save"); ?></button>              
               
               <?php $this->endWidget(); ?>
               
                   
               <!-----END Right content------>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

<?php $this->renderPartial('_admin_script',array("page"=>"form_question_text")); ?> 






