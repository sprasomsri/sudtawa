<div class="bg-pagestudy">
    <div class="w-container">
      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">
              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
              
         </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">สถิติการเข้าสมัคร Course</h1>
              
               <div class="admin-top-menu">
                   <a class="admin-top-menu-link" href="<?php echo Yii::app()->createUrl("admin/default/statInLesson",array("courseId"=>$model_course->id)); ?>">สถิติการเข้าชมบทเรียน</a>
                   <a class="admin-top-menu-link" href="<?php echo Yii::app()->createUrl("admin/default/numberStudentAccess",array("courseId"=>$model_course->id)); ?>">สถิติการเข้าสมัครคอร์ส</a>
                   <a class="admin-top-menu-link" href="<?php echo Yii::app()->createUrl("admin/default/numberCoinIncourse",array("courseId"=>$model_course->id)); ?>">สถิติการใช้เงิน</a>
                   <a class="admin-top-menu-link admin-active-topmenu" href="<?php echo Yii::app()->createUrl("admin/default/statBufferRate",array("courseId"=>$model_course->id)); ?>"> สถิติการ Buffer</a>
               </div>      
               <!----- Right content------>
               <div class="text-center admin-stat-content">
                   <?php
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'form-coupon',                                  
                                    'clientOptions' => array(
                                        'validateOnSubmit' => true,
                                    ),
                                 ));
                    ?>                   
                   
                   <div class="w-row">
                       <div class="w-col w-col-1"></div>
                       <div class="w-col w-col-2">
                            <div class="form-group">
                            <label for="exampleInputEmail1">วันเริ่มต้น</label>                           
                   
                            
                            <?php
                            Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                            $this->widget('CJuiDateTimePicker', array(
                                'name' => "date_start",
                                'value' => $start,
                                'mode' => 'date', //use "time","date" or "datetime" (default)
                                'htmlOptions' => array("class" => "form-control",),
                                'options' => array(
                                //'dateFormat'=>'yy-mm-dd',
                                ), // jquery plugin options
                            ));
                            ?>
                            
                            
                             </div>
                       </div>
                        <div class="w-col w-col-1"></div>
                        <div class="w-col w-col-2">
                              <div class="form-group">
                                  <label for="exampleInputEmail1">วันสิ้นสุด</label>                            
                                  
                                  <?php
                                  Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                                  $this->widget('CJuiDateTimePicker', array(
                                      'value' => $to,
                                      'name' => "date_end",
                                      'htmlOptions' => array("class" => "form-control"),
                                      'mode' => 'date', //use "time","date" or "datetime" (default)
                                      'options' => array(
                                      ), // jquery plugin options
                                  ));
                                  ?>   
                            
                             </div>
                        </div>
                        
                        <div class="w-col w-col-1"></div>                        
                           <div class="w-col w-col-3">
                              <div class="form-group">
                                  <label for="exampleInputEmail1">บทเรียน</label>  
                                  <select name="content_id" class="form-control">
                                      <option value="" <?php if(empty($content_id)){echo "selected";} ?> disabled>  </option>
                                 <?php if(!empty($model_content)){

                                     foreach ($model_content as $content){
                                         $is_select= "";
                                         if($content->id == $content_id){
                                             $is_select= "selected='selected'";
                                         }
                                         echo "<option $is_select value='$content->id'>$content->title </option>";
                                     }


                                    } ?>

                               </select>                    
                             </div>
                        </div>                        
                        
                        
<!--                         <div class="w-col w-col-1"></div>-->
                         <div class="w-col w-col-2">
                              <input type="hidden" name="courseId" value="<?php echo $model_course->id;?>"><br/>
                              <button type="submit" name="submit" value="submit" class="button btn-admin-add" style="margin: 0 auto;">View</button>
                             
                         </div>
                         
                       
                   </div>             
                       <?php $this->endWidget(); ?>                   
                       <hr/>
                       
                      <?php if(!empty($grap)){ ?>
                                
                        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                            <script type="text/javascript">
                              google.load('visualization', '1', {packages: ['corechart']});
                            </script> 
                        <script type="text/javascript">
                              function drawVisualization() {
                                // Some raw data (not necessarily accurate)
                                var data = google.visualization.arrayToDataTable([
                                  <?php echo $grap; ?>
                                ]);

                                var options = {
                                  title : '<?php echo $head; ?>',
                                  vAxis: {title: "เวลา Buffer (วินาที)"},
                                  hAxis: {title: "ปี เดือน วัน"},
                                  seriesType: "bars",
                                  series: {2: {type: "line"}}
                                };

                                var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
                                chart.draw(data, options);
                              }
                              google.setOnLoadCallback(drawVisualization);
                            </script>


                        <div id="chart_div" style="width: 100%; height: 500px;"></div>
​                                    
                       <?php }else{ echo "<p class='text-center'>no stat</p>";}?>
                        
                       <?php if(!empty($grap_person)){ ?>
                        <script type="text/javascript">
                              google.load("visualization", "1", {packages:["corechart"]});
                              google.setOnLoadCallback(drawChart);
                              function drawChart() {
                                var data = google.visualization.arrayToDataTable([
                                  <?php echo $grap_person; ?>
                                ]);

                                var options = {
                                  title: 'สถิติการ Buffer',
                                 vAxis: {title: "เวลา Buffer (วินาที)"},
                                  hAxis: {title: "จำนวนคน"},
                                };

                                var chart = new google.visualization.LineChart(document.getElementById('chart_div_dot'));
                                chart.draw(data, options);
                              }
                        </script>

                         <div id="chart_div_dot" style="width: 100%; height: 500px;"></div>
                
                    <?php } ?>


                       <?php if(!empty($model_sumary)){ ?>

                       <h5> สรุปข้อมูลทั้งหมด</h5>

                        <table class="table table-bordered table-hover admin-stat-tbl">
                            
                          <tr>
                              <td> เวลา Buffer ทั้งหมด </td>
                              <td> <?php echo $model_sumary['sum_time']*3; ?> </td>
                             
                          </tr>  
                          
                          <tr>
                              <td> เวลา Buffer เฉลี่ย </td>
                              <td> <?php echo $model_sumary['avg_time']*3; ?></td>
                             
                          </tr>
                            
                          <tr>
                              <td> เวลา Buffer มากสุด </td>
                              <td> <?php echo $model_sumary['max_time']*3; ?></td>
                             
                          </tr>
                          
                           <tr>
                              <td> เวลา Buffer น้อยสุด </td>
                              <td> <?php echo $model_sumary['min_time']*3; ?></td>
                             
                          </tr>
                          
                        </table>
                       
                       <?php } ?>
                 </div>                                      
               </div>               
               <!-----END Right content------>               
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>


  


  
  

