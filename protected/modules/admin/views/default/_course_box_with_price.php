
<div class="w-row admin-dashboard-list">
    <div class="w-col w-col-3 admin-dashboard-photo">
        
        <?php
                $course_img_path = Yii::app()->basePath . "/../webroot/course/" . $data->course_img;

                if ($data->course_img != null && file_exists($course_img_path)) {
                    $src = "course/" . $data->course_img;
                } else {
                    $src = "images/default_course.png";
                }
              ?>
        
        <img src="<?php echo $src; ?>" alt="">
    </div>
    <div class="w-col w-col-3">
        <h1 class="header-course"><?php echo $data->name; ?></h1> 
<!--        <p class="text-course-pay">
            <?php //echo strip_tags($data->description); ?>
            <br/><br/>
      
       </p>-->
        <br/> <br/>
        <a href='<?php echo Yii::app()->createUrl("/admin/default/operateCourse/id/$data->id"); ?>'class="btn btn-primary"><span class='glyphicon glyphicon-wrench'></span> Manage </a>
       
      <?php             
            $is_have_approve = $data->isModifyContent(); 
            if($is_have_approve){
                echo "<p class='has-approve'><span class='glyphicon glyphicon-warning-sign'></span>   มีรายการรอ Approve </p> ";
            }           
       ?>       
    </div>
    
    
    <div class="w-col w-col-3 student-stat-access">
        <h4 class="stat-std-access star-with-price"> สถิติการเข้าคอร์สของนักเรียน </h4>
        
   
        <a title="View stat student" href="<?php echo Yii::app()->createUrl("admin/default/numberStudentAccess",array("courseId"=>$data->id))?>">
        <table class="stat-dashboard student-stat-dashboard">
            
            <tr>
                <td class="col-dashboard"> <h1 class="day">Day</h1> </td>
                <td class="col-dashboard-value">  
                    <h1 class="day-money"><?php echo number_format($data->getStudentAccessStat("day"));?></h1>
                </td>
            </tr>
            
            <tr>
                <td class="col-dashboard">  <h1 class="week">Week</h1> </td>
                <td class="col-dashboard-value">  
                    <h1 class="day-money"><?php echo number_format($data->getStudentAccessStat("week"));?></h1></td>
            </tr>
            
             <tr class="col-dashboard">
                <td class="col-dashboard">  <h1 class="month">Month</h1> </td>
                <td class="col-dashboard-value">  
                    <h1 class="day-money"><?php echo number_format($data->getStudentAccessStat("month"));?></h1> 
                </td>
            </tr>      
            
             <tr class="col-dashboard">
                <td class="col-dashboard">  <h1 class="month blue-text"><u>All</u></h1> </td>
                <td class="col-dashboard-value">  
                    <h1 class="day-money blue-text"><?php echo number_format($data->getQtyStudent());?></h1> 
                </td>
            </tr>      
            
        </table>
        </a>
        
       
    </div>
    <div class="w-col w-col-3">
        <h4 class="stat-std-access star-with-price"> สถิติการเงินเข้าคอร์ส </h4>
          <a title="View stat money" href="<?php echo Yii::app()->createUrl("admin/default/numberCoinIncourse",array("courseId"=>$data->id))?>">
                <table class="stat-dashboard student-stat-dashboard">
            
            <tr>
                <td class="col-dashboard"> <h1 class="day">Day</h1> </td>
                <td class="col-dashboard-value">  
                    <h1 class="day-money"><?php echo number_format($data->getMoneyStat("day"));?></h1>
                </td>
            </tr>
            
            <tr>
                <td class="col-dashboard">  <h1 class="week">Week</h1> </td>
                <td class="col-dashboard-value">  
                    <h1 class="day-money"><?php echo number_format($data->getMoneyStat("week"));?></h1></td>
            </tr>
            
             <tr class="col-dashboard">
                <td class="col-dashboard">  <h1 class="month">Month</h1> </td>
                <td class="col-dashboard-value">  
                    <h1 class="day-money"><?php echo number_format($data->getMoneyStat("month"));?></h1> 
                </td>
            </tr>      
            
             <tr class="col-dashboard">
                <td class="col-dashboard">  <h1 class="month blue-text"><u>All</u></h1> </td>
                <td class="col-dashboard-value">  
                    <h1 class="day-money blue-text"><?php echo number_format($data->getTotalMoney());?></h1> 
                </td>
            </tr>      
            
        </table>
        </a>
    </div>
    
    
<!--    <div class="w-col w-col-2">
        <h1 class="day-money">500</h1>
        <h1 class="week-money">1,700</h1>
        <h1 class="month-money">4,900</h1>
    </div>-->
   
</div>

