
  
<div class="bg-pagestudy">
    <div class="w-container">


      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">
              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
              
           
              
              
          </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">ข้อมูลคอร์ส</h1>
               <!----- Right content------>
               <?php
               $form = $this->beginWidget('CActiveForm', array(
                   'id' => 'add-course',
                   'htmlOptions' => array('enctype' => 'multipart/form-data',"role"=>"form"),
                   'clientOptions' => array(
                       'validateOnSubmit' => true,
                   ),
               ));
               ?>
               
               
              <?php echo $form->errorSummary($model_course); ?>
                              
               
               <div class="form-group">
                
              <label ><?php echo Yii::t("site","Course Name");?></label>
               <?php echo $form->textField($model_course, 'name',array("class"=>"form-control")); ?>             
              </div>
               
               <div class="form-group">
                <label><?php echo Yii::t("site","Course Name");?> TH</label>
               <?php echo $form->textField($model_course, 'name_th',array("class"=>"form-control")); ?>             
              </div>
               
               <div class="form-group">
                <label><?php echo Yii::t("site","Description");?></label>
              <?php echo $form->textArea($model_course, 'description',array("style"=>"width:100%","rows"=>10,"class"=>"form-control")); ?>        
              </div>
               
              <?php //echo $form->dropDownlist($model_course, 'course_status',array("0"=>"---","1"=>"feature","2"=>"New")); ?>
               
               
              <div class="form-group">
                <label><?php echo Yii::t("site","tag");?></label>             
              <?php echo $form->textField($model_course, 'tag',array("class"=>"form-control")); ?>
              </div>
               
               <div class="form-group">
                <label><?php echo Yii::t("site","about");?></label>             
                <?php echo $form->textField($model_course, 'about',array("class"=>"form-control")); ?>
              </div>
               
               
               <div class="form-group">
                <label><?php echo Yii::t("site","about");?></label>             
                <?php echo $form->textField($model_course, 'about',array("class"=>"form-control")); ?>
              </div>
               
               <div class="form-group">
                <label><?php echo Yii::t("site","Course Price");?></label>             
                <?php echo $form->textField($model_course, 'price',array("class"=>"form-control")); ?>
              </div>
               
               
              <div class="form-group">
                <label><?php echo Yii::t("site", "Teacher Name"); ?></label>   
                                
                 <?php echo Chosen::multiSelect("teacherList",$selected_teacher,$list_all_teacher,array("class"=>"form-control","id"=>"teacherList")); ?>
              </div>
               
               
               
               
                <div class="w-row">
                    <div class="w-col w-col-4">
                          <div class="form-group">
                            <label><?php echo Yii::t("site","Public");?></label>             
                            <?php echo $form->checkBox($model_course, 'is_public'); ?>
                          </div>              
                    </div>
                    <div class="w-col w-col-4">
                         <div class="form-group">
                            <label><?php echo Yii::t("site","affiliate ?");?></label>          
                            <?php echo $form->checkBox($model_course, 'is_affiliate'); ?>
                          </div>                 
                    </div>
                    <div class="w-col w-col-4"></div>
                     
                 </div>      
               
             
               
               
              
               
               
               
               <div class="form-group">
                <label><?php echo Yii::t("site", "Demo Video"); ?></label>   
                <a class="demo-select fancybox.ajax" id="select-demo-vdo" href="<?php echo Yii::app()->createUrl("admin/default/listVideo", array("user_id" => $user_id)); ?>">select demo video</a>   
               
                <?php echo $form->hiddenField($model_course, "demo_video", array("id" => "demo-video-id")) ?>
                <?php
                           if (!empty($demoVideo)) {
                               $video_url = Yii::app()->baseUrl . "/files/$demoVideo->user_id/video/$demoVideo->name";
                               $video_path = Yii::app()->basePath . "/../files/$demoVideo->user_id/video/$demoVideo->name";
                               if (file_exists($video_path)) {
                                   $play = VdoHelper::jwplayer($video_url, '517px', '233px');
                                   echo $play;
                                   echo "<a id='delvdo' href='#'><i class='icon-delete-course'></i> Delete demo video</a>";
                               }
                           }
                 ?>              
              </div>
               
              <div class="form-group">
                <label><?php echo Yii::t("site", "Youtube Demo"); ?></label>   
                <a class="demo-select fancybox.ajax" id="select-demo-vdo" href="<?php echo Yii::app()->createUrl("admin/default/listVideo", array("user_id" => $user_id)); ?>">select demo video</a>   
                 <br/>
                 <?php echo $form->textField($model_course, "youtube_demo",array("class"=>"form-control")) ?>  
                 <?php
                 if (!empty($model_course->youtube_demo)) {
                     echo "<br/>" . $demon_youtube = VdoHelper::jwyoutube($model_course->youtube_demo,"50%");
                 }
                 ?>             
              </div>
               
   
              <div class="w-row">
                    <div class="w-col w-col-5">
                          <div class="form-group">
                            <label><?php echo Yii::t("site","Course Photo");?></label>   
                            <?php echo CHtml::activeFileField($model_course, 'course_img',array("class"=>"input-block-level")) ?>
                            <?php
                            if (!empty($model_course->course_img)) {
                                echo "<a class='tumb' href='course/original/$model_course->course_img'>";
                                echo "<img src='course/$model_course->course_img' height='150px' title='Course cover'>";
                                echo "</a>";
                            }
                            ?>             
                          </div>              
                    </div>
                    <div class="w-col w-col-5">
                          <div class="form-group">
                            <label><?php echo Yii::t("site","ThemePhoto");?></label> 
                              <a class="theme-select fancybox.ajax" id="select-theme-photo" href="<?php echo Yii::app()->createUrl("/admin/default/listPhoto");?>">select Theme Photo</a>
                              <br/>
                             <?php echo $form->hiddenField($model_course,"theme_photo",array("id"=>"theme-phto-id")) ?>
                             <?php
                             if (!empty($model_course->theme_photo)) {
                                 echo "<a class='tumb' href='images/courseBg/$model_course->theme_photo'>";
                                 echo "<img src='images/courseBg/$model_course->theme_photo' height='150px'>";
                                 echo "</a>";
                                 echo "<br/><a id='delphoto' href='#'><i class='icon-delete-course' id='delphoto'></i> Delete Theme's photo</a>";
                             }
                             ?>             
                          </div>               
                    </div>
                    <div class="w-col w-col-2"></div>
                     
                 </div> 
               
           
               
               
             


               <button type="submit" class="btn btn-warning"><?php echo Yii::t("site", "Next"); ?></button>
               <?php $back_url = Yii::app()->createUrl("admin/default/ownCourse") ?>
               
               <input type="button" value="<?php echo Yii::t("site", "Back"); ?>" class="span3 btn btn-gray-submit"
                      onClick="window.location.href='<?php echo $back_url; ?>'">


                     
              <?php $this->endWidget(); ?>
                     
               <!-----END Right content------>
            </div>
          </div>
        </div>
      </div>



    </div>
  </div>




    
   <script type="text/javascript">
    $(document).ready(function() {
        $(".tumb").fancybox({
            helpers: {
            title : {
                type : 'float'
               }
             }
        });    
        
       $("#delphoto").click(function(){
           $("#theme-phto-id").val("");
       });
       
       $("#delvdo").click(function(){
           $("#demo-video-id").val("");
       });        
       $("#select-theme-photo").fancybox({
		maxWidth	: 900,
		maxHeight	: 900,
		fitToView	: false,
		width		: '80%',
		height		: '70%',
		autoSize	: false,
		closeClick	: true,
		openEffect	: 'none',
		closeEffect	: 'none',
                
	});
        
        $(".demo-select").fancybox({
		maxWidth	: 900,
		maxHeight	: 900,
		fitToView	: false,
		width		: '80%',
		height		: '70%',
		autoSize	: false,
		closeClick	: true,
		openEffect	: 'none',
		closeEffect	: 'none',
                
	});
      });
           
    </script>
   

