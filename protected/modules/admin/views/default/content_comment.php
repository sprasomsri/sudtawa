<div class="bg-pagestudy">
    <div class="w-container">

      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">
              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
           
          </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head"> ความคิดเห็น </h1>              
               
               <!----- Right content------>
               <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'form-search',
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                        ),
                    ));
                     
                    ?>    
          
                <div class="w-row">
                       <div class="w-col w-col-2"></div>
                        
                       <div class="w-col w-col-2">
                           <div class="form-group">
                                <label>บทเรียน</label>
                           </div>
                             
                        </div>
                       
                       <div class="w-col w-col-3">
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputEmail2">บทเรียน</label>
                                <select name="content_id" class="form-control">
                                 <option value="" selected disabled>  </option>
                                 <?php if(!empty($model_content)){

                                     foreach ($model_content as $content){
                                         echo "<option value='$content->id'>$content->title </option>";
                                     }


                                 } ?>

                               </select>   
                              </div>
                    </div>
                    <div class="w-col w-col-1"></div>
                    <div class="w-col w-col-2">
                        
                          <button type="submit" name="btn-submit" value="submit" class="button btn-admin-add"> View</button>  
                        
                    </div>
                    <div class="w-col w-col-2"></div>
                       
                </div>
               
                <?php $this->endWidget(); ?>
                   
               <hr/>
               
               <?php
               $this->widget('zii.widgets.CListView', array(
                   'id' => 'user_list',
                   'dataProvider' => $model_suggestions,
                   'itemView' => '_comment_box',
                   'template' => " {summary}\n{pager} \n {items}",
                   'viewData' => array('model_course' => $model_course),
                   'pager' => array(
                       'class' => 'CLinkPager',
                   ),
               ));
               ?>
                       
               <!-----END Right content------>
               
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

