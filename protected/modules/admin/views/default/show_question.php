<script type="text/javascript" src="js/jquery.js"></script>
<!--<script type="text/javascript" src="js/jquery.multifile.js"></script>-->
<script type="text/javascript" src="js/jui/js/jquery-ui.min.js"></script>
<!--<script type="text/javascript" src="js/coursethumbnailuploader.js"></script>-->
<!--<script type="text/javascript" src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>-->

<div class="bg-pagestudy">
    <div class="w-container">


      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">
              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
              
           
          </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">แบบฝึกหัด - <?php echo $model_content->title; ?></h1>
               <!----- Right content------>
               
               
               <ul class='content-list'>


                   <!-- Parent add class ='editable-chapter' for <li> 
                        Children add class = 'editable-lecture' for <li> -->
                   <?php
                   $ques_count = 1;
                   if (!empty($model_all_question)) {

                       #unit ---> loop
                       foreach ($model_all_question as $question) {
                           $question_name = strip_tags($question->question);
                           ?>

                           <li id="content_<?php echo $question->id ?>" class="editable-lecture content">
                               <div class="w-row">
                                   <div class="w-col w-col-10"> 
                                       <span class="content-prefix question">  </span>
                                       <span id="content-name-1" class="content-name"><?php echo $question_name ?></span>
                                   </div>
                                   <div class="w-col w-col-2" align="right">
                                       <?php
                                       if ($question->type == 1 || $question->type == 3) { //1 multichoice ,3 true false
                                           $link_stat = Yii::app()->createUrl("admin/default/showStatInQuestion", array("exercise" => $question->exercise_id, "course" => $model_course->id, "question" => $question->id));


                                           $link_edit = Yii::app()->createUrl("admin/default/OperateQuestion", array("exercise" => $question->exercise_id, "course" => $model_course->id, "type" => $question->type, "question" => $question->id));
                                       }

                                       if ($question->type == 2) {
                                           $link_stat = Yii::app()->createUrl("admin/default/showStatInQuestionText", array("exercise" => $question->exercise_id, "course" => $model_course->id, "question" => $question->id));
                                           $link_edit = Yii::app()->createUrl("admin/default/OperateQuestionText", array("exercise" => $question->exercise_id, "course" => $model_course->id, "question" => $question->id));
                                       }


                                       if ($question->type == 4) {
                                           
                                          
                                           
                                           $link_stat = Yii::app()->createUrl("admin/default/showStatInQuestionText", array("exercise" => $question->exercise_id, "course" => $model_course->id, "question" => $question->id));
                                           
                                            if(empty($question->code_type)){
                                               $question->code_type = "java";
                                           }
                                           
                                           $link_edit = Yii::app()->createUrl("admin/default/operateQuestionCode", array("exercise" => $question->exercise_id, "course" => $model_course->id,"type_code"=>$question->code_type, "question" => $question->id));
                                           
                                           
                                           
                                           
                                       }
                                       ?>


                                       <?php
                                       $img_public = "images/button/on.png";
                                       if ($question->is_public == 0) {
                                           $img_public = "images/button/off.png";
                                       }
                                       ?>

                                       <a href="#" title="public / not public">
                                           <img id="question-<?php echo $question->id; ?>" src="<?php echo $img_public; ?>" onclick="changPublic(<?php echo $question->id ?>, <?php echo $question->is_public; ?>)">
                                       </a>




                                       <a title="Sata-Question:: <?php echo $question_name; ?>" 
                                          href="<?php echo $link_stat; ?>">
                                           <span class="glyphicon glyphicon-stats"></span>
                                       </a> 


                                       <a title="Edit-Question::<?php echo $question_name; ?>" 
                                          href="<?php echo $link_edit; ?>">
                                           <span class="glyphicon glyphicon-pencil"></span>
                                       </a>     

                                       <a id="delete-link" title="Delete-Question::<?php echo $question_name; ?>" 
                                          class ="delete_content_sub"
                                          onclick="return delete_confirm();"
                                          href="<?php echo Yii::app()->createUrl("admin/default/deleteQuestion/id/$question->id/course/$model_course->id") ?>">
                                         <span class="glyphicon glyphicon-remove"></span>
                                       </a>



                                   </div>
                               </div>
                           </li> 

                            <?php
                            $ques_count++;
                        }
                    }
                    ?>

               </ul>
               
                   <div>
                                            <?php $new_question_choice=Yii::app()->createUrl("admin/default/operateQuestion/exercise/$model_exercise->id/course/$model_course->id/type/1")?>
                                            <input type="button" value="<?php echo Yii::t("site","Add Question MulitChoice")?>" 
                                            class="btn btn-warning btn-padding"
                                            onClick="window.location.href='<?php echo $new_question_choice; ?>'"/>
                                            
                                                <?php $new_question_choice=Yii::app()->createUrl("admin/default/operateQuestion/exercise/$model_exercise->id/course/$model_course->id/type/3")?>
                                            <input type="button" value="<?php echo Yii::t("site","Add Question 2 Choice")?>" 
                                            class="btn btn-warning btn-padding"
                                            onClick="window.location.href='<?php echo $new_question_choice; ?>'"/>
                                            
                                            
                                            
                                            
                                            <?php $new_question_text=Yii::app()->createUrl("admin/default/operateQuestionText/exercise/$model_exercise->id/course/$model_course->id")?>
                                            <input type="button" value="<?php echo Yii::t("site","Add Question Text")?>" 
                                            class="btn btn-warning btn-padding"
                                            onClick="window.location.href='<?php echo $new_question_text; ?>'"/>
                                            
                                              <?php 
                                              
                                              //$new_question_text=Yii::app()->createUrl("admin/default/operateQuestionCode/exercise/$model_exercise->id/course/$model_course->id/type_code/java"); 
                                              $new_question_text=Yii::app()->createUrl("admin/default/operateQuestionCode",array("exercise"=>$model_exercise->id,"course"=>$model_course->id,"type_code"=>"java")); 
                                              
                                              ?>
                                            <input type="button" value="<?php echo Yii::t("site","Coding Java")?>" 
                                            class="btn btn-warning btn-padding"
                                            onClick="window.location.href='<?php echo $new_question_text; ?>'"/>
                                            
                                            
                                            <?php 
                                            
                                           // $new_question_text=Yii::app()->createUrl("admin/default/operateQuestionCode/exercise/$model_exercise->id/course/$model_course->id/type_code/python");
                                            
                                            $new_question_text=Yii::app()->createUrl("admin/default/operateQuestionCode",array("exercise"=>$model_exercise->id,"course"=>$model_course->id,"type_code"=>"python")); 
                                            
                                            ?>
                                            
                                            
                                            <input type="button" value="<?php echo Yii::t("site","Coding Python")?>" 
                                            class="btn btn-warning btn-padding"
                                            onClick="window.location.href='<?php echo $new_question_text; ?>'"/>
                                            
                                            
                                            
                                            
                                            <?php $stat_exercise=Yii::app()->createUrl("admin/default/showStatScorePhase/content/$model_content->id/exercise/$model_exercise->id/course/$model_course->id")?>
                                            <input type="button" value="<?php echo Yii::t("site","Stat Exercise")?>" 
                                            class="btn  btn-padding btn-danger"
                                            onClick="window.location.href='<?php echo $stat_exercise; ?>'"/>                                            


                                    <div class="row-fluid row-course">&nbsp;</div>
                             

                            </div><!--container-fluid-->
               
  
              
    
                     
               <!-----END Right content------>
            </div>
          </div>
        </div>
      </div>



    </div>
  </div>

<?php $this->renderPartial('_admin_script',array("page"=>"show_question")); ?> 
 
