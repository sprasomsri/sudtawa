<div class="bg-pagestudy">
    <div class="w-container">
      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
          </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">คำถาม</h1>
               <!----- Right content------>               
                <a href="<?php echo LinkHelper::yiiLink("admin/default/showQuestion/exercise/$exercise/course/$model_course->id")?>" title=' <?php echo Yii::t("site","Question Page");?>' class="pull-right">
                                <img src="images/button/question.png" width='20px' /> <?php echo Yii::t("site","Question Page");?>     
               </a>               
               <?php $this->renderPartial("_question_list",array("model_all_question"=>$model_all_question,
                                                                                 "exercise"=>$exercise,"course_id"=>$model_course->id,
                                                                                 "question_id"=>$model_question->id)); ?>
               <?php
               $form = $this->beginWidget('CActiveForm', array(
                   'id' => 'form-question',
                   'htmlOptions' => array('enctype' => 'multipart/form-data'),
                   'clientOptions' => array(
                       'validateOnSubmit' => true,
                   ),
               ));
               ?>               
               <?php
               if (!empty($message)) {                  
                       echo "<div class='alert alert-success' role='alert'> <span class='glyphicon glyphicon-floppy-saved'></span>&nbsp;&nbsp;  $message</div>";
               }
               ?>         
              <?php echo $form->errorSummary($model_question); ?>
               
                <div class="form-group">                
                <label>คำถาม</label>        
                <?php
                $this->widget('application.widgets.redactorjs.Redactor', array('model' => $model_question, 'attribute' => 'question',
                    'toolbar' => "mini",
                    "htmlOptions" => array("style" => "height:300px; width:100%;","class"=>"form-control"),
                    'editorOptions' => array(
                        //action for img upload
                        'imageUpload' => Yii::app()->createAbsoluteUrl('post/img'),
                        'imageGetJson' => Yii::app()->createAbsoluteUrl('post/listimages'),
                        'fileUpload' => Yii::app()->createAbsoluteUrl('post/uploadFile'),
                        'videoUpload' => Yii::app()->createAbsoluteUrl('post/uploadSelfVideo'),
                        'videoGetJson' => Yii::app()->createAbsoluteUrl('post/listVideo'),
                        'audioUpload' => Yii::app()->createAbsoluteUrl('post/uploadAudio'),
                        'audioGetJson' => Yii::app()->createAbsoluteUrl('post/listAudio'),
                    ),));
                ?>                
               </div>
               
               
                <div class="form-group">                
                <label>ลำดับ</label>               
                   <?php 
                        if ($mode == "new") {
                            $show_order = $qty_question + 1;
                        } else {
                            $show_order = $model_question->show_order;
                        }                                    
                    ?>
                 <?php echo $form->textField($model_question, 'show_order',array("value"=>$show_order,"class"=>"form-control")); ?> 
                </div>
               
                <?php                               
                  if (!empty($list_answer)) {
                   $answer_count = 1;
                   echo "<hr/> <h4> Answer </h4>";
                   echo $form->errorSummary($list_answer);
                   foreach ($list_answer as $key => $answer) {
                 ?>      
                    <div class="form-group ans-count exercise-list" data-order="<?php echo $answer_count;?>">                
                        <label>
                            <?php echo Yii::t("site", "Answer"); ?> : <?php echo $answer_count; ?>                    
                        </label> 
                        
                        <?php
                                $class = "form-control";
                                $is_true = "0";
                                $text_true = "";
                                $img_del_link = "";
                                if ($key == 0) {
                                    $class = "correct-answer form-control";
                                    $is_true = "1";
                                    $text_true = "<label class='correct-des'> &RightArrowLeftArrow; correct answer </label>";
                                }


                                if ($type == 1) { //multichoice
                                    if (!empty($answer->answer) && $key != 0) {
                                        $link_del_ans = LinkHelper::yiiLink("/admin/default/deleteAnswerChoice/id/$answer->id/course/$model_course->id/excercise/$exercise");
                                        $img_del_link = "<a href='$link_del_ans' title='delete answer $answer->answer' class='del-ans'>
                                                                <img src='images/button/delete.png'> Delete Answer 
                                                             </a>";
                                    }
                                    echo $form->textField($answer, "[$key]answer", array("class" => $class));
                                    echo $form->error($answer, "[$key]answer");
                                   
                                    
                                    echo "<br/>";
                                    echo $form->textField($answer, "[$key]hint", array("class" => "hint form-control", "placeholder" => "Hint"));
                                    echo $img_del_link;
                                    echo $text_true;
                                    
                                    
                                    echo $form->hiddenField($answer, "[$key]is_true", array("value" => "$is_true"));
                                } else { //true false choice type=3 
                                    if ($key == 0) {
                                        $class_div = "success";
                                        $input_id = "inputSuccess";
                                        $input_state = " &RightArrowLeftArrow; correct answer";
                                        $is_true = "1";
                                    } else {
                                        $class_div = "error";
                                        $input_id = "inputError";
                                        $input_state = " &RightArrowLeftArrow; uncorrect answer";
                                    }                                  
                                   
                                    echo $form->textField($answer, "[$key]answer", array("id" => $input_id, "class" => "form-control"));
                                    echo $form->error($answer, "[$key]answer");
                                    echo $form->hiddenField($answer, "[$key]is_true", array("value" => "$is_true"));
                                    echo "<br/>";
                                    echo $form->textField($answer, "[$key]hint", array("class" => "hint form-control", "placeholder" => "Hint"));
                                    echo "<span class='help-inline $class_div'>$input_state</span>";
                                    
                                    
                                    }                                
                                ?>           
                    </div>
               
                <?php 
                
                     $answer_count++;
                    }
                  }
               ?>    
              <?php if($type!=3) { //true false Question ?>
              <div id="more-answer-show"> </div>
              <a href="#" id="more-answer" title="add more answer"> <img src="images/button/add_more.png"> </a>
              <?php } ?>            
               <br/><br/>      
               <button type="submit" class="btn btn-warning"><?php echo Yii::t("site", "Save"); ?></button>
    
               <?php $this->endWidget(); ?>     
               <!-----END Right content------>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php $this->renderPartial('_admin_script',array("page"=>"form_question")); ?> 

