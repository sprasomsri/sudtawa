<?php
     #current action Name
    $actionId = $this->getAction()->getId(); 
 ?>

<div class="menu-admin">
    <?php if(!empty($course_name)){ ?>
    <h4 class="course-name"><?php echo $course_name; ?></h4>
    <?php } ?>    
    
    <div class="text-center">
        
        <?php if(!empty($img)){ ?>
        <img class="img-course" src="course/<?php echo $img; ?>">
        <?php } ?>
        
        <div class="list-menu">  
            
            <div class="admin-submenu  <?php if($actionId=="operateCourse") {echo "admin-active-menu";}?>">
                <a type="submit" href="<?php echo Yii::app()->createUrl("admin/default/operateCourse/id/$course_id"); ?>">
                    <div>ข้อมูลคอร์ส</div>
                </a>
            </div>
            
            <?php if(!empty($course_id)){  ?>            
            <div class="admin-submenu <?php if($actionId=="manageContent") {echo "admin-active-menu";}?>">
                <a href="<?php echo Yii::app()->createUrl("admin/default/manageContent/course/$course_id"); ?>">
                    <div>จัดการบทเรียน</div>
                </a>
            </div>            
            
            <div class="admin-submenu">
               <a type="submit" href="<?php echo Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$course_id,"chapterId"=>"1","type"=>"teacher")); ?>" target="_blank">
                    <div>ดูตัวอย่างคอร์สเรียน</div>
                </a>
            </div>
            
            <div class="admin-submenu <?php if($actionId=="statInLesson" || $actionId=="numberStudentAccess" || $actionId=="numberCoinIncourse"|| $actionId=="statBufferRate") {echo "admin-active-menu";}?>">
                <a type="submit" href="<?php echo Yii::app()->createUrl("admin/default/statInLesson",array("courseId"=>$course_id)); ?>">
                <div>สถิติ</div>
                </a>
            </div>
            
            <div class="admin-submenu <?php if($actionId=="contentComment") {echo "admin-active-menu";}?>">
                <a type="submit" href="<?php echo Yii::app()->createUrl("admin/default/contentComment",array("courseId"=>$course_id)); ?>">
                <div>ความคิดเห็น</div>
                </a>
            </div>
            
            <div class="admin-submenu <?php if($actionId=="studentList") {echo "admin-active-menu";}?>">
                <a type="submit" href="<?php echo Yii::app()->createUrl("admin/default/studentList/course/$course_id"); ?>">
                    <div>นักเรียน</div>
                </a>
            </div>
            
            <div class="admin-submenu <?php if($actionId=="teacherList") {echo "admin-active-menu";}?>">
                  <a type="submit" href="<?php echo Yii::app()->createUrl("admin/default/teacherList/course/$course_id"); ?>">
                    <div>ครู</div>
                  </a>
            </div>
                       
            <div class="admin-submenu <?php if($actionId=="relateCourse") {echo "admin-active-menu";}?>">
                <a type="submit" href="<?php echo Yii::app()->createUrl("admin/default/relateCourse/course/$course_id"); ?>">
                <div>คอร์สที่เกี่ยวข้อง</div>
                </a>
            </div>
            
            
            <div class="admin-submenu <?php if($actionId=="courseDivision") {echo "admin-active-menu";}?>">
                <a type="submit" href="<?php echo Yii::app()->createUrl("admin/default/courseDivision",array("courseId"=>$course_id)); ?>">
                    <div>ส่วนแบ่ง</div>
                </a>
            </div>
            
<!--            <div class="admin-submenu  <?php if($actionId=="coupon") {echo "admin-active-menu";}?>">
                <a type="submit" href="<?php echo Yii::app()->createUrl("admin/default/coupon/course/$course_id"); ?>">
                    <div>คูปอง</div>
                </a>
            </div>-->
            
            <div class="admin-submenu  <?php if($actionId=="ownCourse") {echo "admin-active-menu";}?>">
                <a type="submit" href="<?php echo Yii::app()->createUrl("admin/default/ownCourse"); ?>">
                    <div> คอร์สของฉัน </div>
                </a>
            </div>
            <?php } ?>          
            
            <div class="admin-submenu">
                <a href="<?php echo Yii::app()->createUrl("user/logout");?>">
                    <div>ออกจากระบบ</div>
                </a>
            </div>
        </div>
    </div>
</div>
