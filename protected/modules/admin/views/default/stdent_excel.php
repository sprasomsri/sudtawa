<?php
ob_start();
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment;filename=studentlist.xls"); //
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">

<html>

<head>

<meta http-equiv="Content-type" content="text/html;charset=utf-8" />

</head>
<body>


<table x:str border="1" width="100%">

<tr>
 <td colspan="6"><b>รายชื่อนักเรียน คอร์ส <?php echo $model_course->name; ?></b></td>
</tr>

<tr>
<td width="2%" bgcolor="#00BFFF"><b>ลำดับ</b></td>
<td width="28%" bgcolor="#00BFFF"><b>ชื่อ - สกุล</b></td>
<td width="20%" bgcolor="#00BFFF"><b>เลขที่สมาชิกสัตวสภา</b></td>
<td width="30%" bgcolor="#00BFFF"><b>ชื่อหน่วยงาน/โรงพยาบาล/คลินิค</b></td>
<td width="10%" bgcolor="#00BFFF"><b>อีเมล</b></td>
<td width="5%" bgcolor="#00BFFF"><b>โทรศัพท์</b></td>
</tr>

<?php
if(!empty($model_users)){
   $i=1;
    $html = "";
    foreach($model_users as $user){
      
        $html .= "<tr>";
        $html .= "<td>".$i."</td>";
        $html .= "<td>".$user->profile->firstname.' '.$user->profile->lastname."</td>";
        $html .= "<td>".$user->username."</td>";
        $html .= "<td>".$user->profile->company."</td>";
        $html .= "<td>".$user->email."</td>";
         $html .= "<td>".$user->profile->tel."</td>";
        $html .= "</tr>";
        $i++;
    }
echo $html;
}
?>



</table>

</body>

</html>
<?php
exit();
?>