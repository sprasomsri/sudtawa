<div class="body-admin">
    <div id="maincontent">

        <div class="container">
            <div class="row-fluid">


                <?php $this->renderPartial("_admin_menu", array("course_name" => $model_course->name, "course_id" => $model_course->id)); ?>

                <div class="span9">
                    <div class="container-fluid">
                        <div class="admin-bg-body row-course">
                            <div class="container-fluid">

                                         
                           <?php
                                //use Ajax for submit and save data

                                $form = $this->beginWidget('CActiveForm', array(
                                        'id' => 'course-content',
                                         'htmlOptions'=>array(
                                         'enctype'=>'multipart/form-data',
                                         ),
                                        'clientOptions' => array(
                                        'validateOnSubmit' => false,
                                    ),
                                ));
                            ?>
                                

                                <!--Course Detail-->
                                <?php 
                                if($type=="unit"){
                                    $form_name= "หน่วยเรียน [Unit]";
                                    $title="ชื่อบท";
                                }else{
                                     $form_name= "บทเรียน [Lesson]";
                                    $title="ชื่อบทเรียน";
                                }
                                
                                           
                                ?>
                                
                                                               
                                
                                <div class="row-fluid row-course">
                                    <h2><img src="images/course-detail-msg.png" class="h2-img" /> <?php echo $form_name; ?></h2>
                                    <div class="row-fluid line-lightgrey-wrapper">&nbsp;</div> 
                                </div>
                                
                                 <p class="text-right"><a href="<?php echo Yii::app()->createUrl("admin/suggesstion/lesson");?>" class="various fancybox.ajax"> <i class="icon-comment"></i> Suggestion </a></p>
                          
                                <?php echo $form->errorSummary($model_content); ?>
                                <?php //echo $form->errorSummary($model_vdo); ?>
                                
                                <div id="form-content" width="100%">


                                    <div id="error-content" style="display: none;" class="alert error-alert-info"> </div>
                                    <div id="success" style="display: none;" class="alert alert-info"> </div>


                                    <div class="row-fluid content">
                                        <div class="span1"></div>
                                        <div class="span2" align="left"><?php echo $title; ?></div>
                                        <div class="span8" align="left">
                                            <?php echo $form->textField($model_content, 'title'); ?>
                                        </div>
                                        <div class="span1"></div>
                                    </div>                            

                                    <div class="row-fluid content">
                                        <div class="span1"></div>
                                        <div class="span2" align="left">คำอธิบาย</div>
                                        <div class="span8" align="left"> </div>

                                        <div class="span1"></div>
                                    </div> 
                                    <div class="row-fluid content">
                                        <?php
                                        
                                        $this->widget('application.widgets.redactorjs.Redactor', array('model' => $model_content, 'attribute' => 'detail',
                                            'toolbar' => "mini",
                                            "htmlOptions" => array("style" => "height:300px; width:100%;"),
                                            'editorOptions' => array(
                                                //action for img upload
                                                'imageUpload' => Yii::app()->createAbsoluteUrl('post/img'),
                                                'imageGetJson' => Yii::app()->createAbsoluteUrl('post/listimages'),
                                                'fileUpload' => Yii::app()->createAbsoluteUrl('post/uploadFile'),
                                                'videoUpload' => Yii::app()->createAbsoluteUrl('post/uploadSelfVideo'),
                                                'videoGetJson' => Yii::app()->createAbsoluteUrl('post/listVideo'),
                                                'audioUpload' => Yii::app()->createAbsoluteUrl('post/uploadAudio'),
                                                'audioGetJson' => Yii::app()->createAbsoluteUrl('post/listAudio'),
                                                'audioGetJson' => Yii::app()->createAbsoluteUrl('post/listAudio'),
                                                'codeEditor'=>Yii::app()->createAbsoluteUrl('post/codeEditior')
                                            ),));
                                        ?>

                                        <br/>
                                    </div>


                                    <?php if ($type == "lesson") { ?>
                                        <div class="row-fluid content">
                                            <div class="span1"></div>
                                            <div class="span2" align="left">VDO File</div>
                                            <div class="span8" align="left">
                                                    <?php
                                                    $this->widget('FileUploader', array('config' => array(
                                                            'id' => 'video-file-uploader',
                                                            'placeholder' => 'Video file should have size less than 2GB',
                                                            'btnLabel' => 'Choose Video File',
                                                            'url' => Yii::app()->createUrl("admin/default/uploadVideo", array("contentId" => $model_content->id,)),
                                                            'deleteUrl' => '',
                                                            'onDone' => '',
                                                        ),
                                                            )
                                                    );
                                                    ?>
                                              
                                            </div>
                                            <div class="span1"></div>
                                        </div>
                                    <div class="row-fluid content">
                                        <div class="span1"></div>
                                        <div class="span2" align="left"></div>
                                        <div class="span8" align="left">
                                              <span class="note"> <u>Video Type:</u> webm,mp4,3gpp,mov,avi,wmv,flv,mpeg </span> 
                                        </div>                                
                                        
                                    </div>

                                                <?php if (!empty($model_vdo->name)) { ?>
                                            <div class="vdo-block row-fluid content">
                                                <div class="span1"></div>
                                                <div class="span2" align="left"></div>
                                                <div class="span8" align="left">
                                                    <?php
                                                    $video_url = Yii::app()->baseUrl . "/files/$model_vdo->user_id/video/$model_vdo->name";
                                                    $video_path = Yii::app()->basePath . "/../files/$model_vdo->user_id/video/$model_vdo->name";
                                                    if (file_exists($video_path)) {
                                                        $play = VdoHelper::jwplayerStat($video_url, '500px', '300px');
                                                        echo $play;
                                                    }
                                                    ?>
                                                </div>                                   
                                                
                                            </div>
                                                <?php } ?>
                                    <?php } //not type unit?>
                                    
                                    
                                    <?php 
                                    if($type=="code"){
                                        if($code_type=="php" || $model_code->type=="php"){
                                           $this->widget('CodeEditorPhp',array("model_code"=>$model_code));
                                        }else{
                                           $this->widget('CodeEditor',array("model_code"=>$model_code));
                                        }
                                     ?>
                                     <input type="hidden" name="code_type" value="<?php echo $code_type;?>">
                                     <div class="row-fluid content">
                                            <div class="span1"></div>
                                            <div class="span2" align="left">Code Positon</div>
                                            <div class="span8" align="left">
                                                   <select name="code_position">
                                                    <option value="above" <?php if($model_code->position=="above"){echo "selected='selected'";}?>>Above</option>
                                                    <option value="below"  <?php if($model_code->position=="below"){echo "selected='selected'";}?>>Below</option>                                                    
                                                   </select>
                                            </div>
                                            <div class="span1"></div>
                                        </div>
                                    
                                    
                                    
                                    
                                    <?php } ?>
                                    
                                    
                                    <?php if ($type == "lesson" || $type=="code") { ?>
                                        <div class="row-fluid content">
                                            <div class="span1"></div>
                                            <div class="span2" align="left">ภายในบท</div>
                                            <div class="span8" align="left">
                                                    <?php
                                                    echo CHtml::activeDropDownList($model_content, 'parent_id', CHtml::listData(Content::model()->findAll("parent_id='-1' AND course_id='$model_course->id' ORDER BY show_order DESC, id DESC"), 'id', 'title'));
                                                    ?>
                                            </div>
                                            <div class="span1"></div>
                                        </div>
                                    <?php } ?>
                                    
                                    

                                    <br/><br/>
                                </div>
                                <div id="loading" class="center-box" style="display: none;"><img src="images/loader.white.gif"> Encoding Video</div>
                                <div class=" row-course">&nbsp;</div>
                                <!--submit button-->
                                <div class="row-fluid content row-course" id="button-form">                                    
                                    <div class="span3"></div>
                                    <div class="span9" align="right">
                                        <div class="span6"></div>
                                       
                                            
                                            
                                    <?php $back_url = Yii::app()->createUrl("admin/default/manageContent/course/$model_course->id") ?>
                                        <input type="button" value="ย้อนกลับ" class="span3 btn btn-gray-submit"
                                               onClick="window.location.href = '<?php echo $back_url; ?>'">


                                        <button type="submit" class="span3 btn btn-green-submit" id="submit-content">ต่อไป</button>
                                        <!--<button typeclass="span3 btn btn-green-submit" id="submit-content">ต่อไป</button>-->
                                    </div>                                    
                                </div>
                                
                                
                                <div class="row-fluid content row-course" id="success-form-button" style="display: none;">                                    
                                        <div class="span3"></div>
                                        <div class="span9" align="right">
                                            <div class="span8"></div>
                                                                                                                     
                                            <input type="button" value="Finish" class="span3 btn btn-green-submit"
                                               onClick="window.location.href = '<?php echo $back_url; ?>'">
                                        </div>                                    
                                    </div>
                                
                                
                                <!--submit button-->

                            <?php $this->endWidget(); ?>                 
                                
                                
                            </div><!--container-fluid-->
                        </div><!--admin-bg-body row-course-->
                    </div><!--/row-fluid-->
                </div><!--/span9-->

            </div><!--row-fluid-->
        </div><!--container-->

    </div><!--/maincontent-->        
</div> <!-- body-allcourses -->

<?php 
    if(!empty($model_content->id)){
      
         $action_save= Yii::app()->createUrl("admin/default/saveContent",
                                       array("id"=>$model_content->id,"course"=>$model_course->id,"type"=>$type)); 
    }else{
          $action_save= Yii::app()->createUrl("admin/default/saveContent",
                                       array("id"=>"","course"=>$model_course->id,"type"=>$type)); 
    }

    ?>


<script type="text/javascript">
      jQuery.noConflict(); 

             $(document).ready(function() {
                //subit form
                $("#course-content").submit(function(event) {
                event.preventDefault();
                              
                var form_params = $("#course-content").serializeJSON();
//                alert(form_params.toSource());                
         
                $.ajax({
                  url: '<?php echo $action_save; ?>',
                  type: 'POST',
                  data:form_params,
                  dataType: 'json',
                        
                  success: function(json) {
                      
                       if(json["is_validate"]==0){
                            $("#error-content").slideDown();
                            $("#error-content").html(json["error"]);
                       }else{
                          if(json["type"]=="lesson"){
                               
                              $("#error-content").hide();
                              $("#success").show();
                              $("#success").html("Data Have Been saved.");
                              $("#button-form").hide();
                              $("#success-form-button").slideDown();                                                            
                              $("#video-file-uploader").attr('date-url',json["url"]);//not update in new                   
                               fileUploaderChangeHandler('video-file-uploader');                               
//                               window.location.replace("<?php //echo $back_url; ?>");                             
                             
                             }else{
                                                   
                              $("#success").slideDown();
                              $("#success").html("Data has been saved");
                              $("#button-form").hide();
                              $("#success-form-button").slideDown();
//                              $("#form-content").hide();
                               
                           }
                       }
                       
                       
                   }
                });
                });
                
                $(".various").fancybox({
                            maxWidth	: 1500,
                            maxHeight	: 1000,
                            fitToView	: false,
                            width	: '70%',
                            height	: '100%',
                            autoSize	: false,
                            closeClick	: false,
                            openEffect	: 'none',
                            closeEffect	: 'none'
                    });
          
                
             });
           
            
    </script>
    


