<div class="body-admin">
    <div id="maincontent">
        <div class="container">
            <div class="row-fluid">


                <?php $this->renderPartial("_admin_menu", array("course_name" => $model_course->name, "course_id" => $model_course->id)); ?>

                <div class="span9">
                    <div class="container-fluid">
                        <div class="admin-bg-body row-course">
                            <div class="container-fluid">
                                
                                  <div class="row-fluid row-course">
                                        <h2><img src="images/course-content-msg.png" class="h2-img" /><?php echo Yii::t("site","Coupon History")?></h2>
                                         <b><?php echo Yii::t("site","By")?></b>:  <?php echo $model_user->firstname." ".$model_user->lastname; ?>
                                        <div class="row-fluid line-lightgrey-wrapper">&nbsp;</div>                               
                                        
                               </div>                     
                                    
                                <div class="row-fluid content row-course">                                    
                                <div class="span3"></div>
                                <div class="span9" align="right">
                                    <div class="span9"></div>
                                    <button type="submit" class="span3 btn btn-green-submit" onClick="window.location.href='<?php echo LinkHelper::yiiLink("/admin/default/formCoupon/course/$model_course->id")?>'">
                                        <?php echo Yii::t("site","Create Coupon")?>
                                    </button>
                                </div> 
                                </div>
                                
                                <?php
                                    $this->widget('zii.widgets.grid.CGridView', array(
                                        'dataProvider' => $dataProvider,
                                        'columns' => array(
                                            array(
                                                'header' => Yii::t("site","No."),
                                                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)."."',
                                                'htmlOptions'=>array("style"=>"text-align:center;"),
                                            ),
                               

                                            array(
                                                'type' => 'raw',
                                                'header'=>Yii::t("site","quantity"),
                                                'value' => 'makeLinkCoupon($data->id,$data->create_date,"'.$model_course->id.'")',                        
                                                'htmlOptions'=>array("style"=>"text-align:right;"),

                                            ),
                                                                                        
                                            
                                            array(
                                                'header'=>Yii::t("site","Discount"),
                                                'name' => 'discount',
                                                'value' => '$data->discount."%"',
                                                'htmlOptions'=>array("style"=>"text-align:right;"),
                                            ),

                                            array(
                                                'name' => 'create_date',
                                                'value' => 'SiteHelper::dateFormat($data->create_date)',
                                                'htmlOptions'=>array("style"=>"text-align:center;"),
                                            ),
                                            
                                            array(
                                                'name' => 'expire_date',
//                                                 'value' => '$data->expire_date',
                                                'value' => 'SiteHelper::dateFormat($data->expire_date,"date")',
                                                'htmlOptions'=>array("style"=>"text-align:center;"),
                                            ),
                                                                                        
                                             array(
                                                'type' => 'raw',
                                                'header'=>Yii::t("site","Print Coupon"),
                                                'value' => 'printlink($data->create_date,"'.$model_course->id.'")',                        
                                                'htmlOptions'=>array("style"=>"text-align:center;"),

                                            ),
                                        ),
                                    ));
                                    ?>
                            </div><!--container-fluid-->
                        </div><!--admin-bg-body row-course-->
                    </div><!--/row-fluid-->
                </div><!--/span9-->

            </div><!--row-fluid-->
        </div><!--container-->

    </div><!--/maincontent-->        
</div> <!-- body-allcourses -->

<?php 
function makeLinkCoupon($qty,$date,$course){
    $link=  LinkHelper::yiiLink("/admin/default/viewCoupon/date/$date/course/$course");
    $html="<a href='$link' title='View coupon inside'><b>$qty</b></a>" ;
    return $html;
}



function printlink($date,$course){
    $link=  LinkHelper::yiiLink("/admin/default/printCoupon/date/$date/course/$course");
    $html="<a href='$link' title='Print'>
            <img src='images/button/print.png'>
            </a>" ;
    return $html;
}

?>
