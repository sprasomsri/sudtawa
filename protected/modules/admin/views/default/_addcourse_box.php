<div class="w-col w-col-3 w-col-medium-4 course-box">
    <a href="<?php echo Yii::app()->createUrl("admin/default/operateCourse"); ?>" title="Create new course">
        <div class="course-thumbnail">
            <img class="example-image img-responsive" src="images/add_course.png" alt="coursecreek">

            <div class="course-detail">
                <h4 class="course-thumbnail-title">Add Course</h4>
                <div class="course-data">
                    มาร่วมกันพัฒนาประเทศด้วยกันการสอน พร้อมทั้งสร้างรายได้ และชื่อเสียงไปด้วยกันกับ CourseCreek 
                </div>
                <div class="instructor-data">
                    <div class="w-row">
                        <div class="w-col w-col-3">
                            <?php
                            $profile_img = "images/director.jpg";
                            $teacher = "";
                            if (!empty($owner)) {
                                $teacher = $owner->profile;

                                if (!empty($teacher->photo) && file_exists(Yii::app()->basePath . "/../webroot/userphoto/$teacher->photo")) {
                                    $profile_img = "userphoto/$teacher->photo";
                                }
                            }
                            ?>
                        </div>
                        <div class="w-col w-col-9 text-right" style="padding-right: 1%;">
                            <button type="button" class="btn btn-info" align="right"><span class="glyphicon glyphicon-plus"></span> NEW</button>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>

