<link rel="stylesheet" type="text/css" href="css/code.css">
<div class="bg-pagestudy">
    <div class="w-container">
      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
         </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">คำถาม - CODE</h1>
               <!----- Right content------>               
                <a href="<?php echo LinkHelper::yiiLink("admin/default/showQuestion/exercise/$exercise/course/$model_course->id")?>" title=' <?php echo Yii::t("site","Question Page");?>' class="pull-right">
                                <img src="images/button/question.png" width='20px' /> <?php echo Yii::t("site","Question Page");?>     
               </a>
               
               <?php $this->renderPartial("_question_list",array("model_all_question"=>$model_all_question,
                                                                                 "exercise"=>$exercise,"course_id"=>$model_course->id,
                                                                                 "question_id"=>$model_question->id)); ?>
               
               <?php
               $form = $this->beginWidget('CActiveForm', array(
                   'id' => 'form-code-question',
                   'htmlOptions' => array('enctype' => 'multipart/form-data'),
                   'clientOptions' => array(
                       'validateOnSubmit' => true,
                   ),
               ));
               ?>
               
               <?php
               if (!empty($message)) {
                   echo "<div class='alert alert-success' role='alert'> <span class='glyphicon glyphicon-floppy-saved'></span>&nbsp;&nbsp;  $message</div>";
                }
               ?>
               
                <?php echo $form->errorSummary($model_question); ?>
               
               <div class="form-group">                
                <label>คำถาม</label>                 
                 
                <?php
                $this->widget('application.widgets.redactorjs.Redactor', array('model' => $model_question, 'attribute' => 'question',
                    'toolbar' => "mini",
                    "htmlOptions" => array("style" => "height:300px; width:100%;","class"=>"form-control"),
                    'editorOptions' => array(
                        //action for img upload
                        'imageUpload' => Yii::app()->createAbsoluteUrl('post/img'),
                        'imageGetJson' => Yii::app()->createAbsoluteUrl('post/listimages'),
                        'fileUpload' => Yii::app()->createAbsoluteUrl('post/uploadFile'),
                        'videoUpload' => Yii::app()->createAbsoluteUrl('post/uploadSelfVideo'),
                        'videoGetJson' => Yii::app()->createAbsoluteUrl('post/listVideo'),
                        'audioUpload' => Yii::app()->createAbsoluteUrl('post/uploadAudio'),
                        'audioGetJson' => Yii::app()->createAbsoluteUrl('post/listAudio'),
                    ),));
                ?>                       
                 
               </div>
               
               <div class="form-group">                
                <label>initial code</label>                
                     <?php 
                        if($type_code=="java"){                     
                            $this->widget('CodeEditor',array("mode"=>"text","text_code"=>$model_question->initial_code,"input_name"=>"code_default","comment"=>"//initial code")); 
                        }else{
//                             $this->widget('CodeEditorSandBox',array("model_code"=>)); 
                             $this->widget('CodeEditorSandBox',array("model_code"=>$model_answer,"input_name"=>"code","is_exercise"=>"1","is_show_answer"=>"1", "answer"=>$model_answer->answer)); 
                        }
                        
                     ?>
                
                </div>       
               <div class="form-group">                
                <label>ลำดับ</label>                   
                  <?php 
                                    if($mode=="new"){
                                        $show_order=$qty_question+1;
                                    }else{
                                        $show_order=$model_question->show_order;
                                    }
                                    
                   ?>                
                      <?php echo $form->textField($model_question, 'show_order',array("value"=>$show_order,"class"=>"form-control")); ?> 
                </div>
               
               <hr/>
               
               <h4>Answer Code </h4> <?php echo $form->errorSummary($model_question); ?>
               
                 <?php 
                 
                 
                  if($type_code=="java"){
                    $this->widget('CodeEditor',array("mode"=>"text",
                                                                       "input_name"=>"code",
                                                                        "is_exercise"=>"1",
                                                                        "text_code"=>$model_answer->code,
                                                                        "input_answer_name"=>"answer",
                                                                        "answer"=>$model_answer->answer,
                                                                        "is_show_answer"=>1,
                                                                        "is_show_compile"=>"0")); 
                  }else{
                      
                         $this->widget('CodeEditorSandBox',array("model_code"=>$model_answer,"input_name"=>"code","is_exercise"=>"1","is_show_answer"=>"1", "answer"=>$model_answer->answer)); 
                  }
                  
                  ?>             
                <button type="submit" class="btn btn-warning"><?php echo Yii::t("site", "Save"); ?></button>
               
                 <?php $this->endWidget(); ?>
               
               <!-----END Right content------>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

