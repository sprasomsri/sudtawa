<div class="admin-coment-row">
    <div class="w-row">
        <div class="w-col w-col-3 user-colum text-center">
            <div class="admin-user-comment">               
                <?php
                $student_pic_path = Yii::app()->basePath . "/../userphoto/" . $data->profile->photo;

                if (!empty($data->profile->photo) && file_exists($student_pic_path)) {
                    $student_pic = "userphoto/" . $data->profile->photo;
                } else {
                    $student_pic = "images/user_default.jpg";
                }

                if (!empty($data->user->facebook_id)) {
                    $student_pic = "https://graph.facebook.com/" . $data->user->facebook_id . "/picture";
                }
                ?>

                <img src="<?php echo $student_pic; ?>">
            </div>

            <div class="admin-comment-name"><strong><?php echo $data->user->username; ?></strong></div>
            <div class="admin-coment-date"><?php echo SiteHelper::dateFormat($data->create_date); ?></div>
        </div>
        <div class="w-col w-col-9">
            <h4><span class="admin-comment-prefix">แสดงความเห็นในบทเรียน:</span>&nbsp;<strong><?php echo $data->content->title; ?></strong></h4>
            <p class="admin-comment-message">
                <?php echo $data->message; ?>   
            </p>
        </div>
    </div>
</div>