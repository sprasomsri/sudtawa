<form action="<?php echo LinkHelper::yiiLink("admin/default/upload")?>" method="post" enctype="multipart/form-data" id="hello">
    <input type="file" name="uploadedfile" ><br>
    <input type="submit" value="Upload File to Server" >


</form>

<div class="progress">
    <div class="bar"></div >
    <div class="percent">0%</div >
</div>

<div id="status"></div>

<script>
    (function() {

        var bar = $('.bar');
        var percent = $('.percent');
        var status = $('#status');

        $('#hello').ajaxForm({
            beforeSend: function() {
                status.empty();
                var percentVal = '0%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal)
                percent.html(percentVal);

            },
            complete: function(xhr) {
                bar.width("100%");
                percent.html("100%");
                status.html(xhr.responseText);
            }
        });

    })();
</script>