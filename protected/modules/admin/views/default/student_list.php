<div class="bg-pagestudy">
    <div class="w-container">


      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">
              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
              
           
          </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">นักเรียน</h1>
               <!----- Right content------>
               <div class="w-row admin-row-user">
                  <?php
                                $this->widget('zii.widgets.CListView', array(
                                    'id' => 'user_list',
                                    'dataProvider' => $dataProvider,
                                    'itemView' => '_student_card',
                                    'template' => " {summary}\n{pager} \n {items}",
                                    'viewData' => array('model_course' => $model_course),
                                    'pager' => array(
                                        'class' => 'CLinkPager',
                                    ),
                                ));
                   ?>
                   
                   
                   <?php if($mode=="student"){ ?>
                   <div class="row-fluid line-lightgrey-wrapper text-center">


<!--                           <a  class="btn btn-warning" href="<?php echo Yii::app()->createUrl('admin/default/StatProgressOfStudy', array("course" => $model_course->id)); ?>">
                               <?php //echo Yii::t("site", "All Student Progress") ?>
                           </a>-->
                       
                        <a  target="_blank" class="btn btn-primary" href="<?php echo Yii::app()->createUrl('admin/default/stdentexcel', array("course" => $model_course->id)); ?>">
                              ตารางรายการนักเรียน
                        </a>


                   </div>
                   <?php } ?>
       
               </div>  
               <!-----END Right content------>
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>

<script type="text/javascript">
                              $(document).ready(function() {
                                        
                                     $(".various").fancybox({
                                            maxWidth	: 1800 ,
                                            maxHeight	: 1000,
                                            fitToView	: true,
                                            width         : '80%',
                                            height	: '80%',
                                            autoSize	: true,
                                            closeClick	: false,
                                            openEffect	: 'none',
                                            closeEffect	: 'none'
                                    });
                             });
                                
                                                            
</script>


