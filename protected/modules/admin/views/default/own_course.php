<?php LinkHelper::SetPageHistory();?>
<div class="progress-section" >
    <div class="w-container">
           <?php $this->widget('CourseCategoryLink',array("active_box"=>"admin")); ?>   
    </div>
 </div>

<div class="block-receipts">
   
    
            <div class="w-row">
                <div class="w-col w-col-2"></div>
                <div class="w-col w-col-8">
                  
                </div>
                <div class="w-col w-col-2"></div>
            </div>
    
    
            <div class="w-row">
                <div class="w-col w-col-2"></div>
                <div class="w-col w-col-8 course-receipts">
                    <a class="btn btn-primary" href="<?php echo Yii::app()->createUrl("admin/default/operateCourse"); ?>"> Add course</a>          
                     <?php
                        $this->widget('zii.widgets.CListView', array(
                            'id' => 'courselist',
                            'dataProvider' => $courses,
                            'itemView' => '_course_box',
                            'template' => " {summary}\n{pager} \n {items}",
                            //'viewData' => array('lang' => $lang),
                            'pager' => array(
                                'class' => 'CLinkPager',
                            ),
                        ));
                      ?>  
                    
                    
                </div>
                <div class="w-col w-col-2"></div>
            </div>
</div>
    
    

<script>
    $(document).ready(function(){
       document.title = "Admin Course"; 
    });
</script>