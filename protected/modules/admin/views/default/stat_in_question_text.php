<div class="body-admin">
    <div id="maincontent">
        <div class="container">
            <div class="row-fluid">


                <?php $this->renderPartial("_admin_menu", array("course_name" => $model_course->name, "course_id" => $model_course->id)); ?>

                <div class="span9">
                    <div class="container-fluid">
                        <div class="admin-bg-body row-course">
                            <div class="container-fluid">


                                <!--Course Detail-->
                                <div class="row-fluid row-course">
                                    <h2><img src="images/course-detail-msg.png" class="h2-img" /> 
                                        <?php echo Yii::t("site", "Stat Question") . "<br/>" . strip_tags($model_question->question) . "?"; ?>
                                    </h2>
                                    <div class="row-fluid line-lightgrey-wrapper">&nbsp;</div> 
                                </div>


                                <?php $this->widget('ExerciseStatGrap', array("model_question" => $model_question,  "model_exercise"=>$model_exercise, "width" => "700", "height" => "400", "mode" => "questionText")); ?> 

                                <a href='<?php echo LinkHelper::yiiLink("admin/default/showQuestion/exercise/$exercise/course/$model_course->id") ?>' class="right"  title='question-link'>
                                    <img src="images/button/question.png" width='20px' /> <?php echo Yii::t("site", "Question Page"); ?>   
                                </a>

                            </div><!--container-fluid-->
                        </div><!--admin-bg-body row-course-->
                    </div><!--/row-fluid-->
                </div><!--/span9-->

            </div><!--row-fluid-->
        </div><!--container-->

    </div><!--/maincontent-->        



</div> <!-- body-allcourses -->

