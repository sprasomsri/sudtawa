<link rel="stylesheet" type="text/css" href="css/code.css">
<div class="bg-pagestudy">
    <div class="w-container">

      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">
              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
              
           
          </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <?php 
                                if($type=="unit"){
                                    $form_name= "หน่วยเรียน [Unit]";
                                    $title="ชื่อบท";
                                }else{
                                     $form_name= "บทเรียน [Lesson]";
                                    $title="ชื่อบทเรียน";
                                }                                
                                           
              ?>  
                
              <h1 class="admin-head"> <?php echo $form_name; ?></h1>
               <!----- Right content------>
       
               
               
               <?php 
               
               $is_super_admin= Yii::app()->user->getState("superadmin");
               if((!empty($model_content->approve) || $model_content->is_approve==0) && $is_super_admin==1 && !empty($model_content->id)){ ?>
               <br/>
               <p class="text-right">
               
                   <?php
                    $link_approve = Yii::app()->createUrl("admin/default/approvecontent",array("id"=>$model_content->id));
                  
                   if($model_content->is_approve==0){
                       $link_approve=Yii::app()->createUrl("admin/default/appoveNewContentPage",array("content_id"=>$model_content->id));
                   }
                   
                   ?>
                   
                   <a href="<?php echo $link_approve; ?>">
                       <span class="label label-warning"><span class="glyphicon glyphicon-ok"></span> Approve</span> 
                   </a>
               
                   <a href="<?php echo Yii::app()->createUrl("admin/default/DeleteTempContent",array("id"=>$model_content->id,"course"=>$model_content->course_id)); ?>">
                        <span class="label label-default"><span class="glyphicon glyphicon glyphicon-refresh"></span> Not Approve</span>
                   </a>
                   
               </p>
               
               
               <?php } ?>
               
               <div class="w-row admin-row-user">
                   <?php
                    //use Ajax for submit and save data
                  $form = $this->beginWidget('CActiveForm', array(
                       'id' => 'course-content',
                       'htmlOptions' => array(
                           'enctype' => 'multipart/form-data',
                       ),
                       'clientOptions' => array(
                           'validateOnSubmit' => false,
                       ),
                   ));
                   ?>
                   <?php echo $form->errorSummary($model_content); ?>
                   
                    <div id="error-content" style="display: none;" class="alert error-alert-info"> </div>
                    <div id="success" style="display: none;" class="alert alert-info"> </div>
                   
                   <div class="form-group">
                    <label for="exampleInputEmail1"><?php echo $title; ?></label>
                    <?php echo $form->textField($model_content, 'title',array("class"=>"form-control")); ?>
                  </div>
                    
                  <div class="form-group">
                    <label for="exampleInputEmail1">คำอธิบาย</label>
                    <?php
                                        
                                        $this->widget('application.widgets.redactorjs.Redactor', array('model' => $model_content, 'attribute' => 'detail',
                                            'toolbar' => "mini",
                                            "htmlOptions" => array("style" => "height:300px; width:100%;","class"=>"form-control supermarget"),
                                            'editorOptions' => array(
                                                //action for img upload
                                                'imageUpload' => Yii::app()->createAbsoluteUrl('post/img',array("course_id"=>$model_course->id)),
                                                'imageGetJson' => Yii::app()->createAbsoluteUrl('post/listimages',array("course_id"=>$model_course->id)),
                                                'fileUpload' => Yii::app()->createAbsoluteUrl('post/uploadFile',array("course_id"=>$model_course->id)),
                                                'videoUpload' => Yii::app()->createAbsoluteUrl('post/uploadSelfVideo',array("course_id"=>$model_course->id)),
                                                'videoGetJson' => Yii::app()->createAbsoluteUrl('post/listVideo',array("course_id"=>$model_course->id)),
                                                'audioUpload' => Yii::app()->createAbsoluteUrl('post/uploadAudio',array("course_id"=>$model_course->id)),
                                                'audioGetJson' => Yii::app()->createAbsoluteUrl('post/listAudio',array("course_id"=>$model_course->id)),
                                                'codeEditor'=>Yii::app()->createAbsoluteUrl('post/codeEditior')
                                            ),));
                                        ?>
                  </div>
                   
                <?php if ($type == "lesson") { ?>                    
                    
                <div class="form-group">
                    <label for="exampleInputEmail1">VDO File</label>

                               
                          <div id="video-file-uploader"></div>
                          <input type="file" name="file_upload" id="file_upload" />
                          <div id="some_file_queue" style="width:100%" class="row full cen"></div>
                          <!--<a id="upload-start" class="btn btn-inverse btn-bg btn-padding" href="javascript:$('#file_upload').uploadify('upload', '*')"></a>-->
                    
                                        
                         <br/><br/>
                
                          <?php if (!empty($model_vdo->name)) { ?>
                            <!--<div class="vdo-block">-->
                               
                                    <?php
                                    $video_url = Yii::app()->baseUrl . "/files/$model_vdo->user_id/video/$model_vdo->name";
                                    $video_path = Yii::app()->basePath . "/../webroot/files/$model_vdo->user_id/video/$model_vdo->name";
                                    if (file_exists($video_path)) {                          
                                       $play = VdoHelper::jwplayer($model_vdo,$model_content->id);                                       
                                       echo $play;                           
                                    }
                                    ?>
                             <!--</div>-->

                        <?php } ?>
                         
                         <div id="vdo-preview"> </div>                         
                 </div>
                    
                  <div class="form-group">
                    <label for="exampleInputEmail1">Youtube Link</label>
                    <?php echo $form->textField($model_content, 'youtube_link',array("class"=>"form-control")); ?>
                     <?php
                                if (!empty($model_content->youtube_link)) {
                                    echo $play = VdoHelper::jwyoutube($model_content->youtube_link);
                                }
                      ?>
                  </div>
                    
                 <?php } //not type unit ?>
                    
                    
                    
                    <?php
                    if ($type == "code") {
                        if ($code_type == "php" || $model_code->type == "php") {
                            $this->widget('CodeEditorPhp', array("model_code" => $model_code));
                        } else {
                            
                             if ($code_type == "python" || $model_code->type == "python") {
                                $this->widget('CodeEditorSandBox', array("model_code" => $model_code));
                             }else{                   
                                $this->widget('CodeEditor', array("model_code" => $model_code));
                             }
                        }
                        ?>
                       
                    
                     <input type="hidden" name="code_type" value="<?php echo $code_type; ?>">
                    
                     
                     <div class="form-group">
<!--                    <label for="exampleInputEmail1">Code Positon</label>
                    <select name="code_position" class="form-control">
                           <option value="above" <?php if($model_code->position=="above"){echo "selected='selected'";}?>>Above</option>
                           <option value="below"  <?php if($model_code->position=="below"){echo "selected='selected'";}?>>Below</option>                                                    
                    </select>-->
                    
                    </div>            
                                     
                     
                                     
                    <?php } ?>
                    
               <?php if ($type == "lesson" || $type=="code") { ?>
                    <div class="form-group">
                    <label for="exampleInputEmail1">ภายในบท</label>
                     <?php
                          echo CHtml::activeDropDownList($model_content, 'parent_id', CHtml::listData(Content::model()->findAll("parent_id='-1' AND course_id='$model_course->id' ORDER BY show_order DESC, id DESC"), 'id', 'title'),array('class'=>'form-control'));
                     ?>
                  </div>
                    
                   
                 <?php } ?>
                   
            
<!--            <div class="form-group">
               <label><?php echo Yii::t("site", "is free ?"); ?></label>          
               <?php echo $form->checkBox($model_content, 'is_free',array("value"=>1)); ?>
            </div>       -->
                   
                    
           <?php $back_url = Yii::app()->createUrl("admin/default/manageContent/course/$model_course->id") ?>
            <div id="button-form">     
             <button type="submit" class="btn btn-warning" id="submit-content">ต่อไป</button>
             
             <input type="button" value="ย้อนกลับ" class=" btn btn-default"
                            onClick="window.location.href = '<?php echo $back_url; ?>'">
            </div>
        
                                
              <div  id="success-form-button" style="display: none;"> 
                     <br/><br/>
                     <p class="text-success"> ทำการบันทึกข้อมูลแล้ว กดย้อนกลับเพื่อกลับสู่หน้าจัดการคอร์ส </p>
                                                 
              </div>
                    
                    
                    <?php $this->endWidget(); ?>   
               </div>  
                              
               <!-----END Right content------>
            </div>
                      
          </div>
        </div>
      </div>

    </div>
  </div>

<?php 
    if(!empty($model_content->id)){
      
         $action_save= Yii::app()->createUrl("admin/default/saveContent",
                                       array("id"=>$model_content->id,"course"=>$model_course->id,"type"=>$type)); 
    }else{
          $action_save= Yii::app()->createUrl("admin/default/saveContent",
                                       array("id"=>"","course"=>$model_course->id,"type"=>$type)); 
    }

    ?>

   <!--- Admin script --->   
   <?php $this->renderPartial('_admin_script',array("page"=>"form_content","action_save_content"=>$action_save)); ?>  


