<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $dataProvider_people,
    'filter' => $model_person,
    'columns' => array(
        array(
            'header' => 'No.',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)."."',
        ),

        array(
            'name' => 'name',
            'value' => '$data->name."  ".$data->lastname',
        ),
        
        array(
            'header'=>"Join course Date",
            'value' => 'SiteHelper::dateFormat($data->create_date)',
        ),
        
        array(
            'class' => 'CButtonColumn',
            'header' => 'Manage',
            'template' => '{delete}',
            'deleteConfirmation' => "Are You Sure ?",
            'buttons' => array(
         
                'delete' => array(
                    'label' => 'Delete',
                
                    'url' => 'CHtml::normalizeUrl(array("/admin/default/deleteStudent","user_id"=> $data->id,"course"=>$data->course_id))',
                    'imageUrl' => 'images/button/deletePerson.png',
                ),
            ),
        ),
    ),
));
?>