<style type="text/css" title="currentStyle">
    @import "js/datatable/css/demo_page.css";
    @import "js/datatable/css/demo_table.css";
</style>

<div class="bg-pagestudy">
    <div class="w-container">

      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
                         
          </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">ส่วนแบ่ง</h1>
               <!----- Right content------>
               <div class="w-row admin-row-user">             
                 <?php
                  $is_super = Yii::app()->user->getState('superadmin');
                 
                  if(!empty($is_super)){
                       
                    //use Ajax for submit and save data
                    $form = $this->beginWidget('CActiveForm', array(
                         'id' => 'division-content',
                         'htmlOptions' => array(
                             'enctype' => 'multipart/form-data',
                         ),
                         'clientOptions' => array(
                             'validateOnSubmit' => false,
                         ),
                       ));
                    
                    ?>
                   
                   <?php echo $form->errorSummary($model_money_form); ?>
                    
                   <?php if(Yii::app()->user->hasFlash('error_share')):?>
                    <div class="error">
                        <?php echo Yii::app()->user->getFlash('error_share'); ?>
                    </div>
                    <?php endif; ?> 
                   
                   
                    <div class="w-row">
                        <div class="w-col w-col-1"></div>
                        
                        <div class="w-col w-col-3">
                              <label> Teacher share percent </label>
                              <?php echo $form->textField($model_money_form, "teacher_share_percent",array("class"=>"form-control")) ?>  
                        </div>
                        
                        <div class="w-col w-col-1"></div>
                        <div class="w-col w-col-3">
                             <label> Company share percent </label>
                             <?php echo $form->textField($model_money_form, "company_share_percent",array("class"=>"form-control")) ?>  
                        </div>
                        <div class="w-col w-col-1"></div>
                        
                        <div class="w-col w-col-3">
                            <label>&nbsp; </label>
                            <button type="submit" class="btn btn-primary"> Save </button>
                        </div>
                        
                            
                        </div>
                        
                         
                    </div>
                    
                    
                    
                    <?php
                    $this->endWidget();
                    
                  
                  }
                  
                 
                 
                 if(!empty($model_money_div_rate)){
                     echo "<b>RATE</b>";
                     echo "<table class='dataTable' width='100%'>";
                     echo "<thead><tr>";
                     echo  "<th> Teacher </th>"; 
                     echo  "<th> Organization </th>"; 
//                     echo  "<th> Affiliate </th>"; 
                     echo "</tr></thead>";
                     
                     
                      echo "<tbody><tr>";
                     echo  "<td> $model_money_div_rate->teacher_share_percent	 </td>"; 
                     echo  "<td> $model_money_div_rate->company_share_percent	 </td>"; 
//                     echo  "<td> $model_money_div_rate->affiliate_share_percent </td>"; 
                     echo "</tr></tbody>";
                     
                     echo "</table><hr/>";
                 }                 
                 
                 
                 if(!empty($dataProvider)){
                                    $this->widget('zii.widgets.grid.CGridView', array(
                                        'dataProvider' => $dataProvider,
                                        'columns' => array(
                                            array(
                                                'header' => Yii::t("site","No."),
                                                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)."."',
                                                'htmlOptions'=>array("style"=>"text-align:center;"),
                                            ),
//                               
//
                                            array(
                                                'type' => 'raw',
                                                'header'=>Yii::t("site","Create Date"),
                                                'value' => 'SiteHelper::dateFormat($data->create_date,"datetime")',                        
                                                'htmlOptions'=>array("style"=>"text-align:center;"),

                                            ),
                                            
//                                            array(
//                                                'type' => 'raw',
//                                                'header'=>Yii::t("site","Username"),
//                                                'value' => '$data->user->username',                        
//                                                'htmlOptions'=>array("style"=>"text-align:center;"),
//
//                                            ),
                                            
                                            array(
                                                'type' => 'raw',
                                                'header'=>Yii::t("site","(%) Shared"),
                                                'value' => 'number_format($data->teacher_share_percent)',                        
                                                'htmlOptions'=>array("style"=>"text-align:center;"),

                                            ),
                                            
                                            array(
                                                'type' => 'raw',
                                                'header'=>Yii::t("site","Price (Bath)"),
                                                'value' => 'number_format($data->teacher_share_price)',                        
                                                'htmlOptions'=>array("style"=>"text-align:center;"),
                                            ),   
                                        ),
                                    ));
                 }
                  
                 ?>       
               </div>  
               <!-----END Right content------>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>







