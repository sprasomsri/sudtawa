<div class="bg-pagestudy">
    <div class="w-container">
        <div class="admin-page">
            <div class="w-row">
                <div class="w-col w-col-3">              
                    <?php $this->renderPartial("_admin_menu", array("course_name" => $model_course->name, "course_id" => $model_course->id, "img" => $model_course->course_img)); ?>  
                </div>
                <div class="w-col w-col-9">
                    <div class="admin-content">
                        <h1 class="admin-head">คอร์สที่เกี่ยวข้อง</h1>
                        <!----- Right content------>
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'relate-course',
                            'htmlOptions' => array('enctype' => 'multipart/form-data'),
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        ?>
                        
                        <?php echo $form->errorSummary($model_course); ?>
                        <?php
                        if (!empty($message)) {
                            echo "<div class='alert alert-info'>  " . $message . "</div>";
                        }
                        ?>
                        
                        <div class="form-group">
                            <label><?php echo Yii::t("site", "Before Course"); ?></label>
                            <?php echo Chosen::multiSelect("beforeList", $before_course, $all_course, array("style" => "width:100%; height:300px;", "id" => "before-course", "class" => "form-control")); ?>
                        </div>

                        <div class="form-group">
                            <label><?php echo Yii::t("site", "After Course"); ?></label>
                            <?php echo Chosen::multiSelect("afterList", $after_course, $all_course, array("style" => "width:100%; height:300px;", "id" => "after-course", "class" => "form-control")); ?>
                        </div>

                        <button type="submit" class="btn btn-warning"><?php echo Yii::t("site", "Save"); ?></button>    

                        <?php $this->endWidget(); ?> 

                        <!-----END Right content------>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
