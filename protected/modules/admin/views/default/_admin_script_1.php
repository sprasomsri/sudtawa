<?php if($page=="form_content"){ ?>

<script type="text/javascript">
 
    
             $(document).ready(function() {
                //subit form
                $("#course-content").submit(function(event) {
                event.preventDefault();
                              
                var form_params = $("#course-content").serializeJSON();
              
         
                $.ajax({
                  url: '<?php echo $action_save_content; ?>',
                  type: 'POST',
                  data:form_params,
                  dataType: 'json',
                        
                  success: function(json) {
                      
                       if(json["is_validate"]==0){
                            $("#error-content").slideDown();
                            $("#error-content").html(json["error"]);
                       }else{
                          if(json["type"]=="lesson"){
                               
                              $("#error-content").hide();
                              $("#success").show();
                              $("#success").html("Data Have Been saved.");
//                              $("#button-form").hide();
//                              $("#success-form-button").slideDown();                                                            
                              $("#video-file-uploader").attr('date-id',json["id"]);//not update in new    
                              
                              
                              queueUploadSize = $(".uploadify-queue-item").size();
                              
                                                        
                              if(queueUploadSize > 0){
                                   //click upload 
                                $('#file_upload').uploadify('upload', '*');

                                   //send id to gen url ----
                              }else{
                                  $("#success-form-button").slideDown();
                              }                               
                             
                             }else{
                                                   
                              $("#success").slideDown();
                              $("#success").html("Data has been saved");
                              $("#button-form").hide();
                              $("#success-form-button").slideDown();
//                              $("#form-content").hide();
                               
                           }
                       }
                       
                       
                   }
                });
                });                
                   
                
             });
           
            
    </script> 
    <script src="plugin/uploadify/uploadify_lib/jquery.uploadify.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="plugin/uploadify/uploadify_lib/uploadify.css">
    
    <script type="text/javascript">
        $(function() {
            $('#file_upload').uploadify({
                                'swf': 'plugin/uploadify/uploadify_lib/uploadify.swf',
                                //'uploader' : '/plugin/uploadify/uploadify_lib/uploadify.php',
                                'uploader': "<?php echo Yii::app()->createUrl("admin/default/saveVideoContent"); ?>",
                                 //options
                                 'auto': false,
                                 'multi': true,
                                 'buttonText': 'เลือกไฟล์',
                                 'method': 'POST',
                                 'queueSizeLimit' : 1,
                                 'fileTypeExts': '*.mp4;', // defualt '*.*'
                                 'queueID': 'some_file_queue',
                                 'fileObjName': 'Filedata', // default Filedata                                                                      
                                                                        
                                 'onUploadSuccess': function(file, data, response) {                                                                            
                                        var file_name = $("#file_name_upload").val();
                                        if (file_name == "") {
                                             $("#file_name_upload").attr("value", data);
                                        } else {
                                             file_name = file_name + "," + data;
                                             $("#file_name_upload").attr("value", file_name);
                                        }
                                        $("#success-form-button").slideDown();
                                   },
                                  'onUploadError' : function(file, errorCode, errorMsg, errorString) {
                                      alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
                                   }                                                  
                                                                              
                                   });
       });

      </script>

    
    
    
<?php } ?>

<?php if($page=="form_course"){ ?>      
   <script type="text/javascript">
    $(document).ready(function() {
        $(".tumb").fancybox({
            helpers: {
            title : {
                type : 'float'
               }
             }
        });   
        
       $("#delphoto").click(function(){
           $("#theme-phto-id").val("");
       });
       
       $("#delvdo").click(function(){
           $("#demo-video-id").val("");
       });        
       $("#select-theme-photo").fancybox({
		maxWidth	: 900,
		maxHeight	: 900,
		fitToView	: false,
		width		: '80%',
		height		: '70%',
		autoSize	: false,
		closeClick	: true,
		openEffect	: 'none',
		closeEffect	: 'none',
                
	});
        
        $(".demo-select").fancybox({
		maxWidth	: 900,
		maxHeight	: 900,
		fitToView	: false,
		width		: '80%',
		height		: '70%',
		autoSize	: false,
		closeClick	: true,
		openEffect	: 'none',
		closeEffect	: 'none',
                
	});
      });
           
    </script>
      
<?php } ?>

<?php if($page=="form_question"){ ?>
    <script>
        $(document).ready(function(){
            $("#more-answer").click(function(event){
            event.preventDefault();
            var last=$(".ans-count").last().data("order");
            var lasted = last+1;
            var html="  <div class='form-group ans-count exercise-list' align='left' data-order='"+lasted+"'>\n\
                           <label><?php echo Yii::t("site", "Answer"); ?>: "+lasted+" </label> \n\
                           <div class='span8' align='left'>\n\
                           <input name='ExerciseAnswerChoice["+lasted+"][answer]'type='text' class='form-control'/><br />\n\
                           <input name='ExerciseAnswerChoice["+lasted+"][hint]'type='text' placeholder='Hint' maxlength='700' class='hint form-control'/>\n\
                           <input name='ExerciseAnswerChoice["+lasted+"][is_true]' type='hidden'>\n\
                       </div>";
                       $("#more-answer-show").append(html);
                });
      });
    </script>    
<?php } ?>

    
 <?php if($page=="form_question_text"){ ?>
    <script>
    $(document).ready(function(){
    $("#more-answer").on("click",function(event){
        event.preventDefault();
        var last=$(".ans-count").last().data("order");
        var lasted = last+1;
        var html=" <div class='form-group' align='left' data-order='"+lasted+"'>\n\
                   <label> <?php echo Yii::t("site", "Answer"); ?>: "+lasted+" </label> \n\
                   <input name='ExerciseTextAnswer["+lasted+"][answer]'type='text' class='form-control'/>\n\
                   <input name='ExerciseTextAnswer["+lasted+"][sequence]' type='hidden' class='form-control' value="+lasted+"><br/>\n\
                   <input name='ExerciseTextAnswer["+lasted+"][hint]' type='text'  class='hint form-control' placeholder='Hint'>\n\
                    </div>";                             
     $("#more-answer-show").append(html);
                                       
     });
    });                                
    </script>
 <?php } ?>

    
    
    
    
 <?php if($page=="manage_content"){ ?> 
    
   <?php 
        $today = date("Y-m-d H:i:s");
        $action_sort=Yii::app()->createUrl("admin/default/sortContent");
        $action_change_public = Yii::app()->createUrl("admin/default/changePublic");
   ?>
      
   <script type="text/javascript">      
   function changePublic(id,state){       
        $.ajax({                         
            url: "<?php echo $action_change_public; ?>",
            type: 'POST',
            data:{id : id, state : state},
            dataType: 'html',         
            success:function(data) {                   
                var src = ($("#public-"+id).attr("src") === "imagesv1/button/on.png")
                ? "imagesv1/button/off.png"
                : "imagesv1/button/on.png";
                $("#public-"+id).attr("src", src);              
              }              
            });  
    }
      
    function confirmDelete() {
         var msg = confirm('Are you sure?');
         if(msg == false) {
              return false;
        }
    }

    function makeSortable(){
            $('.content-list').sortable({
                    //handle : '.content-handle',
                    update: function() {
                    var contentList = $('.content-list').sortable();
                    var order = $(contentList).sortable('serialize');
                    $.ajax({
                        url: '<?php echo $action_sort; ?>',
                        data: order,
                        dataType: 'json',
                        type: 'POST',
                        success: function(result) {
                             
                           if(result){
                                 $.each(result, function (index, value) {
                              
                                if(value=="error"){
                                     alert('Please contain lesson in unit');
                                      location.reload();
                                 }
                              var html_update = " <div id='approve-sub-"+value+"' class='w-row approve-list-inside gray-text'> \n\
                              <div class='w-col w-col-5'> change order</div>\n\
                              <div class='w-col w-col-3'><b>แก้ไขเมื่อ  </b><?php echo SiteHelper::dateFormat($today);?></div>\n\
                              <div class='w-col w-col-4 text-right'>  <a class='approve-link' data-approveid='"+value+"'> Approve </a> </div>\n\
                              </div>";
                              
                              $("#approve-sub-"+value).remove();
                              $(html_update).appendTo("#content_"+value);       
//                                   
                              });
//                               location.reload();
//                               
                           }
                            makeSortable();
                        }
                    });
                }
            });
        };

        $(function() {
            makeSortable();
        });
        $(document).on({
            mouseenter: function() {
                $("> .edit-panel", this).show();
            },
            mouseleave: function() {
                if (!$(this).hasClass("content-editing"))
                {
                    $("> .edit-panel", this).hide();
                }
            }
        }, ".content-list li div");


    </script>
    <!--script-->
    
    <script>
    $(document).ready(function() {
         $(".editable-lecture").on("click",".approve-link-new",function(){
            var approve_id=$(this).data("approveid");
            
            $.ajax({                         
            url: "<?php echo Yii::app()->createUrl("admin/default/appoveNewContent"); ?>",
            type: 'POST',
            data:{approve_id : approve_id},
            dataType: 'html',         
            success:function(data) {                   
                $("#approve-sub-"+approve_id).hide()
//                $("#content-name-"+approve_id).html(data);  
                $("#content-name-"+approve_id).addClass("success-approve");
              }              
            });             
       });
       
       
       $(".editable-lecture").on("click", ".approve-link", function(){
            var approve_id=$(this).data("approveid");
            console.log(approve_id);                        
            $.ajax({                         
            url: "<?php echo Yii::app()->createUrl("admin/default/appoveContent"); ?>",
            type: 'POST',
            data:{approve_id : approve_id},
            dataType: 'html',         
            success:function(data) {                   
                $("#approve-sub-"+approve_id).hide();  
                $("#content-name-"+approve_id).html(data);
                $("#content-name-"+approve_id).addClass("success-approve");
              }              
            });
            
       });
        
    });
</script>
 
 <?php } ?>

 <?php if($page=="show_question"){ ?>
   <script type="text/javascript" src="js/html5placeholder.jquery.js"></script>
   <?php $action_sort=Yii::app()->createUrl("/admin/default/sortQuestion");
   $action_change_public = Yii::app()->createUrl("admin/default/changPublic",array("type"=>"exercise"));
   ?>
    
    <script type="text/javascript">        
      function changPublic(id,state){
    
        $.ajax({                         
            url: "<?php echo $action_change_public; ?>",
            type: 'POST',
            data:{id : id, state : state, type : "exercise"},
            dataType: 'html',         
            success:function(data) { 
                 var src = ($("#question-"+id).attr("src") === "images/button/on.png")
                ? "images/button/off.png"
                : "images/button/on.png";
                $("#question-"+id).attr("src", src);              
              }              
            });
    }           
    function delete_confirm() {
         var msg = confirm('Are you sure?');
         if(msg == false) {
              return false;
        }
    }

    function makeSortable(){
            $('.content-list').sortable({
                    //handle : '.content-handle',
                    update: function() {
                    var contentList = $('.content-list').sortable();
                    var order = $(contentList).sortable('serialize');
                    $.ajax({
                        url: '<?php echo $action_sort; ?>',
                        data: order,
                        dataType: 'html',
                        type: 'POST',
                        success: function(html) {
                           if(html){
                            $('.editable-content-list-container').html(html);
                           }
                            makeSortable();
                        }
                    });
                }
            });
        };

        $(function() {
            makeSortable();
        });
        $(document).on({
            mouseenter: function() {
                $("> .edit-panel", this).show();
            },
            mouseleave: function() {
                if (!$(this).hasClass("content-editing"))
                {
                    $("> .edit-panel", this).hide();
                }
            }
        }, ".content-list li div");

    </script>
<?php } ?>
   
 

    
    

