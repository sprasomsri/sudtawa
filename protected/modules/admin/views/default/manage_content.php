<script type="text/javascript" src="js/jquery.js"></script>
<!--<script type="text/javascript" src="js/jquery.multifile.js"></script>-->
<script type="text/javascript" src="js/jui/js/jquery-ui.min.js"></script>
<!--<script type="text/javascript" src="js/coursethumbnailuploader.js"></script>-->
<!--<script type="text/javascript" src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>-->

<?php $is_super_admin= Yii::app()->user->getState("superadmin");  ?>
<div class="bg-pagestudy">
    <div class="w-container">


      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">
              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
              
           
          </div>
            <div class="w-col w-col-9">
                <div class="admin-content">
                    <h1 class="admin-head">จัดการบทเรียน</h1>
                    <!----- Right content------>
                    
                    <?php if($is_super_admin==1){ ?>
                    <a href="<?php echo Yii::app()->createUrl("admin/default/approveAllContent",array("course"=>$model_course->id)); ?>" class="btn btn-primary btn-sm text-right"> <span class="glyphicon glyphicon-send"></span> Approve All</a>
                    <?php } ?>
                    
                    <ul class='content-list'>


                        <!-- Parent add class ='editable-chapter' for <li> 
                             Children add class = 'editable-lecture' for <li> -->
                        <?php
                        $unit_count = 1;
                        if (!empty($model_parent_content)) {

                            #unit ---> loop
                            foreach ($model_parent_content as $unit) {
                                ?>                               
                        
                                <li id="content_<?php echo $unit->id ?>" class="lecture-list editable-chapter">
                                    <div class="w-row">
                                        <div class="w-col w-col-8"> 
                                            <span class="content-prefix"><?php echo Yii::t("site", "Unit") ?> <?php echo $unit_count; ?>: </span>
                                            <span id="content-name-<?php echo $unit->id; ?>" class="content-name"><?php echo $unit->title ?></span>
                                        </div>
                                        <div class="w-col w-col-4 edit-panel" align="right">


                                            <a href="<?php echo Yii::app()->createUrl("/admin/default/OperateVideoContent/id/$unit->id/course/$model_course->id/") ?>">
                                              <span class="glyphicon glyphicon-pencil"></span>
                                            </a>     
                                            

                                            <a class ="delete_content_sub" href="<?php echo Yii::app()->createUrl("/admin/default/DeleteVideoContent/id/$unit->id") ?>">
                                               <span class="glyphicon glyphicon-remove"></span>
                                            </a>

                                        </div>
                                    </div>
                             
                                    
                                      <?php  if($unit->is_approve==0 || !empty($unit->modify_id)){?>
                                            
                                          
                                    
                                             <div id='approve-sub-<?php echo $unit->id; ?>' class='w-row approve-list-inside gray-text'>
                                                 <div class="w-col w-col-5"> 
                                                     
                                                       
                                                     <?php if($unit->is_approve==0){ ?>
                                                     New content 
                                                     <?php }else{
                                                         echo $change_list = getListChange($unit); 
                                                     } 
                                                     ?>                                                 
                                                 </div>
                                                 
                                                 
                                                 
                                                 <div class="w-col w-col-3"><b>แก้ไขเมื่อ  </b><?php echo SiteHelper::dateFormat($unit->update_date); ?></div> 
                                                 <div class="w-col w-col-4 text-right">
                                                     <?php if($is_super_admin==1){ ?>
                                                     <?php if($unit->is_approve==0){ ?>
                                                     
                                                      <a class="approve-link-new" data-approveid="<?php echo $unit->id; ?>"> Approve </a> 
                                                     <?php }else{ ?>
                                                       <a class="approve-link" data-approveid="<?php echo $unit->id; ?>"> Approve </a>
                                                      
                                                     <?php } ?>                                                      
                                                     <?php } ?>
                                                 </div>                                     
                                             </div>    
                                    
                                    <?php } ?>
                                    
                                    
                                    
                                </li> 

                                <?php
                                #lesson --->loop
                                
                                $lessons= $unit->getChildContent();
                                if (!empty($lessons)) {
                                    
                                    
                                    
                                    $lesson_count = 1;
                                    foreach ($lessons as $lesson) {
                                        $class = "content";
                                        $img_content = "video.png";
                                        if ($lesson->is_exercise == 1) {
                                            $class = "excercise-content";
                                            $img_content = "pratice.png";
                                        }

                                        if ($lesson->is_code == 1) {
                                            $class = "code-lesson";
                                            $img_content = "code.png";                                            
                                            
                                        }
                                        ?>
                                        
                                                                       
                                        <li id="content_<?php echo $lesson->id; ?>" class="lecture-list editable-lecture <?php echo $class; ?>">
                                            <div class="w-row">
                                                <div class="w-col w-col-8"> 
                                                    <img src="images/button/<?php echo $img_content; ?>" class="img-lecture-state"> &nbsp;
                                                    <span id="content-name-<?php echo $lesson->id; ?>" class="content-name"><?php echo $lesson->title; ?></span>
                                                </div>
                                                
                                                                                 
                                                <div class="w-col w-col-4 edit-panel" align="right">

                                                    <?php
                                                    $edit_link = Yii::app()->createUrl("admin/default/operateVideoContent/id/$lesson->id/course/$model_course->id/type/lesson");
                                                    $delete_link = Yii::app()->createUrl("admin/default/deleteVideoContent/id/$lesson->id");
                                                    $edit_title = "Edit Lesson - " . $lesson->title;
                                                    $del_title = "Delete Lesson - " . $lesson->title;


                                                    if ($lesson->is_exercise == 1) {
                                                        $edit_link = Yii::app()->createUrl("admin/default/operateExercise/id/$lesson->id/course/$model_course->id/type/lesson");
                                                        $delete_link = Yii::app()->createUrl("admin/default/deleteExerciseContent/id/$lesson->id");
                                                        $edit_title = "Edit Exercise - " . $lesson->title;
                                                        $del_title = "Delete Exercise - " . $lesson->title;
                                                    }

                                                    if ($lesson->is_code == 1) {
                                                        $edit_link = Yii::app()->createUrl("admin/default/operateVideoContent/id/$lesson->id/course/$model_course->id/type/code");
                                                        $delete_link = Yii::app()->createUrl("admin/default/deleteVideoContent/id/$lesson->id");
                                                    }
                                                    ?>

                                                    <?php
                                                    $qty_visitor = $lesson->getLessonViews($lesson->id);
                                                    if ($qty_visitor != 0) {
                                                        $stat_link = Yii::app()->createUrl("admin/default/viewLessonStat", array("id" => $lesson->id, "qty" => $qty_visitor));
                                                        $link_stat = "<a href='$stat_link' title='stat lesson $lesson->title view'>" . $qty_visitor . " view(s)"; //                                                                                                                                                       
                                                    } else {
                                                        $link_stat = "0 view";
                                                    }

                                                    echo $link_stat;
                                                    ?>

                                                    <?php
                                                    $img_public = "imagesv1/button/on.png";
                                                    if ($lesson->is_public == 0) {
                                                        $img_public = "imagesv1/button/off.png";
                                                    }
                                                    ?>

                                                    <a href="#" title="public / not public">
                                                        <img id="public-<?php echo $lesson->id; ?>" src="<?php echo $img_public; ?>" onclick="changePublic(<?php echo $lesson->id ?>, <?php echo $lesson->is_public; ?>)">
                                                    </a>



                                                    <a href="<?php echo $edit_link; ?>" title="<?php echo $edit_title; ?>">
                                                       <span class="glyphicon glyphicon-pencil"></span>
                                                    </a>

                                                    <a class ="delete_content_sub" href="<?php echo $delete_link; ?>" onclick="return confirmDelete();" title="<?php echo $del_title; ?>">
                                                       <span class="glyphicon glyphicon-remove"></span>
                                                    </a> 



                                                </div>
                                            </div>
                                            
                                             <?php  if($lesson->is_approve==0){?>
                                             <div id='approve-sub-<?php echo $lesson->id; ?>' class='w-row approve-list-inside gray-text'>
                                                 <div class="w-col w-col-5"> New content </div>
                                                 <div class="w-col w-col-3"><b>แก้ไขเมื่อ  </b><?php echo SiteHelper::dateFormat($lesson->update_date); ?></div> 
                                                 <div class="w-col w-col-4 text-right">
                                                     <?php if($is_super_admin==1){ ?>
                                                      <a class="approve-link-new" data-approveid="<?php echo $lesson->id; ?>"> Approve </a> 
                                                     <?php } ?>
                                                 </div>                                     
                                             </div>                                                
                                            <?php } ?>   
                                            
                                            
                                            
                                            
                                            <?php if(!empty($lesson->modify_id)){         
                                                
                                                   $change_list = getListChange($lesson);                                                     
                                                        
                                             ?>
                                             <div id='approve-sub-<?php echo $lesson->id; ?>' class='w-row approve-list-inside gray-text'> 
                                                  <div class="w-col w-col-5"> 
                                                  
                                                  <?php echo $change_list; ?>
                                                  </div>
                                                 
                                                 <div class="w-col w-col-3"><b>แก้ไขเมื่อ  </b>
                                                     <?php   if(!empty($lesson->date_approve)){
                                                         echo SiteHelper::dateFormat($lesson->date_approve);                                            
                                                     }else{
                                                         echo SiteHelper::dateFormat($lesson->update_date); 
                                                     }
                                            
                                            ?>
                                                 </div> 
                                                <div class="w-col w-col-4 text-right">
                                                    <a class="" href="<?php echo Yii::app()->createUrl("admin/default/viewAppoveContent",array("lesson_id"=>$lesson->id)) ?>"> View Change </a>
                                                    <?php if($is_super_admin==1){ ?>
                                                    <a class="approve-link" data-approveid="<?php echo $lesson->id; ?>"> Approve </a>
                                                    <?php } ?>
                                                
                                                </div>
                                             </div>
                                                
                                            <?php } ?>
                                            
                                            
                                        </li> 

                <?php
                $lesson_count++;
            }
        }
        $unit_count++;
    }
}
?>

                    </ul>


                   
                    <br/>
                    <div class="row-fluid row-course">
                        <div class="btn-group">
                            <?php $new_content = Yii::app()->createUrl("/admin/default/OperateVideoContent/id//course/$course_id/type/unit") ?>
                            <input type="button" value="<?php echo Yii::t("site", "Add Unit") ?>" 
                                   class="btn button btn-warning"
                                   onClick="window.location.href='<?php echo $new_content; ?>'">

                        </div>

                        <?php if (!empty($model_parent_content)) { ?>
                            <div class="btn-group">

                                <?php $new_content_lesson = Yii::app()->createUrl("/admin/default/OperateVideoContent/id//course/$course_id/type/lesson") ?>
                                <input type="button" value="<?php echo Yii::t("site", "Add Lesson") ?>" 
                                       class="btn button btn-warning"
                                       onClick="window.location.href='<?php echo $new_content_lesson; ?>'">


                            </div>


                            
<!--                            <div class="btn-group">
                                <?php $new_content_lesson = Yii::app()->createUrl("/admin/default/OperateExercise/id//course/$course_id/type/lesson") ?>
                                <input type="button" value="<?php echo Yii::t("site", "Add Exercise") ?>" 
                                       class="btn button btn-warning"
                                       onClick="window.location.href='<?php echo $new_content_lesson; ?>'">                                        

                            </div>  -->
                        
                        <br/><br/>
<!--                        <div>                            
                             <button type="button" class="btn btn-danger button"  onClick="window.location.href='<?php echo Yii::app()->createUrl("admin/default/clearStatExercise",array("course"=>$model_course->id)); ?>'"><i class="icon-repeat icon-white"></i> Clear Stat's Exercise</button>
                        </div>-->
                       
                     <?php } ?>

                        </div>
                     <!-----END Right content------>                    
                    
                </div>
            </div>
        </div>
      </div>

    </div>
  </div>

<?php $this->renderPartial('_admin_script',array("page"=>"manage_content")); ?> 


<?php
function getListChange($lesson){
     $change_list = "";
        if ($lesson->is_change_content == 1) {
            $change_list.= "change content, ";
        }
        if ($lesson->is_change_title == 1) {
            $change_list.="change title, ";
        }

        if ($lesson->is_change_order == 1) {
            $change_list.= "change order, ";
        }
        if ($lesson->is_change_file == 1) {
            $change_list.= "change VDO, ";
        }

        $change_list = rtrim($change_list, ", ");
        
        return $change_list;
}



?>