<div class="bg-pagestudy">
    <div class="w-container">
 

      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
          </div>
            
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">ข้อมูลคอร์ส</h1> 
                      
              
               <!----- Right content------>
               <?php
               $form = $this->beginWidget('CActiveForm', array(
                   'id' => 'add-course',
                   'htmlOptions' => array('enctype' => 'multipart/form-data',"role"=>"form"),
                   'clientOptions' => array(
                       'validateOnSubmit' => true,
                   ),
               ));
               ?>
               
               
              <?php echo $form->errorSummary($model_course); ?>
                              
               
               <div class="form-group">
                
              <label ><?php echo Yii::t("site","Course Name");?></label>
               <?php echo $form->textField($model_course, 'name',array("class"=>"form-control")); ?>             
              </div>
               
             
               
               <div class="form-group">
                <label><?php echo Yii::t("site","Description");?></label>
              <?php echo $form->textArea($model_course, 'description',array("style"=>"width:100%","rows"=>10,"class"=>"form-control")); ?>        
              </div>
               
              <?php //echo $form->dropDownlist($model_course, 'course_status',array("0"=>"---","1"=>"feature","2"=>"New")); ?>
               
               
<!--              <div class="form-group">
                <label><?php //echo Yii::t("site","tag");?></label>             
              <?php //echo $form->textField($model_course, 'tag',array("class"=>"form-control")); ?>
              </div>-->
               
<!--               <div class="form-group">
                <label><?php //echo Yii::t("site","about");?></label>             
                <?php //echo $form->textField($model_course, 'about',array("class"=>"form-control")); ?>
              </div>-->
               
               <div class="form-group">
                <label><?php echo Yii::t("site","Course Price");?></label>             
                <?php echo $form->textField($model_course, 'price',array("class"=>"form-control")); ?>
              </div>
                
              <div class="form-group">
                <label><?php echo Yii::t("site", "Teacher Name"); ?></label>   
                                
                 <?php echo Chosen::multiSelect("teacherList",$selected_teacher,$list_all_teacher,array("class"=>"form-control","id"=>"teacherList")); ?>
              </div>
               
               
                <div class="w-row">
                    <div class="w-col w-col-4">
                          <div class="form-group">
                            <label><?php echo Yii::t("site","Public");?></label>             
                            <?php echo $form->checkBox($model_course, 'is_public'); ?>
                          </div>              
                    </div>
                    <div class="w-col w-col-4">
                                      
                    </div>
                    <div class="w-col w-col-4"></div>                     
                 </div>   
               
                <div class="w-row">
                    <div class="w-col w-col-12">
                         <div class="form-group">
                           <label>โมเดลการใช้งาน</label> 
                                <?php if(empty($model_course->id)){?>
                           
                           
                                <div class="radio-inline">
                                    <?php if(empty($model_course->course_option)){  $model_course->course_option=1; }?>
                                     <input  value="1" type="radio" name="course_option" <?php if($model_course->course_option==1){echo "checked='checked'"; }?>> ตามวันเปิดปิดคอร์ส <br/>
                                     <input  value="2" type="radio" name="course_option" <?php if($model_course->course_option==2){echo "checked='checked'"; }?>> ตามระยะเวลาที่กำหนด 
                                     <?php echo $form->hiddenField($model_course, 'course_option',array("id"=>"course-option")); ?>
                                </div>
                               <?php  }else{ 
                                    if($model_course->course_option==1){
                                        echo "ตามวันเปิดปิดคอร์ส";
                                    }else{
                                        echo "ตามระยะเวลาที่กำหนด";
                                    }
                               
                                } ?>
                           
                            </div>
                       </div>
                </div>
               
              <script type="text/javascript">
                   $(document).ready(function() {
                         $('input[name=course_option]').on("change",function(event){
                              event.preventDefault();
                              
                              var option = $('input[name=course_option]:checked').val();         
                              if(option == 1){
                                  $("#course_date").show();
                                  $("#course-duration").hide();
                                  $("#course-option").val(1);
                              }else{
                                   $("#course_date").hide();
                                  $("#course-duration").show();
                                   $("#course-option").val(2);
                              }
                         });
                   });
                 
               
               </script>
               
               
               <?php 
                $display_date = "style='display:none'";
                $display_duration = "style='display:none'";
               if($model_course->course_option==1){  
                   $display_date = "style='display:block'";
               }else{
                   $display_duration= "style='display:block'";
               }               
               ?>
                <div class="w-row" id="course_date" <?php echo $display_date;?> >               
                <div class="w-col w-col-3"> 
                    <div class="form-group">
                   
                   <label><?php echo Yii::t("site", "date_start"); ?></label>   
                    <?php
                                            Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                                            $this->widget('CJuiDateTimePicker', array(
                                            'name' => "date_start",
                                            'value'=>  SiteHelper::dateFromDatetime($model_course->date_start),
                                            'mode' => 'date', //use "time","date" or "datetime" (default)
                                            'htmlOptions'=>array("class"=>"form-control",),    
                                            'options' => array(
                                                //'dateFormat'=>'yy-mm-dd',                                                
                                            ), // jquery plugin options
                                            ));
                          ?>
                   
                </div>
                
                </div>
                 <div class="w-col w-col-2"></div>  
                 <div class="w-col w-col-3">   
                                      
                    <label><?php echo Yii::t("site", "date_end"); ?></label>   
                    <?php
                                            Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                                            $this->widget('CJuiDateTimePicker', array(
                                            'name' => "date_end",
                                            'value'=>SiteHelper::dateFromDatetime($model_course->date_end),
                                            'mode' => 'date', //use "time","date" or "datetime" (default)
                                            'htmlOptions'=>array("class"=>"form-control",),    
                                            'options' => array(
                                                //'dateFormat'=>'yy-mm-dd',                                                
                                            ), // jquery plugin options
                                            ));
                          ?>
                 
                 </div>
                 <div class="w-col w-col-2"></div>                 
              </div>
              
               
             
              <div class="form-group" id="course-duration" <?php echo $display_duration;?>>
                <label><?php echo Yii::t("site","ระยะเวลาที่ดูได้");?> (วัน)</label>         
                <?php echo $form->textField($model_course, 'duration',array("class"=>"form-control","size"=>"50")); ?>
              </div>
             
               
              <div class="form-group">
                <label><?php echo Yii::t("site", "Youtube Demo"); ?></label>   
                <!--<a class="demo-select fancybox.ajax" id="select-demo-vdo" href="<?php echo Yii::app()->createUrl("admin/default/listVideo", array("user_id" => $user_id)); ?>">select demo video</a>-->   
                 <br/>
                 <?php echo $form->textField($model_course, "youtube_demo",array("class"=>"form-control")) ?>  
                 <?php
                 if (!empty($model_course->youtube_demo)) {
                     echo "<br/>" . $demon_youtube = VdoHelper::jwyoutube($model_course->youtube_demo,"50%");
                 }
                 ?>             
              </div>
               
   
              <div class="w-row">
                    <div class="w-col w-col-5">
                          <div class="form-group">
                            <label><?php echo Yii::t("site","Course Photo");?></label>   
                            <?php echo CHtml::activeFileField($model_course, 'course_img',array("class"=>"input-block-level")) ?>
                            <?php
                            if (!empty($model_course->course_img)) {
                                echo "<a class='tumb' href='course/original/$model_course->course_img'>";
                                echo "<img src='course/$model_course->course_img' height='150px' title='Course cover'>";
                                echo "</a>";
                            }
                            ?>             
                          </div>              
                    </div>
                    <div class="w-col w-col-5">
                          <div class="form-group">
                                 
                          </div>               
                    </div>
                    <div class="w-col w-col-2"></div>
                     
                 </div> 
               
               <button type="submit" class="btn btn-warning"><?php echo Yii::t("site", "Next"); ?></button>
               <?php $back_url = Yii::app()->createUrl("admin/default/ownCourse") ?>
               
               <input type="button" value="<?php echo Yii::t("site", "Back"); ?>" class="span3 btn btn-gray-submit"
                      onClick="window.location.href='<?php echo $back_url; ?>'">


                     
              <?php $this->endWidget(); ?>
                     
               <!-----END Right content------>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php $this->renderPartial('_admin_script',array("page"=>"form_course")); ?>  

    


