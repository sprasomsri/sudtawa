<?php 
if(!empty($model_user_files)){
    echo "<ul class='thumbnails'>";
    foreach ($model_user_files as $file){
        echo "<li class='span3'>";
        echo "<a id='vdo-$file->id' href='#' class='thumbnail vdo-cap' data-name='$file->original_name' data-id='$file->id'>";
         $video_image=  str_replace(".mp4", ".jpg", $file->name);
        echo "<img src='files/$file->user_id/video/$video_image' style='height: 180px;' >";
        echo $file->original_name." | ".SiteHelper::dateFormat($file->create_at);
        echo "</a>";
        echo "</li>";        
    }
    echo "</ul>";
}
?>
