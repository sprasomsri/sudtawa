  <div class="admin-box-auto">
    <div class="w-container">
      <div class="w-row admin-user-progress-show">
        <div class="w-col w-col-4">
          <div class="admin-user-info-progress">
              
              
        <?php 
                $student_pic_path = Yii::app()->basePath . "/../userphoto/".$user->profile->photo;

              
                
                if (!empty($user->profile->photo) && file_exists($user->profile->photo)) {
                    $student_pic = "userphoto/".$user->profile->photo;
                } else {
                    $student_pic = "images/user_default.jpg";
                }
                
                if(!empty($user->facebook_id)){            
                   $student_pic= "https://graph.facebook.com/$user->facebook_id/picture";
                   echo "<br/> Facebook Account";
               }

                
                
            ?>
              
            <img class="admin-img-progress" src="<?php echo $student_pic; ?>">
            
           
            
            <div class="admin-comment-name"><strong><?php echo $user->username; ?></strong>
            </div>
            <div class="admin-text-margin"> <?php echo $user->profile->firstname." ".$user->profile->lastname; ?> </div>
            <div class="admin-coment-date" title="register date"><?php echo SiteHelper::dateFormat($user_course->create_date); ?></div>
          </div>
        </div>
        <div class="w-col w-col-8 admin-progress-detail">
          <div class="admin-user-info-progress">
            <h4 class="admin-progress-heading"><span class="admin-comment-prefix">ความก้าวหน้าในคอร์ส:</span>&nbsp;<?php echo $model_course->name; ?></h4>
          </div>
          <p class="admin-course-info">
            <?php echo $model_course->description; ?>
          
          
          </p>
          
          
         
            <?php $this->widget('CourseProgressBox', array("course_id"=>$user_course->course_id,"user_id"=>$user_course->user_id,"show_photo"=>"0","mode"=>"user_info")); ?> 
          
          
          <div class="admin-content-active">
            <h5 class="admin-h5-black">บทเรียนที่เรียนจบแล้ว</h5>
          </div>
          <p>
              <?php 
                if(!empty($end_contents)){
                    $end_content="";
                    foreach ($end_contents as $end){
                        $end_content .= $end->title.", ";
                    }
                    $end_content= rtrim($end_content," ,");
                    echo $end_content;                    
                    
                }else{
                    echo "-";
                }
              
              ?>
              
          
          
          </p>
          <h5 class="admin-h5-black">บทเรียนที่กำลังเรียน</h5>
          <p>
             <?php 
                if(!empty($stuying_contents)){
                    $studying_content="";
                    foreach ($stuying_contents as $studying){
                        $studying_content .= $studying->title.", ";
                    }
                    $studying_content = rtrim($studying_content," ,");
                    echo $studying_content;
                    
                }else{
                    echo "-";
                }
              
              ?>
          
          </p>
        </div>
      </div>
    </div>
  </div>


