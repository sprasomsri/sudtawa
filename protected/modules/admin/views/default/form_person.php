 <!--maincontent-->      
<div id="maincontent">
  
    <div class="container">
        <div class="row-fluid">


            <?php $this->renderPartial("_admin_menu", array("course_name" => $model_course->name, "course_id" => $model_course->id)); ?>

            <div class="span9">

                <div class="container-fluid">
                    <div class="admin-bg-body row-course">
                        <div class="container-fluid">

                            <!--search-->
                          
                             <?php 
                                echo "<h2>".Yii::t("site", "Course").":" . $model_course->name."</h2>";
                             ?>
                            <hr/>
                            <?php
                                //use Ajax for submit and save data

                                $form = $this->beginWidget('CActiveForm', array(
                                        'id' => 'student-form',
                                         'htmlOptions'=>array(
                                         'enctype'=>'multipart/form-data',
                                         ),
                                        'clientOptions' => array(
                                        'validateOnSubmit' => false,
                                    ),
                                ));
                            ?>
                           <div class='search-box'> 
                            <?php
                            //echo Chosen::dropDownList("student_list", $select, $data_for_search,array("class"=>"search-std"));
                                                        
                            $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
                                'name'=>'studentname',
                                'source'=>$data_for_search,
                             
                                'options'=>array(
                                    'minLength'=>'1',
                                ),
                                'htmlOptions'=>array(
                                    'style'=>'height:20px; width:700px;',
                                ),
                            ));                                             
                            ?>
                               <p class="remark"> <u><b>remark:</b></u> type email or name of user </p>
                               <br/>
                                     <?php 
                                        if($type==2){
                                            $button_name=Yii::t("site","Add Teacher");
                                        }else{
                                            $button_name=Yii::t("site","Add Student");
                                        }
                                     
                                     ?>
                                     <button type="submit" class="btn btn-green-submit-box"><?php echo $button_name;?></button>
                              
                            </div>
                                                        
                            <?php $this->endWidget(); ?>
                            
                            <hr/>
                 
                            <?php $this->renderPartial("person_grideview",array("model_person"=>$model_person,
                                                                                  "dataProvider_people"=>$dataProvider_people));?>

                        </div><!--container-fluid-->
                    </div><!--admin-bg-body row-course-->
                </div><!--/row-fluid-->
            </div><!--/span9-->

        </div><!--row-fluid-->
    </div><!--container-->

</div><!--/maincontent-->        


</div> <!-- body-allcourses -->