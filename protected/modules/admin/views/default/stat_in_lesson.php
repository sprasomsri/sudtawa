<div class="bg-pagestudy">
    <div class="w-container">


      <div class="admin-page">
        <div class="w-row">
          <div class="w-col w-col-3">
              
           <?php $this->renderPartial("_admin_menu",array("course_name"=>$model_course->name,"course_id"=>$model_course->id,"img"=>$model_course->course_img)); ?>  
              
           
          </div>
          <div class="w-col w-col-9">
            <div class="admin-content">
              <h1 class="admin-head">สถิติการเข้าบทเรียน</h1>
              
               <div class="admin-top-menu">
                   <a class="admin-top-menu-link admin-active-topmenu" href="<?php echo Yii::app()->createUrl("admin/default/statInLesson",array("courseId"=>$model_course->id)); ?>">สถิติการเข้าชมบทเรียน</a>
                   <a class="admin-top-menu-link " href="<?php echo Yii::app()->createUrl("admin/default/numberStudentAccess",array("courseId"=>$model_course->id)); ?>">สถิติการเข้าสมัครคอร์ส</a>
                   <a class="admin-top-menu-link" href="<?php echo Yii::app()->createUrl("admin/default/numberCoinIncourse",array("courseId"=>$model_course->id)); ?>">สถิติการใช้เงิน</a>
                   <a class="admin-top-menu-link" href="<?php echo Yii::app()->createUrl("admin/default/statBufferRate",array("courseId"=>$model_course->id)); ?>"> สถิติการ Buffer</a>
              </div>      
               <!----- Right content------>
               <div class="text-center admin-stat-content">
                    <?php
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'form-coupon',                                  
                                    'clientOptions' => array(
                                        'validateOnSubmit' => true,
                                    ),
                                 ));
                                ?>
                   
                   
                   <div class="w-row">
                       <div class="w-col w-col-2"></div>
                       <div class="w-col w-col-3">
                            <div class="form-group">
                            <label for="exampleInputEmail1">วันเริ่มต้น</label>
                           
                           <?php
                                            Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                                            $this->widget('CJuiDateTimePicker', array(
                                            'name' => "date_start",
                                            'value'=>$start,
                                            'mode' => 'date', //use "time","date" or "datetime" (default)
                                            'htmlOptions'=>array("class"=>"form-control",),    
                                            'options' => array(
                                                //'dateFormat'=>'yy-mm-dd',                                                
                                            ), // jquery plugin options
                                            ));
                          ?>
                             </div>
                       </div>
                        <div class="w-col w-col-1"></div>
                        <div class="w-col w-col-3">
                              <div class="form-group">
                                  <label for="exampleInputEmail1">วันสิ้นสุด</label>
                            
                            <?php
                                            Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                                            $this->widget('CJuiDateTimePicker', array(
                                             'value'=>$to,
                                            'name' => "date_end",
                                            'mode' => 'date', //use "time","date" or "datetime" (default)
                                             'htmlOptions'=>array("class"=>"form-control"),    
                                            'options' => array(
                                              ), // jquery plugin options
                                            ));
                                            ?>
                            
                             </div>
                        </div>
<!--                         <div class="w-col w-col-1"></div>-->
                         <div class="w-col w-col-2">
                              <input type="hidden" name="courseId" value="<?php echo $model_course->id;?>"><br/>
                              <button type="submit" name="submit" value="submit" class="button btn-admin-add" style="margin: 0 auto;">View</button>
                             
                         </div>
                         <div class="w-col w-col-1"></div>
                       
                   </div>
                   
              
                       <?php $this->endWidget(); ?>
                   
                       <hr/>
                       
                           <?php if($has_stat==TRUE){ ?>
                                <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                                <script type="text/javascript">
                                    google.load('visualization', '1', {packages: ['corechart']});
                                </script>
                                <script type="text/javascript">
                                    function drawVisualization() {
                                     var data = google.visualization.arrayToDataTable([
                                        ['View', <?php echo $lesson_name;?>],
                                        ['Lesson',  <?php echo $qty_lesson_view; ?>],

                                      ]);

                                      // Create and draw the visualization.
                                      new google.visualization.ColumnChart(document.getElementById('visualization')).
                                          draw(data,
                                               {title:"<?php echo $message; ?>",
                                                width:"100%", height:500,
                                                vAxis: {title: "Views"},
                                                hAxis: {title: "Lesson"}}
                                          );
                                    }
                                    google.setOnLoadCallback(drawVisualization);
                                </script>

                                <div class="row-fluid">
                                    <div id="visualization" style="width: 100%; height: 500px;" class="span12"></div>
                                </div>
                                <?php }else{ 
                                    echo $message;
                                }?> 
                   
                   
                 </div>
                                      
               </div>               
               <!-----END Right content------>
               
            </div>
          </div>
        </div>
      </div>



    </div>
  </div>

