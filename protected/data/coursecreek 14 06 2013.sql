-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 14, 2013 at 11:32 AM
-- Server version: 5.5.31-0ubuntu0.13.04.1
-- PHP Version: 5.4.9-4ubuntu2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `coursecreek`
--

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `course_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '-1',
  `detail` text COLLATE utf8_unicode_ci,
  `show_order` tinyint(3) NOT NULL DEFAULT '1',
  `file_id` int(11) DEFAULT NULL,
  `is_exercise` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `title`, `course_id`, `parent_id`, `detail`, `show_order`, `file_id`, `is_exercise`) VALUES
(1, 'what is yii?', 2, -1, '<p>yii description</p>\r\n', 0, NULL, 0),
(8, 'why yii', 2, 1, '', 1, 12, 0),
(9, 'exercise 1', 2, 1, '', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `content_info`
--
CREATE TABLE IF NOT EXISTS `content_info` (
`course_id` int(11)
,`course_name` varchar(500)
,`number_of_content` bigint(21)
);
-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE IF NOT EXISTS `coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `discount` tinyint(3) NOT NULL,
  `expire_date` date NOT NULL,
  `qty_use` tinyint(5) NOT NULL DEFAULT '1',
  `detail` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `course_id` int(11) NOT NULL DEFAULT '-1',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_course_id` (`course_id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_log`
--

CREATE TABLE IF NOT EXISTS `coupon_log` (
  `coupon_id` int(11) NOT NULL,
  `use_course_id` int(11) NOT NULL,
  `used_time` datetime NOT NULL,
  `use_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `name_th` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(5) NOT NULL,
  `course_img` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_status` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `duration` tinyint(5) DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `name`, `name_th`, `tag`, `description`, `about`, `price`, `course_img`, `course_status`, `user_id`, `commission`, `duration`, `is_public`) VALUES
(1, '1', '1', '1', '1', '1', 1, '1', 1, 1, 1, 1, 1),
(2, 'yii framework', 'yii framework', 'program', '<p>test coursecreek application.</p>\r\n', 'programe', 200, 'm862kxhr5fjuyl9wo0ie17vctn3dabqpsz4g_darkwood_floor_1920x1200.jpg', 0, 1, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exercise`
--

CREATE TABLE IF NOT EXISTS `exercise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `qty_show` int(2) NOT NULL DEFAULT '10',
  `is_random` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_content_id` (`content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exercise`
--

INSERT INTO `exercise` (`id`, `content_id`, `qty_show`, `is_random`) VALUES
(1, 9, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exercise_answer_choice`
--

CREATE TABLE IF NOT EXISTS `exercise_answer_choice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `is_true` tinyint(1) NOT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_question_id` (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exercise_answer_choice`
--

INSERT INTO `exercise_answer_choice` (`id`, `answer`, `is_true`, `question_id`) VALUES
(1, 'true', 1, 1),
(2, 'false', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exercise_choice_score`
--

CREATE TABLE IF NOT EXISTS `exercise_choice_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `do_time` datetime NOT NULL,
  `is_true` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exercise_choice_score`
--

INSERT INTO `exercise_choice_score` (`id`, `answer_id`, `question_id`, `user_id`, `do_time`, `is_true`) VALUES
(1, 1, 1, 2, '2013-06-14 10:11:39', 1),
(2, 1, 1, 2, '2013-06-14 10:12:01', 1),
(3, 2, 1, 2, '2013-06-14 10:47:59', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exercise_question`
--

CREATE TABLE IF NOT EXISTS `exercise_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `show_order` tinyint(2) NOT NULL DEFAULT '1',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `exercise_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_exercise_id` (`exercise_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exercise_question`
--

INSERT INTO `exercise_question` (`id`, `question`, `show_order`, `type`, `exercise_id`) VALUES
(1, '<p>Choice Question 1<br>\r\n</p>\r\n', 1, 1, 1),
(2, '<p>TextQuestion (TypeA)inblank __<br>\r\n</p>\r\n', 2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exercise_stat`
--

CREATE TABLE IF NOT EXISTS `exercise_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exercise_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `do_time` datetime NOT NULL,
  `qty_question` int(3) NOT NULL,
  `qty_correct` int(3) NOT NULL,
  `question_list` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exercise_stat`
--

INSERT INTO `exercise_stat` (`id`, `exercise_id`, `user_id`, `do_time`, `qty_question`, `qty_correct`, `question_list`) VALUES
(1, 1, 2, '2013-06-14 10:11:39', 2, 2, '1,2'),
(2, 1, 2, '2013-06-14 10:12:01', 2, 1, '1,2'),
(3, 1, 2, '2013-06-14 10:47:59', 2, 0, '1,2');

-- --------------------------------------------------------

--
-- Table structure for table `exercise_text_answer`
--

CREATE TABLE IF NOT EXISTS `exercise_text_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `question_id` int(11) NOT NULL,
  `sequence` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_question_id` (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exercise_text_answer`
--

INSERT INTO `exercise_text_answer` (`id`, `answer`, `question_id`, `sequence`) VALUES
(1, 'A', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exercise_text_score`
--

CREATE TABLE IF NOT EXISTS `exercise_text_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `answer` varchar(3000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `do_time` datetime NOT NULL,
  `sequence` tinyint(3) NOT NULL DEFAULT '1',
  `is_true` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exercise_text_score`
--

INSERT INTO `exercise_text_score` (`id`, `question_id`, `answer`, `user_id`, `do_time`, `sequence`, `is_true`) VALUES
(1, 2, 'A', 2, '2013-06-14 10:11:39', 1, 1),
(2, 2, 'B', 2, '2013-06-14 10:12:01', 1, 0),
(3, 2, 'B', 2, '2013-06-14 10:47:59', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `original_name` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`id`, `name`, `original_name`, `user_id`, `type`, `create_at`) VALUES
(10, 'coursecreekhe2es0a5rpnoeemrq5uc0insbuneo6wxzkjldayk6e3rrcgtufgv43acodeschool_303.mp4', 'codeschool_303.mp4', 1, 'video', '2013-06-13 18:16:51'),
(12, 'bgxd6o5e3rscnuet6vq0irsn2ufcewkehgzra4l5ryepuaakno03jme_2.mp4', '2.mp4', 1, 'video', '2013-06-14 09:48:43');

-- --------------------------------------------------------

--
-- Table structure for table `file_content`
--

CREATE TABLE IF NOT EXISTS `file_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_file_id` (`file_id`),
  KEY `idx_content_id` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `relate_course`
--

CREATE TABLE IF NOT EXISTS `relate_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `before_course` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `after_course` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `stat_user_study`
--
CREATE TABLE IF NOT EXISTS `stat_user_study` (
`content_has_studied` bigint(21)
,`user_id` int(11)
,`course_id` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `tbl_migration`
--

CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_migration`
--

INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1371108007),
('m130603_075539_create_project_table', 1371108015);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profiles`
--

CREATE TABLE IF NOT EXISTS `tbl_profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `detail` varchar(3000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_profiles`
--

INSERT INTO `tbl_profiles` (`user_id`, `lastname`, `firstname`, `detail`, `photo`) VALUES
(1, '', '', NULL, NULL),
(2, '', '', '', 'bziog5fce947sl8vup3wtek816roq8rhyern8ksc0ajuemd2x_black_wood-wallpaper-1920x1080.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profiles_fields`
--

CREATE TABLE IF NOT EXISTS `tbl_profiles_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `field_size` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `field_size_min` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL,
  `match` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `range` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `error_message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `other_validator` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `default` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `widget` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `widgetparams` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(3) NOT NULL,
  `visible` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `activkey` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lastvisit` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `superuser` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `password`, `email`, `activkey`, `create_at`, `lastvisit`, `superuser`, `status`) VALUES
(1, 'gunsarun@gmail.com', 'd54d1702ad0f8326224b817c796763c9', 'gunsarun@gmail.com', '6708d7e610b1ae6ca41e32022b6d41e8', '2013-06-13 07:28:00', '2013-06-14 03:48:08', 0, 1),
(2, 'gun@gmail.com', 'd54d1702ad0f8326224b817c796763c9', 'gun@gmail.com', '286931beb44426f68f3ebbde3b9daa16', '2013-06-14 03:04:40', '2013-06-14 04:29:50', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_content_log`
--

CREATE TABLE IF NOT EXISTS `user_content_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `study_time` datetime NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_content_id` (`content_id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_content_log`
--

INSERT INTO `user_content_log` (`id`, `content_id`, `user_id`, `study_time`, `course_id`) VALUES
(3, 8, 2, '2013-06-14 11:29:34', 2),
(4, 9, 2, '2013-06-14 11:22:47', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_course`
--

CREATE TABLE IF NOT EXISTS `user_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_course`
--

INSERT INTO `user_course` (`id`, `user_id`, `course_id`, `role`, `create_date`) VALUES
(1, 1, 2, 1, NULL),
(2, 2, 2, 3, '2013-06-14 10:06:22');

-- --------------------------------------------------------

--
-- Table structure for table `user_file`
--

CREATE TABLE IF NOT EXISTS `user_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `original_name` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_permission`
--

CREATE TABLE IF NOT EXISTS `user_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `can_create_course` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_permission`
--

INSERT INTO `user_permission` (`id`, `user_id`, `can_create_course`) VALUES
(1, 1, 1),
(2, 2, 0);

-- --------------------------------------------------------

--
-- Structure for view `content_info`
--
DROP TABLE IF EXISTS `content_info`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `content_info` AS select max(`content`.`course_id`) AS `course_id`,max(`course`.`name`) AS `course_name`,count(`content`.`id`) AS `number_of_content` from (`content` join `course` on((`content`.`course_id` = `course`.`id`))) where (`content`.`parent_id` <> -(1)) group by `content`.`course_id`;

-- --------------------------------------------------------

--
-- Structure for view `stat_user_study`
--
DROP TABLE IF EXISTS `stat_user_study`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stat_user_study` AS select count(`user_content_log`.`content_id`) AS `content_has_studied`,max(`user_content_log`.`user_id`) AS `user_id`,max(`content`.`course_id`) AS `course_id` from (`user_content_log` join `content` on((`user_content_log`.`content_id` = `content`.`id`))) group by `user_content_log`.`course_id`;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `content_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `coupon`
--
ALTER TABLE `coupon`
  ADD CONSTRAINT `fk_coupon_course_course_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_coupon_tbl_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `course`
--
ALTER TABLE `course`
  ADD CONSTRAINT `fk_course_tbl_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `exercise`
--
ALTER TABLE `exercise`
  ADD CONSTRAINT `fk_exercise_content_content_id` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `exercise_answer_choice`
--
ALTER TABLE `exercise_answer_choice`
  ADD CONSTRAINT `fk_exercise_answer_choice_exercise_question_question_id` FOREIGN KEY (`question_id`) REFERENCES `exercise_question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `exercise_question`
--
ALTER TABLE `exercise_question`
  ADD CONSTRAINT `fk_exercise_question_exercise_exercise_id` FOREIGN KEY (`exercise_id`) REFERENCES `exercise` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `exercise_text_answer`
--
ALTER TABLE `exercise_text_answer`
  ADD CONSTRAINT `fk_exercise_text_answer_exercise_question_question_id` FOREIGN KEY (`question_id`) REFERENCES `exercise_question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `fk_file_tbl_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `file_content`
--
ALTER TABLE `file_content`
  ADD CONSTRAINT `fk_file_content_content_content_id` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_file_content_file_file_id` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_profiles`
--
ALTER TABLE `tbl_profiles`
  ADD CONSTRAINT `fk_tbl_profiles_tbl_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_content_log`
--
ALTER TABLE `user_content_log`
  ADD CONSTRAINT `fk_user_content_log_tbl_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_content_log_ibfk_1` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_course`
--
ALTER TABLE `user_course`
  ADD CONSTRAINT `fk_user_course_course_course_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_course_tbl_profiles_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_profiles` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_permission`
--
ALTER TABLE `user_permission`
  ADD CONSTRAINT `fk_user_permission_tbl_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
