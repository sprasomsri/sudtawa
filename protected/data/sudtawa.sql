-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `atm_evidence`;
CREATE TABLE `atm_evidence` (
  `id` int(11) NOT NULL,
  `ref_no` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `tranfer_date` datetime NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `is_approve` tinyint(1) NOT NULL,
  `evidence_file` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `from_account` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `from_bank` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `account_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `qty_money` int(10) NOT NULL,
  `active_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `codeEditor`;
CREATE TABLE `codeEditor` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code` text COLLATE utf8_unicode_ci,
  `admin_id` int(11) NOT NULL,
  `admin_update_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `codeEditor_in_content`;
CREATE TABLE `codeEditor_in_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` text COLLATE utf8_unicode_ci,
  `position` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'below',
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `coin_setting`;
CREATE TABLE `coin_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coin_amount` int(11) NOT NULL,
  `money_amount` int(11) NOT NULL,
  `is_promotion` int(1) NOT NULL DEFAULT '1',
  `code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `active_status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `content`;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `course_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '-1',
  `detail` text COLLATE utf8_unicode_ci,
  `youtube_link` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_order` tinyint(3) NOT NULL DEFAULT '1',
  `file_id` int(11) DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `is_exercise` tinyint(1) NOT NULL,
  `is_code` tinyint(1) NOT NULL,
  `is_free` tinyint(1) NOT NULL,
  `is_approve` tinyint(1) NOT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_course_id` (`course_id`),
  CONSTRAINT `fk_content_course_course_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `content` (`id`, `title`, `course_id`, `parent_id`, `detail`, `youtube_link`, `show_order`, `file_id`, `is_public`, `is_exercise`, `is_code`, `is_free`, `is_approve`, `update_date`) VALUES
(1,	'Unit1',	1,	-1,	'<p>Unit 1 description</p>\r\n',	NULL,	1,	NULL,	1,	0,	0,	0,	1,	'2015-07-29 09:51:39'),
(2,	'Lesson 1',	1,	1,	'',	'',	2,	1,	1,	0,	0,	0,	1,	'2015-07-29 09:51:39'),
(3,	'Lesson 2',	1,	1,	'',	'',	3,	2,	1,	0,	0,	0,	1,	'2015-07-29 09:51:39'),
(5,	'Lesson 3',	1,	4,	'',	'',	5,	3,	1,	0,	0,	0,	1,	'2015-07-29 09:51:39'),
(6,	'Lesson 4',	1,	4,	'',	'',	6,	4,	1,	0,	0,	0,	1,	'2015-07-29 09:51:39');

DROP VIEW IF EXISTS `content_info`;
CREATE TABLE `content_info` (`course_id` int(11), `course_name` varchar(500), `number_of_content` bigint(21));


DROP TABLE IF EXISTS `content_modify`;
CREATE TABLE `content_modify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `original_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '-1',
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci,
  `youtube_link` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_order` tinyint(3) NOT NULL DEFAULT '1',
  `file_id` int(11) DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `is_exercise` tinyint(1) NOT NULL,
  `is_code` tinyint(1) NOT NULL,
  `is_free` tinyint(1) NOT NULL,
  `is_change_content` tinyint(1) NOT NULL,
  `is_change_title` tinyint(1) NOT NULL,
  `is_change_order` tinyint(1) NOT NULL,
  `is_change_file` tinyint(1) NOT NULL,
  `is_approve` tinyint(4) NOT NULL,
  `date_approve` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP VIEW IF EXISTS `content_sort_list`;
CREATE TABLE `content_sort_list` (`show_order` int(4), `parent_id` bigint(11), `title` varchar(500), `id` int(11), `modify_id` int(11), `is_change_content` tinyint(1), `is_change_title` tinyint(1), `is_change_order` tinyint(1), `is_change_file` tinyint(1), `date_approve` datetime, `update_date` datetime, `course_id` int(11), `is_exercise` tinyint(1), `is_code` tinyint(1), `is_public` tinyint(1), `is_approve` tinyint(1));


DROP TABLE IF EXISTS `content_suggestion`;
CREATE TABLE `content_suggestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `discount` tinyint(3) NOT NULL,
  `expire_date` date NOT NULL,
  `qty_use` tinyint(5) NOT NULL DEFAULT '1',
  `detail` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `course_id` int(11) NOT NULL DEFAULT '-1',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_course_id` (`course_id`),
  KEY `idx_user_id` (`user_id`),
  CONSTRAINT `fk_coupon_tbl_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_coupon_course_course_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `coupon_log`;
CREATE TABLE `coupon_log` (
  `coupon_id` int(11) NOT NULL,
  `use_course_id` int(11) NOT NULL,
  `used_time` datetime NOT NULL,
  `use_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `price` int(5) NOT NULL,
  `course_img` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_status` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `duration` tinyint(5) DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `is_affiliate` tinyint(1) NOT NULL DEFAULT '1',
  `demo_video` int(11) DEFAULT NULL,
  `theme_photo` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_demo` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `course_option` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  CONSTRAINT `fk_course_tbl_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `course` (`id`, `name`, `description`, `price`, `course_img`, `course_status`, `user_id`, `duration`, `is_public`, `is_affiliate`, `demo_video`, `theme_photo`, `youtube_demo`, `date_start`, `date_end`, `course_option`) VALUES
(1,	'คอร์สออนไลน์ทดสอบ',	'',	0,	't163vixomrkbwa2h5julp8d7s4z0qcengfy9_c.jpg',	0,	1,	NULL,	1,	1,	NULL,	NULL,	'',	NULL,	NULL,	1);

DROP TABLE IF EXISTS `course_division_rate`;
CREATE TABLE `course_division_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `teacher_share_percent` int(3) NOT NULL,
  `company_share_percent` int(3) NOT NULL,
  `affiliate_share_percent` int(3) NOT NULL,
  `create_date` datetime NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `remark` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `course_division_rate` (`id`, `course_id`, `teacher_share_percent`, `company_share_percent`, `affiliate_share_percent`, `create_date`, `is_default`, `remark`) VALUES
(1,	0,	60,	40,	0,	'2014-09-29 00:00:00',	1,	'DEFAULT MARKET SHARE');

DROP TABLE IF EXISTS `course_money_division`;
CREATE TABLE `course_money_division` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_share_percent` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `teacher_share_price` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `company_share_percent` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `company_share_price` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `affiliate_share_percent` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `affiliate_share_price` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `affiliate_code` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `course_payment`;
CREATE TABLE `course_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime DEFAULT NULL,
  `invoice_number` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `approveCode_first` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `approveCode_next` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_coin` int(11) DEFAULT NULL,
  `result` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `result_next` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apCode` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amt` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fee` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method_name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirm_cs` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remark` text COLLATE utf8_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `course_rating`;
CREATE TABLE `course_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `rating` float NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `exercise`;
CREATE TABLE `exercise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `qty_show` int(2) NOT NULL DEFAULT '10',
  `is_random` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_content_id` (`content_id`),
  CONSTRAINT `fk_exercise_content_content_id` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `exercise_answer_choice`;
CREATE TABLE `exercise_answer_choice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `hint` varchar(700) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_true` tinyint(1) NOT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_question_id` (`question_id`),
  CONSTRAINT `fk_exercise_answer_choice_exercise_question_question_id` FOREIGN KEY (`question_id`) REFERENCES `exercise_question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `exercise_choice_score`;
CREATE TABLE `exercise_choice_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `do_time` datetime NOT NULL,
  `is_true` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `exercise_code_answer`;
CREATE TABLE `exercise_code_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `code` text COLLATE utf8_unicode_ci,
  `answer` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `exercise_code_score`;
CREATE TABLE `exercise_code_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `code` text COLLATE utf8_unicode_ci,
  `answer` text COLLATE utf8_unicode_ci,
  `user_id` int(11) NOT NULL,
  `do_time` datetime NOT NULL,
  `is_true` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `exercise_question`;
CREATE TABLE `exercise_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `initial_code` text COLLATE utf8_unicode_ci,
  `show_order` tinyint(2) NOT NULL DEFAULT '1',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `code_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `exercise_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_exercise_id` (`exercise_id`),
  CONSTRAINT `fk_exercise_question_exercise_exercise_id` FOREIGN KEY (`exercise_id`) REFERENCES `exercise` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `exercise_stat`;
CREATE TABLE `exercise_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exercise_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `do_time` datetime NOT NULL,
  `qty_question` int(3) NOT NULL,
  `qty_correct` int(3) NOT NULL,
  `question_list` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `exercise_text_answer`;
CREATE TABLE `exercise_text_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `question_id` int(11) NOT NULL,
  `hint` varchar(700) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequence` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_question_id` (`question_id`),
  CONSTRAINT `fk_exercise_text_answer_exercise_question_question_id` FOREIGN KEY (`question_id`) REFERENCES `exercise_question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `exercise_text_score`;
CREATE TABLE `exercise_text_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `answer` varchar(3000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `do_time` datetime NOT NULL,
  `sequence` tinyint(3) NOT NULL DEFAULT '1',
  `is_true` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `external_provider`;
CREATE TABLE `external_provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `provider` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'facebook',
  `user_provide_name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `original_name` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `duration` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `file` (`id`, `name`, `original_name`, `user_id`, `type`, `duration`, `create_at`) VALUES
(1,	'coursecreekeqmdc1-1.mp4',	'1-1.mp4',	1,	'video',	'',	'2015-07-29 09:49:55'),
(2,	'coursecreekqk9ep1-2.mp4',	'1-2.mp4',	1,	'video',	'',	'2015-07-29 09:50:18'),
(3,	'coursecreeka2owg2-1.mp4',	'2-1.mp4',	1,	'video',	'',	'2015-07-29 09:50:59'),
(4,	'coursecreek7ces42-2.mp4',	'2-2.mp4',	1,	'video',	'',	'2015-07-29 09:51:26');

DROP TABLE IF EXISTS `file_content`;
CREATE TABLE `file_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `log_student_step`;
CREATE TABLE `log_student_step` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_change_password` tinyint(1) NOT NULL DEFAULT '0',
  `is_edit_profile` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `poll_subject`;
CREATE TABLE `poll_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `lasted_date` datetime NOT NULL,
  `price` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `reference_site`;
CREATE TABLE `reference_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `website` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `times` int(100) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL,
  `lasted_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `reference_site` (`id`, `website`, `times`, `create_date`, `lasted_date`) VALUES
(1,	'http://sudtawa.local/index.php?r=user/registration',	1,	'2015-07-29 09:10:28',	'2015-07-29 09:10:28'),
(2,	'http://sudtawa.local/index.php?r=admin/default/ownCourse',	1,	'2015-07-29 09:27:59',	'2015-07-29 09:27:59'),
(3,	'http://sudtawa.local/index.php?r=site/index',	3,	'2015-07-29 09:28:05',	'2015-07-29 09:52:41'),
(4,	'http://sudtawa.local/index.php?r=admin/default/operateCourse',	4,	'2015-07-29 09:29:14',	'2015-07-29 09:39:27'),
(5,	'http://sudtawa.local/cssv1/combined.css',	17,	'2015-07-29 09:41:11',	'2015-07-29 09:59:01'),
(6,	'http://sudtawa.local/index.php?r=admin/default/OperateVideoContent/id//course/1/type/lesson',	4,	'2015-07-29 09:49:21',	'2015-07-29 09:51:10'),
(7,	'http://sudtawa.local/',	1,	'2015-07-29 09:52:37',	'2015-07-29 09:52:37'),
(8,	'http://sudtawa.local/index.php?r=user/profile/edit',	1,	'2015-07-29 09:53:34',	'2015-07-29 09:53:34'),
(9,	'http://sudtawa.local/index.php?r=course/default/studyCourse&courseId=1&chapterId=1&type=teacher',	1,	'2015-07-29 09:59:08',	'2015-07-29 09:59:08'),
(10,	'http://sudtawa.local/index.php?r=course/default/studyCourse&courseId=1&lesson_id=3',	1,	'2015-07-29 09:59:36',	'2015-07-29 09:59:36');

DROP TABLE IF EXISTS `relate_course`;
CREATE TABLE `relate_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `before_course` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `after_course` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP VIEW IF EXISTS `stat_end_content`;
CREATE TABLE `stat_end_content` (`content_id` int(11), `user_id` int(11), `is_end` tinyint(1));


DROP VIEW IF EXISTS `stat_study_in_lesson`;
CREATE TABLE `stat_study_in_lesson` (`qty_study_lesson` bigint(21), `total_time_view` decimal(32,0), `content_id` int(11), `title` varchar(500), `show_order` tinyint(3), `parent_id` int(11), `course_id` int(11));


DROP VIEW IF EXISTS `stat_user_study`;
CREATE TABLE `stat_user_study` (`content_has_studied` bigint(21), `user_id` int(11), `course_id` int(11), `number_of_content` bigint(21));


DROP TABLE IF EXISTS `tbl_migration`;
CREATE TABLE `tbl_migration` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base',	1438158066),
('m140825_035923_newProject',	1438158070),
('m140929_102536_insert_default_course_share_rate',	1438158070);

DROP TABLE IF EXISTS `tbl_profiles`;
CREATE TABLE `tbl_profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(3000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detail` varchar(3000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tbl_profiles` (`user_id`, `lastname`, `position`, `firstname`, `company`, `detail`, `photo`, `tel`) VALUES
(1,	'',	NULL,	'',	NULL,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `tbl_profiles_fields`;
CREATE TABLE `tbl_profiles_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `field_size` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `field_size_min` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL,
  `match` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `range` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `error_message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `other_validator` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `default` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `widget` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `widgetparams` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(3) NOT NULL,
  `visible` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_id` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activkey` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lastvisit` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `superuser` int(1) NOT NULL,
  `is_inside` tinyint(1) NOT NULL,
  `is_student` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tbl_users` (`id`, `username`, `password`, `email`, `facebook_id`, `activkey`, `create_at`, `lastvisit`, `superuser`, `is_inside`, `is_student`, `status`) VALUES
(1,	'4101200009225',	'36271349c4c9a90d9dcaddc95770b035',	'topscores@gmail.com',	NULL,	'ea5b089a097b2ec78bed1e962004ed3e',	'2015-07-29 09:26:29',	'2015-07-29 09:53:28',	1,	0,	0,	1);

DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `detail` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `user_affiliate`;
CREATE TABLE `user_affiliate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `affiliate_code` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bankaccount` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `subbranch` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci,
  `active_status` tinyint(1) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `user_affiliate_course`;
CREATE TABLE `user_affiliate_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `user_coin`;
CREATE TABLE `user_coin` (
  `user_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `coin` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `user_content_log`;
CREATE TABLE `user_content_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `study_time` datetime NOT NULL,
  `course_id` int(11) NOT NULL,
  `qty_times` int(3) NOT NULL DEFAULT '1',
  `is_end` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user_content_log` (`id`, `content_id`, `user_id`, `study_time`, `course_id`, `qty_times`, `is_end`) VALUES
(1,	2,	1,	'2015-07-29 09:59:05',	1,	1,	0),
(2,	3,	1,	'2015-07-29 09:59:35',	1,	1,	0);

DROP TABLE IF EXISTS `user_course`;
CREATE TABLE `user_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `bought_price` int(5) NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '1',
  `is_head` tinyint(4) NOT NULL,
  `is_show` int(1) NOT NULL DEFAULT '1',
  `create_date` datetime DEFAULT NULL,
  `expire_date` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user_course` (`id`, `user_id`, `course_id`, `bought_price`, `role`, `is_head`, `is_show`, `create_date`, `expire_date`, `is_active`) VALUES
(1,	1,	1,	0,	1,	0,	1,	'2015-07-29 09:41:10',	NULL,	1);

DROP TABLE IF EXISTS `user_file`;
CREATE TABLE `user_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `original_name` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `user_permission`;
CREATE TABLE `user_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `can_create_course` int(11) NOT NULL,
  `is_request` tinyint(1) NOT NULL DEFAULT '0',
  `request_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user_permission` (`id`, `user_id`, `can_create_course`, `is_request`, `request_date`) VALUES
(1,	1,	1,	0,	NULL);

DROP TABLE IF EXISTS `vdo_buffer_rate`;
CREATE TABLE `vdo_buffer_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `times` int(5) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `content_info`;
CREATE ALGORITHM=UNDEFINED DEFINER=`sudtawa`@`localhost` SQL SECURITY DEFINER VIEW `content_info` AS select max(`content`.`course_id`) AS `course_id`,max(`course`.`name`) AS `course_name`,count(`content`.`id`) AS `number_of_content` from (`content` join `course` on((`content`.`course_id` = `course`.`id`))) where (`content`.`parent_id` <> -(1)) group by `content`.`course_id`;

DROP TABLE IF EXISTS `content_sort_list`;
CREATE ALGORITHM=UNDEFINED DEFINER=`sudtawa`@`localhost` SQL SECURITY DEFINER VIEW `content_sort_list` AS select (case when (`content_modify`.`show_order` is not null) then `content_modify`.`show_order` else `content`.`show_order` end) AS `show_order`,(case when (`content_modify`.`parent_id` is not null) then `content_modify`.`parent_id` else `content`.`parent_id` end) AS `parent_id`,`content`.`title` AS `title`,`content`.`id` AS `id`,`content_modify`.`id` AS `modify_id`,`content_modify`.`is_change_content` AS `is_change_content`,`content_modify`.`is_change_title` AS `is_change_title`,`content_modify`.`is_change_order` AS `is_change_order`,`content_modify`.`is_change_file` AS `is_change_file`,`content_modify`.`date_approve` AS `date_approve`,`content`.`update_date` AS `update_date`,`content`.`course_id` AS `course_id`,`content`.`is_exercise` AS `is_exercise`,`content`.`is_code` AS `is_code`,`content`.`is_public` AS `is_public`,`content`.`is_approve` AS `is_approve` from (`content` left join `content_modify` on((`content_modify`.`original_id` = `content`.`id`))) group by `content`.`id`,`content_modify`.`original_id` order by `content_modify`.`show_order`,`content`.`show_order`,`content_modify`.`id`,`content`.`id`;

DROP TABLE IF EXISTS `stat_end_content`;
CREATE ALGORITHM=UNDEFINED DEFINER=`sudtawa`@`localhost` SQL SECURITY DEFINER VIEW `stat_end_content` AS select `content`.`id` AS `content_id`,`user_content_log`.`user_id` AS `user_id`,`user_content_log`.`is_end` AS `is_end` from (`user_content_log` left join `content` on((`content`.`id` = `user_content_log`.`id`))) group by `user_content_log`.`content_id`,`user_content_log`.`user_id`;

DROP TABLE IF EXISTS `stat_study_in_lesson`;
CREATE ALGORITHM=UNDEFINED DEFINER=`sudtawa`@`localhost` SQL SECURITY DEFINER VIEW `stat_study_in_lesson` AS select count(`log`.`id`) AS `qty_study_lesson`,sum(`log`.`qty_times`) AS `total_time_view`,`content`.`id` AS `content_id`,`content`.`title` AS `title`,`content`.`show_order` AS `show_order`,`content`.`parent_id` AS `parent_id`,`content`.`course_id` AS `course_id` from (`content` join `user_content_log` `log` on((`content`.`id` = `log`.`content_id`))) where (`content`.`parent_id` > -(1)) group by `log`.`content_id`;

DROP TABLE IF EXISTS `stat_user_study`;
CREATE ALGORITHM=UNDEFINED DEFINER=`sudtawa`@`localhost` SQL SECURITY DEFINER VIEW `stat_user_study` AS select count(`user_content_log`.`content_id`) AS `content_has_studied`,max(`user_content_log`.`user_id`) AS `user_id`,max(`user_content_log`.`course_id`) AS `course_id`,max(`content_info`.`number_of_content`) AS `number_of_content` from (`user_content_log` join `content_info` on((`user_content_log`.`course_id` = `content_info`.`course_id`))) group by `user_content_log`.`course_id`,`user_content_log`.`user_id`;

-- 2015-07-29 10:01:38
