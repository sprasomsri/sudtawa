-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: May 08, 2013 at 03:53 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `coursecreek`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `content`
-- 

CREATE TABLE `content` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(500) collate utf8_unicode_ci NOT NULL,
  `course_id` int(11) NOT NULL COMMENT '-1 topLevel',
  `parent_id` int(11) NOT NULL default '-1' COMMENT '-1 is Chapter',
  `detail` text collate utf8_unicode_ci,
  `show_order` tinyint(3) NOT NULL default '1',
  `file_id` int(11) default NULL,
  `is_exercise` tinyint(1) NOT NULL default '0' COMMENT 'เป็น แบบฝึกหัด?',
  PRIMARY KEY  (`id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=177 ;

-- 
-- Dumping data for table `content`
-- 

INSERT INTO `content` VALUES (1, 'Class Overview', 1, -1, '', 8, 0, 0);
INSERT INTO `content` VALUES (2, 'So you want to be a Portfolio Manager?', 1, -1, '', 2, 0, 0);
INSERT INTO `content` VALUES (3, ' Learning Objectives', 1, 1, NULL, 24, 1, 0);
INSERT INTO `content` VALUES (4, 'Module Learning objectives', 1, 1, NULL, 12, 2, 0);
INSERT INTO `content` VALUES (5, 'Common metrics for assessing fund performance -1', 1, 1, NULL, 13, 3, 0);
INSERT INTO `content` VALUES (6, 'Common metrics for assessing fund performance -2', 1, 2, NULL, 7, 4, 0);
INSERT INTO `content` VALUES (8, 'Homework1: Construct and Assess a Portfolio.', 1, 1, NULL, 26, 0, 0);
INSERT INTO `content` VALUES (9, 'Mechanics of the market', 1, -1, NULL, 27, 0, 0);
INSERT INTO `content` VALUES (10, 'Module objectives', 1, 1, NULL, 15, 0, 0);
INSERT INTO `content` VALUES (13, 'The Order book', 1, 1, NULL, 22, 0, 0);
INSERT INTO `content` VALUES (14, 'Hedge funds and Arbitrage', 1, 1, NULL, 25, 0, 0);
INSERT INTO `content` VALUES (15, 'The Computing inside a Hedge fund', 1, 1, NULL, 16, 0, 0);
INSERT INTO `content` VALUES (16, 'What is a company worth?', 1, -1, NULL, 28, 0, 0);
INSERT INTO `content` VALUES (19, 'Intrinsic Value: Value of future dividends', 1, 1, NULL, 17, 0, 0);
INSERT INTO `content` VALUES (20, 'How and why news affects prices (Event Study) -1', 1, 1, NULL, 18, 0, 0);
INSERT INTO `content` VALUES (21, 'Fundamental analysis of company value', 1, 2, NULL, 6, 0, 0);
INSERT INTO `content` VALUES (22, 'Setting up software on Unix', 1, -1, NULL, 0, 0, 0);
INSERT INTO `content` VALUES (23, 'Testing QSTK', 1, 2, 'Test', 3, 23, 0);
INSERT INTO `content` VALUES (25, 'Overview and Installation of QSTK', 1, 16, NULL, 29, 0, 0);
INSERT INTO `content` VALUES (26, 'Installation of prerequisits of QSTK on Unix', 1, 1, NULL, 11, 0, 0);
INSERT INTO `content` VALUES (31, 'Installation of QSTK on Unix', 1, 1, NULL, 19, 0, 0);
INSERT INTO `content` VALUES (32, 'Installation of test data', 1, 2, '', 4, 27, 0);
INSERT INTO `content` VALUES (34, 'Testing QSTK continued', 1, 16, NULL, 30, 0, 0);
INSERT INTO `content` VALUES (35, 'Capital Assets pricing model (CAPM)', 1, -1, NULL, 31, 0, 0);
INSERT INTO `content` VALUES (36, 'Capital Assets Pricing Model Overview', 1, 35, NULL, 33, 0, 0);
INSERT INTO `content` VALUES (37, 'CAPM : What is beta ?', 1, 35, NULL, 34, 0, 0);
INSERT INTO `content` VALUES (38, 'How hedge funds use CAPM', 1, 35, NULL, 32, 0, 0);
INSERT INTO `content` VALUES (39, 'Efficient Markets Hypothesis', 1, -1, NULL, 35, 0, 0);
INSERT INTO `content` VALUES (40, 'Information and Arbitrage', 1, 39, NULL, 36, 0, 0);
INSERT INTO `content` VALUES (41, 'Efficient Markets Hypothesis', 1, 39, NULL, 37, 0, 0);
INSERT INTO `content` VALUES (42, 'Event Studies', 1, 39, NULL, 38, 0, 0);
INSERT INTO `content` VALUES (43, 'Event Profiler - QSTK', 1, 39, NULL, 39, 0, 0);
INSERT INTO `content` VALUES (44, 'Portfolio Optimization and Efficient Frontier', 1, -1, NULL, 40, 0, 0);
INSERT INTO `content` VALUES (45, 'Portfolio Optimization and Efficient Frontier - 1', 1, 44, NULL, 41, 0, 0);
INSERT INTO `content` VALUES (46, 'Portfolio Optimization and Efficient Frontier - 2', 1, 44, NULL, 42, 0, 0);
INSERT INTO `content` VALUES (47, 'Correlation and Covariance', 1, 44, NULL, 43, 0, 0);
INSERT INTO `content` VALUES (48, 'Efficient Frontier', 1, 44, NULL, 44, 0, 0);
INSERT INTO `content` VALUES (49, 'How optimizers work', 1, 44, NULL, 45, 0, 0);
INSERT INTO `content` VALUES (50, 'Importance of Data', 1, -1, NULL, 46, 0, 0);
INSERT INTO `content` VALUES (54, 'Digging into Data', 1, 50, NULL, 47, 0, 0);
INSERT INTO `content` VALUES (55, 'Actual Vs Adjusted Price', 1, 50, NULL, 48, 0, 0);
INSERT INTO `content` VALUES (56, 'Data Sanity and Scrubbing', 1, 50, NULL, 49, 0, 0);
INSERT INTO `content` VALUES (57, 'Overview of Homework 3', 1, -1, NULL, 50, 0, 0);
INSERT INTO `content` VALUES (58, 'How Next Two Homeworks Fit Together', 1, 57, NULL, 51, 0, 0);
INSERT INTO `content` VALUES (59, 'Specification for Homework 3', 1, 57, NULL, 52, 0, 0);
INSERT INTO `content` VALUES (60, 'Suggestions on Implementation of Homework 3', 1, 57, NULL, 53, 0, 0);
INSERT INTO `content` VALUES (61, 'Capital Assets Pricing Model', 1, -1, NULL, 54, 0, 0);
INSERT INTO `content` VALUES (62, 'CAPM : Capital Assets Pricing Model', 1, 61, NULL, 55, 0, 0);
INSERT INTO `content` VALUES (63, 'Using CAPM to reduce risk', 1, 61, NULL, 56, 0, 0);
INSERT INTO `content` VALUES (64, 'Overview of Homework 4', 1, -1, NULL, 57, 0, 0);
INSERT INTO `content` VALUES (65, 'How to Assess Event Study', 1, 64, NULL, 58, 0, 0);
INSERT INTO `content` VALUES (66, 'Homework 4', 1, 64, NULL, 59, 0, 0);
INSERT INTO `content` VALUES (67, 'The Fundamental Law', 1, -1, NULL, 60, 0, 0);
INSERT INTO `content` VALUES (68, 'Thought Experiment: Coin Flipping', 1, 67, NULL, 61, 0, 0);
INSERT INTO `content` VALUES (69, 'The Fundamental Law - 1', 1, 67, NULL, 62, 0, 0);
INSERT INTO `content` VALUES (72, 'ทดสอบ Unit 1', 1, 1, '', 21, 26, 0);
INSERT INTO `content` VALUES (73, 'IntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroduction', 9, -1, 'IntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroductionIntroduction', 1, NULL, 0);
INSERT INTO `content` VALUES (74, 'Intro', 9, 73, 'English is a West Germanic language that was first spoken in early medieval England and is now the most widely used language in the world.[4] It is spoken as a first language by the majority populations of several sovereign states, including the United Kingdom, the United States, Canada, Australia, Ireland, New Zealand and a number of Caribbean nations. It is the third-most-common native language in the world, after Mandarin Chinese and Spanish.[5] It is widely learned as a second language and is an official language of the European Union, many Commonwealth countries and the United Nations, as well as in many world organisations.\r\nEnglish arose in the Anglo-Saxon kingdoms of England and what is now southeast Scotland. Following the extensive influence of Great Britain and the United Kingdom from the 17th century to the mid-20th century, through the British Empire, and also of the United States since the mid-20th century,[6][7][8][9] it has been widely propagated around the world, becoming the leading language of international discourse and the lingua franca in many regions.[10][11]\r\nHistorically, English originated from the fusion of closely related dialects, now collectively termed Old English, which were brought to the eastern coast of Great Britain by Germanic settlers (Anglo-Saxons) by the 5th century – with the word English being derived from the name of the Angles,[12] and ultimately from their ancestral region of Angeln (in what is now Schleswig-Holstein). A significant number of English words are constructed on the basis of roots from Latin, because Latin in some form was the lingua franca of the Christian Church and of European intellectual life.[13] The language was further influenced by the Old Norse language because of Viking invasions in the 9th and 10th centuries.\r\nThe Norman conquest of England in the 11th century gave rise to heavy borrowings from Norman-French, and vocabulary and spelling conventions began to give the appearance of a close relationship with Romance languages[14][15] to what had then become Middle English. The Great Vowel Shift that began in the south of England in the 15th century is one of the historical events that mark the emergence of Modern English from Middle English.\r\nOwing to the assimilation of words from many other languages throughout history, modern English contains a very large vocabulary, with complex and irregular spelling, particularly of vowels. Modern English has not only assimilated words from other European languages, but from all over the world. The Oxford English Dictionary lists over 250,000 distinct words, not including many technical, scientific, and slang terms.[16][17]', 1, 28, 0);
INSERT INTO `content` VALUES (75, 'Test', 9, 73, 'History\r\n\r\nMain article: History of the English language\r\nEnglish originated in those dialects of North Sea Germanic that were carried to Britain by Germanic settlers from various parts of what are now the Netherlands, northwest Germany, and Denmark.[28] Up to that point, in Roman Britain the native population is assumed to have spoken the Celtic language Brythonic alongside the acrolectal influence of Latin, from the 400-year Roman occupation.[29]\r\nOne of these incoming Germanic tribes was the Angles,[30] whom Bede believed to have relocated entirely to Britain.[31] The names ''England'' (from Engla land[32] "Land of the Angles") and English (Old English Englisc[33]) are derived from the name of this tribe—but Saxons, Jutes and a range of Germanic peoples from the coasts of Frisia, Lower Saxony, Jutland and Southern Sweden also moved to Britain in this era.[34][35][36]\r\nInitially, Old English was a diverse group of dialects, reflecting the varied origins of the Anglo-Saxon kingdoms of Great Britain[37] but one of these dialects, Late West Saxon, eventually came to dominate, and it is in this that the poem Beowulf is written.\r\nOld English was later transformed by two waves of invasion. The first was by speakers of the North Germanic language branch when Halfdan Ragnarsson and Ivar the Boneless started the conquering and colonisation of northern parts of the British Isles in the 8th and 9th centuries (see Danelaw). The second was by speakers of the Romance language Old Norman in the 11th century with the Norman conquest of England. Norman developed into Anglo-Norman, and then Anglo-French – and introduced a layer of words especially via the courts and government. As well as extending the lexicon with Scandinavian and Norman words these two events also simplified the grammar and transformed English into a borrowing language—more than normally open to accept new words from other languages.\r\nThe linguistic shifts in English following the Norman invasion produced what is now referred to as Middle English; Geoffrey Chaucer''s The Canterbury Tales is its best-known work.\r\nThroughout all this period Latin in some form was the lingua franca of European intellectual life, first the Medieval Latin of the Christian Church, but later the humanist Renaissance Latin, and those that wrote or copied texts in Latin[13] commonly coined new terms from Latin to refer to things or concepts for which there was no existing native English word.\r\nModern English, which includes the works of William Shakespeare[38] and the King James Bible, is generally dated from about 1550, and after the United Kingdom became a colonial power, English served as the lingua franca of the colonies of the British Empire. In the post-colonial period, some of the newly created nations that had multiple indigenous languages opted to continue using English as the lingua franca to avoid the political difficulties inherent in promoting any one indigenous language above the others. As a result of the growth of the British Empire, English was adopted in North America, India, Africa, Australia and many other regions, a trend extended with the emergence of the United States as a superpower in the mid-20th century.', 1, 29, 0);
INSERT INTO `content` VALUES (81, 'แบบฝึกหัดที่ 1', 1, 22, '<p>แบบฝึกหัดที่ 1<span id="pastemarkerend"> ๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆ</span></p>\r\n', 1, NULL, 1);
INSERT INTO `content` VALUES (82, '12345', 1, 22, '<p>23452222</p>\r\n', 1, 31, 0);
INSERT INTO `content` VALUES (83, '123456', 1, 22, '1111111111111111111111111', 1, 32, 0);
INSERT INTO `content` VALUES (130, 'test', 1, 22, '<p>1111111111</p>\r\n', 1, 41, 0);
INSERT INTO `content` VALUES (131, 'sss', 1, -1, '', 1, NULL, 0);
INSERT INTO `content` VALUES (132, 'eeeeeeeee', 1, 22, '', 1, NULL, 0);
INSERT INTO `content` VALUES (133, '1111', 1, 22, '', 1, NULL, 0);
INSERT INTO `content` VALUES (134, '1111', 1, 22, '', 1, NULL, 0);
INSERT INTO `content` VALUES (135, '1111', 1, 22, '', 1, NULL, 0);
INSERT INTO `content` VALUES (136, '22222', 1, 22, '', 1, NULL, 0);
INSERT INTO `content` VALUES (137, 'test', 1, 22, '', 1, 45, 0);
INSERT INTO `content` VALUES (138, '2', 1, 22, '', 1, 46, 0);
INSERT INTO `content` VALUES (139, '26 11 32', 1, 22, '', 1, 48, 0);
INSERT INTO `content` VALUES (140, '1', 1, 22, '<p>111111111111111</p>\r\n', 1, NULL, 1);
INSERT INTO `content` VALUES (141, '1', 1, 22, '<p>111111111111111</p>\r\n', 1, NULL, 1);
INSERT INTO `content` VALUES (142, '1', 1, 22, '<p>111111111111111</p>\r\n', 1, NULL, 1);
INSERT INTO `content` VALUES (143, '1', 1, 22, '<p>111111111111111</p>\r\n', 1, NULL, 1);
INSERT INTO `content` VALUES (144, '1', 1, 22, '<p>111111111111111</p>\r\n', 1, NULL, 1);
INSERT INTO `content` VALUES (145, '1', 1, 22, '<p>111111111111111</p>\r\n', 1, NULL, 1);
INSERT INTO `content` VALUES (146, '111', 2, -1, '<p>11111</p>\r\n', 1, NULL, 0);
INSERT INTO `content` VALUES (147, '111', 2, -1, '<p>1111</p>\r\n', 127, NULL, 0);
INSERT INTO `content` VALUES (148, '11', 2, 146, '<p>1111</p>\r\n', 1, NULL, 0);
INSERT INTO `content` VALUES (149, '22', 2, 146, '', 1, NULL, 0);
INSERT INTO `content` VALUES (150, '333', 2, 146, '', 1, NULL, 0);
INSERT INTO `content` VALUES (151, '444', 2, 146, '<p>444444444444444</p>\r\n', 1, NULL, 0);
INSERT INTO `content` VALUES (152, 'w', 2, 146, '<p>wwwwwwwwwww</p>\r\n', 1, NULL, 0);
INSERT INTO `content` VALUES (153, '2222', 2, 146, '', 1, NULL, 0);
INSERT INTO `content` VALUES (154, 'qqqqqq', 2, 146, '', 1, NULL, 0);
INSERT INTO `content` VALUES (155, '1', 2, -1, '', 1, NULL, 0);
INSERT INTO `content` VALUES (156, '111111112', 2, 146, '<p>1111111111</p>\r\n', 1, 55, 0);
INSERT INTO `content` VALUES (157, '2222222222', 2, 146, '', 1, NULL, 0);
INSERT INTO `content` VALUES (158, '2222222', 2, 146, '<p>22222222222222</p>\r\n', 1, 59, 0);
INSERT INTO `content` VALUES (159, '11111111', 2, 146, '<p>1111111111</p>\r\n', 1, 61, 0);
INSERT INTO `content` VALUES (160, '4', 2, 146, '', 1, NULL, 0);
INSERT INTO `content` VALUES (161, 'g', 2, 146, '', 1, NULL, 0);
INSERT INTO `content` VALUES (162, 'ทดสอบ rrrr', 2, 146, '', 1, 67, 0);
INSERT INTO `content` VALUES (163, 'wwww', 2, 146, '', 1, NULL, 0);
INSERT INTO `content` VALUES (164, 'wwww', 2, 146, '', 1, 68, 0);
INSERT INTO `content` VALUES (165, 'wwwwwwwww', 2, 146, '<p>wwwwww</p>\r\n', 1, 69, 0);
INSERT INTO `content` VALUES (166, 'wwwwwwwwwww', 2, 146, '<p>wwwwwwwwwwwwwwwwwww</p>\r\n', 1, 70, 0);
INSERT INTO `content` VALUES (167, '111111111', 2, 146, '<p>1111111111111111</p>\r\n', 1, 71, 0);
INSERT INTO `content` VALUES (168, 'ddddd', 2, 146, '<p>ddddddddd</p>\r\n', 1, NULL, 0);
INSERT INTO `content` VALUES (169, 'eeeeeeeeeeee', 1, 22, '<p>eeeeeeee</p>\r\n', 1, NULL, 1);
INSERT INTO `content` VALUES (170, 'test exercise', 1, 22, '', 1, NULL, 1);
INSERT INTO `content` VALUES (171, '3', 1, 22, '<p>3</p>\r\n', 1, NULL, 1);
INSERT INTO `content` VALUES (172, '//////////', 1, 22, '<p>////////////</p>\r\n', 1, NULL, 1);
INSERT INTO `content` VALUES (173, '3', 1, 22, '<p>3333333333</p>\r\n', 1, 72, 0);
INSERT INTO `content` VALUES (174, 'test video', 1, 50, '', 1, 73, 0);
INSERT INTO `content` VALUES (175, 'Test 1234', 1, 22, '', 1, 74, 0);
INSERT INTO `content` VALUES (176, 'ทดสอบ 8 05 2556', 1, 22, '<p>แค่</p>\r\n', 1, 75, 0);

-- --------------------------------------------------------

-- 
-- Stand-in structure for view `content_info`
-- 
CREATE TABLE `content_info` (
`course_id` int(11)
,`course_name` varchar(500)
,`number_of_content` bigint(21)
);
-- --------------------------------------------------------

-- 
-- Table structure for table `coupon`
-- 

CREATE TABLE `coupon` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(500) collate utf8_unicode_ci NOT NULL,
  `discount` tinyint(3) NOT NULL COMMENT 'ส่วนลด (%)',
  `expire_date` date NOT NULL,
  `qty_use` tinyint(5) NOT NULL default '1' COMMENT 'จำนวนครั้งที่ใช้ได้',
  `detail` varchar(1000) collate utf8_unicode_ci default NULL,
  `create_date` datetime NOT NULL,
  `course_id` int(11) NOT NULL default '-1' COMMENT '-1 = coupong can use every course',
  `user_id` int(11) NOT NULL COMMENT 'คนสร้าง',
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=217 ;

-- 
-- Dumping data for table `coupon`
-- 

INSERT INTO `coupon` VALUES (105, 'testhLwet142e3KU', 11, '2013-04-25', 1, 'ทดสอบ 2', '2013-04-10 17:51:31', 1, 4);
INSERT INTO `coupon` VALUES (106, 'testrXe4353H6s2K', 11, '2013-04-25', 1, 'ทดสอบ 2', '2013-04-10 17:51:31', 1, 4);
INSERT INTO `coupon` VALUES (107, 'testY3aa4eb1xh5U', 11, '2013-04-25', 1, 'ทดสอบ 2', '2013-04-10 17:51:52', 1, 4);
INSERT INTO `coupon` VALUES (108, 'test45edJkfak432', 11, '2013-04-25', 1, 'ทดสอบ 2', '2013-04-10 17:51:52', 1, 4);
INSERT INTO `coupon` VALUES (109, 'testko15GaZ4@r2', 11, '2013-04-10', 1, '1', '2013-04-10 18:19:01', 1, 4);
INSERT INTO `coupon` VALUES (110, 'sci30rXY11A4qan', 1, '2013-04-18', 1, '1', '2013-04-10 18:20:09', 1, 4);
INSERT INTO `coupon` VALUES (111, '5ar4uY15h12kH', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (112, '5$i2R2Hg2u6u4', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (113, '5g4ra2$tk3353', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (114, '5sguagqb4244Z', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (115, '5iPgfQ2L5354i', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (116, '5a4n6frc62MH&', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (117, '5w4GSk77z3u2V', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (118, '5vLGS828o034n', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (119, '55h2949e3NMwn', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (120, '5kY3Pqnf10n40Q', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (121, '55N0134r4aj311', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (122, '5QpL24rw3zu2a1', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (123, '5wre3c4030ek13', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (124, '541C%JH344d3Kr', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (125, '5brQMda5ce4135', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (126, '5i61bcl43T6FGn', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (127, '53x7n%@z741sxK', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (128, '5Ng348ZN8Jen1n', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (129, '5m9&403r1Vn$9o', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (130, '5%L22r44x#q000', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (131, '5dee442ok1133K', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (132, '5CkS2%n420uD42', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (133, '53qu4TDee2o34E', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (134, '5444i4x2oaMjue', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (135, '5c4V46w2XB535$', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (136, '5q6UexH4J6fY42', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (137, '50dWkc6gu72447', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (138, '58zpBB4L64QW82', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (139, '5k4nhK4@9kX29e', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (140, '5g50e43W0oVu0M', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (141, '51LS3ir5x1EZ43', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (142, '5Fi2Ww5%#4rg32', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (143, '5A3p3Ndib5o4c3', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (144, '5s3x544TeQB4ue', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (145, '555A6S533ue4OB', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (146, '5Llu45L3e66EsH', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (147, '5A5n077F4gcr3$', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (148, '5Qe8v8n543JuE&', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (149, '5395@sa9P4#BNe', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (150, '5%o$Bi6U4o040N', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (151, '51Nn46Un%C14qk', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (152, '5$46s5e43r22FK', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (153, '5V4DgY6xk430p3', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (154, '54C4nU454k46e5', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (155, '54C4nC5ocs3e65', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (156, '5N2g6rd3Om6446', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (157, '576uc&4t47GeA@', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (158, '5G4wa$k8r6er84', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (159, '5eq649K6Nu449f', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (160, '54fr7o050#vg4&', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (161, '5Cy17RIT5VK4b1', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (162, '52O2nr3t5B764$', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (163, '5S%o33EV5k7cg4', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (164, '5GO454C47ono$6', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (165, '5NT5tE7eE5o45B', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (166, '5ro4@X65e72q@6', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (167, '5fnk7e74oZ5Qf7', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (168, '57f85hH8aueOV4', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (169, '559apTPQXy947a', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (170, '5Qr0504C6urn80', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (171, '5Be8vH6MA14qx1', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (172, '582u4#covk2ag6', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (173, '5I6G8XL633546C', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (174, '5g4a6sn84ZeeN4', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (175, '5554o8yOj6vdDr', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (176, '5#68aP4eT6l664', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (177, '52Nc48rc77ee%6', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (178, '56fe8e4S@l8S38', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (179, '5X9&9K48e660g&', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (180, '5umuB047wnH09r', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (181, '5X14$7c1WlMgA9', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (182, '5e429Y4a62ro7P', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (183, '5l3a97qM43olQn', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (184, '50o4d74ckO294r', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (185, '59w55e%sk7i4If', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (186, '54xo75NW6Z9Nb6', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (187, '5e7cO747f9vWPi', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (188, '5a874Aunr853I9', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (189, '54e9X5gc9U49S7', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (190, '518skrDY000h5O4', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (191, '5148e01kZD315j0', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (192, '50I8x4a232cB1ed', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (193, '53yR5n43581oO0n', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (194, '514Eom834TdO024', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (195, '5o4150OGbhNe8f5', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (196, '5&g46yI6k10n8hQ', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (197, '5gMd8qe130Q747e', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (198, '5l48gY2eg0881G4', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (199, '5Pa94Ye80&0916y', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (200, '5q4Hg101wi&9z0y', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (201, '541es11Bmns9r1n', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (202, '54L9k21er1t$5A2', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (203, '5niU59u1341v0w3', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (204, '5i4&9Fyk%4j14e1', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (205, '5m0493Xu5Gs5151', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (206, '55691Dav14s6@ps', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (207, '5e1a94to1D77daz', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (208, '5rs91Bn4gu8e1X8', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (209, '5J939Hu4er@191V', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (210, '52u000KT4Rr101wB', 10, '2013-04-10', 1, 'dddd', '2013-04-10 18:51:20', 1, 4);
INSERT INTO `coupon` VALUES (211, '51uzgU4x1e7Re', 11, '2013-04-02', 1, '111', '2013-04-11 14:49:16', 1, 4);
INSERT INTO `coupon` VALUES (212, '1111mXMa10y%41e5', 12, '2013-04-04', 1, '1111', '2013-04-26 15:41:14', 1, 4);
INSERT INTO `coupon` VALUES (213, '6ce51xeaoD24', 100, '2013-05-14', 1, 'oo', '2013-05-01 17:10:01', 1, 4);
INSERT INTO `coupon` VALUES (214, '64a2u31L60AsC', 12, '2013-05-09', 1, '2', '2013-05-01 17:19:22', 1, 4);
INSERT INTO `coupon` VALUES (215, '14rU1Qejb53e5', 11, '2013-05-14', 1, '', '2013-05-01 17:21:54', 1, 4);
INSERT INTO `coupon` VALUES (216, '6vo81IrKnza4', 12, '0000-00-00', 1, '', '2013-05-01 17:22:07', 1, 4);

-- --------------------------------------------------------

-- 
-- Table structure for table `coupon_log`
-- 

CREATE TABLE `coupon_log` (
  `coupon_id` int(11) NOT NULL,
  `use_course_id` int(11) NOT NULL,
  `used_time` datetime NOT NULL,
  `use_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table `coupon_log`
-- 

INSERT INTO `coupon_log` VALUES (111, 1, '2013-04-10 12:18:47', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `course`
-- 

CREATE TABLE `course` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(500) collate utf8_unicode_ci NOT NULL,
  `name_th` varchar(500) collate utf8_unicode_ci NOT NULL,
  `tag` varchar(20) collate utf8_unicode_ci NOT NULL,
  `description` text collate utf8_unicode_ci,
  `about` text collate utf8_unicode_ci NOT NULL,
  `price` int(5) NOT NULL,
  `course_img` varchar(1000) collate utf8_unicode_ci default NULL,
  `course_status` tinyint(1) NOT NULL default '0',
  `user_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL default '0' COMMENT 'ส่วนแบ่ง (%)',
  `duration` tinyint(5) default NULL COMMENT 'ระยะเวลาในการเปิดคอร์ส หน่วยเป็นวัน ',
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

-- 
-- Dumping data for table `course`
-- 

INSERT INTO `course` VALUES (-1, 'Reserve', 'Reserve', 'Reserve', 'Reserve', 'Reserve', 0, 'Reserve', 0, 1, 0, 1);
INSERT INTO `course` VALUES (1, 'Computational Investing', 'การลงทุนเชิงคำนวณ', 'การลงทุน', 'Find out how modern electronic markets work, why stock prices change in the ways they do, and how computation can help our understanding of them.  Build algorithms and visualizations to inform investing practice.', 'Why do the prices of some companies’ stocks seem to move up and down together while others move separately? What does portfolio “diversification” really mean and how important is it? What should the price of a stock be? How can we discover and exploit the relationships between equity prices automatically? We’ll examine these questions, and others, from a computational point of view. You will learn many of the principles and algorithms hedge funds and investment professionals use to maximize return and reduce risk in equity portfolios.', 2999, 'course/urf2dtli83jpcw1soe9nzb6mga5vkq4y70xh_186617287761.jpg', 1, 4, 0, NULL);
INSERT INTO `course` VALUES (2, 'ภาษาจีน', 'ภาษาจีน', 'ภาษาต่างประเทศ', 'ภาษาจีน', '', 789, '', 1, 4, 0, NULL);
INSERT INTO `course` VALUES (3, 'ชีววิทยา', 'ชีววิทยา', 'เตรียมสอบ', 'ชีววิทยา', '', 1000, '', 0, 4, 0, NULL);
INSERT INTO `course` VALUES (4, 'ภาษาไทยสำหรับใช้ในชีวิตประจำวัน Thai lanquage', 'ภาษาไทยเพื่อการสื่อสารอย่างถูกต้อง Thai Culture Langquage for life ', 'เตรียมสอบ', 'ภาษาไทย', '', 2000, '', 2, 4, 0, NULL);
INSERT INTO `course` VALUES (5, 'ๅ', 'ๅ', 'ๅ', 'ๅ', 'ๅ', 0, 'ๅ', 0, 1, 0, NULL);
INSERT INTO `course` VALUES (6, 'กันทดสอบ', '', '', 'ไไไไ', '', 3500, 'course/nox5gku38szbmjl64iqhc0wy1frpevd9t2a7_9287_d093_8.jpg', 0, 4, 0, NULL);
INSERT INTO `course` VALUES (7, 'กันทดสอบ', '', '', 'ไไไไ', '', 3500, 'course/w1r8auxqc2gtzbvfh4enyp7kd53o09lmsji6_9287_d093_8.jpg', 0, 4, 0, NULL);
INSERT INTO `course` VALUES (8, 'กันทดสอบ', '', '', 'ไไไไ', '', 3500, 'course/cs52yu4hw109ltqage8mn7r6pkdfx3zojivb_IMG_0937.JPG', 0, 4, 0, NULL);
INSERT INTO `course` VALUES (9, 'English', 'ภาษาอังกฤษ', 'english', 'English is a West Germanic language that was first spoken in early medieval England and is now the most widely used language in the world.[4] It is spoken as a first language by the majority populations of several sovereign states, including the United Kingdom, the United States, Canada, Australia, Ireland, New Zealand and a number of Caribbean nations. It is the third-most-common native language in the world, after Mandarin Chinese and Spanish.[5] It is widely learned as a second language and is an official language of the European Union, many Commonwealth countries and the United Nations, as well as in many world organisations.\r\nEnglish arose in the Anglo-Saxon kingdoms of England and what is now southeast Scotland. Following the extensive influence of Great Britain and the United Kingdom from the 17th century to the mid-20th century, through the British Empire, and also of the United States since the mid-20th century,[6][7][8][9] it has been widely propagated around the world, becoming the leading language of international discourse and the lingua franca in many regions.[10][11]\r\nHistorically, English originated from the fusion of closely related dialects, now collectively termed Old English, which were brought to the eastern coast of Great Britain by Germanic settlers (Anglo-Saxons) by the 5th century – with the word English being derived from the name of the Angles,[12] and ultimately from their ancestral region of Angeln (in what is now Schleswig-Holstein). A significant number of English words are constructed on the basis of roots from Latin, because Latin in some form was the lingua franca of the Christian Church and of European intellectual life.[13] The language was further influenced by the Old Norse language because of Viking invasions in the 9th and 10th centuries.\r\nThe Norman conquest of England in the 11th century gave rise to heavy borrowings from Norman-French, and vocabulary and spelling conventions began to give the appearance of a close relationship with Romance languages[14][15] to what had then become Middle English. The Great Vowel Shift that began in the south of England in the 15th century is one of the historical events that mark the emergence of Modern English from Middle English.\r\nOwing to the assimilation of words from many other languages throughout history, modern English contains a very large vocabulary, with complex and irregular spelling, particularly of vowels. Modern English has not only assimilated words from other European languages, but from all over the world. The Oxford English Dictionary lists over 250,000 distinct words, not including many technical, scientific, and slang terms.[16][17]', 'about', 3000, 'course/14ztonw6lap9g5yvmf2ukhr3xdiqsbce807j_1280x1024_keroro01.jpg', 0, 4, 0, NULL);
INSERT INTO `course` VALUES (10, 'Sofware Testing', '', '', 'Sofware Testing', '', 1200, 'course/3ni4bo8y92kpj517elrqvumztsgxcdh60wfa_we.jpg', 0, 4, 0, NULL);
INSERT INTO `course` VALUES (11, 'ภาษาสเปน', '', '', 'ภาษาสเปน', '', 2000, 'course/pv1kgs5lzoxwu738r6tfmacbn0dy2e9iqj4h_q6.jpg', 0, 4, 0, NULL);
INSERT INTO `course` VALUES (12, 'Ajax', '', '', 'Ajax', '', 1000, '', 0, 4, 0, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `exercise`
-- 

CREATE TABLE `exercise` (
  `id` int(11) NOT NULL auto_increment,
  `content_id` int(11) NOT NULL,
  `qty_show` int(2) NOT NULL default '10',
  `is_random` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `content_id` (`content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `exercise`
-- 

INSERT INTO `exercise` VALUES (4, 81, 10, 1);
INSERT INTO `exercise` VALUES (5, 144, 10, 0);
INSERT INTO `exercise` VALUES (6, 145, 10, 0);
INSERT INTO `exercise` VALUES (7, 169, 10, 0);
INSERT INTO `exercise` VALUES (8, 170, 10, 0);
INSERT INTO `exercise` VALUES (9, 171, 10, 0);
INSERT INTO `exercise` VALUES (10, 172, 2, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `exercise_answer_choice`
-- 

CREATE TABLE `exercise_answer_choice` (
  `id` int(11) NOT NULL auto_increment,
  `answer` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `is_true` tinyint(1) NOT NULL default '0' COMMENT 'เป็นคำตอยที่ถูกไหม',
  `question_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `question_id` (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=90 ;

-- 
-- Dumping data for table `exercise_answer_choice`
-- 

INSERT INTO `exercise_answer_choice` VALUES (32, 'ขอแสดงความนับถือ', 1, 15);
INSERT INTO `exercise_answer_choice` VALUES (33, 'ขอแสดงความเคารพ', 0, 15);
INSERT INTO `exercise_answer_choice` VALUES (34, 'ฟันหนู', 1, 16);
INSERT INTO `exercise_answer_choice` VALUES (35, 'ฝนทอง', 0, 16);
INSERT INTO `exercise_answer_choice` VALUES (36, 'ด้วยความเคารพ', 0, 15);
INSERT INTO `exercise_answer_choice` VALUES (37, 'See you', 0, 15);
INSERT INTO `exercise_answer_choice` VALUES (40, 'สระอะ', 0, 16);
INSERT INTO `exercise_answer_choice` VALUES (41, 'สระอา', 0, 16);
INSERT INTO `exercise_answer_choice` VALUES (42, 'กล้วยต้นนี้ออกลูกดกจริงๆ', 1, 17);
INSERT INTO `exercise_answer_choice` VALUES (43, 'น้องพลอยกำลังปลูกพริก', 0, 17);
INSERT INTO `exercise_answer_choice` VALUES (44, 'ต้นไม้ริมแม่น้ำมูล', 0, 17);
INSERT INTO `exercise_answer_choice` VALUES (45, 'ยายไปซื้อกุ้ง', 0, 17);
INSERT INTO `exercise_answer_choice` VALUES (46, ' รัชกาลที่ 6', 1, 18);
INSERT INTO `exercise_answer_choice` VALUES (47, ' รัชกาลที่ 4', 0, 18);
INSERT INTO `exercise_answer_choice` VALUES (48, ' รัชกาลที่ 3', 0, 18);
INSERT INTO `exercise_answer_choice` VALUES (49, ' รัชกาลที่ 8', 0, 18);
INSERT INTO `exercise_answer_choice` VALUES (50, 'สระโอ', 0, 16);
INSERT INTO `exercise_answer_choice` VALUES (55, '44', 1, 19);
INSERT INTO `exercise_answer_choice` VALUES (56, '32', 0, 19);
INSERT INTO `exercise_answer_choice` VALUES (57, '24', 0, 19);
INSERT INTO `exercise_answer_choice` VALUES (58, '28', 0, 19);
INSERT INTO `exercise_answer_choice` VALUES (59, 'พ่อขุนรามคำแหง', 1, 20);
INSERT INTO `exercise_answer_choice` VALUES (60, 'รัชกาลที่1', 0, 20);
INSERT INTO `exercise_answer_choice` VALUES (61, 'ชาวอยุธยา', 0, 20);
INSERT INTO `exercise_answer_choice` VALUES (62, 'ขงจื้อ', 0, 20);
INSERT INTO `exercise_answer_choice` VALUES (63, '1 สี', 1, 21);
INSERT INTO `exercise_answer_choice` VALUES (64, '2 สี', 0, 21);
INSERT INTO `exercise_answer_choice` VALUES (65, '3 สี', 0, 21);
INSERT INTO `exercise_answer_choice` VALUES (66, '5 สี', 0, 21);
INSERT INTO `exercise_answer_choice` VALUES (67, 'พม่า', 1, 22);
INSERT INTO `exercise_answer_choice` VALUES (68, 'สิงคโปร์', 0, 22);
INSERT INTO `exercise_answer_choice` VALUES (69, 'โปร์แลน', 0, 22);
INSERT INTO `exercise_answer_choice` VALUES (70, 'เวียนนา', 0, 22);
INSERT INTO `exercise_answer_choice` VALUES (71, '+543', 1, 23);
INSERT INTO `exercise_answer_choice` VALUES (72, '-543', 0, 23);
INSERT INTO `exercise_answer_choice` VALUES (73, '+2000', 0, 23);
INSERT INTO `exercise_answer_choice` VALUES (74, '-542', 0, 23);
INSERT INTO `exercise_answer_choice` VALUES (75, '77', 1, 24);
INSERT INTO `exercise_answer_choice` VALUES (76, '76', 0, 24);
INSERT INTO `exercise_answer_choice` VALUES (77, '50', 0, 24);
INSERT INTO `exercise_answer_choice` VALUES (78, '51', 0, 24);
INSERT INTO `exercise_answer_choice` VALUES (87, '2', 1, 34);
INSERT INTO `exercise_answer_choice` VALUES (88, '3', 0, 34);
INSERT INTO `exercise_answer_choice` VALUES (89, '4', 0, 34);

-- --------------------------------------------------------

-- 
-- Table structure for table `exercise_choice_score`
-- 

CREATE TABLE `exercise_choice_score` (
  `id` int(11) NOT NULL auto_increment,
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `do_time` datetime NOT NULL,
  `is_true` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='เก็บคะแนน user จากการทำแบบทดสอบ' AUTO_INCREMENT=268 ;

-- 
-- Dumping data for table `exercise_choice_score`
-- 

INSERT INTO `exercise_choice_score` VALUES (1, 34, 16, 4, '2013-04-22 15:45:26', 1);
INSERT INTO `exercise_choice_score` VALUES (11, 34, 16, 4, '2013-04-22 15:46:54', 1);
INSERT INTO `exercise_choice_score` VALUES (18, 59, 20, 4, '2013-04-30 18:43:58', 1);
INSERT INTO `exercise_choice_score` VALUES (19, 34, 16, 1, '2013-04-24 14:16:06', 1);
INSERT INTO `exercise_choice_score` VALUES (20, 46, 18, 4, '2013-04-24 20:27:12', 1);
INSERT INTO `exercise_choice_score` VALUES (21, 74, 23, 4, '2013-04-24 20:27:12', 0);
INSERT INTO `exercise_choice_score` VALUES (22, 44, 17, 4, '2013-04-24 20:27:12', 0);
INSERT INTO `exercise_choice_score` VALUES (23, 69, 22, 9, '2013-04-29 15:54:55', 0);
INSERT INTO `exercise_choice_score` VALUES (24, 49, 18, 9, '2013-04-29 15:54:55', 0);
INSERT INTO `exercise_choice_score` VALUES (25, 44, 17, 17, '2013-05-07 12:45:03', 0);
INSERT INTO `exercise_choice_score` VALUES (26, 71, 23, 17, '2013-05-07 12:45:03', 1);
INSERT INTO `exercise_choice_score` VALUES (27, 41, 16, 17, '2013-05-07 12:45:03', 0);
INSERT INTO `exercise_choice_score` VALUES (28, 48, 18, 17, '2013-05-07 12:45:03', 0);
INSERT INTO `exercise_choice_score` VALUES (29, 65, 21, 17, '2013-05-07 12:45:03', 0);
INSERT INTO `exercise_choice_score` VALUES (30, 55, 19, 17, '2013-05-07 12:45:03', 1);
INSERT INTO `exercise_choice_score` VALUES (31, 36, 15, 17, '2013-05-07 12:45:03', 0);
INSERT INTO `exercise_choice_score` VALUES (32, 75, 24, 17, '2013-05-07 12:45:03', 1);
INSERT INTO `exercise_choice_score` VALUES (33, 44, 17, 17, '2013-05-07 12:45:09', 0);
INSERT INTO `exercise_choice_score` VALUES (34, 71, 23, 17, '2013-05-07 12:45:09', 1);
INSERT INTO `exercise_choice_score` VALUES (35, 41, 16, 17, '2013-05-07 12:45:09', 0);
INSERT INTO `exercise_choice_score` VALUES (36, 48, 18, 17, '2013-05-07 12:45:09', 0);
INSERT INTO `exercise_choice_score` VALUES (37, 65, 21, 17, '2013-05-07 12:45:09', 0);
INSERT INTO `exercise_choice_score` VALUES (38, 55, 19, 17, '2013-05-07 12:45:09', 1);
INSERT INTO `exercise_choice_score` VALUES (39, 36, 15, 17, '2013-05-07 12:45:09', 0);
INSERT INTO `exercise_choice_score` VALUES (40, 75, 24, 17, '2013-05-07 12:45:09', 1);
INSERT INTO `exercise_choice_score` VALUES (41, 44, 17, 17, '2013-05-07 12:45:10', 0);
INSERT INTO `exercise_choice_score` VALUES (42, 71, 23, 17, '2013-05-07 12:45:10', 1);
INSERT INTO `exercise_choice_score` VALUES (43, 41, 16, 17, '2013-05-07 12:45:10', 0);
INSERT INTO `exercise_choice_score` VALUES (44, 48, 18, 17, '2013-05-07 12:45:10', 0);
INSERT INTO `exercise_choice_score` VALUES (45, 65, 21, 17, '2013-05-07 12:45:10', 0);
INSERT INTO `exercise_choice_score` VALUES (46, 55, 19, 17, '2013-05-07 12:45:10', 1);
INSERT INTO `exercise_choice_score` VALUES (47, 36, 15, 17, '2013-05-07 12:45:10', 0);
INSERT INTO `exercise_choice_score` VALUES (48, 75, 24, 17, '2013-05-07 12:45:10', 1);
INSERT INTO `exercise_choice_score` VALUES (49, 66, 21, 17, '2013-05-07 12:46:39', 0);
INSERT INTO `exercise_choice_score` VALUES (50, 42, 17, 17, '2013-05-07 12:46:39', 1);
INSERT INTO `exercise_choice_score` VALUES (51, 68, 22, 17, '2013-05-07 12:46:39', 0);
INSERT INTO `exercise_choice_score` VALUES (52, 34, 16, 17, '2013-05-07 12:46:39', 1);
INSERT INTO `exercise_choice_score` VALUES (53, 57, 19, 17, '2013-05-07 12:46:39', 0);
INSERT INTO `exercise_choice_score` VALUES (54, 48, 18, 17, '2013-05-07 12:46:39', 0);
INSERT INTO `exercise_choice_score` VALUES (55, 62, 20, 17, '2013-05-07 12:46:39', 0);
INSERT INTO `exercise_choice_score` VALUES (56, 33, 15, 17, '2013-05-07 12:46:39', 0);
INSERT INTO `exercise_choice_score` VALUES (57, 77, 24, 17, '2013-05-07 12:51:23', 0);
INSERT INTO `exercise_choice_score` VALUES (58, 49, 18, 17, '2013-05-07 12:51:23', 0);
INSERT INTO `exercise_choice_score` VALUES (59, 33, 15, 17, '2013-05-07 12:51:23', 0);
INSERT INTO `exercise_choice_score` VALUES (60, 34, 16, 17, '2013-05-07 12:51:23', 1);
INSERT INTO `exercise_choice_score` VALUES (61, 42, 17, 17, '2013-05-07 12:51:23', 1);
INSERT INTO `exercise_choice_score` VALUES (62, 61, 20, 17, '2013-05-07 12:51:23', 0);
INSERT INTO `exercise_choice_score` VALUES (63, 57, 19, 17, '2013-05-07 12:51:23', 0);
INSERT INTO `exercise_choice_score` VALUES (64, 74, 23, 17, '2013-05-07 12:51:23', 0);
INSERT INTO `exercise_choice_score` VALUES (65, 76, 24, 17, '2013-05-07 12:52:27', 0);
INSERT INTO `exercise_choice_score` VALUES (66, 42, 17, 17, '2013-05-07 12:52:27', 1);
INSERT INTO `exercise_choice_score` VALUES (67, 69, 22, 17, '2013-05-07 12:52:27', 0);
INSERT INTO `exercise_choice_score` VALUES (68, 65, 21, 17, '2013-05-07 12:52:27', 0);
INSERT INTO `exercise_choice_score` VALUES (69, 37, 15, 17, '2013-05-07 12:52:27', 0);
INSERT INTO `exercise_choice_score` VALUES (70, 60, 20, 17, '2013-05-07 12:52:27', 0);
INSERT INTO `exercise_choice_score` VALUES (71, 73, 23, 17, '2013-05-07 12:52:27', 0);
INSERT INTO `exercise_choice_score` VALUES (72, 46, 18, 17, '2013-05-07 12:52:27', 1);
INSERT INTO `exercise_choice_score` VALUES (73, 66, 21, 17, '2013-05-07 12:54:24', 0);
INSERT INTO `exercise_choice_score` VALUES (74, 71, 23, 17, '2013-05-07 12:54:24', 1);
INSERT INTO `exercise_choice_score` VALUES (75, 76, 24, 17, '2013-05-07 12:54:24', 0);
INSERT INTO `exercise_choice_score` VALUES (76, 36, 15, 17, '2013-05-07 12:54:24', 0);
INSERT INTO `exercise_choice_score` VALUES (77, 45, 17, 17, '2013-05-07 12:54:24', 0);
INSERT INTO `exercise_choice_score` VALUES (78, 62, 20, 17, '2013-05-07 12:54:24', 0);
INSERT INTO `exercise_choice_score` VALUES (79, 34, 16, 17, '2013-05-07 12:54:24', 1);
INSERT INTO `exercise_choice_score` VALUES (80, 37, 15, 17, '2013-05-07 12:59:27', 0);
INSERT INTO `exercise_choice_score` VALUES (81, 47, 18, 17, '2013-05-07 12:59:27', 0);
INSERT INTO `exercise_choice_score` VALUES (82, 34, 16, 17, '2013-05-07 12:59:27', 1);
INSERT INTO `exercise_choice_score` VALUES (83, 55, 19, 17, '2013-05-07 12:59:27', 1);
INSERT INTO `exercise_choice_score` VALUES (84, 77, 24, 17, '2013-05-07 12:59:27', 0);
INSERT INTO `exercise_choice_score` VALUES (85, 64, 21, 17, '2013-05-07 12:59:27', 0);
INSERT INTO `exercise_choice_score` VALUES (86, 44, 17, 17, '2013-05-07 12:59:27', 0);
INSERT INTO `exercise_choice_score` VALUES (87, 70, 22, 17, '2013-05-07 12:59:27', 0);
INSERT INTO `exercise_choice_score` VALUES (88, 73, 23, 17, '2013-05-07 12:59:27', 0);
INSERT INTO `exercise_choice_score` VALUES (89, 63, 21, 17, '2013-05-07 13:00:53', 1);
INSERT INTO `exercise_choice_score` VALUES (90, 58, 19, 17, '2013-05-07 13:00:53', 0);
INSERT INTO `exercise_choice_score` VALUES (91, 77, 24, 17, '2013-05-07 13:00:53', 0);
INSERT INTO `exercise_choice_score` VALUES (92, 45, 17, 17, '2013-05-07 13:00:53', 0);
INSERT INTO `exercise_choice_score` VALUES (93, 41, 16, 17, '2013-05-07 13:01:29', 0);
INSERT INTO `exercise_choice_score` VALUES (94, 32, 15, 17, '2013-05-07 13:01:29', 1);
INSERT INTO `exercise_choice_score` VALUES (95, 63, 21, 17, '2013-05-07 13:01:29', 1);
INSERT INTO `exercise_choice_score` VALUES (96, 34, 16, 17, '2013-05-07 14:17:22', 1);
INSERT INTO `exercise_choice_score` VALUES (97, 42, 17, 17, '2013-05-07 14:17:22', 1);
INSERT INTO `exercise_choice_score` VALUES (98, 55, 19, 17, '2013-05-07 14:17:22', 1);
INSERT INTO `exercise_choice_score` VALUES (99, 36, 15, 17, '2013-05-07 14:17:22', 0);
INSERT INTO `exercise_choice_score` VALUES (100, 71, 23, 17, '2013-05-07 14:17:22', 1);
INSERT INTO `exercise_choice_score` VALUES (101, 75, 24, 17, '2013-05-07 14:17:22', 1);
INSERT INTO `exercise_choice_score` VALUES (102, 67, 22, 17, '2013-05-07 14:17:22', 1);
INSERT INTO `exercise_choice_score` VALUES (103, 65, 21, 17, '2013-05-07 14:17:22', 0);
INSERT INTO `exercise_choice_score` VALUES (104, 58, 19, 17, '2013-05-07 14:18:36', 0);
INSERT INTO `exercise_choice_score` VALUES (105, 71, 23, 17, '2013-05-07 14:18:36', 1);
INSERT INTO `exercise_choice_score` VALUES (106, 48, 18, 17, '2013-05-07 14:18:36', 0);
INSERT INTO `exercise_choice_score` VALUES (107, 62, 20, 17, '2013-05-07 14:18:36', 0);
INSERT INTO `exercise_choice_score` VALUES (108, 42, 17, 17, '2013-05-07 14:18:36', 1);
INSERT INTO `exercise_choice_score` VALUES (109, 37, 15, 17, '2013-05-07 14:18:36', 0);
INSERT INTO `exercise_choice_score` VALUES (110, 76, 24, 17, '2013-05-07 14:18:36', 0);
INSERT INTO `exercise_choice_score` VALUES (111, 65, 21, 17, '2013-05-07 14:18:36', 0);
INSERT INTO `exercise_choice_score` VALUES (112, 49, 18, 17, '2013-05-07 14:20:05', 0);
INSERT INTO `exercise_choice_score` VALUES (113, 62, 20, 17, '2013-05-07 14:20:05', 0);
INSERT INTO `exercise_choice_score` VALUES (114, 55, 19, 17, '2013-05-07 14:20:05', 1);
INSERT INTO `exercise_choice_score` VALUES (115, 45, 17, 17, '2013-05-07 14:20:05', 0);
INSERT INTO `exercise_choice_score` VALUES (116, 70, 22, 17, '2013-05-07 14:20:05', 0);
INSERT INTO `exercise_choice_score` VALUES (117, 71, 23, 17, '2013-05-07 14:20:05', 1);
INSERT INTO `exercise_choice_score` VALUES (118, 65, 21, 17, '2013-05-07 14:20:05', 0);
INSERT INTO `exercise_choice_score` VALUES (119, 34, 16, 17, '2013-05-07 14:20:05', 1);
INSERT INTO `exercise_choice_score` VALUES (120, 33, 15, 17, '2013-05-07 14:20:05', 0);
INSERT INTO `exercise_choice_score` VALUES (121, 74, 23, 17, '2013-05-07 14:23:03', 0);
INSERT INTO `exercise_choice_score` VALUES (122, 68, 22, 17, '2013-05-07 14:23:03', 0);
INSERT INTO `exercise_choice_score` VALUES (123, 41, 16, 17, '2013-05-07 14:23:03', 0);
INSERT INTO `exercise_choice_score` VALUES (124, 64, 21, 17, '2013-05-07 14:23:03', 0);
INSERT INTO `exercise_choice_score` VALUES (125, 49, 18, 17, '2013-05-07 14:23:03', 0);
INSERT INTO `exercise_choice_score` VALUES (126, 37, 15, 17, '2013-05-07 14:23:03', 0);
INSERT INTO `exercise_choice_score` VALUES (127, 44, 17, 17, '2013-05-07 14:23:03', 0);
INSERT INTO `exercise_choice_score` VALUES (128, 61, 20, 17, '2013-05-07 14:23:03', 0);
INSERT INTO `exercise_choice_score` VALUES (129, 78, 24, 17, '2013-05-07 14:23:03', 0);
INSERT INTO `exercise_choice_score` VALUES (130, 74, 23, 17, '2013-05-07 14:23:05', 0);
INSERT INTO `exercise_choice_score` VALUES (131, 68, 22, 17, '2013-05-07 14:23:05', 0);
INSERT INTO `exercise_choice_score` VALUES (132, 41, 16, 17, '2013-05-07 14:23:05', 0);
INSERT INTO `exercise_choice_score` VALUES (133, 64, 21, 17, '2013-05-07 14:23:05', 0);
INSERT INTO `exercise_choice_score` VALUES (134, 49, 18, 17, '2013-05-07 14:23:05', 0);
INSERT INTO `exercise_choice_score` VALUES (135, 37, 15, 17, '2013-05-07 14:23:05', 0);
INSERT INTO `exercise_choice_score` VALUES (136, 44, 17, 17, '2013-05-07 14:23:05', 0);
INSERT INTO `exercise_choice_score` VALUES (137, 61, 20, 17, '2013-05-07 14:23:05', 0);
INSERT INTO `exercise_choice_score` VALUES (138, 78, 24, 17, '2013-05-07 14:23:05', 0);
INSERT INTO `exercise_choice_score` VALUES (139, 74, 23, 17, '2013-05-07 14:23:10', 0);
INSERT INTO `exercise_choice_score` VALUES (140, 68, 22, 17, '2013-05-07 14:23:10', 0);
INSERT INTO `exercise_choice_score` VALUES (141, 41, 16, 17, '2013-05-07 14:23:10', 0);
INSERT INTO `exercise_choice_score` VALUES (142, 64, 21, 17, '2013-05-07 14:23:10', 0);
INSERT INTO `exercise_choice_score` VALUES (143, 49, 18, 17, '2013-05-07 14:23:10', 0);
INSERT INTO `exercise_choice_score` VALUES (144, 37, 15, 17, '2013-05-07 14:23:10', 0);
INSERT INTO `exercise_choice_score` VALUES (145, 44, 17, 17, '2013-05-07 14:23:10', 0);
INSERT INTO `exercise_choice_score` VALUES (146, 61, 20, 17, '2013-05-07 14:23:10', 0);
INSERT INTO `exercise_choice_score` VALUES (147, 78, 24, 17, '2013-05-07 14:23:10', 0);
INSERT INTO `exercise_choice_score` VALUES (148, 74, 23, 17, '2013-05-07 14:23:12', 0);
INSERT INTO `exercise_choice_score` VALUES (149, 68, 22, 17, '2013-05-07 14:23:12', 0);
INSERT INTO `exercise_choice_score` VALUES (150, 41, 16, 17, '2013-05-07 14:23:12', 0);
INSERT INTO `exercise_choice_score` VALUES (151, 64, 21, 17, '2013-05-07 14:23:12', 0);
INSERT INTO `exercise_choice_score` VALUES (152, 49, 18, 17, '2013-05-07 14:23:12', 0);
INSERT INTO `exercise_choice_score` VALUES (153, 37, 15, 17, '2013-05-07 14:23:12', 0);
INSERT INTO `exercise_choice_score` VALUES (154, 44, 17, 17, '2013-05-07 14:23:12', 0);
INSERT INTO `exercise_choice_score` VALUES (155, 61, 20, 17, '2013-05-07 14:23:12', 0);
INSERT INTO `exercise_choice_score` VALUES (156, 78, 24, 17, '2013-05-07 14:23:12', 0);
INSERT INTO `exercise_choice_score` VALUES (157, 47, 18, 17, '2013-05-07 14:24:21', 0);
INSERT INTO `exercise_choice_score` VALUES (158, 57, 19, 17, '2013-05-07 14:24:21', 0);
INSERT INTO `exercise_choice_score` VALUES (159, 67, 22, 17, '2013-05-07 14:24:21', 1);
INSERT INTO `exercise_choice_score` VALUES (160, 36, 15, 17, '2013-05-07 14:24:21', 0);
INSERT INTO `exercise_choice_score` VALUES (161, 40, 16, 17, '2013-05-07 14:24:21', 0);
INSERT INTO `exercise_choice_score` VALUES (162, 43, 17, 17, '2013-05-07 14:24:21', 0);
INSERT INTO `exercise_choice_score` VALUES (163, 66, 21, 17, '2013-05-07 14:24:21', 0);
INSERT INTO `exercise_choice_score` VALUES (164, 61, 20, 17, '2013-05-07 14:24:21', 0);
INSERT INTO `exercise_choice_score` VALUES (165, 47, 18, 17, '2013-05-07 14:25:00', 0);
INSERT INTO `exercise_choice_score` VALUES (166, 57, 19, 17, '2013-05-07 14:25:00', 0);
INSERT INTO `exercise_choice_score` VALUES (167, 67, 22, 17, '2013-05-07 14:25:00', 1);
INSERT INTO `exercise_choice_score` VALUES (168, 36, 15, 17, '2013-05-07 14:25:00', 0);
INSERT INTO `exercise_choice_score` VALUES (169, 40, 16, 17, '2013-05-07 14:25:00', 0);
INSERT INTO `exercise_choice_score` VALUES (170, 43, 17, 17, '2013-05-07 14:25:00', 0);
INSERT INTO `exercise_choice_score` VALUES (171, 66, 21, 17, '2013-05-07 14:25:00', 0);
INSERT INTO `exercise_choice_score` VALUES (172, 61, 20, 17, '2013-05-07 14:25:00', 0);
INSERT INTO `exercise_choice_score` VALUES (173, 61, 20, 17, '2013-05-07 14:25:18', 0);
INSERT INTO `exercise_choice_score` VALUES (174, 45, 17, 17, '2013-05-07 14:25:18', 0);
INSERT INTO `exercise_choice_score` VALUES (175, 48, 18, 17, '2013-05-07 14:25:18', 0);
INSERT INTO `exercise_choice_score` VALUES (176, 58, 19, 17, '2013-05-07 14:25:18', 0);
INSERT INTO `exercise_choice_score` VALUES (177, 34, 16, 17, '2013-05-07 14:25:18', 1);
INSERT INTO `exercise_choice_score` VALUES (178, 69, 22, 17, '2013-05-07 14:25:18', 0);
INSERT INTO `exercise_choice_score` VALUES (179, 36, 15, 17, '2013-05-07 14:25:18', 0);
INSERT INTO `exercise_choice_score` VALUES (180, 75, 24, 17, '2013-05-07 14:25:18', 1);
INSERT INTO `exercise_choice_score` VALUES (181, 61, 20, 17, '2013-05-07 14:25:59', 0);
INSERT INTO `exercise_choice_score` VALUES (182, 45, 17, 17, '2013-05-07 14:25:59', 0);
INSERT INTO `exercise_choice_score` VALUES (183, 48, 18, 17, '2013-05-07 14:25:59', 0);
INSERT INTO `exercise_choice_score` VALUES (184, 58, 19, 17, '2013-05-07 14:25:59', 0);
INSERT INTO `exercise_choice_score` VALUES (185, 34, 16, 17, '2013-05-07 14:25:59', 1);
INSERT INTO `exercise_choice_score` VALUES (186, 69, 22, 17, '2013-05-07 14:25:59', 0);
INSERT INTO `exercise_choice_score` VALUES (187, 36, 15, 17, '2013-05-07 14:25:59', 0);
INSERT INTO `exercise_choice_score` VALUES (188, 75, 24, 17, '2013-05-07 14:25:59', 1);
INSERT INTO `exercise_choice_score` VALUES (189, 71, 23, 17, '2013-05-07 14:34:32', 1);
INSERT INTO `exercise_choice_score` VALUES (190, 47, 18, 17, '2013-05-07 16:10:19', 0);
INSERT INTO `exercise_choice_score` VALUES (191, 64, 21, 17, '2013-05-07 16:10:19', 0);
INSERT INTO `exercise_choice_score` VALUES (192, 59, 20, 17, '2013-05-07 16:10:19', 1);
INSERT INTO `exercise_choice_score` VALUES (193, 55, 19, 17, '2013-05-07 16:10:19', 1);
INSERT INTO `exercise_choice_score` VALUES (194, 45, 17, 17, '2013-05-07 16:10:19', 0);
INSERT INTO `exercise_choice_score` VALUES (195, 40, 16, 17, '2013-05-07 16:10:19', 0);
INSERT INTO `exercise_choice_score` VALUES (196, 72, 23, 17, '2013-05-07 16:10:19', 0);
INSERT INTO `exercise_choice_score` VALUES (197, 41, 16, 17, '2013-05-07 17:07:45', 0);
INSERT INTO `exercise_choice_score` VALUES (198, 67, 22, 17, '2013-05-07 17:07:45', 1);
INSERT INTO `exercise_choice_score` VALUES (199, 75, 24, 17, '2013-05-07 17:07:45', 1);
INSERT INTO `exercise_choice_score` VALUES (200, 58, 19, 17, '2013-05-07 17:07:45', 0);
INSERT INTO `exercise_choice_score` VALUES (201, 71, 23, 17, '2013-05-07 17:07:45', 1);
INSERT INTO `exercise_choice_score` VALUES (202, 45, 17, 17, '2013-05-07 17:07:45', 0);
INSERT INTO `exercise_choice_score` VALUES (203, 48, 18, 17, '2013-05-07 17:07:45', 0);
INSERT INTO `exercise_choice_score` VALUES (204, 36, 15, 17, '2013-05-07 17:07:45', 0);
INSERT INTO `exercise_choice_score` VALUES (205, 60, 20, 17, '2013-05-07 17:07:45', 0);
INSERT INTO `exercise_choice_score` VALUES (206, 34, 16, 17, '2013-05-07 19:01:18', 1);
INSERT INTO `exercise_choice_score` VALUES (207, 71, 23, 17, '2013-05-07 19:01:18', 1);
INSERT INTO `exercise_choice_score` VALUES (208, 55, 19, 17, '2013-05-07 19:01:18', 1);
INSERT INTO `exercise_choice_score` VALUES (209, 32, 15, 17, '2013-05-07 19:01:18', 1);
INSERT INTO `exercise_choice_score` VALUES (210, 75, 24, 17, '2013-05-07 19:01:18', 1);
INSERT INTO `exercise_choice_score` VALUES (211, 46, 18, 17, '2013-05-07 19:01:18', 1);
INSERT INTO `exercise_choice_score` VALUES (212, 59, 20, 17, '2013-05-07 19:01:18', 1);
INSERT INTO `exercise_choice_score` VALUES (213, 44, 17, 17, '2013-05-07 19:01:18', 0);
INSERT INTO `exercise_choice_score` VALUES (214, 67, 22, 17, '2013-05-07 19:01:18', 1);
INSERT INTO `exercise_choice_score` VALUES (215, 35, 16, 17, '2013-05-07 19:02:33', 0);
INSERT INTO `exercise_choice_score` VALUES (216, 67, 22, 17, '2013-05-07 19:02:33', 1);
INSERT INTO `exercise_choice_score` VALUES (217, 59, 20, 17, '2013-05-07 19:02:33', 1);
INSERT INTO `exercise_choice_score` VALUES (218, 46, 18, 17, '2013-05-07 19:02:33', 1);
INSERT INTO `exercise_choice_score` VALUES (219, 71, 23, 17, '2013-05-07 19:02:33', 1);
INSERT INTO `exercise_choice_score` VALUES (220, 65, 21, 17, '2013-05-07 19:02:33', 0);
INSERT INTO `exercise_choice_score` VALUES (221, 32, 15, 17, '2013-05-07 19:02:33', 1);
INSERT INTO `exercise_choice_score` VALUES (222, 55, 19, 17, '2013-05-07 19:02:33', 1);
INSERT INTO `exercise_choice_score` VALUES (223, 43, 17, 17, '2013-05-07 19:02:33', 0);
INSERT INTO `exercise_choice_score` VALUES (224, 69, 22, 17, '2013-05-07 19:03:05', 0);
INSERT INTO `exercise_choice_score` VALUES (225, 75, 24, 17, '2013-05-07 19:03:05', 1);
INSERT INTO `exercise_choice_score` VALUES (226, 46, 18, 17, '2013-05-07 19:03:05', 1);
INSERT INTO `exercise_choice_score` VALUES (227, 32, 15, 17, '2013-05-07 19:03:05', 1);
INSERT INTO `exercise_choice_score` VALUES (228, 43, 17, 17, '2013-05-07 19:03:05', 0);
INSERT INTO `exercise_choice_score` VALUES (229, 34, 16, 17, '2013-05-07 19:03:05', 1);
INSERT INTO `exercise_choice_score` VALUES (230, 59, 20, 17, '2013-05-07 19:03:05', 1);
INSERT INTO `exercise_choice_score` VALUES (231, 65, 21, 17, '2013-05-07 19:03:05', 0);
INSERT INTO `exercise_choice_score` VALUES (232, 65, 21, 17, '2013-05-08 11:22:00', 0);
INSERT INTO `exercise_choice_score` VALUES (233, 42, 17, 17, '2013-05-08 11:22:00', 1);
INSERT INTO `exercise_choice_score` VALUES (234, 55, 19, 17, '2013-05-08 11:22:00', 1);
INSERT INTO `exercise_choice_score` VALUES (235, 67, 22, 17, '2013-05-08 11:22:00', 1);
INSERT INTO `exercise_choice_score` VALUES (236, 75, 24, 17, '2013-05-08 11:22:00', 1);
INSERT INTO `exercise_choice_score` VALUES (237, 34, 16, 17, '2013-05-08 11:22:00', 1);
INSERT INTO `exercise_choice_score` VALUES (238, 32, 15, 17, '2013-05-08 11:22:00', 1);
INSERT INTO `exercise_choice_score` VALUES (239, 46, 18, 17, '2013-05-08 11:22:00', 1);
INSERT INTO `exercise_choice_score` VALUES (240, 59, 20, 17, '2013-05-08 11:22:00', 1);
INSERT INTO `exercise_choice_score` VALUES (241, 44, 17, 17, '2013-05-08 11:55:54', 0);
INSERT INTO `exercise_choice_score` VALUES (242, 55, 19, 17, '2013-05-08 11:55:54', 1);
INSERT INTO `exercise_choice_score` VALUES (243, 32, 15, 17, '2013-05-08 11:55:54', 1);
INSERT INTO `exercise_choice_score` VALUES (244, 46, 18, 17, '2013-05-08 11:55:54', 1);
INSERT INTO `exercise_choice_score` VALUES (245, 50, 16, 17, '2013-05-08 11:55:54', 0);
INSERT INTO `exercise_choice_score` VALUES (246, 67, 22, 17, '2013-05-08 11:55:54', 1);
INSERT INTO `exercise_choice_score` VALUES (247, 65, 21, 17, '2013-05-08 11:55:54', 0);
INSERT INTO `exercise_choice_score` VALUES (248, 71, 23, 17, '2013-05-08 11:55:54', 1);
INSERT INTO `exercise_choice_score` VALUES (249, 59, 20, 17, '2013-05-08 11:55:54', 1);
INSERT INTO `exercise_choice_score` VALUES (250, 55, 19, 17, '2013-05-08 12:03:13', 1);
INSERT INTO `exercise_choice_score` VALUES (251, 42, 17, 17, '2013-05-08 12:03:13', 1);
INSERT INTO `exercise_choice_score` VALUES (252, 34, 16, 17, '2013-05-08 12:03:13', 1);
INSERT INTO `exercise_choice_score` VALUES (253, 46, 18, 17, '2013-05-08 12:03:13', 1);
INSERT INTO `exercise_choice_score` VALUES (254, 33, 15, 17, '2013-05-08 12:03:13', 0);
INSERT INTO `exercise_choice_score` VALUES (255, 67, 22, 17, '2013-05-08 12:03:13', 1);
INSERT INTO `exercise_choice_score` VALUES (256, 75, 24, 17, '2013-05-08 12:03:13', 1);
INSERT INTO `exercise_choice_score` VALUES (257, 59, 20, 17, '2013-05-08 12:03:13', 1);
INSERT INTO `exercise_choice_score` VALUES (258, 65, 21, 17, '2013-05-08 12:03:13', 0);
INSERT INTO `exercise_choice_score` VALUES (259, 55, 19, 17, '2013-05-08 12:03:53', 1);
INSERT INTO `exercise_choice_score` VALUES (260, 59, 20, 17, '2013-05-08 12:03:53', 1);
INSERT INTO `exercise_choice_score` VALUES (261, 46, 18, 17, '2013-05-08 12:03:53', 1);
INSERT INTO `exercise_choice_score` VALUES (262, 33, 15, 17, '2013-05-08 12:03:53', 0);
INSERT INTO `exercise_choice_score` VALUES (263, 65, 21, 17, '2013-05-08 12:03:53', 0);
INSERT INTO `exercise_choice_score` VALUES (264, 75, 24, 17, '2013-05-08 12:03:53', 1);
INSERT INTO `exercise_choice_score` VALUES (265, 42, 17, 17, '2013-05-08 12:03:53', 1);
INSERT INTO `exercise_choice_score` VALUES (266, 67, 22, 17, '2013-05-08 12:03:53', 1);
INSERT INTO `exercise_choice_score` VALUES (267, 71, 23, 17, '2013-05-08 12:03:53', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `exercise_question`
-- 

CREATE TABLE `exercise_question` (
  `id` int(11) NOT NULL auto_increment,
  `question` text character set utf8 NOT NULL,
  `show_order` tinyint(2) NOT NULL default '1',
  `type` tinyint(1) NOT NULL default '1' COMMENT '1 choic 2textfield',
  `exercise_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `exercise_id` (`exercise_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=37 ;

-- 
-- Dumping data for table `exercise_question`
-- 

INSERT INTO `exercise_question` VALUES (15, 'ข้อใดเป็นคำลงท้ายของจดหมายราชการ', 2, 1, 4);
INSERT INTO `exercise_question` VALUES (16, '	 รูปสระ " มีชื่อเรียกว่าอะไร', 1, 1, 4);
INSERT INTO `exercise_question` VALUES (17, 'ข้อใดมีคำควบกล้ำมากที่สุด', 10, 1, 4);
INSERT INTO `exercise_question` VALUES (18, 'โรคฝีดาษระบาดในสมัยใด', 8, 1, 4);
INSERT INTO `exercise_question` VALUES (19, 'อักษรไทยมีกี่ตัวอักษร', 9, 1, 4);
INSERT INTO `exercise_question` VALUES (20, 'ใครประดิษฐ์ตัวอักษรไทย', 6, 1, 4);
INSERT INTO `exercise_question` VALUES (21, 'ธงชาติไทยมีกี่สี', 7, 1, 4);
INSERT INTO `exercise_question` VALUES (22, 'ประเทศใดอนาเขตติดกับไทย', 5, 1, 4);
INSERT INTO `exercise_question` VALUES (23, 'การคิด พ.ศ. จาก ค.ศ. ทำอย่างไร', 3, 1, 4);
INSERT INTO `exercise_question` VALUES (24, 'ประเทศไทยมีกี่จังหวัด', 4, 1, 4);
INSERT INTO `exercise_question` VALUES (33, '<p>wwww___wwwww</p>\r\n', 1, 2, 10);
INSERT INTO `exercise_question` VALUES (34, '<p>3</p>\r\n', 2, 1, 10);
INSERT INTO `exercise_question` VALUES (35, '<p>She __ a cat.</p>\r\n', 1, 2, 10);
INSERT INTO `exercise_question` VALUES (36, '<p>what is __name?&nbsp;</p>\r\n<p>My name is __.</p>\r\n', 1, 2, 4);

-- --------------------------------------------------------

-- 
-- Table structure for table `exercise_stat`
-- 

CREATE TABLE `exercise_stat` (
  `id` int(11) NOT NULL auto_increment,
  `exercise_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `do_time` datetime NOT NULL,
  `qty_question` int(3) NOT NULL default '0' COMMENT 'จำนวนคำถามใน Exercise',
  `qty_correct` int(3) NOT NULL default '0' COMMENT 'จำนวนข้อที่ตอบถูก',
  `question_list` varchar(300) collate utf8_unicode_ci default NULL COMMENT 'คำถามที่ถูกเลือกมาทำในแบบทดสอบ',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

-- 
-- Dumping data for table `exercise_stat`
-- 

INSERT INTO `exercise_stat` VALUES (1, 4, 17, '2013-05-07 14:33:50', 10, 1, '16,22,24,19,23,36,36,17,18,15,20');
INSERT INTO `exercise_stat` VALUES (2, 4, 17, '2013-05-07 14:34:32', 1, 1, '16,22,24,19,23,36,36,17,18,15,20');
INSERT INTO `exercise_stat` VALUES (3, 4, 17, '2013-05-07 16:10:19', 10, 3, '16,22,24,19,23,36,36,17,18,15,20');
INSERT INTO `exercise_stat` VALUES (4, 4, 17, '2013-05-07 17:07:45', 10, 4, '16,22,24,19,23,36,36,17,18,15,20');
INSERT INTO `exercise_stat` VALUES (5, 4, 17, '2013-05-07 18:43:47', 10, 0, '19,16,17,22,36,21,18,24,15,23');
INSERT INTO `exercise_stat` VALUES (6, 4, 17, '2013-05-07 19:01:18', 10, 9, '16,23,19,36,15,24,18,20,17,22');
INSERT INTO `exercise_stat` VALUES (7, 4, 17, '2013-05-07 19:02:33', 10, 7, '16,36,22,20,18,23,21,15,19,17');
INSERT INTO `exercise_stat` VALUES (8, 4, 17, '2013-05-07 19:03:05', 10, 6, '22,24,18,19,15,17,16,20,36,21');
INSERT INTO `exercise_stat` VALUES (9, 4, 17, '2013-05-08 11:22:00', 10, 9, '21,36,17,19,22,24,16,15,18,20');
INSERT INTO `exercise_stat` VALUES (10, 4, 17, '2013-05-08 11:55:54', 10, 7, '17,19,15,18,16,22,36,21,23,20');
INSERT INTO `exercise_stat` VALUES (11, 4, 17, '2013-05-08 12:03:13', 10, 8, '19,17,16,36,18,15,22,24,20,21');
INSERT INTO `exercise_stat` VALUES (12, 4, 17, '2013-05-08 12:03:53', 10, 8, '19,20,18,15,21,36,24,17,22,23');

-- --------------------------------------------------------

-- 
-- Table structure for table `exercise_text_answer`
-- 

CREATE TABLE `exercise_text_answer` (
  `id` int(11) NOT NULL auto_increment,
  `answer` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `question_id` int(11) NOT NULL,
  `sequence` int(2) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `question_id` (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ตารางเก็บคำตอบ ของ question แบบ text' AUTO_INCREMENT=10 ;

-- 
-- Dumping data for table `exercise_text_answer`
-- 

INSERT INTO `exercise_text_answer` VALUES (6, 'www', 33, 1);
INSERT INTO `exercise_text_answer` VALUES (7, 'has', 35, 1);
INSERT INTO `exercise_text_answer` VALUES (8, '1', 36, 1);
INSERT INTO `exercise_text_answer` VALUES (9, '2', 36, 2);

-- --------------------------------------------------------

-- 
-- Table structure for table `exercise_text_score`
-- 

CREATE TABLE `exercise_text_score` (
  `id` int(11) NOT NULL auto_increment,
  `question_id` int(11) NOT NULL,
  `answer` varchar(3000) collate utf8_unicode_ci default NULL,
  `user_id` int(11) NOT NULL,
  `do_time` datetime NOT NULL,
  `sequence` tinyint(3) NOT NULL default '1',
  `is_true` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `question_id` (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ตารางเก็บ คำตอบ คำถามประเภท text' AUTO_INCREMENT=55 ;

-- 
-- Dumping data for table `exercise_text_score`
-- 

INSERT INTO `exercise_text_score` VALUES (51, 36, '1', 17, '2013-05-08 12:03:53', 1, 1);
INSERT INTO `exercise_text_score` VALUES (52, 36, '2', 17, '2013-05-08 12:03:53', 2, 1);
INSERT INTO `exercise_text_score` VALUES (53, 36, '1', 4, '2013-05-08 11:22:30', 1, 0);
INSERT INTO `exercise_text_score` VALUES (54, 36, '2', 4, '2013-05-08 11:22:30', 2, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `file`
-- 

CREATE TABLE `file` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `original_name` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(100) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=76 ;

-- 
-- Dumping data for table `file`
-- 

INSERT INTO `file` VALUES (23, 'rhre0rufl6vca4nms6gkjeokwpdez0snoy2qcue3ait5n3eea5rgxub_1.mp4', '1.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (26, 'eot3ocijmhaureaqe0nnyx3rge5vdup6r0skfeab46uk2szgercwn5l_4.mp4', '4.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (27, 'uxyuondm02rsz6bvt5ecpeke30q4rguahai6scekarg5wrneejo3nlf_1.mp4', '1.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (28, 'haasenv662yklngwcbrepuu3re3e5rd0mxknjegfroec5uqos4t0iaz_2.mp4', '2.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (29, 'c5abghvpwek4satrujckzr6gy06enux5ernr3a2luqfeeeosoi0m3dn_4.mp4', '4.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (30, '4an5ftvuzumnjoe0grco3nraed2au6xygrwsi0es53ehpc6qkkerlbe_1.mp4', '1.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (31, '6e0rnv5ztayghlau3iperbcxnonfg0ecwkoasm2usrre4kd6j5eeuq3_4.mp4', '4.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (32, 'xrscn304dky5fzual0vi6jqe5prrhutersonua2eeow3agcge6kenbm_1.mp4', '1.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (34, '53aqh6uui4rlwnakreaxds0ngcke2scepzfmr0e6b3utgvjeeyoon5r_codeschool_301.mp4', 'codeschool_301.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (35, 'febd3o3e0rwmaex4u6an2pgguilynuejkhzcervrssekao5c0t65nqr_codeschool_301.mp4', 'codeschool_301.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (36, 'rzhjexuasbntyg2remiko4o5r30g6qere3wneu0laevsccfdp6nkua5_codeschool_304.mp4', 'codeschool_304.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (37, '5n3emkrw30p26ejsuxokbvenrl0hc4aeeu5fezanqrusgiygcarotd6_codeschool_303.mp4', 'codeschool_303.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (39, 'erjge0rsnfpeco5a5ouykvadrtwe3k462ex0zub3cun6ailhsmnrgqe_codeschool_301.mp4', 'codeschool_301.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (40, 'xrmtqsaow2ysn0nz3jcahvkee6e4rricgea5pgukdlo5ebr6u0nufe3_codeschool_304.mp4', 'codeschool_304.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (41, '5p33eudgeavuxm5kfzg0wlrheeaync66n0sirej2eaq4obrnktcsruo_1.mp4', '1.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (42, 'egwoe20v0rauisj3t6decymr6nr4eocuxn5gufrsabzeakkln53pqhe_coupong template.txt', 'coupong template.txt', 4, 'vdo');
INSERT INTO `file` VALUES (43, 'rhkoiu2fgtsu6o6nqn0derv3e3me4enrwa0eagc5xpcklsr5uajzeyb_confirm del box.txt', 'confirm del box.txt', 4, 'vdo');
INSERT INTO `file` VALUES (44, 'tasuo2nnvn5zqfwkderuiachr6mb36usrej05crogagxeeeply3ke40_8.mp4', '8.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (45, 'wr0ebxkkmhn2r6nseco4evrategrulieyna5guf6e0ucso3ajdq53zp_7.mp4', '7.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (46, 'lmdajrpanhk2ewcnuekssxyo0g30qzeorareet3eiuurfc465g65vbn_1.mp4', '1.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (47, 'e6elexsqcrgfnra4mzsahuwoe3jr6ko5t0yn2cg5vu3dibkp0neurae_4.mp4', '4.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (48, 'bndn3erh6ajl6kmvgn3gceeesqarfy5c4xrpuakutr5iews2ou00oez_codeschool_301.mp4', 'codeschool_301.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (49, 'jroyrvno36b3fcng2u65ri4keegns5adeamueqhu0swxtaezelp0rck_codeschool_305.mp4', 'codeschool_305.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (50, 'rr5tgph5w2gqykju3cr4emvulsonx06uefoe063eseezdbaarikacnn_codeschool_305.mp4', 'codeschool_305.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (51, '3m0eocylse6fjxea40ekrnrcprkar6ninetguev2ubasgqd535uhzwo_codeschool_305.mp4', 'codeschool_305.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (52, 'rq5jdg2vcnsn6epksan4mb0oerkayefagelzt36u0rheruw3ieoxu5c_codeschool_305.mp4', 'codeschool_305.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (53, 'g0skbjcirw6aueuvflkmxt04pre3qesnn2e5e53ohueryarzacondg6_codeschool_305.mp4', 'codeschool_305.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (54, '6rw3em3eygrnerk4sheedcajuorfa6bigznn5o0pasclut2ukq5evx0_codeschool_305.mp4', 'codeschool_305.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (55, '6e2aj4aukvu3e5qngwccy0lbhk3eennogem5s6rauosfzxrit0pderr_codeschool_305.mp4', 'codeschool_305.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (56, 'ohbrzgsc0usrce3rmj0arin2l5paduku4vqexfee3et5gy6oa6kwnne_codeschool_304.mp4', 'codeschool_304.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (57, '33zgx6wol0eamsb2urrpreknuanoece06dyivhj5asgtneqrucfe45k_codeschool_304.mp4', 'codeschool_304.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (58, '6o2eeecnfaptgarev0kjmoez54herrklsgs65n0uub3wcadixqur3ny_codeschool_305.mp4', 'codeschool_305.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (59, 'm6ze3evnrlefbes25ghopu0syonjrdweeu0aaa5rikn4grxqtu6kcc3_codeschool_304.mp4', 'codeschool_304.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (60, 'ugqyeovinror5rw3kesf4nzsdhuc32gacab0n6pxaj6etee5uk0emrl_codeschool_305.mp4', 'codeschool_305.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (61, 'n06ox0nz3py42emw6algnaaiub5ordretfksecjhguscer5vkeue3qr_codeschool_305.mp4', 'codeschool_305.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (62, 'unhajwdose6kknaueinrcfvyse3xz4eg2tr50ebm6rc0gru5pqa3eol_codeschool_304.mp4', 'codeschool_304.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (63, 'er60ukpfaes6ntgdko3xra0e2cnc4uenbuq3lzga5iheorsywjve5mr_codeschool_304.mp4', 'codeschool_304.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (64, 'wtyqosnxgr6fzrrmarhe0lk6udee4ab3seic2vueu3neka5njgcop50_codeschool_305.mp4', 'codeschool_305.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (65, 'rgruousaan6pwji2frexuennc5ledbyvzerqgksm5och36t34e0ake0_codeschool_304.mp4', 'codeschool_304.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (66, 'afzc5trrve2ngjonays0xs4nk3elgq3euuk5wbmirrceoaue66edph0_codeschool_304.mp4', 'codeschool_304.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (67, 'eofaeydekqbt64e3nre20sk5rnuospmxnwcgr6iacva3rguhjeuz0l5_codeschool_304.mp4', 'codeschool_304.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (68, '46moikspyle6nvk3rbo2nagear03f55ajrehcrceszxgudu0eqntewu_codeschool_304.mp4', 'codeschool_304.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (69, '4utz3nrjhlx5koug6cpgbosvn0e2yr6e0qaianf3ceerm5eskduwaer_codeschool_302.mp4', 'codeschool_302.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (70, 'eunbephz5re0iv6ouewgag23omjrcd06qkrek3nseacyrs5uxnaltf4_codeschool_305.mp4', 'codeschool_305.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (71, 'baz6fs2n0dsce5kremtlqceran6wu5rxkgrnp3jue4hev0eaygo3uio_codeschool_302.mp4', 'codeschool_302.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (72, 'mxa0syfrgpkebsncark35rezqgt2ewieaohuru6d6ce3lejonv04u5n_codeschool_305.mp4', 'codeschool_305.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (73, '2zeeshilnurgx650uau4cnmaykkaedrnws65jr3orceqop0bfe3evgt_codeschool_305.mp4', 'codeschool_305.mp4', 17, 'vdo');
INSERT INTO `file` VALUES (74, 'aehai6ubzywsro5ne4xur5vdr2cj3gmnp0rgousnaek6l0eqctee3kf_codeschool_301.mp4', 'codeschool_301.mp4', 17, 'vdo');
INSERT INTO `file` VALUES (75, 'p5x00nogerzeuyrgevudfrbinrenkla6ce5oa4skqc3ems6t32jhwau_3.mp4', '3.mp4', 4, 'vdo');

-- --------------------------------------------------------

-- 
-- Table structure for table `file_content`
-- 

CREATE TABLE `file_content` (
  `id` int(11) NOT NULL auto_increment,
  `file_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `file_id` (`file_id`),
  KEY `content_id` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `file_content`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `relate_course`
-- 

CREATE TABLE `relate_course` (
  `id` int(11) NOT NULL auto_increment,
  `course_id` int(11) NOT NULL,
  `before_course` varchar(100) collate utf8_unicode_ci default NULL,
  `after_course` varchar(100) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `relate_course`
-- 

INSERT INTO `relate_course` VALUES (1, 1, '6,9,10', '4,6,7,10,11');
INSERT INTO `relate_course` VALUES (2, 9, '1', '1');

-- --------------------------------------------------------

-- 
-- Stand-in structure for view `stat_user_study`
-- 
CREATE TABLE `stat_user_study` (
`content_has_studied` bigint(21)
,`user_id` int(11)
,`course_id` int(11)
);
-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_migration`
-- 

CREATE TABLE `tbl_migration` (
  `version` varchar(255) collate utf8_unicode_ci NOT NULL,
  `apply_time` int(11) default NULL,
  PRIMARY KEY  (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table `tbl_migration`
-- 

INSERT INTO `tbl_migration` VALUES ('m000000_000000_base', 1363842238);

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_profiles`
-- 

CREATE TABLE `tbl_profiles` (
  `user_id` int(11) NOT NULL auto_increment,
  `lastname` varchar(50) NOT NULL default '',
  `firstname` varchar(50) NOT NULL default '',
  `detail` varchar(3000) default NULL,
  `photo` varchar(500) default NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

-- 
-- Dumping data for table `tbl_profiles`
-- 

INSERT INTO `tbl_profiles` VALUES (1, 'Admin', 'Administrator', NULL, '298thul6kcazvrd8creeq7ysu4wog03epeisxobmjf81nk85r_IMG_0937.JPG');
INSERT INTO `tbl_profiles` VALUES (2, 'Demo', 'Demo', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (4, 'prasomsri', 'sarun', '222222222222222222222', '88i4jex29fc6uc8spsk3y8750kzurhelnrdoormbewtg1eaqv_sunny.jpg');
INSERT INTO `tbl_profiles` VALUES (5, '', '', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (6, '', '', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (7, '', '', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (8, 'prasomsri', 'sarun', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (9, 'eeeeeeeeeeeee', 'bbbb', '', NULL);
INSERT INTO `tbl_profiles` VALUES (10, 'ทดสอบ', 'sarun112', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (11, 'ss', 'ss', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (12, 'prasomsri', 'กัน', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (13, 'ศรัณย์', 'sarun', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (14, 'ศรัณย์', 'sarun', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (15, 'prasomsri', 'sarun', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (16, '', '', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (17, '', '', NULL, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_profiles_fields`
-- 

CREATE TABLE `tbl_profiles_fields` (
  `id` int(10) NOT NULL auto_increment,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` varchar(15) NOT NULL default '0',
  `field_size_min` varchar(15) NOT NULL default '0',
  `required` int(1) NOT NULL default '0',
  `match` varchar(255) NOT NULL default '',
  `range` varchar(255) NOT NULL default '',
  `error_message` varchar(255) NOT NULL default '',
  `other_validator` varchar(5000) NOT NULL default '',
  `default` varchar(255) NOT NULL default '',
  `widget` varchar(255) NOT NULL default '',
  `widgetparams` varchar(5000) NOT NULL default '',
  `position` int(3) NOT NULL default '0',
  `visible` int(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `tbl_profiles_fields`
-- 

INSERT INTO `tbl_profiles_fields` VALUES (1, 'lastname', 'Last Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 1, 3);
INSERT INTO `tbl_profiles_fields` VALUES (2, 'firstname', 'First Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 0, 3);

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_users`
-- 

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activkey` varchar(128) NOT NULL default '',
  `create_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `lastvisit` timestamp NOT NULL default '0000-00-00 00:00:00',
  `superuser` int(1) NOT NULL default '0',
  `status` int(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

-- 
-- Dumping data for table `tbl_users`
-- 

INSERT INTO `tbl_users` VALUES (1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'webmaster@example.com', '9a24eff8c15a6a141ece27eb6947da0f', '2013-03-21 13:29:02', '2013-04-11 16:33:49', 1, 1);
INSERT INTO `tbl_users` VALUES (2, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'demo@example.com', '099f825543f7850cc038b90aaff39fac', '2013-03-21 13:29:02', '2013-03-28 14:54:25', 0, 1);
INSERT INTO `tbl_users` VALUES (4, 'gun', '81dc9bdb52d04dc20036dbd8313ed055', 'gunsarun@gmail.com', '1de177922186eeb3e7cba41f07f56399', '2013-03-21 15:49:38', '2013-05-08 11:24:26', 1, 1);
INSERT INTO `tbl_users` VALUES (5, 'arnupharp@larngeartech.com', 'f561aaf6ef0bf14d4208bb46a4ccb3ad', 'arnupharp@larngeartech.com', 'b0c33778992e1d78b56bfebfff186801', '2013-03-27 13:41:14', '0000-00-00 00:00:00', 0, 1);
INSERT INTO `tbl_users` VALUES (6, 'topscores@gmail.com', '1b1399f21b2fefa7602555883b4c42e5', 'topscores@gmail.com', 'b469d40dc5d4183b3e80ee0d0080a31d', '2013-03-28 12:21:21', '0000-00-00 00:00:00', 0, 1);
INSERT INTO `tbl_users` VALUES (7, 'sarun@larngeartech.com', '827ccb0eea8a706c4c34a16891f84e7b', 'sarun@larngeartech.com', 'c4137cb8e7c74880f72e988e2d0b64d3', '2013-03-29 14:06:06', '0000-00-00 00:00:00', 0, 1);
INSERT INTO `tbl_users` VALUES (8, 'gunsarun1@gmail.com', '5c74061189c0b65362569830e05f7542', 'gunsarun1@gmail.com', 'a0427fcc1f1bc768a6852f5732a4b93f', '2013-03-29 17:15:17', '0000-00-00 00:00:00', 0, 1);
INSERT INTO `tbl_users` VALUES (9, 'ping@ping.com', 'df911f0151f9ef021d410b4be5060972', 'ping@ping.com', '00b198f6a162784788be1669a5efb309', '2013-04-29 14:16:23', '0000-00-00 00:00:00', 0, 1);
INSERT INTO `tbl_users` VALUES (10, 'abcd@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'abcd@gmail.com', 'a778583a2e01156ea5c9d0b7d0d946ac', '2013-04-29 17:48:31', '0000-00-00 00:00:00', 0, 1);
INSERT INTO `tbl_users` VALUES (11, 'gungun@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'gungun@gmail.com', 'e4efbae051eeb171f8901ba2775d6c6e', '2013-04-29 17:53:09', '0000-00-00 00:00:00', 0, 1);
INSERT INTO `tbl_users` VALUES (12, 'leon@gmail.com', 'd54d1702ad0f8326224b817c796763c9', 'leon@gmail.com', '4e7137ef07fc22b9bf097a8779f736eb', '2013-04-30 14:21:52', '2013-04-30 15:23:41', 0, 1);
INSERT INTO `tbl_users` VALUES (13, 'gun3004@gmail.com', 'd54d1702ad0f8326224b817c796763c9', 'gun3004@gmail.com', '764ecdf1e617f456796e2300dd18382a', '2013-04-30 17:14:04', '0000-00-00 00:00:00', 0, 1);
INSERT INTO `tbl_users` VALUES (14, 'gun3004@gmail1.com', 'd54d1702ad0f8326224b817c796763c9', 'gun3004@gmail1.com', '66d7173acc88b68cd83aac295f9194ee', '2013-04-30 17:14:59', '0000-00-00 00:00:00', 0, 1);
INSERT INTO `tbl_users` VALUES (15, 'gun@gun.com', 'b0baee9d279d34fa1dfd71aadb908c3f', 'gun@gun.com', '5638a14648a8799dac2f584c2fe6024a', '2013-04-30 17:17:50', '0000-00-00 00:00:00', 0, 1);
INSERT INTO `tbl_users` VALUES (16, 'gun12@gmail.com', 'd54d1702ad0f8326224b817c796763c9', 'gun12@gmail.com', '174afd470e27aab6d47fee9a5708a7b8', '2013-05-01 11:06:23', '2013-05-01 12:06:47', 0, 1);
INSERT INTO `tbl_users` VALUES (17, 'guntest@gmail.com', 'd54d1702ad0f8326224b817c796763c9', 'guntest@gmail.com', '8f2274b9ccfc8cdf2fab253001f7ebb4', '2013-05-07 10:26:03', '2013-05-08 11:55:21', 0, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `user_content_log`
-- 

CREATE TABLE `user_content_log` (
  `id` int(11) NOT NULL auto_increment,
  `content_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `study_time` datetime NOT NULL COMMENT 'วันเวลาที่เรียน',
  PRIMARY KEY  (`id`),
  KEY `content_id` (`content_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ตาราง เก็บ ความสัมพันธ์ ระหว่าง user และ Course ที่เรัยนเพื่' AUTO_INCREMENT=70 ;

-- 
-- Dumping data for table `user_content_log`
-- 

INSERT INTO `user_content_log` VALUES (1, 23, 9, '2013-04-29 15:59:15');
INSERT INTO `user_content_log` VALUES (2, 26, 9, '2013-04-29 15:38:48');
INSERT INTO `user_content_log` VALUES (3, 25, 9, '2013-04-29 15:59:13');
INSERT INTO `user_content_log` VALUES (4, 38, 9, '2013-04-29 15:59:11');
INSERT INTO `user_content_log` VALUES (5, 34, 9, '2013-04-29 15:39:16');
INSERT INTO `user_content_log` VALUES (6, 68, 9, '2013-04-29 15:59:00');
INSERT INTO `user_content_log` VALUES (7, 82, 9, '2013-04-29 17:40:35');
INSERT INTO `user_content_log` VALUES (8, 83, 9, '2013-04-29 17:40:36');
INSERT INTO `user_content_log` VALUES (9, 130, 9, '2013-04-29 17:40:37');
INSERT INTO `user_content_log` VALUES (10, 65, 9, '2013-04-29 16:01:56');
INSERT INTO `user_content_log` VALUES (11, 62, 9, '2013-04-29 16:02:05');
INSERT INTO `user_content_log` VALUES (12, 58, 9, '2013-04-29 15:59:03');
INSERT INTO `user_content_log` VALUES (13, 54, 9, '2013-04-29 15:59:06');
INSERT INTO `user_content_log` VALUES (14, 45, 9, '2013-04-29 15:59:08');
INSERT INTO `user_content_log` VALUES (15, 40, 9, '2013-04-29 15:59:10');
INSERT INTO `user_content_log` VALUES (16, 74, 9, '2013-04-29 15:59:58');
INSERT INTO `user_content_log` VALUES (17, 75, 9, '2013-04-29 15:59:50');
INSERT INTO `user_content_log` VALUES (18, 136, 9, '2013-04-29 17:40:41');
INSERT INTO `user_content_log` VALUES (19, 134, 9, '2013-04-29 17:40:39');
INSERT INTO `user_content_log` VALUES (20, 135, 9, '2013-04-29 17:40:40');
INSERT INTO `user_content_log` VALUES (21, 137, 9, '2013-04-29 17:40:44');
INSERT INTO `user_content_log` VALUES (22, 138, 9, '2013-04-29 17:40:45');
INSERT INTO `user_content_log` VALUES (23, 139, 9, '2013-04-29 17:40:46');
INSERT INTO `user_content_log` VALUES (24, 132, 9, '2013-04-29 17:40:37');
INSERT INTO `user_content_log` VALUES (25, 133, 9, '2013-04-29 17:40:38');
INSERT INTO `user_content_log` VALUES (26, 40, 4, '2013-04-29 17:44:34');
INSERT INTO `user_content_log` VALUES (27, 82, 4, '2013-04-29 18:43:53');
INSERT INTO `user_content_log` VALUES (28, 83, 4, '2013-04-29 18:43:54');
INSERT INTO `user_content_log` VALUES (29, 130, 4, '2013-04-29 18:44:06');
INSERT INTO `user_content_log` VALUES (30, 81, 4, '2013-05-07 11:17:57');
INSERT INTO `user_content_log` VALUES (31, 132, 4, '2013-04-29 18:43:58');
INSERT INTO `user_content_log` VALUES (32, 133, 4, '2013-04-29 18:41:35');
INSERT INTO `user_content_log` VALUES (33, 134, 4, '2013-04-29 18:40:20');
INSERT INTO `user_content_log` VALUES (34, 135, 4, '2013-04-29 17:58:26');
INSERT INTO `user_content_log` VALUES (35, 136, 4, '2013-04-29 17:58:25');
INSERT INTO `user_content_log` VALUES (36, 137, 4, '2013-04-29 18:41:39');
INSERT INTO `user_content_log` VALUES (37, 138, 4, '2013-04-29 18:41:59');
INSERT INTO `user_content_log` VALUES (38, 139, 4, '2013-04-29 18:41:52');
INSERT INTO `user_content_log` VALUES (39, 140, 4, '2013-04-29 18:29:41');
INSERT INTO `user_content_log` VALUES (40, 141, 4, '2013-04-29 18:41:37');
INSERT INTO `user_content_log` VALUES (41, 142, 4, '2013-04-29 17:58:32');
INSERT INTO `user_content_log` VALUES (42, 143, 4, '2013-04-29 17:58:33');
INSERT INTO `user_content_log` VALUES (43, 144, 4, '2013-04-29 17:58:33');
INSERT INTO `user_content_log` VALUES (44, 145, 4, '2013-04-29 17:58:34');
INSERT INTO `user_content_log` VALUES (45, 68, 4, '2013-04-29 18:15:28');
INSERT INTO `user_content_log` VALUES (46, 69, 4, '2013-04-29 18:15:26');
INSERT INTO `user_content_log` VALUES (47, 65, 4, '2013-04-29 18:15:44');
INSERT INTO `user_content_log` VALUES (48, 62, 4, '2013-04-29 18:14:55');
INSERT INTO `user_content_log` VALUES (49, 54, 4, '2013-04-29 18:14:56');
INSERT INTO `user_content_log` VALUES (50, 66, 4, '2013-04-29 18:15:43');
INSERT INTO `user_content_log` VALUES (51, 81, 17, '2013-05-08 12:03:29');
INSERT INTO `user_content_log` VALUES (52, 171, 17, '2013-05-07 18:34:55');
INSERT INTO `user_content_log` VALUES (53, 141, 17, '2013-05-07 11:39:26');
INSERT INTO `user_content_log` VALUES (54, 142, 17, '2013-05-07 11:39:27');
INSERT INTO `user_content_log` VALUES (55, 143, 17, '2013-05-07 11:39:28');
INSERT INTO `user_content_log` VALUES (56, 144, 17, '2013-05-07 18:34:58');
INSERT INTO `user_content_log` VALUES (57, 145, 17, '2013-05-07 11:39:30');
INSERT INTO `user_content_log` VALUES (58, 137, 17, '2013-05-07 11:39:44');
INSERT INTO `user_content_log` VALUES (59, 23, 17, '2013-05-08 11:22:12');
INSERT INTO `user_content_log` VALUES (60, 62, 17, '2013-05-07 13:07:06');
INSERT INTO `user_content_log` VALUES (61, 82, 17, '2013-05-07 18:41:37');
INSERT INTO `user_content_log` VALUES (62, 83, 17, '2013-05-07 18:41:14');
INSERT INTO `user_content_log` VALUES (63, 140, 17, '2013-05-07 18:35:01');
INSERT INTO `user_content_log` VALUES (64, 139, 17, '2013-05-07 18:35:03');
INSERT INTO `user_content_log` VALUES (65, 130, 17, '2013-05-07 18:43:58');
INSERT INTO `user_content_log` VALUES (66, 132, 17, '2013-05-07 18:43:52');
INSERT INTO `user_content_log` VALUES (67, 26, 17, '2013-05-08 11:18:57');
INSERT INTO `user_content_log` VALUES (68, 25, 17, '2013-05-08 11:18:59');
INSERT INTO `user_content_log` VALUES (69, 68, 17, '2013-05-08 11:19:01');

-- --------------------------------------------------------

-- 
-- Table structure for table `user_course`
-- 

CREATE TABLE `user_course` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `role` tinyint(1) NOT NULL default '1' COMMENT '1 owner,2 instructor,3 student',
  `create_date` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `course_id` (`course_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

-- 
-- Dumping data for table `user_course`
-- 

INSERT INTO `user_course` VALUES (5, 4, 1, 1, '2012-04-30 11:11:11');
INSERT INTO `user_course` VALUES (6, 4, 2, 3, '2012-04-30 11:11:11');
INSERT INTO `user_course` VALUES (9, 4, 3, 3, '2012-04-30 11:11:11');
INSERT INTO `user_course` VALUES (10, 9, 1, 3, '2012-04-24 11:11:11');
INSERT INTO `user_course` VALUES (11, 9, 9, 2, '2012-04-30 11:11:11');
INSERT INTO `user_course` VALUES (12, 10, 1, 1, '2013-04-30 19:56:39');
INSERT INTO `user_course` VALUES (13, 2, 1, 1, '2013-04-30 19:56:54');
INSERT INTO `user_course` VALUES (14, 5, 1, 1, '2013-04-30 19:58:15');
INSERT INTO `user_course` VALUES (15, 1, 1, 3, '2013-04-30 20:02:09');
INSERT INTO `user_course` VALUES (16, 6, 1, 3, '2013-05-01 12:05:29');
INSERT INTO `user_course` VALUES (17, 13, 1, 3, '2013-05-01 12:21:41');
INSERT INTO `user_course` VALUES (18, 17, 1, 3, NULL);
INSERT INTO `user_course` VALUES (20, 17, 8, 3, '2013-05-07 13:23:25');

-- --------------------------------------------------------

-- 
-- Table structure for table `user_permission`
-- 

CREATE TABLE `user_permission` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `can_create_course` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `user_permission`
-- 

INSERT INTO `user_permission` VALUES (1, 4, 1);
INSERT INTO `user_permission` VALUES (2, 17, 0);

-- --------------------------------------------------------

-- 
-- Structure for view `content_info`
-- 
DROP TABLE IF EXISTS `content_info`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `coursecreek`.`content_info` AS select max(`coursecreek`.`content`.`course_id`) AS `course_id`,max(`coursecreek`.`course`.`name`) AS `course_name`,count(`coursecreek`.`content`.`id`) AS `number_of_content` from (`coursecreek`.`content` join `coursecreek`.`course` on((`coursecreek`.`content`.`course_id` = `coursecreek`.`course`.`id`))) where (`coursecreek`.`content`.`parent_id` <> -(1)) group by `coursecreek`.`content`.`course_id`;

-- --------------------------------------------------------

-- 
-- Structure for view `stat_user_study`
-- 
DROP TABLE IF EXISTS `stat_user_study`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `coursecreek`.`stat_user_study` AS select count(`coursecreek`.`user_content_log`.`content_id`) AS `content_has_studied`,max(`coursecreek`.`user_content_log`.`user_id`) AS `user_id`,max(`coursecreek`.`content`.`course_id`) AS `course_id` from (`coursecreek`.`user_content_log` join `coursecreek`.`content` on((`coursecreek`.`user_content_log`.`content_id` = `coursecreek`.`content`.`id`))) group by `coursecreek`.`user_content_log`.`user_id`;

-- 
-- Constraints for dumped tables
-- 

-- 
-- Constraints for table `content`
-- 
ALTER TABLE `content`
  ADD CONSTRAINT `content_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `coupon`
-- 
ALTER TABLE `coupon`
  ADD CONSTRAINT `coupon_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `coupon_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `course`
-- 
ALTER TABLE `course`
  ADD CONSTRAINT `course_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `exercise`
-- 
ALTER TABLE `exercise`
  ADD CONSTRAINT `exercise_ibfk_1` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `exercise_answer_choice`
-- 
ALTER TABLE `exercise_answer_choice`
  ADD CONSTRAINT `exercise_answer_choice_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `exercise_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `exercise_question`
-- 
ALTER TABLE `exercise_question`
  ADD CONSTRAINT `exercise_question_ibfk_1` FOREIGN KEY (`exercise_id`) REFERENCES `exercise` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `exercise_text_answer`
-- 
ALTER TABLE `exercise_text_answer`
  ADD CONSTRAINT `exercise_text_answer_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `exercise_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `exercise_text_score`
-- 
ALTER TABLE `exercise_text_score`
  ADD CONSTRAINT `exercise_text_score_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `exercise_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `file`
-- 
ALTER TABLE `file`
  ADD CONSTRAINT `file_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `file_content`
-- 
ALTER TABLE `file_content`
  ADD CONSTRAINT `file_content_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `file_content_ibfk_2` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `tbl_profiles`
-- 
ALTER TABLE `tbl_profiles`
  ADD CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE;

-- 
-- Constraints for table `user_content_log`
-- 
ALTER TABLE `user_content_log`
  ADD CONSTRAINT `user_content_log_ibfk_1` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_content_log_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `user_course`
-- 
ALTER TABLE `user_course`
  ADD CONSTRAINT `user_course_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_profiles` (`user_id`),
  ADD CONSTRAINT `user_course_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`);

-- 
-- Constraints for table `user_permission`
-- 
ALTER TABLE `user_permission`
  ADD CONSTRAINT `user_permission_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
