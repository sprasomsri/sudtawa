-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 28, 2013 at 05:27 PM
-- Server version: 5.5.29
-- PHP Version: 5.3.10-1ubuntu3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `coursecreek`
--

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `course_id` int(11) NOT NULL COMMENT '-1 topLevel',
  `parent_id` int(11) NOT NULL DEFAULT '-1' COMMENT '-1 is Chapter',
  `detail` text COLLATE utf8_unicode_ci,
  `show_order` tinyint(3) NOT NULL DEFAULT '1',
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=71 ;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `title`, `course_id`, `parent_id`, `detail`, `show_order`, `file_id`) VALUES
(1, 'Class Overview', 1, -1, '', 0, 0),
(2, 'So you want to be a Portfolio Manager?', 1, -1, '', 2, 0),
(3, ' Learning Objectives', 1, 1, NULL, 1, 1),
(4, 'Module Learning objectives', 1, 2, NULL, 3, 2),
(5, 'Common metrics for assessing fund performance -1', 1, 2, NULL, 4, 3),
(6, 'Common metrics for assessing fund performance -2', 1, 2, NULL, 5, 4),
(7, 'Demo', 1, 2, NULL, 6, 0),
(8, 'Homework1: Construct and Assess a Portfolio.', 1, 2, NULL, 7, 0),
(9, 'Mechanics of the market', 1, -1, NULL, 8, 0),
(10, 'Module objectives', 1, 9, NULL, 9, 0),
(13, 'The Order book', 1, 9, NULL, 10, 0),
(14, 'Hedge funds and Arbitrage', 1, 9, NULL, 11, 0),
(15, 'The Computing inside a Hedge fund', 1, 9, NULL, 12, 0),
(16, 'What is a company worth?', 1, -1, NULL, 13, 0),
(19, 'Intrinsic Value: Value of future dividends', 1, 16, NULL, 14, 0),
(20, 'How and why news affects prices (Event Study) -1', 1, 16, NULL, 15, 0),
(21, 'Fundamental analysis of company value', 1, 16, NULL, 16, 0),
(22, 'Setting up software on Unix', 1, -1, NULL, 17, 0),
(25, 'Overview and Installation of QSTK', 1, 22, NULL, 18, 0),
(26, 'Installation of prerequisits of QSTK on Unix', 1, 22, NULL, 19, 0),
(31, 'Installation of QSTK on Unix', 1, 22, NULL, 20, 0),
(32, 'Installation of test data', 1, 22, NULL, 21, 0),
(33, 'Testing QSTK', 1, 22, NULL, 22, 0),
(34, 'Testing QSTK continued', 1, 22, NULL, 23, 0),
(35, 'Capital Assets pricing model (CAPM)', 1, -1, NULL, 24, 0),
(36, 'Capital Assets Pricing Model Overview', 1, 35, NULL, 25, 0),
(37, 'CAPM : What is beta ?', 1, 35, NULL, 26, 0),
(38, 'How hedge funds use CAPM', 1, 35, NULL, 27, 0),
(39, 'Efficient Markets Hypothesis', 1, -1, NULL, 28, 0),
(40, 'Information and Arbitrage', 1, 39, NULL, 29, 0),
(41, 'Efficient Markets Hypothesis', 1, 39, NULL, 30, 0),
(42, 'Event Studies', 1, 39, NULL, 31, 0),
(43, 'Event Profiler - QSTK', 1, 39, NULL, 32, 0),
(44, 'Portfolio Optimization and Efficient Frontier', 1, -1, NULL, 33, 0),
(45, 'Portfolio Optimization and Efficient Frontier - 1', 1, 44, NULL, 34, 0),
(46, 'Portfolio Optimization and Efficient Frontier - 2', 1, 44, NULL, 35, 0),
(47, 'Correlation and Covariance', 1, 44, NULL, 36, 0),
(48, 'Efficient Frontier', 1, 44, NULL, 37, 0),
(49, 'How optimizers work', 1, 44, NULL, 38, 0),
(50, 'Importance of Data', 1, -1, NULL, 39, 0),
(54, 'Digging into Data', 1, 50, NULL, 40, 0),
(55, 'Actual Vs Adjusted Price', 1, 50, NULL, 41, 0),
(56, 'Data Sanity and Scrubbing', 1, 50, NULL, 42, 0),
(57, 'Overview of Homework 3', 1, -1, NULL, 43, 0),
(58, 'How Next Two Homeworks Fit Together', 1, 57, NULL, 44, 0),
(59, 'Specification for Homework 3', 1, 57, NULL, 45, 0),
(60, 'Suggestions on Implementation of Homework 3', 1, 57, NULL, 46, 0),
(61, 'Capital Assets Pricing Model', 1, -1, NULL, 47, 0),
(62, 'CAPM : Capital Assets Pricing Model', 1, 61, NULL, 48, 0),
(63, 'Using CAPM to reduce risk', 1, 61, NULL, 49, 0),
(64, 'Overview of Homework 4', 1, -1, NULL, 50, 0),
(65, 'How to Assess Event Study', 1, 64, NULL, 51, 0),
(66, 'Homework 4', 1, 64, NULL, 52, 0),
(67, 'The Fundamental Law', 1, -1, NULL, 53, 0),
(68, 'Thought Experiment: Coin Flipping', 1, 67, NULL, 57, 0),
(69, 'The Fundamental Law - 1', 1, 67, NULL, 58, 0),
(70, 'The Fundamental Law - 2', 1, 67, NULL, 59, 0);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `name_th` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `teacher` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(5) NOT NULL,
  `course_img` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `course_status` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `name`, `name_th`, `tag`, `description`, `about`, `teacher`, `price`, `course_img`, `course_status`, `user_id`) VALUES
(1, 'Computational Investing', 'การลงทุนเชิงคำนวณ', 'การลงทุน', 'Find out how modern electronic markets work, why stock prices change in the ways they do, and how computation can help our understanding of them.  Build algorithms and visualizations to inform investing practice.', 'Why do the prices of some companies’ stocks seem to move up and down together while others move separately? What does portfolio “diversification” really mean and how important is it? What should the price of a stock be? How can we discover and exploit the relationships between equity prices automatically? We’ll examine these questions, and others, from a computational point of view. You will learn many of the principles and algorithms hedge funds and investment professionals use to maximize return and reduce risk in equity portfolios.', 'Tucker Balch', 2999, 'course/1.png', 0, 4),
(2, 'ภาษาจีน', 'ภาษาจีน', 'ภาษาต่างประเทศ', 'ภาษาจีน', '', 'ภาษาจีน', 789, '', 1, 4),
(3, 'ชีววิทยา', 'ชีววิทยา', 'เตรียมสอบ', 'ชีววิทยา', '', 'ชีววิทยา', 1000, '', 0, 4),
(4, 'ภาษาไทย', 'ภาษาไทย', 'เตรียมสอบ', 'ภาษาไทย', '', 'ภาษาไทย', 2000, '', 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `original_name` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`id`, `name`, `original_name`, `user_id`, `type`) VALUES
(1, '2-1-ClassOverview.mp4', '2 - 1 - Class Overview.mp4', 4, '1'),
(2, '2-2-Computational_Investing_021.mp4', '2-2-Computational_Investing_021.mp4', 4, '1'),
(3, '2-3-Computational_Investing_022.mp4', '2-3-Computational_Investing_022.mp4', 4, '1'),
(4, '2-4-Computational_Investing_023.mp4', '2-4-Computational_Investing_023.mp4', 4, '1');

-- --------------------------------------------------------

--
-- Table structure for table `file_content`
--

CREATE TABLE IF NOT EXISTS `file_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `file_id` (`file_id`),
  KEY `content_id` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_migration`
--

CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_migration`
--

INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1363842238);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profiles`
--

CREATE TABLE IF NOT EXISTS `tbl_profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_profiles`
--

INSERT INTO `tbl_profiles` (`user_id`, `lastname`, `firstname`) VALUES
(1, 'Admin', 'Administrator'),
(2, 'Demo', 'Demo'),
(4, 'prasomsri', 'sarun'),
(5, '', ''),
(6, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profiles_fields`
--

CREATE TABLE IF NOT EXISTS `tbl_profiles_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` varchar(15) NOT NULL DEFAULT '0',
  `field_size_min` varchar(15) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_profiles_fields`
--

INSERT INTO `tbl_profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
(1, 'lastname', 'Last Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 1, 3),
(2, 'firstname', 'First Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `password`, `email`, `activkey`, `create_at`, `lastvisit`, `superuser`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'webmaster@example.com', '9a24eff8c15a6a141ece27eb6947da0f', '2013-03-21 06:29:02', '0000-00-00 00:00:00', 1, 1),
(2, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'demo@example.com', '099f825543f7850cc038b90aaff39fac', '2013-03-21 06:29:02', '2013-03-28 07:54:25', 0, 1),
(4, 'gun', '81dc9bdb52d04dc20036dbd8313ed055', 'gunsarun@gmail.com', '1de177922186eeb3e7cba41f07f56399', '2013-03-21 08:49:38', '2013-03-25 09:57:33', 1, 1),
(5, 'arnupharp@larngeartech.com', 'f561aaf6ef0bf14d4208bb46a4ccb3ad', 'arnupharp@larngeartech.com', 'b0c33778992e1d78b56bfebfff186801', '2013-03-27 06:41:14', '0000-00-00 00:00:00', 0, 1),
(6, 'topscores@gmail.com', '1b1399f21b2fefa7602555883b4c42e5', 'topscores@gmail.com', 'b469d40dc5d4183b3e80ee0d0080a31d', '2013-03-28 05:21:21', '0000-00-00 00:00:00', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_course`
--

CREATE TABLE IF NOT EXISTS `user_course` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 owner,2 instructor,3 student',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user_course`
--

INSERT INTO `user_course` (`user_id`, `course_id`, `role`) VALUES
(5, 1, 3);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `content_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `course`
--
ALTER TABLE `course`
  ADD CONSTRAINT `course_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `file_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `file_content`
--
ALTER TABLE `file_content`
  ADD CONSTRAINT `file_content_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `file_content_ibfk_2` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_profiles`
--
ALTER TABLE `tbl_profiles`
  ADD CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
