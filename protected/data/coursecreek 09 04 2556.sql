-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- โฮสต์: localhost
-- เวลาในการสร้าง: 
-- รุ่นของเซิร์ฟเวอร์: 5.0.51
-- รุ่นของ PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- ฐานข้อมูล: `coursecreek`
-- 

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `content`
-- 

CREATE TABLE `content` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(500) collate utf8_unicode_ci NOT NULL,
  `course_id` int(11) NOT NULL COMMENT '-1 topLevel',
  `parent_id` int(11) NOT NULL default '-1' COMMENT '-1 is Chapter',
  `detail` text collate utf8_unicode_ci,
  `show_order` tinyint(3) NOT NULL default '1',
  `file_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=73 ;

-- 
-- dump ตาราง `content`
-- 

INSERT INTO `content` VALUES (1, 'Class Overview', 1, -1, '', 4, 0);
INSERT INTO `content` VALUES (2, 'So you want to be a Portfolio Manager?', 1, -1, '', 0, 0);
INSERT INTO `content` VALUES (3, ' Learning Objectives', 1, 1, NULL, 20, 1);
INSERT INTO `content` VALUES (4, 'Module Learning objectives', 1, 1, NULL, 7, 2);
INSERT INTO `content` VALUES (5, 'Common metrics for assessing fund performance -1', 1, 1, NULL, 8, 3);
INSERT INTO `content` VALUES (6, 'Common metrics for assessing fund performance -2', 1, 1, NULL, 9, 4);
INSERT INTO `content` VALUES (7, 'Demo', 1, 1, NULL, 10, 0);
INSERT INTO `content` VALUES (8, 'Homework1: Construct and Assess a Portfolio.', 1, 1, NULL, 22, 0);
INSERT INTO `content` VALUES (9, 'Mechanics of the market', 1, -1, NULL, 23, 0);
INSERT INTO `content` VALUES (10, 'Module objectives', 1, 1, NULL, 11, 0);
INSERT INTO `content` VALUES (13, 'The Order book', 1, 1, NULL, 18, 0);
INSERT INTO `content` VALUES (14, 'Hedge funds and Arbitrage', 1, 1, NULL, 21, 0);
INSERT INTO `content` VALUES (15, 'The Computing inside a Hedge fund', 1, 1, NULL, 12, 0);
INSERT INTO `content` VALUES (16, 'What is a company worth?', 1, -1, NULL, 24, 0);
INSERT INTO `content` VALUES (19, 'Intrinsic Value: Value of future dividends', 1, 1, NULL, 13, 0);
INSERT INTO `content` VALUES (20, 'How and why news affects prices (Event Study) -1', 1, 1, NULL, 14, 0);
INSERT INTO `content` VALUES (21, 'Fundamental analysis of company value', 1, 1, NULL, 5, 0);
INSERT INTO `content` VALUES (22, 'Setting up software on Unix', 1, -1, NULL, 3, 0);
INSERT INTO `content` VALUES (23, 'Testing QSTK', 1, 2, 'm[pldl;;dkfkldklfkldkflkdlflkdlkdkdkldks;d'';s;d\r\n', 2, 23);
INSERT INTO `content` VALUES (25, 'Overview and Installation of QSTK', 1, 16, NULL, 25, 0);
INSERT INTO `content` VALUES (26, 'Installation of prerequisits of QSTK on Unix', 1, 1, NULL, 6, 0);
INSERT INTO `content` VALUES (31, 'Installation of QSTK on Unix', 1, 1, NULL, 15, 0);
INSERT INTO `content` VALUES (32, 'Installation of test data', 1, 2, '', 1, 27);
INSERT INTO `content` VALUES (34, 'Testing QSTK continued', 1, 16, NULL, 26, 0);
INSERT INTO `content` VALUES (35, 'Capital Assets pricing model (CAPM)', 1, -1, NULL, 27, 0);
INSERT INTO `content` VALUES (36, 'Capital Assets Pricing Model Overview', 1, 35, NULL, 29, 0);
INSERT INTO `content` VALUES (37, 'CAPM : What is beta ?', 1, 35, NULL, 30, 0);
INSERT INTO `content` VALUES (38, 'How hedge funds use CAPM', 1, 35, NULL, 28, 0);
INSERT INTO `content` VALUES (39, 'Efficient Markets Hypothesis', 1, -1, NULL, 31, 0);
INSERT INTO `content` VALUES (40, 'Information and Arbitrage', 1, 39, NULL, 32, 0);
INSERT INTO `content` VALUES (41, 'Efficient Markets Hypothesis', 1, 39, NULL, 33, 0);
INSERT INTO `content` VALUES (42, 'Event Studies', 1, 39, NULL, 34, 0);
INSERT INTO `content` VALUES (43, 'Event Profiler - QSTK', 1, 39, NULL, 35, 0);
INSERT INTO `content` VALUES (44, 'Portfolio Optimization and Efficient Frontier', 1, -1, NULL, 36, 0);
INSERT INTO `content` VALUES (45, 'Portfolio Optimization and Efficient Frontier - 1', 1, 44, NULL, 37, 0);
INSERT INTO `content` VALUES (46, 'Portfolio Optimization and Efficient Frontier - 2', 1, 44, NULL, 38, 0);
INSERT INTO `content` VALUES (47, 'Correlation and Covariance', 1, 44, NULL, 39, 0);
INSERT INTO `content` VALUES (48, 'Efficient Frontier', 1, 44, NULL, 40, 0);
INSERT INTO `content` VALUES (49, 'How optimizers work', 1, 44, NULL, 41, 0);
INSERT INTO `content` VALUES (50, 'Importance of Data', 1, -1, NULL, 42, 0);
INSERT INTO `content` VALUES (54, 'Digging into Data', 1, 50, NULL, 43, 0);
INSERT INTO `content` VALUES (55, 'Actual Vs Adjusted Price', 1, 50, NULL, 44, 0);
INSERT INTO `content` VALUES (56, 'Data Sanity and Scrubbing', 1, 50, NULL, 45, 0);
INSERT INTO `content` VALUES (57, 'Overview of Homework 3', 1, -1, NULL, 46, 0);
INSERT INTO `content` VALUES (58, 'How Next Two Homeworks Fit Together', 1, 57, NULL, 47, 0);
INSERT INTO `content` VALUES (59, 'Specification for Homework 3', 1, 57, NULL, 48, 0);
INSERT INTO `content` VALUES (60, 'Suggestions on Implementation of Homework 3', 1, 57, NULL, 49, 0);
INSERT INTO `content` VALUES (61, 'Capital Assets Pricing Model', 1, -1, NULL, 50, 0);
INSERT INTO `content` VALUES (62, 'CAPM : Capital Assets Pricing Model', 1, 61, NULL, 51, 0);
INSERT INTO `content` VALUES (63, 'Using CAPM to reduce risk', 1, 61, NULL, 52, 0);
INSERT INTO `content` VALUES (64, 'Overview of Homework 4', 1, -1, NULL, 53, 0);
INSERT INTO `content` VALUES (65, 'How to Assess Event Study', 1, 64, NULL, 54, 0);
INSERT INTO `content` VALUES (66, 'Homework 4', 1, 64, NULL, 55, 0);
INSERT INTO `content` VALUES (67, 'The Fundamental Law', 1, -1, NULL, 56, 0);
INSERT INTO `content` VALUES (68, 'Thought Experiment: Coin Flipping', 1, 67, NULL, 57, 0);
INSERT INTO `content` VALUES (69, 'The Fundamental Law - 1', 1, 67, NULL, 58, 0);
INSERT INTO `content` VALUES (70, '2', 1, 1, '2', 19, 24);
INSERT INTO `content` VALUES (71, 'test', 1, 1, 'ttt', 16, 25);
INSERT INTO `content` VALUES (72, 'ทดสอบ Unit 1', 1, 1, '', 17, 26);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `coupon`
-- 

CREATE TABLE `coupon` (
  `id` int(11) NOT NULL auto_increment,
  `discount` tinyint(3) NOT NULL COMMENT 'ส่วนลด (%)',
  `expire_date` date NOT NULL,
  `qty_use` tinyint(5) NOT NULL COMMENT 'จำนวนครั้งที่ใช้ได้',
  `used_time` tinyint(5) NOT NULL COMMENT 'จำนวนครั้งที่ใช้ไปแล้ว',
  `user_id` int(11) NOT NULL COMMENT 'คนสร้าง',
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- 
-- dump ตาราง `coupon`
-- 


-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `coupon_log`
-- 

CREATE TABLE `coupon_log` (
  `coupon_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `used_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `coupon_log`
-- 


-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `course`
-- 

CREATE TABLE `course` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(500) collate utf8_unicode_ci NOT NULL,
  `name_th` varchar(500) collate utf8_unicode_ci NOT NULL,
  `tag` varchar(20) collate utf8_unicode_ci NOT NULL,
  `description` varchar(1000) collate utf8_unicode_ci default NULL,
  `about` text collate utf8_unicode_ci NOT NULL,
  `teacher` varchar(200) collate utf8_unicode_ci NOT NULL,
  `price` int(5) NOT NULL,
  `course_img` varchar(1000) collate utf8_unicode_ci default NULL,
  `course_status` tinyint(1) NOT NULL default '0',
  `user_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL default '0' COMMENT 'ส่วนแบ่ง (%)',
  `diration` tinyint(5) default NULL COMMENT 'ระยะเวลาในการเปิดคอร์ส หน่วยเป็นวัน ',
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- 
-- dump ตาราง `course`
-- 

INSERT INTO `course` VALUES (1, 'Computational Investing', 'การลงทุนเชิงคำนวณ', 'การลงทุน', 'Find out how modern electronic markets work, why stock prices change in the ways they do, and how computation can help our understanding of them.  Build algorithms and visualizations to inform investing practice.', 'Why do the prices of some companies’ stocks seem to move up and down together while others move separately? What does portfolio “diversification” really mean and how important is it? What should the price of a stock be? How can we discover and exploit the relationships between equity prices automatically? We’ll examine these questions, and others, from a computational point of view. You will learn many of the principles and algorithms hedge funds and investment professionals use to maximize return and reduce risk in equity portfolios.', 'Tucker Balch', 2999, 'course/urf2dtli83jpcw1soe9nzb6mga5vkq4y70xh_186617287761.jpg', 0, 4, 0, NULL);
INSERT INTO `course` VALUES (2, 'ภาษาจีน', 'ภาษาจีน', 'ภาษาต่างประเทศ', 'ภาษาจีน', '', 'ภาษาจีน', 789, '', 1, 4, 0, NULL);
INSERT INTO `course` VALUES (3, 'ชีววิทยา', 'ชีววิทยา', 'เตรียมสอบ', 'ชีววิทยา', '', 'ชีววิทยา', 1000, '', 0, 4, 0, NULL);
INSERT INTO `course` VALUES (4, 'ภาษาไทย', 'ภาษาไทย', 'เตรียมสอบ', 'ภาษาไทย', '', 'ภาษาไทย', 2000, '', 2, 4, 0, NULL);
INSERT INTO `course` VALUES (5, 'ๅ', 'ๅ', 'ๅ', 'ๅ', 'ๅ', 'ๅๅ', 0, 'ๅ', 0, 1, 0, NULL);
INSERT INTO `course` VALUES (6, 'กันทดสอบ', '', '', 'ไไไไ', '', 'กัน ', 3500, 'course/nox5gku38szbmjl64iqhc0wy1frpevd9t2a7_9287_d093_8.jpg', 0, 4, 0, NULL);
INSERT INTO `course` VALUES (7, 'กันทดสอบ', '', '', 'ไไไไ', '', 'กัน ', 3500, 'course/w1r8auxqc2gtzbvfh4enyp7kd53o09lmsji6_9287_d093_8.jpg', 0, 4, 0, NULL);
INSERT INTO `course` VALUES (8, 'กันทดสอบ', '', '', 'ไไไไ', '', 'กัน ', 3500, 'course/cs52yu4hw109ltqage8mn7r6pkdfx3zojivb_IMG_0937.JPG', 0, 4, 0, NULL);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `file`
-- 

CREATE TABLE `file` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `original_name` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(100) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

-- 
-- dump ตาราง `file`
-- 

INSERT INTO `file` VALUES (23, 'rhre0rufl6vca4nms6gkjeokwpdez0snoy2qcue3ait5n3eea5rgxub_1.mp4', '1.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (24, 'ea5due6qryjsgrueo3n0sv3pm6k4eeaztbrcluwrhecag5konifn2x0_a.mp4', 'a.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (25, 'aucsa3r6zenlnrk0eouux2mcgvy50pa5sgrie6e3hqtnde4reowbjkf_a.mp4', 'a.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (26, 'eot3ocijmhaureaqe0nnyx3rge5vdup6r0skfeab46uk2szgercwn5l_4.mp4', '4.mp4', 4, 'vdo');
INSERT INTO `file` VALUES (27, 'uxyuondm02rsz6bvt5ecpeke30q4rguahai6scekarg5wrneejo3nlf_1.mp4', '1.mp4', 4, 'vdo');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `file_content`
-- 

CREATE TABLE `file_content` (
  `id` int(11) NOT NULL auto_increment,
  `file_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `file_id` (`file_id`),
  KEY `content_id` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- 
-- dump ตาราง `file_content`
-- 


-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `tbl_migration`
-- 

CREATE TABLE `tbl_migration` (
  `version` varchar(255) collate utf8_unicode_ci NOT NULL,
  `apply_time` int(11) default NULL,
  PRIMARY KEY  (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `tbl_migration`
-- 

INSERT INTO `tbl_migration` VALUES ('m000000_000000_base', 1363842238);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `tbl_profiles`
-- 

CREATE TABLE `tbl_profiles` (
  `user_id` int(11) NOT NULL auto_increment,
  `lastname` varchar(50) NOT NULL default '',
  `firstname` varchar(50) NOT NULL default '',
  `detail` varchar(3000) default NULL,
  `photo` varchar(500) default NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- 
-- dump ตาราง `tbl_profiles`
-- 

INSERT INTO `tbl_profiles` VALUES (1, 'Admin', 'Administrator', NULL, '298thul6kcazvrd8creeq7ysu4wog03epeisxobmjf81nk85r_IMG_0937.JPG');
INSERT INTO `tbl_profiles` VALUES (2, 'Demo', 'Demo', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (4, 'prasomsri', 'sarun', '222222222222222222222', '88i4jex29fc6uc8spsk3y8750kzurhelnrdoormbewtg1eaqv_sunny.jpg');
INSERT INTO `tbl_profiles` VALUES (5, '', '', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (6, '', '', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (7, '', '', NULL, NULL);
INSERT INTO `tbl_profiles` VALUES (8, 'prasomsri', 'sarun', NULL, NULL);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `tbl_profiles_fields`
-- 

CREATE TABLE `tbl_profiles_fields` (
  `id` int(10) NOT NULL auto_increment,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` varchar(15) NOT NULL default '0',
  `field_size_min` varchar(15) NOT NULL default '0',
  `required` int(1) NOT NULL default '0',
  `match` varchar(255) NOT NULL default '',
  `range` varchar(255) NOT NULL default '',
  `error_message` varchar(255) NOT NULL default '',
  `other_validator` varchar(5000) NOT NULL default '',
  `default` varchar(255) NOT NULL default '',
  `widget` varchar(255) NOT NULL default '',
  `widgetparams` varchar(5000) NOT NULL default '',
  `position` int(3) NOT NULL default '0',
  `visible` int(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- dump ตาราง `tbl_profiles_fields`
-- 

INSERT INTO `tbl_profiles_fields` VALUES (1, 'lastname', 'Last Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 1, 3);
INSERT INTO `tbl_profiles_fields` VALUES (2, 'firstname', 'First Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 0, 3);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `tbl_users`
-- 

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activkey` varchar(128) NOT NULL default '',
  `create_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `lastvisit` timestamp NOT NULL default '0000-00-00 00:00:00',
  `superuser` int(1) NOT NULL default '0',
  `status` int(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- 
-- dump ตาราง `tbl_users`
-- 

INSERT INTO `tbl_users` VALUES (1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'webmaster@example.com', '9a24eff8c15a6a141ece27eb6947da0f', '2013-03-21 13:29:02', '0000-00-00 00:00:00', 1, 1);
INSERT INTO `tbl_users` VALUES (2, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'demo@example.com', '099f825543f7850cc038b90aaff39fac', '2013-03-21 13:29:02', '2013-03-28 14:54:25', 0, 1);
INSERT INTO `tbl_users` VALUES (4, 'gun', '81dc9bdb52d04dc20036dbd8313ed055', 'gunsarun@gmail.com', '1de177922186eeb3e7cba41f07f56399', '2013-03-21 15:49:38', '2013-04-09 15:27:20', 1, 1);
INSERT INTO `tbl_users` VALUES (5, 'arnupharp@larngeartech.com', 'f561aaf6ef0bf14d4208bb46a4ccb3ad', 'arnupharp@larngeartech.com', 'b0c33778992e1d78b56bfebfff186801', '2013-03-27 13:41:14', '0000-00-00 00:00:00', 0, 1);
INSERT INTO `tbl_users` VALUES (6, 'topscores@gmail.com', '1b1399f21b2fefa7602555883b4c42e5', 'topscores@gmail.com', 'b469d40dc5d4183b3e80ee0d0080a31d', '2013-03-28 12:21:21', '0000-00-00 00:00:00', 0, 1);
INSERT INTO `tbl_users` VALUES (7, 'sarun@larngeartech.com', '827ccb0eea8a706c4c34a16891f84e7b', 'sarun@larngeartech.com', 'c4137cb8e7c74880f72e988e2d0b64d3', '2013-03-29 14:06:06', '0000-00-00 00:00:00', 0, 1);
INSERT INTO `tbl_users` VALUES (8, 'gunsarun1@gmail.com', '5c74061189c0b65362569830e05f7542', 'gunsarun1@gmail.com', 'a0427fcc1f1bc768a6852f5732a4b93f', '2013-03-29 17:15:17', '0000-00-00 00:00:00', 0, 1);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `user_content_log`
-- 

CREATE TABLE `user_content_log` (
  `id` int(11) NOT NULL auto_increment,
  `content_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `study_time` datetime NOT NULL COMMENT 'วันเวลาที่เรียน',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ตาราง เก็บ ความสัมพันธ์ ระหว่าง user และ Course ที่เรัยนเพื่' AUTO_INCREMENT=7 ;

-- 
-- dump ตาราง `user_content_log`
-- 

INSERT INTO `user_content_log` VALUES (1, 21, 4, '2013-04-09 15:22:55');
INSERT INTO `user_content_log` VALUES (2, 32, 4, '2013-04-09 19:10:06');
INSERT INTO `user_content_log` VALUES (3, 23, 4, '2013-04-09 15:22:41');
INSERT INTO `user_content_log` VALUES (4, 38, 4, '2013-04-09 15:22:49');
INSERT INTO `user_content_log` VALUES (5, 54, 4, '2013-04-09 15:22:52');
INSERT INTO `user_content_log` VALUES (6, 54, 1, '2013-04-09 15:36:34');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `user_course`
-- 

CREATE TABLE `user_course` (
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `role` tinyint(1) NOT NULL default '1' COMMENT '1 owner,2 instructor,3 student'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `user_course`
-- 

INSERT INTO `user_course` VALUES (4, 1, 3);
INSERT INTO `user_course` VALUES (4, 8, 3);
INSERT INTO `user_course` VALUES (1, 1, 3);
INSERT INTO `user_course` VALUES (2, 1, 3);
INSERT INTO `user_course` VALUES (5, 1, 3);
INSERT INTO `user_course` VALUES (6, 1, 3);
INSERT INTO `user_course` VALUES (5, 1, 1);
INSERT INTO `user_course` VALUES (4, 3, 1);
INSERT INTO `user_course` VALUES (4, 4, 1);
INSERT INTO `user_course` VALUES (4, 2, 1);
INSERT INTO `user_course` VALUES (4, 6, 1);

-- 
-- Constraints for dumped tables
-- 

-- 
-- Constraints for table `content`
-- 
ALTER TABLE `content`
  ADD CONSTRAINT `content_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `coupon`
-- 
ALTER TABLE `coupon`
  ADD CONSTRAINT `coupon_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

-- 
-- Constraints for table `course`
-- 
ALTER TABLE `course`
  ADD CONSTRAINT `course_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `file`
-- 
ALTER TABLE `file`
  ADD CONSTRAINT `file_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `file_content`
-- 
ALTER TABLE `file_content`
  ADD CONSTRAINT `file_content_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `file_content_ibfk_2` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `tbl_profiles`
-- 
ALTER TABLE `tbl_profiles`
  ADD CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE;
