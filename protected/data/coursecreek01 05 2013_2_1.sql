-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: May 01, 2013 at 05:14 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `coursecreek`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `content`
-- 

CREATE TABLE `content` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(500) collate utf8_unicode_ci NOT NULL,
  `course_id` int(11) NOT NULL COMMENT '-1 topLevel',
  `parent_id` int(11) NOT NULL default '-1' COMMENT '-1 is Chapter',
  `detail` text collate utf8_unicode_ci,
  `show_order` tinyint(3) NOT NULL default '1',
  `file_id` int(11) default NULL,
  `is_exercise` tinyint(1) NOT NULL default '0' COMMENT 'เป็น แบบฝึกหัด?',
  PRIMARY KEY  (`id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=173 ;

-- --------------------------------------------------------

-- 
-- Stand-in structure for view `content_info`
-- 
CREATE TABLE `content_info` (
`course_id` int(11)
,`course_name` varchar(500)
,`number_of_content` bigint(21)
);
-- --------------------------------------------------------

-- 
-- Table structure for table `coupon`
-- 

CREATE TABLE `coupon` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(500) collate utf8_unicode_ci NOT NULL,
  `discount` tinyint(3) NOT NULL COMMENT 'ส่วนลด (%)',
  `expire_date` date NOT NULL,
  `qty_use` tinyint(5) NOT NULL default '1' COMMENT 'จำนวนครั้งที่ใช้ได้',
  `detail` varchar(1000) collate utf8_unicode_ci default NULL,
  `create_date` datetime NOT NULL,
  `course_id` int(11) NOT NULL default '-1' COMMENT '-1 = coupong can use every course',
  `user_id` int(11) NOT NULL COMMENT 'คนสร้าง',
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=214 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `coupon_log`
-- 

CREATE TABLE `coupon_log` (
  `coupon_id` int(11) NOT NULL,
  `use_course_id` int(11) NOT NULL,
  `used_time` datetime NOT NULL,
  `use_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

-- 
-- Table structure for table `course`
-- 

CREATE TABLE `course` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(500) collate utf8_unicode_ci NOT NULL,
  `name_th` varchar(500) collate utf8_unicode_ci NOT NULL,
  `tag` varchar(20) collate utf8_unicode_ci NOT NULL,
  `description` text collate utf8_unicode_ci,
  `about` text collate utf8_unicode_ci NOT NULL,
  `price` int(5) NOT NULL,
  `course_img` varchar(1000) collate utf8_unicode_ci default NULL,
  `course_status` tinyint(1) NOT NULL default '0',
  `user_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL default '0' COMMENT 'ส่วนแบ่ง (%)',
  `duration` tinyint(5) default NULL COMMENT 'ระยะเวลาในการเปิดคอร์ส หน่วยเป็นวัน ',
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `exercise`
-- 

CREATE TABLE `exercise` (
  `id` int(11) NOT NULL auto_increment,
  `content_id` int(11) NOT NULL,
  `qty_show` int(2) NOT NULL default '10',
  `is_random` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `content_id` (`content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `exercise_answer_choice`
-- 

CREATE TABLE `exercise_answer_choice` (
  `id` int(11) NOT NULL auto_increment,
  `answer` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `is_true` tinyint(1) NOT NULL default '0' COMMENT 'เป็นคำตอยที่ถูกไหม',
  `question_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `question_id` (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=90 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `exercise_choice_score`
-- 

CREATE TABLE `exercise_choice_score` (
  `id` int(11) NOT NULL auto_increment,
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `do_time` datetime NOT NULL,
  `is_true` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='เก็บคะแนน user จากการทำแบบทดสอบ' AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `exercise_question`
-- 

CREATE TABLE `exercise_question` (
  `id` int(11) NOT NULL auto_increment,
  `question` text character set utf8 NOT NULL,
  `show_order` tinyint(2) NOT NULL default '1',
  `type` tinyint(1) NOT NULL default '1' COMMENT '1 choic 2textfield',
  `exercise_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `exercise_id` (`exercise_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `exercise_text_answer`
-- 

CREATE TABLE `exercise_text_answer` (
  `id` int(11) NOT NULL auto_increment,
  `answer` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `question_id` int(11) NOT NULL,
  `sequence` int(2) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `question_id` (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ตารางเก็บคำตอบ ของ question แบบ text' AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `exercise_text_score`
-- 

CREATE TABLE `exercise_text_score` (
  `id` int(11) NOT NULL auto_increment,
  `question_id` int(11) NOT NULL,
  `answer` varchar(3000) collate utf8_unicode_ci NOT NULL,
  `score` float NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `question_id` (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ตารางเก็บ คำตอบ คำถามประเภท text' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `file`
-- 

CREATE TABLE `file` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `original_name` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(100) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=72 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `file_content`
-- 

CREATE TABLE `file_content` (
  `id` int(11) NOT NULL auto_increment,
  `file_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `file_id` (`file_id`),
  KEY `content_id` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `relate_course`
-- 

CREATE TABLE `relate_course` (
  `id` int(11) NOT NULL auto_increment,
  `course_id` int(11) NOT NULL,
  `before_course` varchar(100) collate utf8_unicode_ci default NULL,
  `after_course` varchar(100) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

-- 
-- Stand-in structure for view `stat_user_study`
-- 
CREATE TABLE `stat_user_study` (
`content_has_studied` bigint(21)
,`user_id` int(11)
,`course_id` int(11)
);
-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_migration`
-- 

CREATE TABLE `tbl_migration` (
  `version` varchar(255) collate utf8_unicode_ci NOT NULL,
  `apply_time` int(11) default NULL,
  PRIMARY KEY  (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_profiles`
-- 

CREATE TABLE `tbl_profiles` (
  `user_id` int(11) NOT NULL auto_increment,
  `lastname` varchar(50) NOT NULL default '',
  `firstname` varchar(50) NOT NULL default '',
  `detail` varchar(3000) default NULL,
  `photo` varchar(500) default NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_profiles_fields`
-- 

CREATE TABLE `tbl_profiles_fields` (
  `id` int(10) NOT NULL auto_increment,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` varchar(15) NOT NULL default '0',
  `field_size_min` varchar(15) NOT NULL default '0',
  `required` int(1) NOT NULL default '0',
  `match` varchar(255) NOT NULL default '',
  `range` varchar(255) NOT NULL default '',
  `error_message` varchar(255) NOT NULL default '',
  `other_validator` varchar(5000) NOT NULL default '',
  `default` varchar(255) NOT NULL default '',
  `widget` varchar(255) NOT NULL default '',
  `widgetparams` varchar(5000) NOT NULL default '',
  `position` int(3) NOT NULL default '0',
  `visible` int(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_users`
-- 

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activkey` varchar(128) NOT NULL default '',
  `create_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `lastvisit` timestamp NOT NULL default '0000-00-00 00:00:00',
  `superuser` int(1) NOT NULL default '0',
  `status` int(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `user_content_log`
-- 

CREATE TABLE `user_content_log` (
  `id` int(11) NOT NULL auto_increment,
  `content_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `study_time` datetime NOT NULL COMMENT 'วันเวลาที่เรียน',
  PRIMARY KEY  (`id`),
  KEY `content_id` (`content_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ตาราง เก็บ ความสัมพันธ์ ระหว่าง user และ Course ที่เรัยนเพื่' AUTO_INCREMENT=51 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `user_course`
-- 

CREATE TABLE `user_course` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `role` tinyint(1) NOT NULL default '1' COMMENT '1 owner,2 instructor,3 student',
  `create_date` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `course_id` (`course_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `user_permission`
-- 

CREATE TABLE `user_permission` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `can_create_course` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

-- 
-- Structure for view `content_info`
-- 
DROP TABLE IF EXISTS `content_info`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `coursecreek`.`content_info` AS select max(`coursecreek`.`content`.`course_id`) AS `course_id`,max(`coursecreek`.`course`.`name`) AS `course_name`,count(`coursecreek`.`content`.`id`) AS `number_of_content` from (`coursecreek`.`content` join `coursecreek`.`course` on((`coursecreek`.`content`.`course_id` = `coursecreek`.`course`.`id`))) where (`coursecreek`.`content`.`parent_id` <> -(1)) group by `coursecreek`.`content`.`course_id`;

-- --------------------------------------------------------

-- 
-- Structure for view `stat_user_study`
-- 
DROP TABLE IF EXISTS `stat_user_study`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `coursecreek`.`stat_user_study` AS select count(`coursecreek`.`user_content_log`.`content_id`) AS `content_has_studied`,max(`coursecreek`.`user_content_log`.`user_id`) AS `user_id`,max(`coursecreek`.`content`.`course_id`) AS `course_id` from (`coursecreek`.`user_content_log` join `coursecreek`.`content` on((`coursecreek`.`user_content_log`.`content_id` = `coursecreek`.`content`.`id`))) group by `coursecreek`.`user_content_log`.`user_id`;

-- 
-- Constraints for dumped tables
-- 

-- 
-- Constraints for table `content`
-- 
ALTER TABLE `content`
  ADD CONSTRAINT `content_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `coupon`
-- 
ALTER TABLE `coupon`
  ADD CONSTRAINT `coupon_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `coupon_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `course`
-- 
ALTER TABLE `course`
  ADD CONSTRAINT `course_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `exercise`
-- 
ALTER TABLE `exercise`
  ADD CONSTRAINT `exercise_ibfk_1` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `exercise_answer_choice`
-- 
ALTER TABLE `exercise_answer_choice`
  ADD CONSTRAINT `exercise_answer_choice_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `exercise_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `exercise_question`
-- 
ALTER TABLE `exercise_question`
  ADD CONSTRAINT `exercise_question_ibfk_1` FOREIGN KEY (`exercise_id`) REFERENCES `exercise` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `exercise_text_answer`
-- 
ALTER TABLE `exercise_text_answer`
  ADD CONSTRAINT `exercise_text_answer_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `exercise_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `exercise_text_score`
-- 
ALTER TABLE `exercise_text_score`
  ADD CONSTRAINT `exercise_text_score_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `exercise_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `file`
-- 
ALTER TABLE `file`
  ADD CONSTRAINT `file_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `file_content`
-- 
ALTER TABLE `file_content`
  ADD CONSTRAINT `file_content_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `file_content_ibfk_2` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `tbl_profiles`
-- 
ALTER TABLE `tbl_profiles`
  ADD CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE;

-- 
-- Constraints for table `user_content_log`
-- 
ALTER TABLE `user_content_log`
  ADD CONSTRAINT `user_content_log_ibfk_1` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_content_log_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `user_course`
-- 
ALTER TABLE `user_course`
  ADD CONSTRAINT `user_course_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_profiles` (`user_id`),
  ADD CONSTRAINT `user_course_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`);

-- 
-- Constraints for table `user_permission`
-- 
ALTER TABLE `user_permission`
  ADD CONSTRAINT `user_permission_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
