<?php
return array(
	// Menu

        'All courses'=>'All courses',
        'Register'=>"Register",
        'Login'=>"Login",
        'Logout'=>"Logout",
        "Let's study"=>"Let's study",
        "Choose your course and start now"=>"Choose your course and start now",
        "View All Courses"=>"View All Courses",
        "Course's Detail"=>"Course's Detail",
        "Student"=>"Student",
        "Teacher"=>"Teacher",
        "Manage Content"=>"Manage Content",
        "Course Preview"=>"Course Preview",
        "My Course"=>"My Course",
        "Change Password"=>"Change Password",
        "Reset Password"=>"Reset Password",
        "Please login or Register"=>"Please login or Register",
        "Remember me"=>"Remember me",
        "Teaching Course"=>"Teaching Course",
        "Redeem a Coupon"=>"Redeem a Coupon",
        "Take Course"=>"Take Course",
        "Course Description"=>"Course Description",  
        "Course's Content"=>"Course's Content",
        "Unit"=>"Unit",
        "Relate Course"=>"Relate Course",
        "Base Course"=>"Base Course",
        "Continue Course"=>"Continue Course",
        "None"=>"None",
        "Register Student"=>"Register Student",
        "Study This Course"=>"Study This Course",
        "person(s)"=>"person(s)",
        "Others Member"=>"Others Member",
        "Course Infomation"=>"Course Infomation",
        "Course Name"=>"Course Name",
        "Description"=>"Description",
        "Teacher Name"=>"Teacher Name",
        "Category"=>"Category",
        "Course Photo"=>"Course Photo",
        "Teacher Information"=>"Teacher Information",
        "Teacher Name"=>"Teacher Name",
        "Detail"=>"Detail",
        "Add Teacher"=>"Add Teacher",
        "Course Price"=>"Course Price",
        "Create New Course"=>"Create New Course",
        "Add Unit"=>"Add Unit",
        "Add Lesson"=>"Add Lesson",
        "Add Exercise"=>"Add Exercise",
        "Continue"=>"Continue",
        "Review"=>"Review",
        "Create Coupon"=>"Create Coupon",
        "Discount"=>"Discount",
        "Coupon Description"=>"Coupon Description",
        "Coupon"=>"Coupon",
        "Quality Use"=>"Quality Use",
        "Expire Date"=>"Expire Date",
        "Quality Coupon"=>"Quality Coupon",
        "Form"=>"Form",
        "Prefix"=>"Prefix",
        "Print Coupon"=>"Print Coupon",
        "Coupong Home"=>"Coupong Home",
        "quantity"=>"quantity",
        "No."=>"No.",
        "Coupon History"=>"Coupon History",
        "By"=>"By",
        "Coupon Date"=>"Coupon Date",
        "Code"=>"Code",
        "Quantity Use"=>"Quantity Use",
        "tag"=>"tag",
        "about"=>"about",
        "Has Studyied"=>"Has Studyied",
        "Before Course"=>"Before Course",
        "After Course"=>"After Course",
        "Save"=>"Save",
        "Home"=>"Home",
        "Edit ProFile"=>"Edit ProFile",
        "DashBoard"=>"DashBoard",
        "Logout"=>"Logout",
        "Question"=>"Question",
        "Show Order"=>"Show Order",
        "Answer"=>"Answer",
        "Course Status"=>"Course Status",
        "Exercise"=>"Exercise",
        "Add Question MultiChoice"=>"Add Question MulitChoice",
        "Add Question 2 Choice"=>"Add Question 2 Choice",
        "Question Page"=>"Question Page",
        "Question Page"=>"Question Page",
        "Quantity true"=>"Quantity true",
        "Quantity false"=>"Quantity false",
        "All Person"=>"All Person",
        "person(s)"=>"person(s)",
        "Percent"=>"Percent",
        "Stat Question"=>"Stat Question",
        "Back"=>"Back",
        "Nobody Does This Test"=>"Nobody Does This Test",
        "Stat Exercise"=>"Stat Exercise",
        "Stat Correct Score Phase"=>"Stat Correct Score Phase",
        "point(s)"=>"point(s)",
        "MyCourse"=>"MyCourse",
        "Student"=>"Student",
        "Teacher"=>"Teacher",
        "Course"=>"Course",
        "Add Student"=>"Add Student",
        "Add Question Text"=>"Add Question Text",
        "Review This Exercise"=>"Review This Exercise",
        "Public"=>"Public",
        "Add Teacher"=>"Add Teacher",
        "UserFile"=>"UserFile",
        "All Student Progress"=>"All Student Progress",
        "Lesson Stat"=>"Lesson Stat",


        "Start learning today"=>"Start learning today",    
        "All courses are provided by Center for Veterinary Continuing Education"=>"All courses are provided by Center for Veterinary Continuing Education",

        "Faculty of Veterinary Science, Chulalongkorn University, Henri-Dunant Rd., Pathumwan, Bangkok 10330, Thailand. Tel.+66(0) 2218 9771,3 Fax.+66(0) 2255 3910"=>"Faculty of Veterinary Science, Chulalongkorn University, Henri-Dunant Rd., Pathumwan, Bangkok 10330, Thailand. Tel.+66(0) 2218 9771,3 Fax.+66(0) 2255 3910",
);
