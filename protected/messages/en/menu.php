<?php
return array(
        'Profile' => 'Profile',
        'File manager' => 'File manager',
        'Dashboard' => 'Dashboard',
        'Upload ATM slip' => 'Upload ATM slip',        
        'Logout' => 'Logout',

        'Approve ATM slip' => 'Approve ATM slip',
        'Approve Teacher' => 'Approve Teacher',
        'Create student account' => 'Create student account',
);
