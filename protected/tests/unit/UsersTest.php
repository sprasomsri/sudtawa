<?php
//example assigun for all model in onetime
//use for model


class UsersTest extends CDbTestCase
{
    /**
     * @dataProvider provider
     */
    
    
    //use validate form model to check all 
    public function testValidate($a)
    {
        $u = new Users;
        $u->setAttributes(array(
            'username'=>$a,
        ));
        
        $this->assertTrue($u->validate());

    }
 
    
    //set attibute form model     
    public function provider()
    {
        return array(
            array(''),   // no username
            array('A'),   // username too short
            array('0123456789012345678901234567890123456789'),   // username too long
            array('A!B@C#D$'),   // username non alphanumeric
            array('admin'),   // username not unique 
            array('test_user'),   // pass test
 
            );
    }
}

?>
