<?php

class m140825_035923_newProject extends CDbMigration
{
	public function up()
{
            
            
    $this->createTable('atm_evidence', array(
        'id'=>'int(11) NOT NULL',
        'ref_no'=>'varchar(500) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'course_id'=>'int(11) NOT NULL',
        'tranfer_date'=>'datetime NOT NULL',
        'create_date'=>'datetime NOT NULL',
        'update_date'=>'datetime NOT NULL',
        'is_approve'=>'tinyint(1) NOT NULL',
        'evidence_file'=>'varchar(500) NOT NULL',        
        'from_account'=>'varchar(500) NOT NULL',
        'from_bank'=>'varchar(500) NOT NULL',
        'account_name'=>'varchar(500) NOT NULL',
        'qty_money'=>'int(10) NOT NULL',
        'active_status'=>'tinyint(1) NOT NULL DEFAULT "1"',
    ), '');

            
            
    $this->createTable('codeEditor', array(
        'id'=>'varchar(100) NOT NULL',
        'code'=>'text DEFAULT NULL',
        'admin_id'=>'int(11) NOT NULL',
        'admin_update_id'=>'int(11) NOT NULL',
        'create_date'=>'datetime NOT NULL',
        'update_date'=>'datetime NOT NULL',
    ), '');

    $this->addPrimaryKey('pk_codeEditor', 'codeEditor', 'id');

    $this->createTable('codeEditor_in_content', array(
        'id'=>'pk',
        'content_id'=>'int(11) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'code'=>'text DEFAULT NULL',
        'position'=>'varchar(50) NOT NULL DEFAULT "below"',
        'type'=>'varchar(100) DEFAULT NULL',
    ), '');

    $this->createTable('coin_setting', array(
        'id'=>'pk',
        'coin_amount'=>'int(11) NOT NULL',
        'money_amount'=>'int(11) NOT NULL',
        'is_promotion'=>'int(1) NOT NULL DEFAULT "1"',
        'code'=>'varchar(100) DEFAULT NULL',
        'description'=>'varchar(500) DEFAULT NULL',
        'create_date'=>'datetime NOT NULL',
        'update_date'=>'datetime NOT NULL',
        'active_status'=>'int(1) NOT NULL DEFAULT "1"',
    ), '');

    $this->createTable('content', array(
        'id'=>'pk',
        'title'=>'varchar(500) NOT NULL',
        'course_id'=>'int(11) NOT NULL',
        'parent_id'=>'int(11) NOT NULL DEFAULT "-1"',
        'detail'=>'text DEFAULT NULL',
        'youtube_link'=>'varchar(500) DEFAULT NULL',
        'show_order'=>'tinyint(3) NOT NULL DEFAULT "1"',
        'file_id'=>'int(11) DEFAULT NULL',
        'is_public'=>'tinyint(1) NOT NULL DEFAULT "1"',
        'is_exercise'=>'tinyint(1) NOT NULL',
        'is_code'=>'tinyint(1) NOT NULL',
        'is_free'=>'tinyint(1) NOT NULL',
        'is_approve'=>'tinyint(1) NOT NULL',
        'update_date'=>'datetime DEFAULT NULL',
    ), '');

    $this->createIndex('idx_course_id', 'content', 'course_id', FALSE);


    $this->createTable('content_modify', array(
        'id'=>'pk',
        'original_id'=>'int(11) NOT NULL',
        'parent_id'=>'int(11) NOT NULL DEFAULT "-1"',
        'title'=>'varchar(500) NOT NULL',
        'detail'=>'text DEFAULT NULL',
        'youtube_link'=>'varchar(500) DEFAULT NULL',
        'show_order'=>'tinyint(3) NOT NULL DEFAULT "1"',
        'file_id'=>'int(11) DEFAULT NULL',
        'is_public'=>'tinyint(1) NOT NULL DEFAULT "1"',
        'is_exercise'=>'tinyint(1) NOT NULL',
        'is_code'=>'tinyint(1) NOT NULL',
        'is_free'=>'tinyint(1) NOT NULL',
        'is_change_content'=>'tinyint(1) NOT NULL',
        'is_change_title'=>'tinyint(1) NOT NULL',
        'is_change_order'=>'tinyint(1) NOT NULL',
        'is_change_file'=>'tinyint(1) NOT NULL',
        'is_approve'=>'tinyint(4) NOT NULL',
        'date_approve'=>'datetime DEFAULT NULL',
    ), '');

    

    

    $this->createTable('content_suggestion', array(
        'id'=>'pk',
        'content_id'=>'int(11) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'message'=>'text NOT NULL',
        'create_date'=>'datetime NOT NULL',
    ), '');

    $this->createTable('coupon', array(
        'id'=>'pk',
        'code'=>'varchar(500) NOT NULL',
        'discount'=>'tinyint(3) NOT NULL',
        'expire_date'=>'date NOT NULL',
        'qty_use'=>'tinyint(5) NOT NULL DEFAULT "1"',
        'detail'=>'varchar(1000) DEFAULT NULL',
        'create_date'=>'datetime NOT NULL',
        'course_id'=>'int(11) NOT NULL DEFAULT "-1"',
        'user_id'=>'int(11) NOT NULL',
    ), '');

    $this->createIndex('idx_course_id', 'coupon', 'course_id', FALSE);

    $this->createIndex('idx_user_id', 'coupon', 'user_id', FALSE);

    $this->createTable('coupon_log', array(
        'coupon_id'=>'int(11) NOT NULL',
        'use_course_id'=>'int(11) NOT NULL',
        'used_time'=>'datetime NOT NULL',
        'use_user_id'=>'int(11) NOT NULL',
    ), '');

    $this->createTable('course', array(
        'id'=>'pk',
        'name'=>'varchar(500) NOT NULL',
//        'name_th'=>'varchar(500) NOT NULL',
//        'tag'=>'varchar(20) NOT NULL',
        'description'=>'text DEFAULT NULL',
//        'about'=>'text NOT NULL',
        'price'=>'int(5) NOT NULL',
        'course_img'=>'varchar(1000) DEFAULT NULL',
        'course_status'=>'tinyint(1) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
//        'commission'=>'int(11) NOT NULL',
        'duration'=>'tinyint(5) DEFAULT NULL',
        'is_public'=>'tinyint(1) NOT NULL DEFAULT "1"',
        'is_affiliate'=>'tinyint(1) NOT NULL DEFAULT "1"',
        'demo_video'=>'int(11) DEFAULT NULL',
        'theme_photo'=>'varchar(500) DEFAULT NULL',
        'youtube_demo'=>'varchar(500) DEFAULT NULL',
        'date_start'=>'datetime DEFAULT NULL',
        'date_end'=>'datetime DEFAULT NULL',
        'course_option'=>'tinyint(1) NOT NULL DEFAULT "1"',
    ), '');
    

    $this->createIndex('idx_user_id', 'course', 'user_id', FALSE);

    $this->createTable('course_division_rate', array(
        'id'=>'pk',
        'course_id'=>'int(11) NOT NULL',
        'teacher_share_percent'=>'int(3) NOT NULL',
        'company_share_percent'=>'int(3) NOT NULL',
        'affiliate_share_percent'=>'int(3) NOT NULL',
        'create_date'=>'datetime NOT NULL',
        'is_default'=>'tinyint(1) NOT NULL',
        'remark'=>'text DEFAULT NULL',
    ), '');

    $this->createTable('course_money_division', array(
        'id'=>'pk',
        'teacher_share_percent'=>'varchar(500) NOT NULL',
        'teacher_share_price'=>'varchar(500) NOT NULL',
        'company_share_percent'=>'varchar(500) NOT NULL',
        'company_share_price'=>'varchar(500) NOT NULL',
        'affiliate_share_percent'=>'varchar(500) NOT NULL',
        'affiliate_share_price'=>'varchar(500) NOT NULL',
        'affiliate_code'=>'varchar(500) DEFAULT NULL',
        'course_id'=>'int(11) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'create_date'=>'datetime NOT NULL',
    ), '');

    $this->createTable('course_payment', array(
        'id'=>'pk',
        'user_id'=>'int(11) NOT NULL',
        'course_id'=>'int(11) NOT NULL',
        'create_date'=>'datetime NOT NULL',
        'update_date'=>'datetime DEFAULT NULL',
        'invoice_number'=>'varchar(500) NOT NULL',
        'approveCode_first'=>'varchar(500) NOT NULL',
        'approveCode_next'=>'varchar(500) DEFAULT NULL',
        'qty_coin'=>'int(11) DEFAULT NULL',
        'result'=>'varchar(500) DEFAULT NULL',
        'result_next'=>'varchar(500) DEFAULT NULL',
        'apCode'=>'varchar(500) DEFAULT NULL',
        'amt'=>'varchar(500) DEFAULT NULL',
        'fee'=>'varchar(500) DEFAULT NULL',
        'method'=>'varchar(500) DEFAULT NULL',
        'method_name'=>'varchar(500) DEFAULT NULL',
        'confirm_cs'=>'varchar(10) DEFAULT NULL',
        'remark'=>'text DEFAULT NULL',
        'is_active'=>'tinyint(1) NOT NULL DEFAULT "1"'
    ), '');

    $this->createTable('course_rating', array(
        'id'=>'pk',
        'user_id'=>'int(11) NOT NULL',
        'course_id'=>'int(11) NOT NULL',
        'rating'=>'float NOT NULL',
        'create_date'=>'datetime NOT NULL',
        'update_date'=>'datetime NOT NULL',
    ), '');

    $this->createTable('exercise', array(
        'id'=>'pk',
        'content_id'=>'int(11) NOT NULL',
        'qty_show'=>'int(2) NOT NULL DEFAULT "10"',
        'is_random'=>'tinyint(1) NOT NULL',
    ), '');

    $this->createIndex('idx_content_id', 'exercise', 'content_id', FALSE);

    $this->createTable('exercise_answer_choice', array(
        'id'=>'pk',
        'answer'=>'varchar(1000) NOT NULL',
        'hint'=>'varchar(700) DEFAULT NULL',
        'is_true'=>'tinyint(1) NOT NULL',
        'question_id'=>'int(11) NOT NULL',
    ), '');

    $this->createIndex('idx_question_id', 'exercise_answer_choice', 'question_id', FALSE);

    $this->createTable('exercise_choice_score', array(
        'id'=>'pk',
        'answer_id'=>'int(11) NOT NULL',
        'question_id'=>'int(11) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'do_time'=>'datetime NOT NULL',
        'is_true'=>'tinyint(1) NOT NULL',
    ), '');

    $this->createTable('exercise_code_answer', array(
        'id'=>'pk',
        'question_id'=>'int(11) NOT NULL',
        'code'=>'text DEFAULT NULL',
        'answer'=>'text NOT NULL',
    ), '');

    $this->createTable('exercise_code_score', array(
        'id'=>'pk',
        'question_id'=>'int(11) NOT NULL',
        'code'=>'text DEFAULT NULL',
        'answer'=>'text DEFAULT NULL',
        'user_id'=>'int(11) NOT NULL',
        'do_time'=>'datetime NOT NULL',
        'is_true'=>'tinyint(1) NOT NULL',
    ), '');

    $this->createTable('exercise_question', array(
        'id'=>'pk',
        'question'=>'text NOT NULL',
        'initial_code'=>'text DEFAULT NULL',
        'show_order'=>'tinyint(2) NOT NULL DEFAULT "1"',
        'type'=>'tinyint(1) NOT NULL DEFAULT "1"',
        'code_type'=>'varchar(100) DEFAULT NULL',
        'is_public'=>'tinyint(1) NOT NULL DEFAULT "1"',
        'exercise_id'=>'int(11) NOT NULL',
    ), '');

    $this->createIndex('idx_exercise_id', 'exercise_question', 'exercise_id', FALSE);

    $this->createTable('exercise_stat', array(
        'id'=>'pk',
        'exercise_id'=>'int(11) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'do_time'=>'datetime NOT NULL',
        'qty_question'=>'int(3) NOT NULL',
        'qty_correct'=>'int(3) NOT NULL',
        'question_list'=>'varchar(300) DEFAULT NULL',
    ), '');

    $this->createTable('exercise_text_answer', array(
        'id'=>'pk',
        'answer'=>'varchar(1000) NOT NULL',
        'question_id'=>'int(11) NOT NULL',
        'hint'=>'varchar(700) DEFAULT NULL',
        'sequence'=>'int(2) NOT NULL DEFAULT "1"',
    ), '');

    $this->createIndex('idx_question_id', 'exercise_text_answer', 'question_id', FALSE);

    $this->createTable('exercise_text_score', array(
        'id'=>'pk',
        'question_id'=>'int(11) NOT NULL',
        'answer'=>'varchar(3000) DEFAULT NULL',
        'user_id'=>'int(11) NOT NULL',
        'do_time'=>'datetime NOT NULL',
        'sequence'=>'tinyint(3) NOT NULL DEFAULT "1"',
        'is_true'=>'tinyint(1) NOT NULL',
    ), '');

    $this->createTable('external_provider', array(
        'id'=>'pk',
        'user_id'=>'int(11) NOT NULL',
        'provider'=>'varchar(100) NOT NULL DEFAULT "facebook"',
        'user_provide_name'=>'varchar(500) DEFAULT NULL',
        'email'=>'varchar(500) NOT NULL',
        'provider_id'=>'varchar(500) NOT NULL',
        'create_date'=>'datetime NOT NULL',
    ), '');

    $this->createTable('file', array(
        'id'=>'pk',
        'name'=>'varchar(1000) NOT NULL',
        'original_name'=>'varchar(1000) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'type'=>'varchar(100) NOT NULL',
        'duration'=>'varchar(500) DEFAULT NULL',
        'create_at'=>'datetime NOT NULL',
    ), '');

    $this->createTable('file_content', array(
        'id'=>'pk',
        'file_id'=>'int(11) NOT NULL',
        'content_id'=>'int(11) NOT NULL',
    ), '');

    $this->createTable('poll_subject', array(
        'id'=>'pk',
        'category'=>'varchar(1000) NOT NULL',
        'name'=>'varchar(1000) NOT NULL',
        'user_id'=>'int(11) DEFAULT NULL',
        'lasted_date'=>'datetime NOT NULL',
        'price'=>'varchar(500) NOT NULL DEFAULT "0"',
    ), '');

    $this->createTable('reference_site', array(
        'id'=>'pk',
        'website'=>'varchar(1000) NOT NULL',
        'times'=>'int(100) NOT NULL DEFAULT "1"',
        'create_date'=>'datetime NOT NULL',
        'lasted_date'=>'datetime NOT NULL',
    ), '');

    $this->createTable('relate_course', array(
        'id'=>'pk',
        'course_id'=>'int(11) NOT NULL',
        'before_course'=>'varchar(100) DEFAULT NULL',
        'after_course'=>'varchar(100) DEFAULT NULL',
    ), '');




    $this->createTable('tbl_profiles', array(
        'user_id'=>'pk',
        'lastname'=>'varchar(50) NOT NULL',
        'position'=>'varchar(500) DEFAULT NULL',
        'firstname'=>'varchar(50) NOT NULL',
        'company'=>'varchar(3000) DEFAULT NULL',
        'detail'=>'varchar(3000) DEFAULT NULL',
        'photo'=>'varchar(500) DEFAULT NULL',
        'tel'=>'VARCHAR(20) DEFAULT NULL'
    ), '');
    
        $this->createTable('log_student_step', array(
        'user_id'=>'pk',
        'is_change_password'=>'tinyint(1) NOT NULL DEFAULT "0"',    
        'is_edit_profile'=>'tinyint(1) NOT NULL DEFAULT "0"'
  
    ), '');
    
    
    

    $this->createTable('tbl_profiles_fields', array(
        'id'=>'pk',
        'varname'=>'varchar(50) NOT NULL',
        'title'=>'varchar(255) NOT NULL',
        'field_type'=>'varchar(50) NOT NULL',
        'field_size'=>'varchar(15) NOT NULL DEFAULT "0"',
        'field_size_min'=>'varchar(15) NOT NULL DEFAULT "0"',
        'required'=>'int(1) NOT NULL',
        'match'=>'varchar(255) NOT NULL',
        'range'=>'varchar(255) NOT NULL',
        'error_message'=>'varchar(255) NOT NULL',
        'other_validator'=>'varchar(5000) NOT NULL',
        'default'=>'varchar(255) NOT NULL',
        'widget'=>'varchar(255) NOT NULL',
        'widgetparams'=>'varchar(5000) NOT NULL',
        'position'=>'int(3) NOT NULL',
        'visible'=>'int(1) NOT NULL',
    ), '');

    $this->createTable('tbl_users', array(
        'id'=>'pk',
        'username'=>'varchar(128) NOT NULL',
        'password'=>'varchar(128) NOT NULL',
        'email'=>'varchar(128) NOT NULL',
        'facebook_id'=>'varchar(500) DEFAULT NULL',
        'activkey'=>'varchar(128) NOT NULL',
        'create_at'=>'timestamp NOT NULL',
        'lastvisit'=>'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
        'superuser'=>'int(1) NOT NULL',
        'is_inside'=>'tinyint(1) NOT NULL',
        'is_student'=>'tinyint(1) NOT NULL DEFAULT "0"',
        'status'=>'int(1) NOT NULL',
    ), '');

    $this->createTable('test', array(
        'id'=>'pk',
        'detail'=>'text DEFAULT NULL',
    ), '');

    $this->createTable('user_affiliate', array(
        'id'=>'pk',
        'user_id'=>'int(11) NOT NULL',
        'affiliate_code'=>'varchar(500) DEFAULT NULL',
        'bankaccount'=>'varchar(200) DEFAULT NULL',
        'bank_name'=>'varchar(500) NOT NULL',
        'subbranch'=>'varchar(200) DEFAULT NULL',
        'telephone'=>'varchar(100) NOT NULL',
        'detail'=>'text DEFAULT NULL',
        'active_status'=>'tinyint(1) NOT NULL',
        'create_date'=>'datetime NOT NULL',
    ), '');

    $this->createTable('user_affiliate_course', array(
        'id'=>'pk',
        'user_id'=>'int(11) NOT NULL',
        'course_id'=>'int(11) NOT NULL',
        'affiliate_id'=>'int(11) NOT NULL',
    ), '');

    $this->createTable('user_coin', array(
        'user_id'=>'int(11) NOT NULL',
        'create_date'=>'datetime NOT NULL',
        'update_date'=>'datetime NOT NULL',
        'coin'=>'int(11) NOT NULL',
    ), '');

    $this->addPrimaryKey('pk_user_coin', 'user_coin', 'user_id');

    $this->createTable('user_content_log', array(
        'id'=>'pk',
        'content_id'=>'int(11) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'study_time'=>'datetime NOT NULL',
        'course_id'=>'int(11) NOT NULL',
        'qty_times'=>'int(3) NOT NULL DEFAULT "1"',
        'is_end'=>'tinyint(1) NOT NULL',
    ), '');

    $this->createTable('user_course', array(
        'id'=>'pk',
        'user_id'=>'int(11) NOT NULL',
        'course_id'=>'int(11) NOT NULL',
        'bought_price'=>'int(5) NOT NULL',
        'role'=>'tinyint(1) NOT NULL DEFAULT "1"',
        'is_head'=>'tinyint(4) NOT NULL',
        'is_show'=>'int(1) NOT NULL DEFAULT "1"',
        'create_date'=>'datetime DEFAULT NULL',
        'expire_date'=>'datetime DEFAULT NULL',
        'is_active'=>'tinyint(1) NOT NULL DEFAULT "1"',
        
    ), '');

    $this->createTable('user_file', array(
        'id'=>'pk',
        'name'=>'varchar(1000) NOT NULL',
        'original_name'=>'varchar(2000) NOT NULL',
        'type'=>'varchar(100) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'create_at'=>'datetime NOT NULL',
    ), '');

    $this->createTable('user_permission', array(
        'id'=>'pk',
        'user_id'=>'int(11) NOT NULL',
        'can_create_course'=>'int(11) NOT NULL',       
        'is_request'=>'tinyint(1) NOT NULL DEFAULT "0"',
        'request_date'=>'datetime DEFAULT NULL',
    ), '');

    $this->createTable('vdo_buffer_rate', array(
        'id'=>'pk',
        'content_id'=>'int(11) NOT NULL',
        'user_id'=>'int(11) NOT NULL',
        'times'=>'int(5) NOT NULL DEFAULT "1"',
        'create_date'=>'datetime NOT NULL',
    ), '');
    
    
    
   $sql_view_content_info="CREATE VIEW `content_info` AS select max(`content`.`course_id`) AS `course_id`,max(`course`.`name`) AS `course_name`,count(`content`.`id`) AS `number_of_content` from (`content` join `course` on((`content`.`course_id` = `course`.`id`))) where (`content`.`parent_id` <> -(1)) group by `content`.`course_id`";
   $this->execute($sql_view_content_info);
    
        
    $sql_view_approve_list= "CREATE VIEW `content_sort_list` AS select (case when (`content_modify`.`show_order` is not null) then `content_modify`.`show_order` else `content`.`show_order` end) AS `show_order`,(case when (`content_modify`.`parent_id` is not null) then `content_modify`.`parent_id` else `content`.`parent_id` end) AS `parent_id`,`content`.`title` AS `title`,`content`.`id` AS `id`,`content_modify`.`id` AS `modify_id`,`content_modify`.`is_change_content` AS `is_change_content`,`content_modify`.`is_change_title` AS `is_change_title`,`content_modify`.`is_change_order` AS `is_change_order`,`content_modify`.`is_change_file` AS `is_change_file`,`content_modify`.`date_approve` AS `date_approve`,`content`.`update_date` AS `update_date`,`content`.`course_id` AS `course_id`,`content`.`is_exercise` AS `is_exercise`,`content`.`is_code` AS `is_code`,`content`.`is_public` AS `is_public`,`content`.`is_approve` AS `is_approve` from (`content` left join `content_modify` on((`content_modify`.`original_id` = `content`.`id`))) group by `content`.`id`,`content_modify`.`original_id` order by `content_modify`.`show_order`,`content`.`show_order`,`content_modify`.`id`,`content`.`id`;";
    $this->execute($sql_view_approve_list); 
    
    $view_end_lesson="CREATE VIEW stat_end_content AS
                SELECT content.id AS content_id,user_content_log.user_id AS user_id,user_content_log.is_end as is_end
                FROM content
                RIGHT JOIN user_content_log ON content.id = user_content_log.id
                GROUP BY user_content_log.content_id,user_content_log.user_id
                ";
    $this->execute($view_end_lesson); 

    $sql_study_inlesson="CREATE VIEW `stat_study_in_lesson` AS select count(`log`.`id`) AS `qty_study_lesson`,sum(`log`.`qty_times`) AS `total_time_view`,`content`.`id` AS `content_id`,`content`.`title` AS `title`,`content`.`show_order` AS `show_order`,`content`.`parent_id` AS `parent_id`,`content`.`course_id` AS `course_id` from (`content` join `user_content_log` `log` on((`content`.`id` = `log`.`content_id`))) where (`content`.`parent_id` > -(1)) group by `log`.`content_id`";
    $this->execute($sql_study_inlesson);

    $sql_stat_user_study="CREATE VIEW `stat_user_study` AS select count(`user_content_log`.`content_id`) AS `content_has_studied`,max(`user_content_log`.`user_id`) AS `user_id`,max(`user_content_log`.`course_id`) AS `course_id`,max(`content_info`.`number_of_content`) AS `number_of_content` from (`user_content_log` join `content_info` on((`user_content_log`.`course_id` = `content_info`.`course_id`))) group by `user_content_log`.`course_id`,`user_content_log`.`user_id`";
    $this->execute($sql_stat_user_study);
    
    

    $this->addForeignKey('fk_content_course_course_id', 'content', 'course_id', 'course', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_coupon_course_course_id', 'coupon', 'course_id', 'course', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_coupon_tbl_users_user_id', 'coupon', 'user_id', 'tbl_users', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_course_tbl_users_user_id', 'course', 'user_id', 'tbl_users', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_exercise_content_content_id', 'exercise', 'content_id', 'content', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_exercise_answer_choice_exercise_question_question_id', 'exercise_answer_choice', 'question_id', 'exercise_question', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_exercise_question_exercise_exercise_id', 'exercise_question', 'exercise_id', 'exercise', 'id', 'NO ACTION', 'NO ACTION');

    $this->addForeignKey('fk_exercise_text_answer_exercise_question_question_id', 'exercise_text_answer', 'question_id', 'exercise_question', 'id', 'NO ACTION', 'NO ACTION');

}


public function down()
{
    $this->dropForeignKey('fk_content_course_course_id', 'content');

    $this->dropForeignKey('fk_coupon_course_course_id', 'coupon');

    $this->dropForeignKey('fk_coupon_tbl_users_user_id', 'coupon');

    $this->dropForeignKey('fk_course_tbl_users_user_id', 'course');

    $this->dropForeignKey('fk_exercise_content_content_id', 'exercise');

    $this->dropForeignKey('fk_exercise_answer_choice_exercise_question_question_id', 'exercise_answer_choice');

    $this->dropForeignKey('fk_exercise_question_exercise_exercise_id', 'exercise_question');

    $this->dropForeignKey('fk_exercise_text_answer_exercise_question_question_id', 'exercise_text_answer');

    $this->dropTable('codeEditor');
    $this->dropTable('codeEditor_in_content');
    $this->dropTable('coin_setting');
    $this->dropTable('content');
    $this->dropTable('content_info');
    $this->dropTable('content_modify');
    $this->dropTable('content_sort_list');
    $this->dropTable('content_suggestion');
    $this->dropTable('coupon');
    $this->dropTable('coupon_log');
    $this->dropTable('course');
    $this->dropTable('course_division_rate');
    $this->dropTable('course_money_division');
    $this->dropTable('course_payment');
    $this->dropTable('course_rating');
    $this->dropTable('exercise');
    $this->dropTable('exercise_answer_choice');
    $this->dropTable('exercise_choice_score');
    $this->dropTable('exercise_code_answer');
    $this->dropTable('exercise_code_score');
    $this->dropTable('exercise_question');
    $this->dropTable('exercise_stat');
    $this->dropTable('exercise_text_answer');
    $this->dropTable('exercise_text_score');
    $this->dropTable('external_provider');
    $this->dropTable('file');
    $this->dropTable('file_content');
    $this->dropTable('poll_subject');
    $this->dropTable('reference_site');
    $this->dropTable('relate_course');
    $this->dropTable('stat_end_content');
    $this->dropTable('stat_study_in_lesson');
    $this->dropTable('stat_user_study');
    $this->dropTable('tbl_profiles');
    $this->dropTable('tbl_profiles_fields');
    $this->dropTable('tbl_users');
    $this->dropTable('test');
    $this->dropTable('user_affiliate');
    $this->dropTable('user_affiliate_course');
    $this->dropTable('user_coin');
    $this->dropTable('user_content_log');
    $this->dropTable('user_course');
    $this->dropTable('user_file');
    $this->dropTable('user_permission');
    $this->dropTable('vdo_buffer_rate');
    $this->dropTable("atm_evidence");
    
}

}