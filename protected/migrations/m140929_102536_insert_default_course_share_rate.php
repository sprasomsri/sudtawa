<?php

class m140929_102536_insert_default_course_share_rate extends CDbMigration
{
	public function up()
	{
		$this->insert('course_division_rate', array(
			'course_id' => '0',
			'teacher_share_percent' => '60',
			'company_share_percent' => '40',
			'affiliate_share_percent' => '0',
			'create_date' => date('2014-09-29 00:00:00'),
			'is_default' => '1',
			'remark' => 'DEFAULT MARKET SHARE',
		));
	}

	public function down()
	{
		$this->delete('course_division_rate', "course_id='0'");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
