<?php 
//controller for redactor uploads
class SyncController extends Controller {



     
    public function actionSyncCourse($course_id=""){
        
        
       //get sring
       //what table (course, content,codeEditor_in_content,exercise,user )
       //create insert command 
        $user_id = Yii::app()->user->id;
        $data_array=array();
        $model_course=  Course::model()->findByPk($course_id);
        if(!empty($model_course)){
//            $data_array['course']="id=$model_course->id,name=$model_course->name,
//                                  name_th=$model_course->name_th,tag=$model_course->tag,
//                                  description=$model_course->description,
//                                  about=$model_course->about,price=$model_course->price,
//                                  course_img=$model_course->course_img,
//                                  course_status=$model_course->course_status,
//                                  user_id=$model_course->user_id,commission=$model_course->commission,
//                                  duration=$model_course->duration,is_public=$model_course->is_public,
//                                  demo_video=$model_course->demo_video";
//           
            
            $list_field=$model_course->attributeLabels();
            $data_course="";
            foreach ($list_field as $field=>$name_field){
                $data_course.=$field."=".$model_course->$field.",";
            }
            $data_course= rtrim($data_course, ",");
            $data_array['course']=$data_course;            
          
            
            
           $model_contents=  Content::model()->findAll("course_id='$course_id'"); 
           
           $lis_exercise_content=""; #tbl exercise
           $list_code_lesson=""; #tbl codeEditor_in_content
           $list_exercise="";
           $user_list="";
           $file_list="";
           
           if(!empty($model_contents)){              
               $data_content="";
               foreach ($model_contents as $content){
                   $list_field_content=$content->attributeLabels();
                   
                   if($content->is_exercise==1){
                       $lis_exercise_content.= $content->id.",";
                   }
                   
                   if($content->is_code==1){
                       $list_code_lesson= $content->id.",";
                   }
                   
                   if(!empty($content->file_id) && $content->file_id != "NULL"){
                       $file_list .= $content->file_id.",";
                   }
                   
                   
                   foreach ($list_field_content as $field => $name_field){
                       $data_content.= $field."=".$content->$field.",";
                    }
                    $data_content=  rtrim($data_content,",");
                    $data_content.="#####";
               }  
               $data_array['content']= rtrim($data_content, "#####");     
               
           }           
          
           
           
           if(!empty($lis_exercise_content)){
               $lis_exercise_content=  rtrim($lis_exercise_content, ",");
               $sql_exercise= "SELECT * FROM exercise WHERE content_id IN ($lis_exercise_content)";
               $model_exercises=  Exercise::model()->findAllBySql($sql_exercise);
               
               if(!empty($model_exercises)){
                   $data_exercise="";
                   foreach ($model_exercises as $exercise){
                       $list_exercise.=$exercise->id.",";
                       
                       $list_field_exercise=$exercise->attributeLabels();
                       foreach ($list_field_exercise as $field => $name_field){
                            $data_exercise.= $field."=".$exercise->$field.",";
                        }
                        $data_exercise=  rtrim($data_exercise,",");
                        $data_exercise.="#####";                                              
                   }
                   $data_array['exercise']=rtrim($data_exercise, "#####");
               }    
           }
           
           
            if(!empty($list_exercise)){
               $list_exercise=  rtrim($list_exercise,",");
               $sql_exercise_question="SELECT * FROM exercise_question WHERE exercise_id IN ($list_exercise)";
               $model_questions=  ExerciseQuestion::model()->findAllBySql($sql_exercise_question);
               $data_question="";
               $question_list="";
               
               if(!empty($model_questions)){
                   foreach ($model_questions as $question){
                       $question_list.=$question->id.",";
                       
                       $list_field_question=$question->attributeLabels();
                       
                       foreach ($list_field_question as $field => $name_field){
                            $data_question.= $field."=".$question->$field.",";
                        }
                        $data_question=  rtrim($data_question,",");
                        $data_question.="#####";        
                       
                   }
                    $data_array['exercise_question']=rtrim($data_question, "#####");
               }
               
               
               if(!empty($question_list)){
                   $question_list=rtrim($question_list,",");
                   
                   $sql_answer_choice="SELECT * FROM exercise_answer_choice WHERE question_id IN ($question_list)";
                   $sql_answer_code = "SELECT * FROM exercise_code_answer WHERE question_id IN ($question_list)";
                   $sql_answer_text = "SELECT * FROM exercise_text_answer WHERE question_id IN ($question_list)";
                   
                   $model_answer_choices=  ExerciseAnswerChoice::model()->findAllBySql($sql_answer_choice);
                   if(!empty($model_answer_choices)){
                       $data_choice="";
                       foreach ($model_answer_choices as $choice){
                           
                           $list_field_choice=$choice->attributeLabels();
                           foreach ($list_field_choice as $field => $name_field){
                                $data_choice.= $field."=".$choice->$field.",";
                           }
                           $data_choice=  rtrim($data_choice,",");
                           $data_choice.="#####";  
                       }
                    
                        $data_array['exercise_answer_choice']=rtrim($data_choice, "#####");
                  }
                  
                  
                  
                  $model_answer_code=  ExerciseCodeAnswer::model()->findAllBySql($sql_answer_code);
                  if(!empty($model_answer_code)){
                      $data_code="";
                      foreach ($model_answer_code as $code){
                        $list_field_answer_code=$code->attributeLabels();
                        
                          foreach ($list_field_answer_code as $field => $name_field){
                                $data_code.= $field."=".$code->$field.",";
                           }
                            $data_code=  rtrim($data_code,",");
                            $data_code.="#####";  
                           
                      }
                     $data_array['exercise_code_answer']=  rtrim($data_code, "#####");                         
                  }
                  
                  $model_answer_texts=  ExerciseTextAnswer::model()->findAllBySql($sql_answer_text);
                  if(!empty($model_answer_texts)){
                      $data_text="";
                      foreach ($model_answer_texts as $text){
                            $list_field_answer_text=$text->attributeLabels();
                            
                            foreach ($list_field_answer_text as $field => $name_field){
                                  $data_text.= $field."=".$text->$field.",";
                             }
                             $data_text=  rtrim($data_text,",");
                             $data_text.="#####"; 
                      }
                      
                     $data_array['exercise_text_answer']= rtrim($data_text,"#####");         
                  }                  
                } 
                  
                
                $user_course_sql="SELECT * FROM user_course WHERE course_id ='$course_id' AND (user_id='$user_id' OR role <= '2') ";  
                
                $model_usercourse = UserCourse::model()->findAllBySql($user_course_sql);
                
                if (!empty($model_usercourse)) {
                    $data_usercourse = "";                    
                    foreach ($model_usercourse as $user_course) {
                        $user_list.=$user_course->user_id.",";
                        
                        $list_field_userincourse = $user_course->attributeLabels();
                        
                        foreach ($list_field_userincourse as $field => $name_field) {
                            $data_usercourse.= $field . "=" . $user_course->$field . ",";
                        }
                        
                         $data_usercourse=  rtrim($data_usercourse,",");
                         $data_usercourse.="#####"; 
                    }
                     $data_array['exercise_text_answer']= rtrim($data_text,"#####"); 
                }
                
                
                if(!empty($user_list)){
                    $user_list=  rtrim($user_list, ",");
                    $sql_user="SELECT * FROM tbl_users WHERE id IN ($user_list)";
                    
                    $model_users=  Users::model()->findAllBySql($sql_user);
                    
                    if(!empty($model_users)){
                        $data_user="";
                        foreach ($model_users as $user){
                            $list_field_user=$user->attributeLabels();
                            
                            foreach ($list_field_user as $field => $name_field){
                                $data_user.= $field . "=" . $user->$field . ",";
                            }
                            
                            $data_user=  rtrim($data_user,",");
                            $data_user.="#####";
                        }
                         $data_array['tbl_users']= rtrim($data_user,"#####"); 
                    }                    
                }
                
               
                if(!empty($file_list)){
                    $file_list= rtrim($file_list, ",");
                    $sql_file = "SELECT * FROM file WHERE id IN ($file_list)";
                    
                    $model_files =  File::model()->findAllBySql($sql_file);
                    $data_file="";
                    
                    foreach ($model_files as $file){
                          
                        $list_field_file=$file->attributeLabels();
                          
                        foreach ($list_field_file as $field => $name_field){
                             $data_file.= $field . "=" . $file->$field . ",";
                        }
                        
                         $data_file=  rtrim($data_file,",");
                         $data_file.="#####";
                          
                    }                     
                    $data_array['file']= rtrim($data_file,"#####"); 
                    
                }
                
               
           } //empty list exercise
           
           
           if(!empty($list_code_lesson)){
               $data_code="";
               $list_code_lesson= rtrim($list_code_lesson, ",");
               $sql_code_editor = "SELECT * FROM codeEditor_in_content WHERE content_id IN ($list_code_lesson)";
               $model_code= CodeEditorInContent::model()->findAllBySql($sql_code_editor);
               
               if(!empty($model_code)){
                   $data_code="";
                   foreach ($model_code as $code){
                        $list_field_code=$code->attributeLabels();
                        
                        foreach ($list_field_code as $field => $name_field){
                            $data_code.=$field."=".$code->$field.",";
                        }
                        $data_code.="#####";
                        
                   }
                   $data_array['codeEditor_in_content']=$data_code; 
                   
               }               
           }
           
             //userpermission
           $sql_permission="SELECT * FROM user_permission WHERE user_id='$user_id'";
           $model_userpermission= UserPermission::model()->findAllBySql($sql_permission);
              
              if (!empty($model_userpermission)) {
                $data_permission = "";
                foreach ($model_userpermission as $permission) {
                    $list_field_permission = $permission->attributeLabels();

                    foreach ($list_field_permission as $field => $name_field) {
                        $data_permission.= $field . "=" . $permission->$field . ",";
                    }

                    $data_permission = rtrim($data_permission, ",");
                    $data_permission.="#####";
                }
                 $data_array['user_permission']=$data_permission; 
            }  
            
        }
 

        
        $this->render("/post/sync_course",array("data_array"=>$data_array,"course_id"=>$course_id));
   }

}



?>
