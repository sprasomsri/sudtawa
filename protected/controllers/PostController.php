<?php 
//controller for redactor uploads
class PostController extends Controller {

    public function actionImg() {
   
      if (!empty($_FILES)) {
            $user_id = Yii::app()->user->id;
            $target_path = Yii::app()->basePath . "/../webroot/files/$user_id/pictures/";
     

            $file_type_list=array("jpg","png","gif","jpeg");
            list($type,$kind)=explode("/", $_FILES["file"]["type"]);
            
            if($type!=="image"){
          
                 echo "<script>
                       alert('worng type should be >> jpg jpeg gif OR png');                      
                      </script>";
                 exit;
            }

            #check is hane directory
            if (!is_dir($target_path)) {
                mkdir($target_path, 0775, true);
            }
            
            $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263'), 0, 50);
            $new_name = "coursecreek_".$random . "_" . $_FILES['file']['name'];

            $target_path_save = Yii::app()->basePath . "/../webroot/files/$user_id/pictures/" . $new_name;

            //save file to directory
            if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path_save)) {
                $this->saveFileInfo("image", $new_name, $_FILES['file']['name'], $user_id);      
                
                $img_src = "files/$user_id/pictures/$new_name";
                echo "<img src='$img_src'>";
            } else {
                 echo "<script>
                       alert('Sorry!!! Upload Fail Please try again');                      
                      </script>";
                 exit;
            }
        }
    }

    public function actionListimages() {
        $user_id = Yii::app()->user->id;
        $target_path = Yii::app()->basePath . "/../webroot/files/$user_id/pictures/";
        $file_address = "files/$user_id/pictures/";

        if (is_dir($target_path)) {
            $files = CFileHelper::findFiles($file_address, array('fileTypes' => array('gif', 'png', 'jpg', 'jpeg')));
            if (!empty($files)) {
                $data = array();
                foreach ($files as $file) {
                    $data[] = array(
                        'thumb' => $file,
                        'image' => $file,
                    );
                }
                echo CJSON::encode($data);
               exit;
            }
        }
    }
    
    /*
     public function actionGetAudioPlayer(){
        $id=$_POST['id'];
                
        $model_file=  File::model()->findByPk($id);
        $user_id =$model_file->user_id;
        $audio_mp3_url = Yii::app()->baseUrl . "/files/$user_id/audio/$model_file->name";
        
        list($name,$type)=  explode(".", $model_file->name);
        $ogg_file=$name.".ogg";
        $audio_ogg_url = Yii::app()->baseUrl . "/files/$user_id/audio/$ogg_file";
         
         echo "<audio controls>
                   <source src='$audio_mp3_url' type='audio/mpeg'> <!--- .mp3 for  for Internet Explorer, Chrome, and Safari -->
                   <source src='$audio_ogg_url' type='audio/ogg'> <!--- .ogg for   Firefox and Opera) -->
                   <embed width='50%' src='$audio_mp3_url'>
                </audio>";
    }   
    */
    
    

    #upload And Return link
    public function actionCodeEditior() {
//        $this->layout = "//layouts/ace";
//         $this->layout = "//layouts/blank";
        
//        $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263'), 0, 10);        
//        $date=date("d");        
//        $random=$date.$random;        
        
         $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263'), 0, 10);        
         $date=date("d");        
         $random=$date.$random;  
         
        $link=Yii::app()->createUrl("post/createEditor",array("random"=>$random));  
        $iframe="<iframe src='$link' height='300' width='100%' class='ace-frame' allowtransparency='true' ></iframe>";
        echo $iframe;
        
        
    }
                
        public function actionCreateEditor($random=""){
            $this->layout = "//layouts/ace";   
            $this->render("codeEditor",array("random"=>$random));
        }
        
        public function actionSaveEditor(){            
            $user_id = Yii::app()->user->id;
            
            $model_usercourse=  UserCourse::model()->find("user_id='$user_id'");           
            if($model_usercourse->role<3){ //owner or teacher                            
                $div_id=$_POST["id"]; 
                
                $code="";                 
                 if(isset($_POST['code'])){
                    $code=$_POST['code'];
                 }                   
                   
                 $model_codeEditor= CodeEditorInFrame::model()->findByPk($div_id);
                 
                  if(empty($model_codeEditor)){
                       $model_codeEditor=new CodeEditorInFrame();
                       $model_codeEditor->admin_id=$user_id;
                       $model_codeEditor->create_date=date("Y-m-d H:i:s");
                  }
//                
                $model_codeEditor->id=$div_id;
                $model_codeEditor->code=$code;
                $model_codeEditor->update_date=date("Y-m-d H:i:s");
                $model_codeEditor->admin_update_id=$user_id;     
                              
                $model_codeEditor->save();                
//                echo "save";
           
            }      
        }
        
        public function actionShowDataInEditor(){
            $id=$_POST['id'];                  
            $model_code= CodeEditorInFrame::model()->findByPk($id);
            if(!empty($model_code)){
                echo $model_code->code;
            }else{
                echo "";
            }
        }

        public function actionUploadFile() {
       
        if (!empty($_FILES)) {
            $user_id = Yii::app()->user->id;
            list($type,$kind)=explode("/", $_FILES["file"]["type"]);
            
                        
            $target_path = Yii::app()->basePath . "/../webroot/files/$user_id/files/";
            
             if (!is_dir($target_path)) {
                mkdir($target_path, 0775, true);
            }
            
           $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263'), 0, 50);
           $file_name= $_FILES['file']['name'];
           $new_name = "coursecreek_".$random . "_" . $_FILES['file']['name'];
           $target_path_save = Yii::app()->basePath . "/../webroot/files/$user_id/files/" . $new_name;
           
           #save File
            if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path_save)) {                
                 $this->saveFileInfo("file_link", $new_name, $_FILES['file']['name'], $user_id); 
                
                $kind= SiteHelper::getImgFileType($kind);
                $file_src = "files/$user_id/files/$new_name";
                echo "$kind <a href='$file_src' title='Download' class='editor-attch'> $file_name </a>";
            } else {
                 echo "<script>
                       alert('Sorry!!! Upload Fail Please try again');                      
                      </script>";
                 exit;
            }         
        }
    }
    
    
    
    public function actionUploadSelfVideo() {
        
        if (!empty($_FILES)) {
            
           $file_type_list = array("webm", "mp4", "3gpp", "mov", "avi", "wmv", "flv", "mpeg");
            list($type, $kind) = explode("/", $_FILES["file"]["type"]);

            if ($type !== "video" || (!in_array($kind, $file_type_list))) {

                echo "<script>
                         alert('worng type should be >> webm 3gpp mov avi wmv flv OR mpeg');                      
                        </script>";
                exit;
            } else {    
            
                $user_id = Yii::app()->user->id;
                $target_path = Yii::app()->basePath . "/../webroot/files/$user_id/video/";

                #check is hane directory
                if (!is_dir($target_path)) {
                    mkdir($target_path, 0757, true);
                }

                $path_save_original = Yii::app()->basePath . "/../webroot/files/$user_id/video/" . $_FILES['file']['name'];
                
                //save file to directory
                if (move_uploaded_file($_FILES['file']['tmp_name'], $path_save_original)) {

                   #encode video
                    $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263'), 0, 50);
                    $new_name = "coursecreek" . $random .$_FILES['file']['name'];
                
                    $reserveString=array(","," ","(",")","<",">");
                    $replace=array("","","","","","");
                    $new_name=  str_replace($reserveString,$replace, $new_name); 
                    
                   # Generate thumbnail ---------------------------------------
                  
                    
                   list($type_file,$type_vdo)=  explode("/", $_FILES['file']['type']);                    
                   $thumbnail_name=  VdoHelper::generateVideohTumbnailname($new_name,$type_vdo);
                   $thumbnail_vdo = Yii::app()->basePath . "/../webroot/files/$user_id/video/$thumbnail_name";
                   VdoHelper::generateVideoThumbnail($path_save_original, $thumbnail_vdo);
                    
                  #encode video 
                   $encode_file_save = Yii::app()->basePath . "/../webroot/files/$user_id/video/".$new_name;
                   VdoHelper::encodeVideoH264($path_save_original, $encode_file_save,0);
//                                  
                    $video_url = Yii::app()->baseUrl . "/files/$user_id/video/$new_name";
                    $video_path = Yii::app()->basePath . "/../webroot/files/$user_id/video/$new_name";
                    
                    $file_id=$this->saveFileInfo("video", $new_name, $_FILES['file']['name'], $user_id,true); 
                    
                                
                    if (file_exists($video_path)) {
//                        $play = VdoHelper::jwplayer($video_url);
//                        echo "<div style='display:block; width:auto;' class='vdo-editor'> $play </div>";
                        
                        
                        $link=Yii::app()->createUrl("post/CreateVideoIFrame",array('id'=>$file_id));     
                        
                        $iframe="<iframe src='$link' height='800' width='600' ></iframe>";
                        echo $iframe;
                        
                        
                        
                    }
                } else {
                    echo "<script>
                       alert('Sorry!!! Upload Fail Please try again');                      
                      </script>";
                    exit;
                }
            }
        }
    }
    
    
        public function actionListAudio() {
        
        $user_id = Yii::app()->user->id;
        
        $criteria = new CDbCriteria;
        $criteria->select = "t.*";
        $criteria->condition = "user_id='$user_id' AND type= 'audio'";
        $criteria->order = "id DESC";

        $model_user_files=  File::model()->findAll($criteria);
        if(!empty($model_user_files)){
            $data = array();
            $url=Yii::app()->createUrl("post/getAudioPlayer"); //url for return audio player
            $audio_icon = Yii::app()->baseUrl . "/images/button/mp3.png";
            foreach ($model_user_files as $file){
                             
                    $createTime= "Upload: ".SiteHelper::dateFormat($file->create_at);
                    $data[] = array(

                           'thumb' =>$audio_icon,
                           'file' => $file->original_name,
                           'id' =>$file->id,
                           'url'=>$url,
                           'createTime'=>$createTime,
                       );

                
            }
             echo CJSON::encode($data);
             exit;
        }
    }
    
    
    
    public function actionGetAudioPlayer(){
        $id=$_POST['id'];
                
        $model_file=  File::model()->findByPk($id);
        $user_id =$model_file->user_id;
        $audio_mp3_url = Yii::app()->baseUrl . "/files/$user_id/audio/$model_file->name";
        
        list($name,$type)=  explode(".", $model_file->name);
        $ogg_file=$name.".ogg";
        $audio_ogg_url = Yii::app()->baseUrl . "/files/$user_id/audio/$ogg_file";
         
         echo "<audio controls>
                   <source src='$audio_mp3_url' type='audio/mpeg'> <!--- .mp3 for  for Internet Explorer, Chrome, and Safari -->
                   <source src='$audio_ogg_url' type='audio/ogg'> <!--- .ogg for   Firefox and Opera) -->
                   <embed width='50%' src='$audio_mp3_url'>
                </audio>";
    }   
    
    
    
    
    public function actionUploadAudio() {
        if (!empty($_FILES)) {

            list($type, $kind) = explode("/", $_FILES["file"]["type"]);

            if ($type != "audio" && $type !="mp3") {
                echo "<script>
                         alert('worng type should be >> mp3');                      
                        </script>";
            } else {              
               $user_id = Yii::app()->user->id;
               $target_path = Yii::app()->basePath . "/../webroot/files/$user_id/audio/";
            
                if (!is_dir($target_path)) {
                   mkdir($target_path, 0775, true);
               }
                
               $random = substr(str_shuffle('abcoursecreekdefgunhijklarngearmnopqestuvwxyz5040563263'), 0, 50);
               $new_name="coursecreek".$random.".$kind";
               $target_path_save = Yii::app()->basePath . "/../webroot/files/$user_id/audio/" . $new_name;
               

                //save file to directory
                if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path_save)) {

                    $audio_mp3_url = Yii::app()->baseUrl . "/files/$user_id/audio/$new_name";
                    $audio_path = Yii::app()->basePath . "/../webroot/files/$user_id/audio/$new_name";
                    
                    //create ogg file                  
                    list($file_name,$file_type)=  explode(".", $new_name);
                    $file_ogg=$file_name.".ogg";
                                      
                    $target_path_ogg_file = Yii::app()->basePath . "/../webroot/files/$user_id/audio/$file_ogg";
                    //$convert_ogg="ffmpeg -i $audio_path -vcodec libtheora -acodec libvorbis $target_path_ogg_file";
                    $convert_ogg ="ffmpeg -i $audio_path -acodec libvorbis -aq 60 -map_meta_data 0:0 $target_path_ogg_file";
                    
                    exec($convert_ogg);
                    $audio_ogg_url = Yii::app()->baseUrl . "/files/$user_id/audio/$file_ogg";
                    
                    $this->saveFileInfo("audio", $new_name, $_FILES['file']['name'], $user_id); 
                                              

                   if (file_exists($audio_path)) {
                    echo "<audio controls>
                             <source src='$audio_mp3_url' type='audio/mpeg'> <!--- .mp3 for  for Internet Explorer, Chrome, and Safari -->
                             <source src='$audio_ogg_url' type='audio/ogg'> <!--- .ogg for   Firefox and Opera) -->
                             <embed width='50%' src='$audio_mp3_url'>
                          </audio>
                       ";
                   
                    }
                } else {
                    echo "<script>
                       alert('Sorry!!! Upload Fail Please try again');                      
                      </script>";
                    exit;
                }
            }
           
        }
    }
    
    
    public function actionListVideo() {
        
        $user_id = Yii::app()->user->id;
        $criteria = new CDbCriteria;
        $criteria->select = "t.*";

        $criteria->condition = "user_id='$user_id' AND type= 'video'";
        $criteria->order = "id DESC";

        $model_user_files=  File::model()->findAll($criteria);
        if(!empty($model_user_files)){
            $data = array();
             $file_path = Yii::app()->baseUrl . "/files/$user_id/video/";
             $url=Yii::app()->createUrl("post/getVideoFrameLink");//url for ajax link
             
            foreach ($model_user_files as $file){
           
//                if(file_exists($file_path_vdo)){
                    $createTime= "Upload: ".SiteHelper::dateFormat($file->create_at);
                    $video_image=  str_replace(".mp4", ".jpg", $file_path.$file->name);
                    $data[] = array(

                           'thumb' =>$video_image,
                           'image' => $video_image,
                           'id' =>$file->id,
                           'url'=>$url,
                           'createTime'=>$createTime,
                       );
//                }
                
            }
             echo CJSON::encode($data);
             exit;
        }
    }
    
    public function actionGetVideoFrameLink(){
        $id=$_POST['id'];
        $link=Yii::app()->createUrl("post/CreateVideoIFrame",array('id'=>$id));       
        $iframe="<iframe src='$link' height='500px' width='700px' class='iframe-vdo'></iframe>";
        echo $iframe; 
    }
    
    #show video in iframe
     public function actionCreateVideoIFrame($id){
        $this->layout = "//layouts/blank";
        $user_id = Yii::app()->user->id;
        $model_file=  File::model()->findByPk($id);
        $this->render("show_video",array("model_file"=>$model_file));
    }    
    
    
    public function saveFileInfo($type, $name, $original_name, $user_id,$is_return_id=false) {
        $model_user_file = new File();
        $model_user_file->user_id = $user_id;
        $model_user_file->name = $name;
        $model_user_file->original_name = $original_name;
        $model_user_file->type = $type;
        $model_user_file->create_at = date("Y-m-d H:i:s");
        $model_user_file->save();
        
        if($is_return_id==1){
            return $model_user_file->id;
        }
        
    }
    
    public function actionCompileCode($is_show_compile = "1") {
        $code = $_POST['code']; #----(0)
        #find class name
        $map_class = preg_match("/public+[[:space:]]+class+[[:space:]]+[a-zA-Z]+(\{|[[:space:]])/", $code, $output);

        list($public, $class_title, $class_name, $spance) = explode(" ", $output[0]);

        if (empty($class_name)) {
            echo "กรุณา กำหนดชื่อคลาส";
        } else {
            $excute_result = "";
            $excute_result_html = "";
            $compile_code = "";

            $file_name = $class_name . ".java"; #-----(1)                    
            $user_id = Yii::app()->user->id;
            $target_path = Yii::app()->basePath . "/../webroot/code/$user_id/";

            if (!is_dir($target_path)) {
                mkdir($target_path, 0777, true);
            }

            $target_path_file = $target_path . $file_name; #----(2)
            #mode w =>clear old file and create new
            $objFopen = fopen($target_path_file, "w");
            fwrite($objFopen, $code);

            if ($objFopen) {
                //echo "<p class='text-compile'>File writed.</p>";
            } else {
                echo "<p class='text-error'>File can not write.<br/>";
            }

            $command_compile = "javac $target_path_file 2>&1";
            exec($command_compile, $compile_result, $return_compile);


            if (!empty($command_compile) && $is_show_compile == 1) {
                echo "<div class='code-result'>";
                foreach ($compile_result as $compile) {
                    $compile_code.= $compile . "<br/>";
                }
                echo "</div>";
            }

            $command_run = "cd $target_path; java $class_name 2>&1";
            exec($command_run, $run_result, $return_run);


            if (!empty($run_result)) {

                foreach ($run_result as $run) {
                    //echo $run."<br/>";
                    $excute_result.=$run;
                    $excute_result_html.= $run . "<br/>";
                }
            }

            if ($is_show_compile == 1) {
                echo "<div class='code-result'>";
                echo "<p class='text-compile'> javac $file_name</p>";
                echo $compile_code;
                echo "<p class='text-compile'> java $class_name</p>";
                echo $excute_result_html;
                echo "</div>";
            } else {
                echo strip_tags($excute_result);
            }
        }
    }
    
    
     public function actionCompileCodeJson($is_show_compile = "1") {
        $code = $_POST['code']; #----(0)
        #find class name
        $map_class = preg_match("/public+[[:space:]]+class+[[:space:]]+[a-zA-Z]+(\{|[[:space:]])/", $code, $output);

        list($public, $class_title, $class_name, $spance) = explode(" ", $output[0]);

        if (empty($class_name)) {
            echo "กรุณา กำหนดชื่อคลาส";
        } else {
            $excute_result = "";
            $excute_result_html = "";

            $file_name = $class_name . ".java"; #-----(1)                    
            $user_id = Yii::app()->user->id;
            $target_path = Yii::app()->basePath . "/../webroot/code/$user_id/";

            if (!is_dir($target_path)) {
                mkdir($target_path, 0777, true);
            }

            $target_path_file = $target_path . $file_name; #----(2)

            $target_path_file_class=$target_path.$class_name.".class";
            if(file_exists($target_path_file_class)){
                unlink($target_path_file_class);
            }            

            
            $objFopen = fopen($target_path_file, "w");
            fwrite($objFopen, $code);

            if ($objFopen) {

            } else {
                echo "<p class='text-error'>File can not write.<br/>";
            }
            
            $command_compile = "javac $target_path_file 2>&1";
            exec($command_compile, $compile_result, $return_compile);


            if (!empty($command_compile) && $is_show_compile == 1) {
                echo "<div class='code-result'>";
                foreach ($compile_result as $compile) {
                    echo $compile . "<br/>";
                }
                echo "</div>";
            }

            $command_run = "cd $target_path; java $class_name 2>&1";
            exec($command_run, $run_result, $return_run);
            
            if (!empty($run_result)) {            
                foreach ($run_result as $run) {
                    $excute_result.=$run;
                    $excute_result_html.= $run . "<br/>";
                }
            }else{
                $excute_result_html="no code for compile.";
            }
            
            $html_compile="<div class='code-result'>
                                <p class='text-compile'> javac $file_name</p>
                                <p class='text-compile'> java $class_name</p>
                                $excute_result_html
                           </div> ";
                
                $json = array("is_show_compile" => 0, "answer" => $excute_result,"compile_result"=>$html_compile);
                $json=json_encode($json);                
                echo $json;
        }
    }
    
    public function actionCompileCodePhp(){
 
        
        if(!empty($_POST['code'])){
                 $code = $_POST['code'];
                $user_id = Yii::app()->user->id;
                if (empty($user_id)) {
                    $user_id = 0;
                }

                $target_path = Yii::app()->basePath . "/views/code/$user_id/";
                if (!is_dir($target_path)) {
                    mkdir($target_path, 0777, true);
                }
                

                $random = $_POST['random'];
                $file_name = $random . ".php";

                $target_path_file = $target_path . $file_name; #----(2)


                $objFopen = fopen($target_path_file, "w");
                fwrite($objFopen, $code);

                if (!$objFopen) {
                      echo "<p class='text-error'>File can not write.<br/>";
                } 

                $this->renderPartial("/code/$user_id/$random");
            }else{
                echo "no code for compile";
            }        
    }
    
    
    public function actionCompileCodeSandBoxPython(){
        
        
        if(!empty($_POST['code'])){
                $code = $_POST['code'];
                $user_id = Yii::app()->user->id;
                if (empty($user_id)) {
                    $user_id = 0;
                }
                
                
              
                $target_path = Yii::app()->basePath . "/../webroot/code/$user_id/";
                if (!is_dir($target_path)) {
                    mkdir($target_path, 0777, true);
                }     

                $random = $_POST['random'];
                
                
                $file_name = $random . ".py";

                $target_path_file = $target_path . $file_name; #----(2)

      
                if(file_exists($target_path_file)){
                    unlink($target_path_file);
                }    
                
               
                $objFopen = fopen($target_path_file, "w");
                fwrite($objFopen, $code);
                
                
                if ($objFopen) {
                    
                    
                    $command_run = "cd /var/www/sandbox; python sample4.py $target_path_file 2>&1";
                    exec($command_run, $compile_result, $return_compile); //run comand to get json code
               
                    
          
//                    echo $compile_result[3];
                    
                    if(!empty($compile_result[3])){          
                         $file_name_json = $random . ".json";
                         $target_path_file_json = $target_path . $file_name_json; #----(3)
                         
                         $objFopen = fopen($target_path_file_json, "w");
                         fwrite($objFopen, $compile_result[3]);
                         
                          if ($objFopen) {
                              
                             $string = file_get_contents($target_path_file_json);
                             $json_content=json_decode($string,true);  
                             if(!empty($json_content['output'])){
                                echo "<div class='code-result'>".$json_content['output']."</div>";       
                             }else{
  
                               $json_content['stderr'] = str_replace($target_path_file, "<span class='red-text'>Error </span>",  $json_content['stderr']);
                               $json_content['stderr'] = str_replace("File", " ",  $json_content['stderr']);
                               
                               echo "<div class='code-result'>".$json_content['stderr']."</div>";       
                             }
                          }              
                        
                    }else{
                        //show error
                      
                        $this->renderPartial("testpython",array("compile_result"=>$compile_resul,"return_compile"=>$return_compile));
                    }
                  
                } else {
                    echo "<p class='text-error'>File can not write.<br/>";
                }             
                
                //$this->renderPartial("/code/$user_id/$random");
            }else{
                echo "no code for compile";
            } 
    }

    



//    public function actionVideoCheck($url){        
//      
//        $video_file = Yii::app()->request->hostInfo.$url;
//        $video_file = "http://localhost/coursecreek/files/1/video/11.mp4";
////        
//        //$filename=$vdo_path;
//        if (file_exists($video_file)) { ob_clean(); flush(); };
//        header('Content-Type: video/mp4');
//        header('Content-Disposition: attachment;filename=file.mp4');
//        
//        header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
//        header("Content-Description: File Transfer");
//        header("Content-Transfer-Encoding: binary");
//        //header('Content-Length: ' . filesize($video_file));
//        readfile($video_file);
//        
//     }
     
     
      public function actionVideoCheck($file_id){        

        $model_file = File::model()->findByPk($file_id);
       
        if(!empty($model_file)){

            $video_url = Yii::app()->baseUrl . "/files/$model_file->user_id/video/$model_file->name";
            $video_file = Yii::app()->request->hostInfo.$video_url;   

            #sample URL
            #$video_file = "http://localhost/coursecreek/files/1/video/11.mp4";
            
          if (file_exists($video_file)) { ob_clean(); flush(); };
            header('Content-Type: video/mp4');
            header('Content-Disposition: attachment;filename=file.mp4');

            header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
            header("Content-Description: File Transfer");
            header("Content-Transfer-Encoding: binary");
            header("X-Sendfile: $video_file");
            header("Accept-Ranges: bytes");
            
            //header('Content-Length: ' . filesize($video_file));
            readfile($video_file);
        }
        
     }
     
     
     public function actionTestVideo(){
         $model_content = Content::model()->findByPk(76);
         
         $this->render("testVideo",array("model_content"=>$model_content));
     }

     

     public function actionVideoCheckTest($file_id){        

        $model_file = File::model()->findByPk($file_id);
       
        if(!empty($model_file)){
    
//           $video_file = "http://coursecreek.com/files/11.mp4"; 
           $video_file =Yii::app()->getBaseUrl(true)."/files/$model_file->user_id/video/$model_file->name";
           
          if (file_exists($video_file)) { ob_clean(); flush(); };
            header('Content-Type: video/mp4');
            header('Content-Disposition: attachment;filename=file.mp4');

            header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
            header("Content-Description: File Transfer");
            header("Content-Transfer-Encoding: binary");
            header("X-Sendfile: $video_file");
            header("Accept-Ranges: bytes");
            
            //header('Content-Length: ' . filesize($video_file));
            readfile($video_file);
        }
        
     }
    
     
     
     

}



?>
