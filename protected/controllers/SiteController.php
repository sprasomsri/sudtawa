<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */    
    
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
        
         /**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
            
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
                 SiteHelper::setMetaTag("keywords","coursecreek");
                 SiteHelper::setMetaTag("description","เรียนหนังสือ online");
                 LinkHelper::SetPageHistory();
                 
                 $today = date("Y-m-d H:i:s");                 
                 $criteria = new CDbCriteria;
                 $criteria->condition="(date_start <= '$today' OR date_start IS NULL) AND (date_end >= '$today' OR date_end IS NULL) AND t.id != '-1' AND is_public='1' AND id IN (SELECT course_id FROM content)";
                 $criteria->limit='8';
                 $criteria->order="RAND()";            
                 $model_courses=  Course::model()->findAll($criteria);
                
                 
                 $user_id = Yii::app()->user->id;
                 SiteHelper::CheckReference();                 
                
                 
                  if(isset($_POST['btn-submit'])){
                   if(!empty($_POST['subject'])){
                       
                       foreach($_POST['subject'] as $subject){
                           $model_subject= PollSubjectBase::model()->find("name='$subject'");
                           if(empty($model_subject)){
                               $model_subject= new PollSubjectBase();
                               $model_subject->name=$subject;
                           }else{
                               $model_subject->qty_votes+=1;
                           }
                           $model_subject->lasted_date=date("Y-m-d H:i:s");
                           $model_subject->save();
                          
                       }
                        $this->redirect(Yii::app()->createUrl("site/index"));
                   }
                   
                }
                 		
                 $this->render('index',array("model_courses"=>$model_courses,
                                             "user_id"=>$user_id));

	}
        
        
        
        public function actionPdf(){
           
            $this->render("pdf");
        }

        
        /**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }else{
                $this->render('error', $error);
            }
            
	}



	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
        
        public function actionChangeLanguage($lang,$return){
            Yii::app()->user->setState('lang',$lang);
            $replace_url  = str_replace("-", "/", $return);
            $this->redirect($replace_url);
        }

        
       public function actionTestFacebook(){
            $facebook = new Facebook(array(
                    'appId'  => '407397162716127',
                    'secret' => '7e84a67fa39d10df7ff6be4755ac3baf',
                  ));
           
           
           $user = $facebook->getUser();
                 if(!empty($user) || $user!=0){
                 
                     
            }
           
           
            $this->render("test_facebook",array("facebook"=>$facebook));
        }
        
     
        public function actionTestPlayer(){         
          
            $this->render("testplayer");
        }
        


        public function actionSavePoll(){
            $name= $_POST['name'];
            $category=$_POST['category'];
            $price= $_POST['price'];
            $user_id = Yii::app()->user->id;
            $model_poll = new PollSubjectBase();
            $model_poll->category=$category;
            $model_poll->name=$name;
            $model_poll->price=$price;
            $model_poll->lasted_date=date("Y-m-d H:i:s");
            if(!empty($user_id)){
                $model_poll->user_id=$user_id;
            }
            $model_poll->save();
            
            
        }
        
        
        
        
}