<?php
class CodeEditorSandBox extends CWidget{
    # Variable get when this widget was call  
     public $code="";
     public $model_code="";
     public $type="python";
     public $is_exercise = 0;
     public $input_name="code";
     public $input_answer_name="answer";
     public $is_show_answer="0";
     public $answer="";
     public $comment="";
     public $default_code="";





     # Run Widget ---> comtroller
	function run() {
        // Render widget
        $user_id = Yii::app()->user->id;
        if (empty($user_id)) {
            $user_id = 0;
        }
        
        if(!empty($this->model_code)){
            $this->code=  $this->model_code->code;
        }else{
            if(!empty($this->default_code)){
                $this->code =$this->default_code;
            }
        }
        
    
        $random = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz0123456789'), 0, 3)."_".date("YmdHis");
        
        
        $this->render('code_editor_sandbox', array("code" => $this->code,"type"=>$this->type,                                               
                                                "random" => $random,"is_exercise"=>$this->is_exercise,
                                                "is_show_answer"=>$this->is_show_answer,
                                                "answer"=>  $this->answer,"comment"=>$this->comment,
                                                "input_name"=>$this->input_name,
                                                "input_answer_name"=>$this->input_answer_name));
    }
}
?>