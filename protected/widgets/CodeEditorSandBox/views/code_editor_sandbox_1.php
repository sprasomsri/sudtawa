<div class="row-fluid code">
    <div class="span12">
            
        <div id="<?php echo $random; ?>" class="editor-box"><?php if(!empty($code)){ echo $code; }else{echo $comment; }?></div>
        
        <textarea name="<?php echo $input_answer_name; ?>" id="input-<?php echo $random; ?>"><?php echo $code; ?></textarea>
        
        
        <div id="run-<?php echo $random;?>" class="play-box"> 
            <a class="btn btn-xs" type="button" id="play-<?php echo $random; ?>"><span class="glyphicon glyphicon-play"></span></a>
            <a class="btn btn-xs" type="button" id="stop-<?php echo $random; ?>"><span class="glyphicon glyphicon-stop"></span></a>
            
            <div id="loading-<?php echo $random;?>"class='loading-widget' style='display:none;'>
                <img src="images/bar-loading.gif" width="40px"> Writing code..
            </div>
        </div>
        <div id="show-<?php echo $random;?>" class="show-compile"></div>
    </div>               
    <?php  
    if($is_exercise ==1){ 
        echo "กด <span class='glyphicon glyphicon-play'></span> เพื่อเพิ่มคำตอบ";             
    }
    ?>
    
     <textarea name="<?php echo $input_answer_name; ?>" id="answer-<?php echo $random; ?>" data-show="<?php echo $is_show_answer; ?>" rows="10" style="width: 95%;" ><?php echo $answer; ?></textarea>
    
    
</div>
 
<?php 
    if($type=="python"){
        $compile_url= Yii::app()->createUrl("post/compileCodeSandBoxPython");
    }
?>

<script type="text/javascript"> 
      $(document).ready(function() {
        var editor = ace.edit("<?php echo $random; ?>");
        editor.setTheme("ace/theme/mono_industrial");
        editor.getSession().setMode("ace/mode/python");
        var textarea = $('textarea[id="input-<?php echo $random?>"]').hide();
        var random= $("#<?php echo $random; ?>").data("file");
        
        var show_ans=$("#answer-<?php echo $random?>").data('show');
      
        if(show_ans==0){
            var answer_textarea = $('textarea[id="answer-<?php echo $random?>"]').hide();
        }else{
            $("#answer-title-<?php echo $random?>").show();
             var answer_textarea = $('textarea[id="answer-<?php echo $random?>"]');
        }
        

        $("#<?php echo $random; ?>").on("mouseleave", function() {
            var data = editor.getSession().getValue();
            textarea.val(data);
            $("#loading-<?php echo $random; ?>").hide();
        });

        $("#<?php echo $random; ?>").on("click", function() {
            $("#loading-<?php echo $random; ?>").show();
        });
        
        
        //Run php file
         $("#play-<?php echo $random; ?>").on("click", function() {            
             var code_post=editor.getSession().getValue();          
             
             
             $.ajax({                         
                url: "<?php echo $compile_url; ?>",              
                type: 'POST',
                data:{'code':code_post,'random':random},
                dataType: 'html',              
                cache: false,
                beforeSend:function(){
                    $("#loading-<?php echo $random; ?>").show();
                    $(".save").hide();        
                },
                success:function(data) {
                     $("#loading-<?php echo $random; ?>").hide();
                     $("#show-<?php echo $random; ?>").html(data);
                     $("#show-<?php echo $random; ?>").show();
                     answer_textarea.val(data);   

                  }
            });
            
            
        });
        
           $("#stop-<?php echo $random; ?>").on("click", function() {
                $("#loading-<?php echo $random; ?>").hide();
                $("#show-<?php echo $random; ?>").hide();
        });
        
    })
</script>   