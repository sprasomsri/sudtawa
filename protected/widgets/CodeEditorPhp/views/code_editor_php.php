<div class="row-fluid code">
    <div class="span12">
            
        
        <div id="<?php echo $random; ?>" class="editor-box" data-file="<?php echo $random; ?>"><?php echo $code; ?></div>
        <textarea name="code" id="input-<?php echo $random; ?>"><?php echo $code; ?></textarea>
        
        
        <div id="run-<?php echo $random;?>" class="play-box"> 
            <a class="btn btn-xs" type="button" id="play-<?php echo $random; ?>"><span class="glyphicon glyphicon-play"></span></a>
            <a class="btn btn-xs" type="button" id="stop-<?php echo $random; ?>"><span class="glyphicon glyphicon-stop"></span></a>
            
            <div id="loading-<?php echo $random;?>"class='loading-widget' style='display:none;'>
                <img src="images/bar-loading.gif" width="40px"> Writing code..
            </div>
        </div>
        <div id="show-<?php echo $random;?>" class="show-compile"></div>
    </div>                                    
</div>
 
<?php $compile_url= Yii::app()->createUrl("post/compileCodePhp");?>
<script type="text/javascript"> 
      $(document).ready(function() {
        var editor = ace.edit("<?php echo $random; ?>");
        editor.setTheme("ace/theme/crimson_editor");
        editor.getSession().setMode("ace/mode/php");
        var textarea = $('textarea[id="input-<?php echo $random?>"]').hide();
        var random= $("#<?php echo $random; ?>").data("file");

        $("#<?php echo $random; ?>").on("mouseleave", function() {
            var data = editor.getSession().getValue();
            textarea.val(data);
            $("#loading-<?php echo $random; ?>").hide();
        });

        $("#<?php echo $random; ?>").on("click", function() {
            $("#loading-<?php echo $random; ?>").show();
        });
        
        
        //Run php file
         $("#play-<?php echo $random; ?>").on("click", function() {            
             var code_post=editor.getSession().getValue();          
             
             $.ajax({                         
                url: "<?php echo $compile_url; ?>",              
                type: 'POST',
                data:{'code':code_post,'random':random},
                dataType: 'html',              
                cache: false,
                beforeSend:function(){
                    $("#loading-<?php echo $random; ?>").show();
                    $(".save").hide();        
                },
                success:function(data) {
                     $("#loading-<?php echo $random; ?>").hide();
                     $("#show-<?php echo $random; ?>").html(data);
                     $("#show-<?php echo $random; ?>").show();

                  }
            });
            
            
        });
        
           $("#stop-<?php echo $random; ?>").on("click", function() {
                $("#loading-<?php echo $random; ?>").hide();
                $("#show-<?php echo $random; ?>").hide();
        });
        
    })
</script>   