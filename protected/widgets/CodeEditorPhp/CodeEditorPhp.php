<?php
class CodeEditorPhp extends CWidget{
    # Variable get when this widget was call  
     public $code="";
     public $model_code="";
     
     

     # Run Widget ---> comtroller
	function run() {
        // Render widget
        $user_id = Yii::app()->user->id;
        if (empty($user_id)) {
            $user_id = 0;
        }
        
        if(!empty($this->model_code)){
            $this->code=  $this->model_code->code;
        }
        
        
        $random = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz0123456789'), 0, 10);
        $this->render('code_editor_php', array("code" => $this->code,                                               
                                                "random" => $random));
    }
}
?>