<?php
class CouponBox extends CWidget{
    # Variable get when this widget was call  
     public $data;
     public $course_img;
     public $course_name;

    # Run Widget ---> comtroller
	function run() {
        // Render widget
        $user_id = Yii::app()->user->id;
               
        $this->render('coupon_box', array("data"=>$this->data,"course_img"=>$this->course_img,"course_name"=>$this->course_name));
    }
}
?>
