

<a href="#" class="span4 course-card" id="coupon_box_<?php echo $data->id; ?>">
    <div class="row-fluid" >
        <div class="span12 subject">
            <div class='pic-box'>               
                
                <?php if ($course_img != null && file_exists($course_img)) { ?>	
                    <img src="<?php echo $course_img; ?>" >
                <?php } else { ?>
                    <img src="<?php echo Yii::app()->request->baseUrl . '/images/img-course.png'; ?>"/>
                <?php } ?>
         
            </div>
            <h4 class="title"><?php echo $course_name; ?></h4>
            <h4>DisCount: <?php echo $data->discount; ?> %</h4>
            <h4>หมายเลขคูปอง: <?php echo $data->code; ?> </h4>
           <?php echo $data->detail; ?>
        </div>
    </div>
    <hr>
    <div class="course-footer" >
        <div class="icon-teacher">Expire Date:<?php echo $data->expire_date; ?></div>
        <div class="icon-student">จำนวน: <?php echo $data->qty_use; ?> Account(s)</div>
        
        <div class="clear"></div>
    </div>
</a>
