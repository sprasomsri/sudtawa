<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

    // Load the Visualization API and the piechart package.
    google.load('visualization', '1.0', {'packages': ['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);

    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Question');
        data.addColumn('number', 'Quantity');
        data.addRows(<?php echo $grap; ?>);

        // Set chart options
        var options = {'title': '<?php echo " ";?>',
            'width': <?php echo $width; ?>,
            'height': <?php echo $height; ?>};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('<?php echo $div_id; ?>'));
        chart.draw(data, options);
    }
</script>

<?php if($all_person!=0){ ?>
  <div id="<?php echo $div_id; ?>"></div><br/>
  
      
   <?php
  
   if($mode=="question"){
    $true_percent=($qty_true/$all_person)*100;
    $false_percent=($qty_false/$all_person)*100;
    
        echo "<p class='result-exercise'>".Yii::t("site","Quantity true")."  :: <span> $qty_true ".Yii::t("site","person(s)")."=> ".number_format($true_percent,2)."%</span></p> ";
        echo "<p class='result-exercise'>".Yii::t("site","Quantity false").":: <span> $qty_false ".Yii::t("site","person(s)")."=>".number_format($false_percent,2)."%</span></p>";;
        echo "<p class='result-exercise'>". Yii::t("site","All Person") .":: <span> $all_person ".Yii::t("site","person(s)")."</span></p>";
   }else{
        echo "<p class='result-exercise'>". Yii::t("site","All Person") .":: <span> $all_person ".Yii::t("site","person(s)")."</span></p>";
   }
  
  ?>
<?php }else{ ?>
  <p class="center"> - <?php echo Yii::t("site","Nobody Do This Test"); ?> - </p>
<?php } ?>

