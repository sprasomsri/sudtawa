<?php
class ExerciseStatGrap extends CWidget{
    # Variable get when this widget was call  
     public $model_question;
     public $model_exercise;
     public $width;
     public $height;
     public $div_id;
     public $mode; //3mode 1>question - show stat score was selected in each answer
                   //      2>questionText - show stat score in question type text
                   //      3>exercise - Shoe score Range Form All Exercise
     
     
     

    # Run Widget ---> comtroller
	function run() {
        // Render widget
         $grap="";
         $qty_true=0;
         $qty_false=0;
         $all_person=0;
         $list_true_answer="";
         
         if(empty($this->div_id)){
             $this->div_id="chart_div";
         }        
         
         #stat in each question choice
         if($this->mode=="question"){
        
             $answers=$this->model_question->answer; //relation ExerciseAnswerChoice
             foreach($answers as $answer){
                $sql = "SELECT * 
                       FROM exercise_choice_score
                       WHERE answer_id='$answer->id' AND id IN 
                            (SELECT MAX(id) as id 
                            FROM exercise_choice_score 
                            WHERE question_id='".$this->model_question->id."'
                            GROUP BY user_id)
                        ";
                
                $lasted_score=ExerciseChoiceScore::model()->findAllBySql($sql);
                $qty_answer=  count($lasted_score);
                $title=  $this->model_question->question;
                
              if($qty_answer>0){
                   $grap.="['$answer->answer',$qty_answer],";
                }
                $all_person+=$qty_answer;
                if($answer->is_true==1){
                   $qty_true+=$qty_answer;
                }else{
                   $qty_false+=$qty_answer;
                }                        
         }
         
         $grap=  rtrim($grap, ",");
         $grap="[".$grap."]";
         }
         
         #stat in each question type Text
         if($this->mode=="questionText"){           
         
             $quetions=$this->model_exercise->question;
             $title=$this->model_question->question;
             $title="";   
             
             
             $list_true_answer=$this->model_question->answerText;
             $number_of_answer=count($list_true_answer);
             
             
             $sql_select_max_date="SELECT MAX(id) as id,MAX(do_time) as do_time,user_id
                                    FROM `exercise_text_score`
                                    WHERE question_id = '2'
                                    GROUP BY user_id
                                    ORDER BY do_time DESC,id DESC";
             
             
             $list_id_max_time=ExerciseTextScore::model()->findAllBySql($sql_select_max_date);
             
          
             $array_id_top_date= array();
             $id_list= "";
             
             if(!empty($list_id_max_time)){
                foreach($list_id_max_time as $obj){
                  
                    $model_list_answer_in_question=  ExerciseTextScore::model()->findAll("user_id='$obj->user_id' AND do_time='$obj->do_time'");
                    if(!empty($model_list_answer_in_question)){
                        foreach($model_list_answer_in_question as $list_sub_answer){
                            $id_list.= $list_sub_answer->id.",";
                        }
                    }

                }
             }
            
             $id_list=  rtrim($id_list, ",");
          
             if(!empty($id_list)){
             $sql_select_user_max_time = "SELECT SUM(is_true) as sum_score
                                                FROM `exercise_text_score` 
                                                WHERE id IN ($id_list)";      
             }else{
                  $sql_select_user_max_time = "SELECT SUM(is_true) as sum_score
                                                FROM `exercise_text_score`";   
             }
             
                          
             
             $list_score=ExerciseTextScore::model()->findAllBySql($sql_select_user_max_time);
             $array_list_answer_point=array();
             
             if(count($list_score)!=0){
                 foreach($list_score as $score){
                     if(empty( $array_list_answer_point[$score->sum_score])){
                         $array_list_answer_point[$score->sum_score]=1;
                     }else{
                         $array_list_answer_point[$score->sum_score]+=1;
                     }
                     
                     $all_person++;
                 }
                 
                    //create grapg
                    foreach($array_list_answer_point as $point=>$qty_person){
                        $label_graph= "correct $point";
                        $grap.="['$label_graph',$qty_person],";
                    }
                    
                    $grap=  rtrim($grap, ",");
                    $grap="[".$grap."]";
                 }
             
               
         }
         
                  
         #all Exercise stat
         if($this->mode=="exercise"){
             //on mode exercise
          
             $title=$this->model_exercise->content->title;
             $score_array=array();

            //query Total Score Lasted which user got
                 
                $sql=" SELECT qty_correct,user_id,do_time
                       FROM `exercise_stat`
                       WHERE id IN (SELECT MAX(id) as id 
                                    FROM exercise_stat 
                                    WHERE exercise_id = '".$this->model_exercise->id."'
                                    GROUP BY user_id)";


                $total_lasted_score= ExerciseStat::model()->findAllBySql($sql);
                          
                
                //save score to array
                if (!empty($total_lasted_score)) {
                    foreach ($total_lasted_score as $score_phase) {
                        if(!empty($score_array[$score_phase->qty_correct])){
                          $score_array[$score_phase->qty_correct]+=1;
                            
                        }else{
                          $score_array[$score_phase->qty_correct]=1;
                        }
                        $all_person++;
                        
                    }
                }
           
             
            if(!empty($score_array)){
                 foreach($score_array as $score=>$quanlity_person){
                     $string_score=$score.Yii::t("site","point(s)");
                     
                    $grap.="['$string_score',$quanlity_person],";
                    
                 }
                 
                 $grap=  rtrim($grap, ",");
                 $grap="[".$grap."]";
             }
                          
         }
         
         
         $this->render('exercise_stat_grap', array("width"=>$this->width,
                                             "height"=>$this->height,
                                             "grap"=>$grap,
                                             "all_person"=>$all_person,
                                             "qty_true"=>$qty_true,
                                             "qty_false"=>$qty_false,
                                             "title"=>$title,
                                             "mode"=>$this->mode,
                                             "div_id"=>$this->div_id,
                                            ));
    }   
}
?>
