<?php 
    $user_in_course=  UserCourse::model()->find("user_id='$user_id' AND course_id='$data->id'");
    
        if (!empty($user_in_course)) {
           
        if ($user_in_course->role == 1 || $user_in_course->role==2) {
               $course_link = Yii::app()->createUrl("/admin/default/operateCourse/id/$data->id");
           }else{
               $course_link = Yii::app()->createUrl("/course/default/studyCourse/", array("courseId" => $data->id));
           }
       } else {
           $course_link = Yii::app()->createUrl("/course/default/viewCourse",array("id"=>$data->id));
       }
         $description = strip_tags($data->description);

        
?>

<div id="coursebox-<?php echo $data->id; ?>" class="w-col w-col-3 course-box">
    <div class="course">
        <div class="course-image">
             <?php
                $course_img_path = Yii::app()->basePath . "/../webroot/course/" . $data->course_img;

                if ($data->course_img != null && file_exists($course_img_path)) {
                    $src = "course/" . $data->course_img;
                } else {
                    $src = "images/default_course.png";
                }
              ?>            
            
           <a href="<?php echo $course_link; ?>">
               <img class="images-course" src="<?php echo $src; ?>">
           </a>
        </div>
        <div>
            <a href="<?php echo $course_link; ?>"><h2 class="course-name"><?php echo $data->name; ?></h2><a>
            <a href="<?php echo $course_link; ?>">
            <p class="text-course">
               <?php echo $description ?>;
               <br/><br/>
               <span class="course-timeline">                   
                <?php                 
                    if($data->course_option ==1){
                        if(!empty($data->date_start)){
                            echo "<u>".Yii::t('CourseBox.messages', 'Start')."</u> &nbsp;&nbsp;&nbsp;&nbsp;". SiteHelper::dateFormat($data->date_start,"datetime",0);
                        }

                        if(!empty($data->date_end)){
                            echo "<br/>";
                            echo "<u>".Yii::t('CourseBox.messages', 'End')."</u> &nbsp;&nbsp;&nbsp&nbsp;&nbsp;". SiteHelper::dateFormat($data->date_end,"datetime",0);
                        }
                    }else{
                        if(!empty($data->duration)){
                            echo "ระยะเวลาเรียน $data->duration วัน";
                        }
                    }      
                ?> 
                </span>
            </p>
            </a>
            
            <div class="block-button">
                <a class="button-course-icon" href="<?php echo $course_link; ?>"><?php echo Yii::t('CourseBox.messages', 'View detail'); ?></a>
            </div>
        </div>
    </div>
</div>

