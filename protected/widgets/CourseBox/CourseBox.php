<?php
class CourseBox extends CWidget{
    # Variable get when this widget was call  
     public $data;

    # Run Widget ---> comtroller
	function run() {
        // Render widget
        $user_id = Yii::app()->user->id;
        $course_id=$this->data->id;
        $owner=  UserCourse::model()->find("course_id=:course_id AND (role='1' OR role='2') ORDER BY is_head DESC,role ASC",array(':course_id'=>$course_id));
                
        $student_number = UserCourse::model()->Count("course_id='$course_id' AND role='3'");                
                
                
        $model_rating=Yii::app()->db->createCommand("SELECT AVG( rating ) AS rating , COUNT( id ) as qty
                                                     FROM  course_rating WHERE course_id='$course_id'")->queryRow(); 
        $facebook_id="";
        if(empty($owner->profile->photo)&&!empty($owner)){
            $facebook_id= UsersFaceBook::model()->findByPk($owner->user_id)->facebook_id;
        }
        
        
        $this->render('course_box', array("data"=>$this->data,"student_number"=>$student_number,
                                          "user_id"=>$user_id,"owner"=>$owner,"model_rating"=>$model_rating,"facebook_id"=>$facebook_id));
    }
}
?>
