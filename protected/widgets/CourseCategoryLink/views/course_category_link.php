<div class="w-row menu-course">
    <div class="w-col-12">    
        <ul class="nav nav-pills">
          <li <?php if($active_box=="DashBoard"){ echo "class='active'"; } ?>>
              <a href="<?php echo Yii::app()->createUrl("user/dashboard/index");?>"> <?php echo Yii::t("site", "Studying Course"); ?> </a>
          </li>

          <li <?php if($active_box=="CompleateCourse"){ echo "class='active'"; } ?>>
              <a href="<?php echo Yii::app()->createUrl("user/dashboard/CompleateCourse");?>"><?php echo Yii::t("site", "Complete Course"); ?></a>
          </li>
          <?php if(!empty($model_permission) && $model_permission->can_create_course==1){ ?>
          <li <?php if($active_box=="admin"){ echo "class='active'"; } ?>>
              <a href="<?php echo Yii::app()->createUrl('/admin/default/ownCourse'); ?>"><?php echo Yii::t("site", "Teaching Course"); ?></a>
          </li>
          <?php } ?>
        </ul>
    </div>
</div>
