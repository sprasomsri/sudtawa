<?php

class CourseCategoryLink extends CWidget {
    # Run Widget ---> comtroller

    public $active_box="";
    function run() {
        // Render widget
        $user_id = Yii::app()->user->id;   
        $model_permission="";
        
        if(!empty($user_id)){
            $model_permission=  UserPermission::model()->find("user_id='$user_id'");
        }    
        
        $this->render('course_category_link', array("active_box"=>$this->active_box,"user_id"=>$user_id,"model_permission"=>$model_permission));
    }

}

?>
