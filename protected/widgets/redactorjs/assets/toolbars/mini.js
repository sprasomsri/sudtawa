if (typeof RTOOLBAR == 'undefined') var RTOOLBAR = {};

RTOOLBAR['mini'] = {
	html:
	{
		title: RLANG.html,
		func: 'toggle',
		separator: true
	},
		
	styles:
	{ 
		title: RLANG.styles,
		func: 'show', 				
		dropdown: 
	    {
			 p:
			 {
			 	title: RLANG.paragraph,			 
			 	exec: 'formatblock',
			 	param: '<p>'
			 },
			 blockquote:
			 {
			 	title: RLANG.quote,
			 	exec: 'formatblock',	
			 	param: '<blockquote>',
			 	style: 'font-style: italic; color: #666; padding-left: 10px;'			 			 	
			 },
			 pre:
			 {  
			 	title: RLANG.code,
			 	exec: 'formatblock',
			 	param: '<pre>',
			 	style: 'font-family: monospace, sans-serif;'
			 },
			 h1:
			 {
			 	title: RLANG.header1,			 
			 	exec: 'formatblock',   
			 	param: '<h1>',			 	
			 	style: 'font-size: 30px; line-height: 36px; font-weight: bold;'
			 },
			 h2:
			 {
			 	title: RLANG.header2,			 
			 	exec: 'formatblock',   
			 	param: '<h2>',			 	
			 	style: 'font-size: 24px; line-height: 36px; font-weight: bold;'
			 },
			 h3:
			 {
			 	title: RLANG.header3,			 
			 	exec: 'formatblock', 
			 	param: '<h3>',			 	  
			 	style: 'font-size: 20px; line-height: 30px;  font-weight: bold;'
			 },		
			 h4:
			 {
			 	title: RLANG.header4,			 
			 	exec: 'formatblock', 
			 	param: '<h3>',			 	  
			 	style: 'font-size: 16px; line-height: 26px;  font-weight: bold;'
			 }																	
		},
		separator: true
	},
	bold:
	{
		title: RLANG.bold,
		exec: 'bold'
	}, 
	italic: 
	{
		title: RLANG.italic,
		exec: 'italic',
		separator: true		
	},
	insertunorderedlist:
	{
		title: '&bull; ' + RLANG.unorderedlist,
		exec: 'insertunorderedlist'
	},
	insertorderedlist: 
	{
		title: '1. ' + RLANG.orderedlist,
		exec: 'insertorderedlist'
	},
	outdent: 
	{	
		title: '< ' + RLANG.outdent,
		exec: 'outdent'
	},
	indent:
	{
		title: '> ' + RLANG.indent,
		exec: 'indent',
		separator: true
	},
                
       justifyleft:
	{	
		exec: 'JustifyLeft', 
		name: 'JustifyLeft', 
		title: RLANG.align_left
	},
                
	justifycenter:
	{
		exec: 'JustifyCenter', 
		name: 'JustifyCenter', 
		title: RLANG.align_center
	},
	justifyright: 
	{
		exec: 'JustifyRight', 
		name: 'JustifyRight', 
		title: RLANG.align_right
	},
                
                
                
        
                
	link:
	{ 
		title: RLANG.link, 
		func: 'show', 				
		dropdown: 
		{
			link: 	{name: 'link', title: RLANG.link_insert, func: 'showLink'},
			unlink: {exec: 'unlink', name: 'unlink', title: RLANG.unlink}
		}															
	},


	fontcolor:
	{
		title: RLANG.fontcolor, 
		func: 'show'
	},	


	horizontalrule: 
	{
		exec: 'inserthorizontalrule', 
		name: 'horizontalrule', 
		title: RLANG.horizontalrule
	},	
                
        file:
	{
		title: RLANG.file,
		func: 'showFile'
	},

	image:
	{
		title: RLANG.image, 						
		func: 'showImage' 			
	},
                
                
        video:
	{
            /* Embed code */
		title: RLANG.video,
		func: 'showVideo'
	},
                
                
        videoSelf: {
            /* Video Self Upload*/   
            title: RLANG.videoSelf, /* title (tooltip)  >>> map wth foder lang[Rlang] en.js*/
            name: 'test',
            func: 'uploadSelfVideo', /* active function on redactor.js */

        },
                
         audio: {
            /* Video Self Upload*/   
            title: RLANG.audio, /* title (tooltip)  >>> map wth foder lang[Rlang] en.js*/
            name: 'test',
            func: 'uploadAudio', /* active function on redactor.js */

        },
                
                
        
     		
//	fullscreen:
//	{
//		title: RLANG.fullscreen,
//		func: 'fullscreen'
//	},


        
  
};