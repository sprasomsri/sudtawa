
<?php if($mode=="default"){ ?>
<div class="w-row course-progress">
    <div class="w-col w-col-3">
        
        <?php
            $course_img = "course/$model_course->course_img";

            if (!empty($model_course->course_img)) {
                $course_src_pic = $course_img;
            }else{
                $course_src_pic = "images/default_course.png";               
            }
        ?>	
        
        <a href="<?php echo Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$model_course->id))?>"> 
            <img src="<?php echo $course_src_pic; ?>" class="img-responsive img-thumbnail img-opacity"/>
        </a>
    </div>

    <div class="w-col w-col-9">
        
       <h3 class="dashboard-head"><?php echo $model_course->name;?></h3>
       <div class="instructor orange-text"><?php echo $course_owner; ?></div>


        <?php
           echo strip_tags($model_course->description);
        ?>    

        <div class="w-row">
            <div class="w-col w-col-10">
                    <div class="progress-course hidden-xs">
                        <?php
                        $width_bar = "auto";
                        $class_bar="text-left";
                        if($course_progress+1 >10){
                            $width_bar =$course_progress+1;
                            $class_bar = "text-right";
                        }

                        ?>


                        <div style="width:<?php echo $width_bar; ?>%;" class="progress-course <?php echo $class_bar; ?>"><?php echo number_format($course_progress, 2); ?>%<span class="glyphicon glyphicon-map-marker"></span></div>
                    </div>        

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $course_progress;?>%">
                            <span class="sr-only"></span>
                        </div>
                    </div>
        
           </div> 
      <div class="w-col w-col-2 text-center">
         <?php if($course_progress<100){
                               $link_to_study=Yii::t("site","Continues");
                        }else{
                               $link_to_study=Yii::t("site","Review");
                        }
             ?>
        
         <a class="review-lesson btn btn-primary" href="<?php echo Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>$model_course->id))?>"><?php echo $link_to_study; ?></a>
      </div>   
      </div>
    </div>

</div>
<?php }else{ 
    if($mode=="user_info"){    
?>
<h5 class="admin-h5-black">ความก้าวหน้า (<?php echo $course_progress; ?>%)</h5>  

        <?php if($course_progress > 2){?>
        <div class="hidden-xs orange-text">
            <div style="width:<?php echo $course_progress + 1; ?>%;" class="progress-qty"><?php echo number_format($course_progress, 2); ?>%<span class="glyphicon glyphicon-map-marker"></span></div>
        </div>        
        <?php } ?>

        <div class="progress">
            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $course_progress;?>%">
                <span class="sr-only"></span>
            </div>
        </div>



 
    <?php }} ?>



