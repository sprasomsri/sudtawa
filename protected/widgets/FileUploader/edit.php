<?php
$contentId = $_POST['contentId'];
$contentPrefix = $_POST['contentPrefix'];
if($contentId == 1): $content = "รู้จักลักษณะของภาษาจีน"; endif;
if($contentId == 2): $content = "การออกเสียงภาษาจีน"; endif;
if($contentId == 3): $content = "เกี่ยวกับคอร์สนี้"; endif;
if($contentId == 4): $content = ""; endif;
?>

<?php if($contentId <= 2): ?>
<div class="row-fluid">
<div class="span10"> 
    <span class="content-prefix"><?php echo $contentPrefix;?> </span>
    <span id="content-name-<?php echo $contentId;?>" class="content-name">
    	<input id="txt-content-name-<?php echo $contentId;?>" type="text" value="<?php echo $content;?>" class="input-edit-course">
	</span>
</div>
<div class="span2" align="right">
    <a class="btn btn-white-bg" onclick="js:commitContent(<?php echo $contentId;?>,'<?php echo $contentPrefix;?>')" title="บันทึก"><i class="icon-submit-edit"></i></a>
    <a class="btn btn-white-bg" onclick="js:cancelEditContent(<?php echo $contentId;?>, '<?php echo $contentPrefix;?>')" title="ยกเลิก"><i class="icon-cancel-edit"></i></a>
    <a class="btn btn-white-bg" onclick="js:deleteContent(<?php echo $contentId;?>)" title="ลบ"><i class="icon-delete-course"></i></a>
</div>
</div>
<?php endif; ?>

<?php if($contentId == 3): ?>
<div class="row-fluid">
<div class="span10"> 
    <span class="content-prefix"><?php echo $contentPrefix;?> </span>
    <span id="content-name-<?php echo $contentId;?>" class="content-name">
		<input id="txt-content-name-<?php echo $contentId;?>" type="text" value="<?php echo $content;?>" class="input-edit-course"	>
	</span>
</div>
<div class="span2" align="right">
    <a class="btn btn-white-bg" onclick="js:commitContent(<?php echo $contentId;?>,'<?php echo $contentPrefix;?>')" title="บันทึก"><i class="icon-submit-edit"></i></a>
    <a class="btn btn-white-bg" onclick="js:cancelEditContent(<?php echo $contentId;?>, '<?php echo $contentPrefix;?>')" title="ยกเลิก"><i class="icon-cancel-edit"></i></a>
    <a class="btn btn-white-bg" onclick="js:deleteContent(<?php echo $contentId;?>)" title="ลบ"><i class="icon-delete-course"></i></a>
</div>
<div class="line-lightgrey-wrapper">&nbsp;</div>
</div>
<div class="row-fluid editable-row">
    <div class="row-fluid row-course">
        <div class="span2 editable-content-body">สื่อการสอน:</div>
        <div class="span3 editable-media">
            <div class="span4"><i class="icon-editable-video"></i></div>
            <div class="span8">
                <div class="editable-media-title">
                    video.avi
                </div>
                <div class="editable-media-size">
                    34.50 MB
                </div>
                <span class='editable-del'><a href="#"><i class="icon-editable-del"></i></a></span>
            </div>                
        </div>
        <div class="span7"></div>
    </div>
    <div class="row-fluid row-course">
        <div class="span2 editable-content-body">ไฟล์ประกอบ:</div>
        <div class="span3 editable-media">
            <div class="span4"><i class="icon-editable-script"></i></div>
            <div class="span8">
                <div class="editable-media-title">
                    script.pdf
                </div>
                <div class="editable-media-size">
                    645 KB
                </div>
                <span class='editable-del'><a href="#"><i class="icon-editable-del"></i></a></span>
            </div>                
        </div>
        <div class="span7"></div>
    </div>
 	<div class="row-fluid row-course">
        <div class="span2"></div>
        <div class="span10">	
            <div class="file-uploader-container">
            <div id="1-progressbar-container" class="file-name progress progress-info progress-striped active">
            <span id="1-label" class="file-name-text" data-placeholder="upload"></span>
            <div id="1-progressbar" class="bar" style="width: 0%"></div>
            </div>
            <div id="1-upload-btn" class="btn file-upload-btn">
            <span>Upload</span>
            <input class="file-file" 
            id="1" 
            type="file"
            data-url="#"
            data-deleteUrl="#"
            onchange="js:fileUploaderChangeHandler('1')"
            >
            </div>	
            <div id="1-done-btn" 
            class="btn btn-success file-done-btn" style="display:none" 
            onclick="onDone">
            <span>Done</span>
            </div>
            <div id="1-upload-cancel-btn" class="btn btn-danger file-upload-cancel-btn" style="display:none;">
            <span>Cancel</span>
            </div>
            </div> 
        </div>
    </div>
</div>

<?php endif; ?>

<?php if($contentId == 4): ?>
<div class="row-fluid">
<div class="span10"> 
    <span class="content-prefix"><?php echo $contentPrefix;?> </span>
    <?php if($content != ""): ?>
    <span id="content-name-<?php echo $contentId;?>" class="content-name">
		<input id="txt-content-name-<?php echo $contentId;?>" type="text" value="<?php echo $content;?>" class="input-edit-course"	>
	</span>
    <?php endif; ?>
</div>
<div class="span2" align="right">
    <a class="btn btn-white-bg" onclick="js:commitContent(<?php echo $contentId;?>,'<?php echo $contentPrefix;?>')" title="บันทึก"><i class="icon-submit-edit"></i></a>
    <a class="btn btn-white-bg" onclick="js:cancelEditContent(<?php echo $contentId;?>, '<?php echo $contentPrefix;?>')" title="ยกเลิก"><i class="icon-cancel-edit"></i></a>
    <a class="btn btn-white-bg" onclick="js:deleteContent(<?php echo $contentId;?>)" title="ลบ"><i class="icon-delete-course"></i></a>
</div>
<div class="row-fluid line-lightgrey-wrapper">&nbsp;</div>
</div>
<div class="row-fluid editable-row">
    <div class="row-fluid row-course">
        <div class="span2 editable-content-body">ประเภทคำถาม:</div>
        <div class="span10">
        	<div class="btn-toolbar">
                <div data-toggle="buttons-radio">
                  <button type="button" class="btn btn-editable-multiple-choice"></button>
                  <button type="button" class="btn btn-editable-true-or-false"></button>
                  <button type="button" class="btn btn-editable-fill-in-the-blank"></button>
                </div>
            </div>        
        </div>
    </div>
    <div class="row-fluid row-course">
        <div class="editable-content-body">รายการคำถาม:</div>
        <div class="row-fluid">
        	<div class="row-fluid">
                <span class="editable-media-inner">
                    <a class="btn btn-white-bg" title="แก้ไข"><i class="icon-edit-course"></i></a>
                    <a class="btn btn-white-bg" title="ลบ"><i class="icon-delete-course"></i></a>
                </span>
                <span class="editable-content-body">1. จงเติมคำในช่องว่าง</span>
            </div>        
        	<div class="row-fluid">
                <span class="editable-media-inner">
                    <a class="btn btn-white-bg" title="แก้ไข"><i class="icon-edit-course"></i></a>
                    <a class="btn btn-white-bg" title="ลบ"><i class="icon-delete-course"></i></a>
                </span>
                <span class="editable-content-body">2. จงเลือกข้อที่ถูกต้อง</span>
            </div>        
        </div>       
    </div>
</div>

<?php endif; ?>

<script>
$('.editable-media').hover(
  function() {
    $(this).find('span.editable-del').show();
  },
  function() {
    $(this).find('span.editable-del').hide();
  }
);
</script>