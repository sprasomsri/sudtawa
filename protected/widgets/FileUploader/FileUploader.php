<?php
class FileUploader extends BaseWidget
{
	public $config;
        public $model;
        public $name;

	function init()
	{
		//$this->publishAssets(__DIR__);
            $widgetPath = Yii::app()->basePath.'/widgets/FileUploader/';
            $this->publishAssets($widgetPath);
	}

	function run()
	{
		$this->render('fileuploader',
			array(
				'config' => $this->config,
                                'model' => $this->model,
                                'name'=>$this->name
			)
		);
	}
}
