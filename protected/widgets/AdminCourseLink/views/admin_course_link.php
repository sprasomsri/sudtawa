<?php if (!empty($model_user) && ($model_user->can_create_course == 1)) { ?>
      <?php $member_regis_link = Yii::app()->createUrl('/admin/default/ownCourse') ?>
    <button class="btn btn-inverse btn-bg btn-padding" onClick="window.location.href = '<?php echo $member_regis_link; ?>'"><i class="icon-course-complete"></i><?php echo Yii::t("site", "Owner's Course") ?></button>
<?php } ?>
