<?php
class CodeEditor extends CWidget{
    # Variable get when this widget was call  
     public $code="";
     public $model_code="";
     
     #2 mode model and text
     public $mode="model";
     
     #for mode text
     public $input_name="code";
     public $text_code="";
     public $is_exercise="0";
     public $input_answer_name="answer";
     public $is_show_compile="1";     
     public $comment ="";
     
     public $is_show_answer="0";
     public $answer="";





     # Run Widget ---> comtroller
	function run() {
        // Render widget
        $user_id = Yii::app()->user->id;
        $random = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz0123456789'), 0, 10);
        
        if($this->mode=="model"){
           
            $this->render('code_editor', array("code"=>$this->code,                                         
                                               "model_code"=>$this->model_code,                                                  
                                               "random"=>$random));
        }else{
            
            if($this->is_exercise==0){
            
            $this->render('code_editor_text', array("code"=>$this->text_code,                                                                          
                                            "input_name"=>  $this->input_name,
                                             "is_exercise"=>  $this->is_exercise,
                                             "input_answer_name"=>  $this->input_answer_name,
                                             "is_show_compile"=>  $this->is_show_compile,
                                             "comment"=>  $this->comment,
                                             "random"=>$random,));
            }else{
                
                $this->render('code_editor_exercise', array("code"=>$this->text_code,                                                                          
                                            "input_name"=>  $this->input_name,
                                             "is_exercise"=>  $this->is_exercise,
                                             "input_answer_name"=>  $this->input_answer_name,
                                             "is_show_compile"=>  $this->is_show_compile,
                                             "comment"=>  $this->comment,
                                             "is_show_answer"=>  $this->is_show_answer,
                                             "answer"=>  $this->answer,
                                             "random"=>$random,));
                
            }
        }
    }
}
?>