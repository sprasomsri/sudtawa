<div class="row-fluid code">
    <div class="span12">
        <?php 
            if(!empty($model_code)){
                $code=$model_code->code;
            }else{
                $code=$default_code;
            }
        
        ?>
        
        
        <div id="<?php echo $random; ?>" class="editor-box"><?php echo $code; ?></div>
        <textarea name="code" id="input-<?php echo $random; ?>"></textarea>
        
        
        <div id="run-<?php echo $random;?>" class="play-box"> 

            
            
           <a class="btn btn-xs play-btn" type="button" id="play-<?php echo $random; ?>"><span class="glyphicon glyphicon-play"></i></a>
           <a class="btn btn-xs play-btn" type="button" id="stop-<?php echo $random; ?>"><span class="glyphicon glyphicon-stop"></i></a>
            
            
            <div id="loading-<?php echo $random;?>"class='loading-widget' style='display:none;'>
                <img src="images/bar-loading.gif" width="40px"> Writing code..
            </div>
        </div>
        <div id="show-<?php echo $random;?>" class="show-compile"></div>
    </div>                                    
</div>

 
<?php $compile_url= Yii::app()->createUrl("post/compileCode");?>
<script type="text/javascript"> 
      $(document).ready(function() {
        var editor = ace.edit("<?php echo $random; ?>");
        editor.setTheme("ace/theme/crimson_editor");
        editor.getSession().setMode("ace/mode/java");
        var textarea = $('textarea[id="input-<?php echo $random?>"]').hide();


        $("#<?php echo $random; ?>").on("mouseleave", function() {
            var data = editor.getSession().getValue();
            textarea.val(data);
            $("#loading-<?php echo $random; ?>").hide();
        });

        $("#<?php echo $random; ?>").on("click", function() {
            $("#loading-<?php echo $random; ?>").show();
        });
        
         $("#play-<?php echo $random; ?>").on("click", function() {
            //ajax run data'
            
             var code_post=editor.getSession().getValue();
           
             $.ajax({                         
                url: "<?php echo $compile_url; ?>",
                type: 'POST',
                data:{code:code_post},
                dataType: 'html',
                cache: false,
                beforeSend:function(){
                    $("#loading-<?php echo $random; ?>").show();
                    $(".save").hide();
                },
                success:function(data) {
                     $("#loading-<?php echo $random; ?>").hide();
                     $("#show-<?php echo $random; ?>").html(data);
                     $("#show-<?php echo $random; ?>").show();

                  }
            });
            
            
        });
        
           $("#stop-<?php echo $random; ?>").on("click", function() {
            $("#loading-<?php echo $random; ?>").hide();
            $("#show-<?php echo $random; ?>").hide();
        });
        
    })
</script>   