<div class="row-fluid code">
    <div class="span12">
        <div id="<?php echo $random; ?>" class="editor-box"><?php if(!empty($code)){ echo $code; }else{echo $comment; }?></div>
        
        <div id="run-<?php echo $random;?>" class="play-box"> 
         
                <a class="btn btn-xs play-btn" type="button" id="play-<?php echo $random; ?>"><span class="glyphicon glyphicon-play"></i></a>
                <a class="btn btn-xs play-btn" type="button" id="stop-<?php echo $random; ?>"><span class="glyphicon glyphicon-stop"></i></a>
          
            <div id="loading-<?php echo $random;?>"class='loading-widget' style='display:none;'>
                <img src="images/bar-loading.gif" width="40px"> Writing code..
            </div>
        </div>
      
        <div id="show-<?php echo $random;?>" class="show-compile"></div> 
        
        
        <textarea name="<?php echo $input_name; ?>" id="input-<?php echo $random; ?>"></textarea>
         <!-- answer for check exercise answer -->
        <textarea name="<?php echo $input_answer_name; ?>" id="answer-<?php echo $random; ?>"></textarea>
        
        
    </div>                                    
</div>

 
<?php   
    if($is_show_compile==1){
        $compile_url= Yii::app()->createUrl("post/compileCode");
    }else{
        $compile_url= Yii::app()->createUrl("post/compileCode",array("is_show_compile"=>$is_show_compile));
    }
    
    
    
    
    
?>
<script type="text/javascript"> 
      $(document).ready(function() {
        var editor = ace.edit("<?php echo $random; ?>");
        editor.setTheme("ace/theme/crimson_editor");
        editor.getSession().setMode("ace/mode/java");
        var textarea = $('textarea[id="input-<?php echo $random?>"]').hide();
        var answer_textarea = $('textarea[id="answer-<?php echo $random?>"]').hide();


        $("#<?php echo $random; ?>").on("mouseleave", function() {
            var data = editor.getSession().getValue();
             textarea.val(data);           
            $("#loading-<?php echo $random; ?>").hide();
        });

        $("#<?php echo $random; ?>").on("click", function() {
            $("#loading-<?php echo $random; ?>").show();
        });
        
         $("#play-<?php echo $random; ?>").on("click", function() {
            //ajax run data'
            
             var code_post=editor.getSession().getValue();
             
             $.ajax({                         
                url: "<?php echo $compile_url; ?>",
                type: 'POST',
                data:{code:code_post},
                dataType: 'html',
                cache: false,
                beforeSend:function(){
                    $("#loading-<?php echo $random; ?>").show();
                    $(".save").hide();
                },
                success:function(data) {
                     $("#loading-<?php echo $random; ?>").hide();
                     $("#show-<?php echo $random; ?>").html(data);
                     $("#show-<?php echo $random; ?>").show();
                     answer_textarea.val(data);
                  }
            });
            
            
        });
        
            $("#stop-<?php echo $random; ?>").on("click", function() {
            $("#loading-<?php echo $random; ?>").hide();
            $("#show-<?php echo $random; ?>").hide();
        });
        
    })
</script>   