
<div class="row-fluid code">
    <div class="span12">
        <div id="<?php echo $random; ?>" class="editor-box"><?php if(!empty($code)){ echo $code; }else{echo $comment; }?></div>
        
        <div id="run-<?php echo $random;?>" class="play-box"> 
                 
                <div class="box-btn"><span class="glyphicon glyphicon-play" id="play-<?php echo $random; ?>"></span> </div>
                <div class="box-btn"><span class="glyphicon glyphicon-stop" id="stop-<?php echo $random; ?>"></i></div>            
    
            <div id="loading-<?php echo $random;?>"class='loading-widget' style='display:none;'>
                <img src="images/bar-loading.gif" width="40px"> Writing code..
            </div>
        </div>
      
        <div id="show-<?php echo $random;?>" class="show-compile"></div> 
        
        
        <textarea name="<?php echo $input_name; ?>" id="input-<?php echo $random; ?>"></textarea>
         <!-- answer for check exercise answer -->
         <div id="answer-title-<?php echo $random; ?>" style="display:none;">  
             <h4> Answer </h4>
             Please <span class="glyphicon glyphicon-play"></span> for assign answer.
         </div>
         
         
         
        
         <textarea name="<?php echo $input_answer_name; ?>" id="answer-<?php echo $random; ?>" data-show="<?php echo $is_show_answer; ?>" rows="10" style="width: 95%;" ><?php echo $answer; ?></textarea>
        
        
        
        
    </div>                                    
</div>

 
<?php $compile_url= Yii::app()->createUrl("post/compileCodeJson",array("is_show_compile"=>$is_show_compile));  ?>

<script type="text/javascript"> 
      $(document).ready(function() {
        var editor = ace.edit("<?php echo $random; ?>");
        editor.setTheme("ace/theme/crimson_editor");
        editor.getSession().setMode("ace/mode/java");
        var textarea = $('textarea[id="input-<?php echo $random?>"]').hide();
        
        var show_ans=$("#answer-<?php echo $random?>").data('show');
      
        if(show_ans==0){
            var answer_textarea = $('textarea[id="answer-<?php echo $random?>"]').hide();
        }else{
            $("#answer-title-<?php echo $random?>").show();
             var answer_textarea = $('textarea[id="answer-<?php echo $random?>"]');
        }


        $("#<?php echo $random; ?>").on("mouseleave", function() {
            var data = editor.getSession().getValue();
             textarea.val(data);           
            $("#loading-<?php echo $random; ?>").hide();
        });

        $("#<?php echo $random; ?>").on("click", function() {
            $("#loading-<?php echo $random; ?>").show();
        });
        
         $("#play-<?php echo $random; ?>").on("click", function() {
            //ajax run data'
            
             var code_post=editor.getSession().getValue();
             
             $.ajax({                         
                url: "<?php echo $compile_url; ?>",               
                type: 'POST',
                data:{code:code_post},
                dataType: 'json',
                cache: false,
                beforeSend:function(){
                    $("#loading-<?php echo $random; ?>").show();
                    $(".save").hide();
                },
                success:function(json) {                          
                     $("#loading-<?php echo $random; ?>").hide();            
                     $("#show-<?php echo $random; ?>").html(json.compile_result);
                     $("#show-<?php echo $random; ?>").show();
                     answer_textarea.val(json.answer);                     
                  }
            });
            
            
        });
        
            $("#stop-<?php echo $random; ?>").on("click", function() {
            $("#loading-<?php echo $random; ?>").hide();
            $("#show-<?php echo $random; ?>").hide();
        });
        
    })
</script>   