<?php
class FaceBookLogin extends CWidget{
    # Variable get when this widget was call  
     public $size="default";
     public $mode="index";
     public $word="เข้าสู่ระบบ ด้วย Facebook";
     public $course_id;

    # Run Widget ---> comtroller"data"=>$this->data,
        function run() {

        $user_id=Yii::app()->user->id;
         $current_uri=$_SERVER['REQUEST_URI'];
         $return_uri=$_SERVER['REQUEST_URI'];

         $facebook = SiteHelper::getFacbook();

         $model=new UserLogin;

         $loginUrl = $facebook->getLoginUrl(array("scope"=>"email"));

        $user = $facebook->getUser();

         if (!empty($user)&&empty($user_id)) {
           
            try {
              // Proceed knowing you have a logged in user who's authenticated.
              $user_profile = $facebook->api('/me');

            } catch (FacebookApiException $e) {
              error_log($e);
              $user = null;
            }

//            echo "<img src='https://graph.facebook.com/$user/picture' />";

           $model=new UserLogin;

        $is_success = $model->faceBookAccess($user_profile);
           //set state

           if($is_success==true){
               //setstate
              Yii::app()->user->setState('facebook', $user);
//              header("refresh: 1;");
              
              if(!empty($this->course_id)){
                    $return_url=Yii::app()->createUrl("course/default/studyCourse",array("courseId"=>  $this->course_id));
                      header("refresh: 1;url=$return_url");
              }else{
              
                    if(!empty(Yii::app()->user->returnUrl)){
                        $return_url=Yii::app()->user->returnUrl;
                        $user_id=Yii::app()->user->id;
                        
                        if(!empty($user_id)){
                            $return_url=Yii::app()->createUrl("user/dashboard/index");
                        }
                                                
                        header("refresh: 1;url=$return_url");
                    }else{
                        $return_url=Yii::app()->createUrl("site/index");
                        header("refresh: 1;url=$return_url");
                    }
              }
              
            }

       }

        $this->render('facebook_login', array("size"=>$this->size,
                                              "mode"=>  $this->mode,
                                              "word"=>  $this->word,
                                              "facebook"=>$facebook,
                                              "user"=>$user,"loginUrl"=>$loginUrl));
    }
}
?>
        