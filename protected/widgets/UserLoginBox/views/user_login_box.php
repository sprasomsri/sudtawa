<?php $user_id = Yii::app()->user->id; ?>
<?php if (empty($user_id) || $user_id == "0") { ?>
<div class="block-login">
    <div class="w-form">
        <?php
                    
                $form = $this->beginWidget('CActiveForm', array(
                'id' => 'regis',
                'action' => Yii::app()->createUrl('/user/login/login'),
                'enableClientValidation' => true,
                'enableAjaxValidation' => true,
                'htmlOptions' => array("class" => "form-login"),
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
    
        ?>        
            <h1 class="text-login">LOGIN</h1>
            <label for="name">Student Id:</label>
            <input type="text" name="UserLogin[username]" placeholder="Enter your student id" class="w-input" required="required">
           
            <label for="email">Password:</label>
            <input type="password" class="w-input password" id="email" name="UserLogin[password]" required="required"  placeholder="Password"200>
              <input name="rememberMe" type="checkbox" class="" checked="" value="1"> Remember Me ?
            
            <div class="bg-button-login">                
                <input class="w-button button-login" type="submit" value="LOGIN" data-wait="Please wait...">
            </div>
            
              <a href="<?php echo Yii::app()->createUrl("user/recovery");?>" title="Forgot Password"> Forgot Password </a>
            
            
         <?php $this->endWidget(); ?>
        
    </div>
</div>
<?php } ?>


