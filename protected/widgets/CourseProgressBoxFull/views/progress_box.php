
<div id="precontent">
    <div class="container-fluid">
        <div class="container shadow-bottom">
            <div class="row-fluid shadow-top">
                <div class="row-fluid course-title-group">

                    <div class="span3 course-title-percentage label-blue">
                        <?php if($type=="student"){ ?>
                            <div><?php echo Yii::t("site","Has Studyied"); ?></div>
                            <div class="percentage-text"><?php echo (int)$course_progress."%";?></div>
                        <?php }else{ ?>
                             <div>Preview Course</div>
                             <div class="percentage-text"> </div>
                        <?php } ?>
                    </div><!-- course-title-percentage -->

                    <div class="span9 course-title-name">
                        <div class="course-name"><?php echo $model_course->name.":".$model_course->name_th;?></div>
                        <div class="teacher-name">
                            <span class="teacher-name-label"><?php echo Yii::t("site","Teacher"); ?></span>
                            <?php echo $course_owner ?>
                        </div>
                    </div> &nbsp;&nbsp;&nbsp;<br/>
                    
                </div><!-- course-title-group -->
                <?php if($mode=="full"){ ?>
                    <div class="course-progress">
                        <div class="progress progress-wrapper">
                            
                            <?php if(!empty($progress_list)){ ?> 
                            <?php 
                                 $unit_count=1;
                                 $grap_period=97/count($progress_list);
                                 foreach($progress_list as $list){ 
                            ?>
                            
                             <div class="progress progress-inner completed" style="width:<?php echo $grap_period."%"; ?>">
                                <div class="lesson-label"><?php echo $unit_count;?></div>
                                <div class="percentage-label"><?php echo $list; ?>%</div>
                                <div class="bar bar-success" style="width: <?php echo $list; ?>%;"></div>
                            </div>
                            
                            
                            
                            <?php $unit_count++; }}?>
                            
                          </div>
                        
                    </div>
                <?php } ?>
            </div><!-- row-fluid -->
        </div><!-- container -->
    </div><!-- container-fluid-->
</div><!--precontent-->