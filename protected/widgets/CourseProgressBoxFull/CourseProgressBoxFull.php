<?php
class CourseProgressBoxFull extends CWidget{

    public $course_id;
    //mode =half >>>not show progress full 
    //     = full >>> show progress bar
    public $mode="full";
    public $type="student";
    
# Run Widget ---> comtroller
	function run() {
        // Render widget
            
        $user_id = Yii::app()->user->id;
        if(!empty($user_id)){
            $model_course=  Course::model()->findByPk($this->course_id);
            $model_content_main=  Content::model()->findAll("course_id='$this->course_id' AND parent_id='-1' ORDER BY show_order ASC");
            $progress_list=array();
            
            $all_child=0;
            $has_studied=0;
            $course_progress=0;
            
            if(!empty($model_content_main)){
                $qty_main=count($model_content_main);
                             
                foreach($model_content_main as $main){
                    if(!empty($main->childContents)){
                    $child_in_unit=count($main->childContents); //All Child in chapter
                    $all_child+=$child_in_unit;
                    $count_studied=0;
                        foreach($main->childContents as $sub){
                                
                               $is_study_content=  UserContentLog::model()->Count("content_id='$sub->id' AND user_id='$user_id'"); 
                               if($is_study_content>0){
                                   $count_studied++;
                                   $has_studied+=1;
                               }
                                          
                            
                        }
                         $progress_list[$main->id]=($count_studied/$child_in_unit)*100;           
                        
                    }else{
                      // have parent not have child <<< not for Calulate                     
                    }
                }
                
                if($all_child!=0){
                    $course_progress=($has_studied/$all_child)*100; # all study Percent 
                }
                
            }
            
        }
        
        $user_owner_id=  UserCourse::model()->find("course_id='$this->course_id' AND (role='1' OR role='2')")->user_id;
        if(!empty($user_owner_id)){
          
             $course_owner= Profile::model()->find("user_id='$user_owner_id'");
             $name_owner=$course_owner->firstname." ".$course_owner->lastname;
        }
        
        
   
        $this->render("progress_box",array("model_course"=>$model_course,
                                           "progress_list"=>$progress_list,
                                           "course_progress"=>$course_progress,
                                           "course_owner"=>$name_owner,
                                           "mode"=>$this->mode,
                                            "type"=>$this->type));
        
        
  
        
     }
}
?>
