<?php

/**
 * This is the model class for table "coupon".
 *
 * The followings are the available columns in table 'coupon':
 * @property integer $id
 * @property integer $discount
 * @property string $expire_date
 * @property integer $qty_use
 * @property integer $user_id
 */
class Coupon extends CouponBase
{
    public $qty;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CouponBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
        public function getRoundCoupon($course,$user_id,$is_admin="") {
        $criteria = new CDbCriteria;
        $criteria->select="MAX(expire_date) as expire_date,
                           MAX(create_date) as create_date,
                           COUNT(t.id) as id,
                           MAX(t.discount) as discount";
        $criteria->condition = "course_id ='$course' AND user_id='$user_id'";
        $criteria->order = "create_date DESC";
        $criteria->group="create_date";

        return new CActiveDataProvider('Coupon', array(
            'pagination' => array('pagesize' => 20),
            'criteria' => $criteria,
        ));
    }
    
        
    public function getCouponInGroup($date,$course,$user_id){
        $criteria = new CDbCriteria;
        $select="MAX(t.code) as code,
                 MAX(t.discount) as discount,
                 COUNT(coupon_log.coupon_id) as qty,
                 MAX(t.course_id) as course_id";
        
        $criteria->condition = "t.course_id ='$course' AND t.user_id='$user_id' AND t.create_date='$date'";
        $criteria->join="LEFT JOIN coupon_log ON t.id=coupon_log.coupon_id";
        $criteria->group="t.id";
      // $criteria->group="coupon_log.coupon_id";
        
     
       
       
        return new CActiveDataProvider('Coupon', array(
            'pagination' => array('pagesize' => 20),
            'criteria' => $criteria,
        ));
        
    }

	
}