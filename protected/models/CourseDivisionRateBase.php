
<?php

/**
 * This is the model class for table "course_division_rate".
 *
 * The followings are the available columns in table 'course_division_rate':
 * @property integer $id
 * @property integer $course_id
 * @property integer $teacher_share_percent
 * @property integer $company_share_percent
 * @property string $create_date
 * @property integer $is_default
 * @property string $remark
 */
class CourseDivisionRateBase extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'course_division_rate';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('course_id, teacher_share_percent, company_share_percent, create_date', 'required'),
            array('course_id, teacher_share_percent, company_share_percent, is_default', 'numerical', 'integerOnly'=>true),
            array('remark', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, course_id, teacher_share_percent, company_share_percent, create_date, is_default, remark', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'course_id' => 'Course',
            'teacher_share_percent' => 'Teacher Share Percent',
            'company_share_percent' => 'Company Share Percent',
            'create_date' => 'Create Date',
            'is_default' => 'Is Default',
            'remark' => 'Remark',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('course_id',$this->course_id);
        $criteria->compare('teacher_share_percent',$this->teacher_share_percent);
        $criteria->compare('company_share_percent',$this->company_share_percent);
        $criteria->compare('create_date',$this->create_date,true);
        $criteria->compare('is_default',$this->is_default);
        $criteria->compare('remark',$this->remark,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CourseDivisionRateBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}