<?php

/**
 * This is the model class for table "stat_user_study".
 *
 * The followings are the available columns in table 'stat_user_study':
 * @property string $content_has_studied
 * @property integer $user_id
 * @property integer $course_id
 */
class StatUserStudy extends StatUserStudyBase
{
    public $name;
    public $lastname;
    public $email;
    public $photo;
    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StatUserStudyBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getListUserProgressInCourse($course,$key_word){
            $criteria = new CDbCriteria;
            $criteria->select= "t.*,
                                tbl_profiles.firstname as name,
                                tbl_profiles.firstname as lastname,
                                tbl_profiles.photo as photo,
                                tbl_users.email as email";
            $criteria->condition="t.course_id='$course'";
            $criteria->join=" INNER JOIN tbl_users ON t.user_id = tbl_users.id
                              INNER JOIN tbl_profiles ON t.user_id = tbl_profiles.user_id";
            
            if(!empty($key_word)){
                $criteria.= " AND (tbl_users.email likes '%$key_word%' OR tbl_profiles.firstname like '%$key_word%' OR tbl_profiles.lastname like '%$keyword%')";
            }
            
             return new CActiveDataProvider('StatUserStudy', array(
            'pagination' => array('pagesize' => 20),
            'criteria' => $criteria,
        ));     
            
        }
        
    

	
}