<?php

/**
 * This is the model class for table "exercise_stat".
 *
 * The followings are the available columns in table 'exercise_stat':
 * @property integer $id
 * @property integer $exercise_id
 * @property integer $user_id
 * @property string $do_time
 * @property integer $qty_question
 * @property integer $qty_correct
 * @property string $question_list
 */
class ExerciseStatBase extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ExerciseStatBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'exercise_stat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('exercise_id, user_id, do_time', 'required'),
			array('exercise_id, user_id, qty_question, qty_correct', 'numerical', 'integerOnly'=>true),
			array('question_list', 'length', 'max'=>300),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, exercise_id, user_id, do_time, qty_question, qty_correct, question_list', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'exercise_id' => 'Exercise',
			'user_id' => 'User',
			'do_time' => 'Do Time',
			'qty_question' => 'Qty Question',
			'qty_correct' => 'Qty Correct',
			'question_list' => 'Question List',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('exercise_id',$this->exercise_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('do_time',$this->do_time,true);
		$criteria->compare('qty_question',$this->qty_question);
		$criteria->compare('qty_correct',$this->qty_correct);
		$criteria->compare('question_list',$this->question_list,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}