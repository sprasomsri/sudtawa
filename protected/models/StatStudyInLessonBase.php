<?php

/**
 * This is the model class for table "stat_study_in_lesson".
 *
 * The followings are the available columns in table 'stat_study_in_lesson':
 * @property string $qty_study_lesson
 * @property string $total_time_view
 * @property integer $content_id
 * @property string $title
 * @property integer $show_order
 * @property integer $parent_id
 * @property integer $course_id
 */
class StatStudyInLessonBase extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'stat_study_in_lesson';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, course_id', 'required'),
            array('content_id, show_order, parent_id, course_id', 'numerical', 'integerOnly'=>true),
            array('qty_study_lesson', 'length', 'max'=>21),
            array('total_time_view', 'length', 'max'=>32),
            array('title', 'length', 'max'=>500),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('qty_study_lesson, total_time_view, content_id, title, show_order, parent_id, course_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'qty_study_lesson' => 'Qty Study Lesson',
            'total_time_view' => 'Total Time View',
            'content_id' => 'Content',
            'title' => 'Title',
            'show_order' => 'Show Order',
            'parent_id' => 'Parent',
            'course_id' => 'Course',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('qty_study_lesson',$this->qty_study_lesson,true);
        $criteria->compare('total_time_view',$this->total_time_view,true);
        $criteria->compare('content_id',$this->content_id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('show_order',$this->show_order);
        $criteria->compare('parent_id',$this->parent_id);
        $criteria->compare('course_id',$this->course_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return StatStudyInLessonBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}