<?php

/**
 * This is the model class for table "user_course".
 *
 * The followings are the available columns in table 'user_course':
 * @property integer $id
 * @property integer $user_id
 * @property integer $course_id
 * @property integer $bought_price
 * @property integer $role
 * @property integer $is_head
 * @property integer $is_show
 * @property string $create_date
 * @property string $expire_date
 * @property integer $is_active
 */
class UserCourseBase extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_course';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, course_id', 'required'),
            array('user_id, course_id, bought_price, role, is_head, is_show, is_active', 'numerical', 'integerOnly'=>true),
            array('create_date, expire_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, course_id, bought_price, role, is_head, is_show, create_date, expire_date, is_active', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'course_id' => 'Course',
            'bought_price' => 'Bought Price',
            'role' => 'Role',
            'is_head' => 'Is Head',
            'is_show' => 'Is Show',
            'create_date' => 'Create Date',
            'expire_date' => 'Expire Date',
            'is_active' => 'Is Active',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('course_id',$this->course_id);
        $criteria->compare('bought_price',$this->bought_price);
        $criteria->compare('role',$this->role);
        $criteria->compare('is_head',$this->is_head);
        $criteria->compare('is_show',$this->is_show);
        $criteria->compare('create_date',$this->create_date,true);
        $criteria->compare('expire_date',$this->expire_date,true);
        $criteria->compare('is_active',$this->is_active);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserCourseBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}