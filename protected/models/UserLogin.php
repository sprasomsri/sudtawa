<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class UserLogin extends CFormModel
{
	public $username;
        public $email;
	public $password;
	public $rememberMe;
        public $facebook_id;

        /**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
        
		return array(
			// username and password are required
			array('username, password', 'required'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
                    
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe'=>UserModule::t("Remember me next time"),
			'username'=>UserModule::t("username or email"),
			'password'=>UserModule::t("password"),
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())  // we only want to authenticate when no input errors
		{
		
                    $identity=new UserIdentity($this->username,$this->password);
                    
                    //file in module
                    $identity->authenticate();
			switch($identity->errorCode)
			{
				case UserIdentity::ERROR_NONE:
					$duration=$this->rememberMe ? Yii::app()->controller->module->rememberMeTime : 0;
					Yii::app()->user->login($identity,$duration);
					break;
				case UserIdentity::ERROR_EMAIL_INVALID:
					$this->addError("username",UserModule::t("Email is incorrect."));
					break;
				case UserIdentity::ERROR_USERNAME_INVALID:
					$this->addError("username",UserModule::t("Username is incorrect."));
					break;
				case UserIdentity::ERROR_STATUS_NOTACTIV:
					$this->addError("status",UserModule::t("You account is not activated."));
					break;
				case UserIdentity::ERROR_STATUS_BAN:
					$this->addError("status",UserModule::t("You account is blocked."));
					break;
				case UserIdentity::ERROR_PASSWORD_INVALID:
					$this->addError("password",UserModule::t("Password is incorrect."));
					break;
			}
		}
	}
        public function faceBookAccess($facebook_obj) {
            $email = $facebook_obj['email'];
            $facebook_id = $facebook_obj['id'];
            $name_facebook = $facebook_obj['name'];
            $first_name = $facebook_obj['first_name'];
            $lastname = $facebook_obj['last_name'];
            $link = $facebook_obj['link'];
            $random = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz0123456789'), 0, 100);

            $model_external_provider=  ExternalProvider::model()->find("email = '$email' && provider_id ='$facebook_id'");
            if(!empty($model_external_provider)){
                $model_user=  User::model()->findByPk($model_external_provider->user_id);
              
            }else{   
            
            //firsttime login with face book
            $model_user = UsersFaceBook::model()->find("email='$email' AND facebook_id='$facebook_id'");
            if (empty($model_user)) {
                $model_user = new UsersFaceBook();
                $model_user->username = $email;
                $model_user->password = "";
                $model_user->facebook_id = $facebook_id;
                $model_user->email = $email;
                $model_user->activkey = $random;
                $model_user->create_at = date("Y-m-d H:i:s");
                $model_user->lastvisit = date("Y-m-d H:i:s");
                $model_user->superuser = 0;
                $model_user->status = 1;
                $model_user->save(false);

                $model_profile = new ProfilesEdit();
                $model_profile->firstname = $first_name;
                $model_profile->lastname = $lastname;
                $model_profile->user_id = $model_user->id;
                $model_profile->save(false);
                
                $model_user_permission=new UserPermission();
                $model_user_permission->user_id=$model_user->id;
                $model_user_permission->can_create_course="0";
                $model_user_permission->save();

                
    //	      $identity->authenticate();               
                }            
            }
            
            $model_user->lastvisit = date("Y-m-d H:i:s");
            $model_user->save(false); 
            Yii::app()->user->setState('username', $model_user->username);
            Yii::app()->user->setState('superadmin', $model_user->superuser);
                      
            $this->username=$model_user->username;
            $this->password=" ";
            Yii::app()->user->id=$model_user->id;
            
            return TRUE; 
            

      
    }
        

        
        
        
        
       
}
