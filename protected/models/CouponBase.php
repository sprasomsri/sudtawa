<?php

/**
 * This is the model class for table "coupon".
 *
 * The followings are the available columns in table 'coupon':
 * @property integer $id
 * @property string $code
 * @property integer $discount
 * @property string $expire_date
 * @property integer $qty_use
 * @property string $detail
 * @property string $create_date
 * @property integer $course_id
 * @property integer $user_id
 */
class CouponBase extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CouponBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'coupon';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code, discount, expire_date, create_date, user_id', 'required'),
			array('discount, qty_use, course_id, user_id', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>500),
			array('detail', 'length', 'max'=>1000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, code, discount, expire_date, qty_use, detail, create_date, course_id, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => 'Code',
			'discount' => 'Discount',
			'expire_date' => 'Expire Date',
			'qty_use' => 'Qty Use',
			'detail' => 'Detail',
			'create_date' => 'Create Date',
			'course_id' => 'Course',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('expire_date',$this->expire_date,true);
		$criteria->compare('qty_use',$this->qty_use);
		$criteria->compare('detail',$this->detail,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('course_id',$this->course_id);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}