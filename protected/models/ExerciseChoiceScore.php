<?php

/**
 * This is the model class for table "exercise_choice_score".
 *
 * The followings are the available columns in table 'exercise_choice_score':
 * @property integer $id
 * @property integer $answer_id
 * @property integer $question_id
 * @property integer $user_id
 * @property string $do_time
 * @property integer $is_true
 */
class ExerciseChoiceScore extends ExerciseChoiceScoreBase {

    public $answer;
    public $sequence;

    public static function model($className = __CLASS__) {
        
        return parent::model($className);
        
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'answer' => array(self::HAS_ONE, 'ExerciseAnswerChoice', 'answer_id'),
        );
    }

}