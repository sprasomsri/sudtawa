<?php

/**
 * This is the model class for table "file_content".
 *
 * The followings are the available columns in table 'file_content':
 * @property integer $id
 * @property integer $file_id
 * @property integer $content_id
 */
class FileContent extends FileContentBase
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FileContentBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	
}