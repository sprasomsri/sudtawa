<?php

/**
 * This is the model class for table "content".
 *
 * The followings are the available columns in table 'content':
 * @property integer $id
 * @property string $title
 * @property integer $course_id
 * @property integer $parent_id
 * @property string $detail
 * @property string $youtube_link
 * @property integer $show_order
 * @property integer $file_id
 * @property integer $is_public
 * @property integer $is_exercise
 * @property integer $is_code
 * @property integer $is_free
 * @property integer $is_approve
 * @property string $update_date
 */
class ContentBase extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'content';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, course_id', 'required'),
            array('course_id, parent_id, show_order, file_id, is_public, is_exercise, is_code, is_free, is_approve', 'numerical', 'integerOnly'=>true),
            array('title, youtube_link', 'length', 'max'=>500),
            array('detail, update_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, course_id, parent_id, detail, youtube_link, show_order, file_id, is_public, is_exercise, is_code, is_free, is_approve, update_date', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'course_id' => 'Course',
            'parent_id' => 'Parent',
            'detail' => 'Detail',
            'youtube_link' => 'Youtube Link',
            'show_order' => 'Show Order',
            'file_id' => 'File',
            'is_public' => 'Is Public',
            'is_exercise' => 'Is Exercise',
            'is_code' => 'Is Code',
            'is_free' => 'Is Free',
            'is_approve' => 'Is Approve',
            'update_date' => 'Update Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('course_id',$this->course_id);
        $criteria->compare('parent_id',$this->parent_id);
        $criteria->compare('detail',$this->detail,true);
        $criteria->compare('youtube_link',$this->youtube_link,true);
        $criteria->compare('show_order',$this->show_order);
        $criteria->compare('file_id',$this->file_id);
        $criteria->compare('is_public',$this->is_public);
        $criteria->compare('is_exercise',$this->is_exercise);
        $criteria->compare('is_code',$this->is_code);
        $criteria->compare('is_free',$this->is_free);
        $criteria->compare('is_approve',$this->is_approve);
        $criteria->compare('update_date',$this->update_date,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ContentBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}