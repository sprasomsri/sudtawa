<?php

/**
 * This is the model class for table "coupon_log".
 *
 * The followings are the available columns in table 'coupon_log':
 * @property integer $coupon_id
 * @property integer $course_id
 * @property string $used_time
 * @property integer $user_id
 */
class CouponLog extends CouponLogBase
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CouponLogBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	
}