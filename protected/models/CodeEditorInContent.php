<?php

/**
 * This is the model class for table "codeEditor_in_content".
 *
 * The followings are the available columns in table 'codeEditor_in_content':
 * @property integer $id
 * @property integer $content_id
 * @property integer $user_id
 * @property string $code
 * @property string $position
 */
class CodeEditorInContent extends CodeEditorInContentBase
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
