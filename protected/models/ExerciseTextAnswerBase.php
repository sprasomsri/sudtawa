<?php

/**
 * This is the model class for table "exercise_text_answer".
 *
 * The followings are the available columns in table 'exercise_text_answer':
 * @property integer $id
 * @property string $answer
 * @property integer $question_id
 * @property string $hint
 * @property integer $sequence
 */
class ExerciseTextAnswerBase extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ExerciseTextAnswerBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'exercise_text_answer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('answer, question_id', 'required'),
			array('question_id, sequence', 'numerical', 'integerOnly'=>true),
			array('answer', 'length', 'max'=>1000),
			array('hint', 'length', 'max'=>700),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, answer, question_id, hint, sequence', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'answer' => 'Answer',
			'question_id' => 'Question',
			'hint' => 'Hint',
			'sequence' => 'Sequence',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('answer',$this->answer,true);
		$criteria->compare('question_id',$this->question_id);
		$criteria->compare('hint',$this->hint,true);
		$criteria->compare('sequence',$this->sequence);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}