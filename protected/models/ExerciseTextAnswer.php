<?php

/**
 * This is the model class for table "exercise_text_answer".
 *
 * The followings are the available columns in table 'exercise_text_answer':
 * @property integer $id
 * @property string $answer
 * @property integer $question_id
 * @property integer $sequence
 */
class ExerciseTextAnswer extends ExerciseTextAnswerBase
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	
}