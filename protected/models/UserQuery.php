<?php

class UserQuery extends User
{
        public $name,$lastname,$create_date,$course_id;
    
    
	public function getPersonInCourse($course,$type) {
      
        $criteria = new CDbCriteria;
        $criteria->select="tbl_profiles.firstname as name,
                           tbl_profiles.lastname as lastname,
                           user_course.create_date as create_date,
                           user_course.course_id as course_id,
                           user_course.user_id as id";
        $criteria->condition = "user_course.role='$type' AND user_course.course_id='$course'";
        $criteria->join="INNER JOIN user_course ON user.id=user_course.user_id
                         INNER JOIN tbl_profiles ON user.id = tbl_profiles.user_id";
        $criteria->order="user_course.create_date DESC";
        
              //search form gride view
        $criteria->compare('tbl_profiles.firstname', $this->name, true);
        $criteria->addSearchCondition('tbl_profiles.lastname',$this->name,true,'OR');

        return new CActiveDataProvider(get_class($this), array(
            'pagination' => array(
                'pageSize' => 15,
            ),
            'criteria' => $criteria,
        ));
    }
}
