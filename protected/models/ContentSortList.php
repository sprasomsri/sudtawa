<?php

/**
 * This is the model class for table "content_sort_list".
 *
 * The followings are the available columns in table 'content_sort_list':
 * @property integer $show_order_test
 * @property string $title
 * @property integer $id
 * @property integer $modify_id
 * @property integer $is_change_content
 * @property integer $is_change_title
 * @property integer $is_change_order
 * @property integer $is_change_file
 * @property string $date_approve
 * @property string $update_date
 * @property integer $course_id
 * @property integer $parent_id
 */




class ContentSortList extends ContentSortListBase
{
    public $is_approve=0;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'content_sort_list';
    }


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    
    public function relations(){
                $user_id = Yii::app()->user->id;
              
		return array(
			'childContents'=>array(self::HAS_MANY, 'ContentSortList', 'parent_id',
                                                'order'=>'show_order ASC,id ASC','condition'=>"is_public='1'"),
                        'childContentsManage'=>array(self::HAS_MANY, 'ContentSortList', 'parent_id',
                                                'order'=>'show_order ASC'),
                        'course'=>array(self::BELONGS_TO, 'Course', 'course_id'),                    
                        'log'=>array(self::HAS_MANY, 'UserContentLog', 'user_id',),   
                        'approve'=>array(self::HAS_ONE, 'ContentModifyBase', 'original_id'),                    
		);
    }
    
    public function getChildContent(){
        $model_children = ContentSortList::model()->findAll("parent_id='$this->id' ORDER BY show_order ASC");
        return $model_children;
    }
    
    public function getLessonViews($id){
            $views=0;
            $sql="SELECT SUM( qty_times ) as qty_times
                  FROM user_content_log 
                  WHERE content_id='$id'";
            
            $model= UserContentLog::model()->findBySql($sql);
            if(!empty($model)){
                $views=$model->qty_times;
            }
            return number_format($views);
            
     }
    
    
    
    
}

?>