<?php

/**
 * This is the model class for table "codeEditor".
 *
 * The followings are the available columns in table 'codeEditor':
 * @property string $id
 * @property string $code
 * @property integer $admin_id
 * @property integer $admin_update_id
 * @property string $create_date
 * @property string $update_date
 */
class CodeEditorInFrame extends CodeEditorBase
{
   
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}