<?php

/**
 * This is the model class for table "course_money_division".
 *
 * The followings are the available columns in table 'course_money_division':
 * @property integer $id
 * @property string $teacher_share_percent
 * @property string $teacher_share_price
 * @property string $company_share_percent
 * @property string $company_share_price
 * @property string $affiliate_share_percent
 * @property string $affiliate_share_price
 * @property string $affiliate_code
 * @property integer $course_id
 * @property integer $user_id
 * @property string $create_date
 */
class CourseMoneyDivisionBase extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'course_money_division';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('teacher_share_percent, teacher_share_price, company_share_percent, company_share_price, affiliate_share_percent, affiliate_share_price, course_id, user_id, create_date', 'required'),
            array('course_id, user_id', 'numerical', 'integerOnly'=>true),
            array('teacher_share_percent, teacher_share_price, company_share_percent, company_share_price, affiliate_share_percent, affiliate_share_price, affiliate_code', 'length', 'max'=>500),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, teacher_share_percent, teacher_share_price, company_share_percent, company_share_price, affiliate_share_percent, affiliate_share_price, affiliate_code, course_id, user_id, create_date', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'teacher_share_percent' => 'Teacher Share Percent',
            'teacher_share_price' => 'Teacher Share Price',
            'company_share_percent' => 'Company Share Percent',
            'company_share_price' => 'Company Share Price',
            'affiliate_share_percent' => 'Affiliate Share Percent',
            'affiliate_share_price' => 'Affiliate Share Price',
            'affiliate_code' => 'Affiliate Code',
            'course_id' => 'Course',
            'user_id' => 'User',
            'create_date' => 'Create Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('teacher_share_percent',$this->teacher_share_percent,true);
        $criteria->compare('teacher_share_price',$this->teacher_share_price,true);
        $criteria->compare('company_share_percent',$this->company_share_percent,true);
        $criteria->compare('company_share_price',$this->company_share_price,true);
        $criteria->compare('affiliate_share_percent',$this->affiliate_share_percent,true);
        $criteria->compare('affiliate_share_price',$this->affiliate_share_price,true);
        $criteria->compare('affiliate_code',$this->affiliate_code,true);
        $criteria->compare('course_id',$this->course_id);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('create_date',$this->create_date,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CourseMoneyDivisionBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}