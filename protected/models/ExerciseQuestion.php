<?php

/**
 * This is the model class for table "exercise_question".
 *
 * The followings are the available columns in table 'exercise_question':
 * @property integer $id
 * @property string $question
 * @property integer $show_order
 * @property integer $exercise_id
 */
class ExerciseQuestion extends ExerciseQuestionBase
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ExerciseQuestion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function relations(){
              
		return array(
			                    
                       'answer'=>array(self::HAS_MANY, 'ExerciseAnswerChoice', 'question_id',
                                                'order'=>"RAND()"),
                    
                       'answerText'=>array(self::HAS_MANY, 'ExerciseTextAnswer', 'question_id',
                                                'order'=>"sequence ASC"),
                    
                       'lasted_score'=>array(self::HAS_MANY, 'ExerciseChoiceScore', 'question_id',
                                                       "condition"=>"question_id IN (SELECT question_id FROM exercise_choice_score GROUP BY user_id ORDER BY do_time DESC)"), //ขาด ว่าจะ select max date ไง + where ไงว่าเป็นชอบไหน
                    
                       'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
                    
                       'trueAnswerChoice'=>array(self::BELONGS_TO, 'ExerciseAnswerChoice', 'question_id',
                                                            "condition"=>"is_true='1'"),
                         'answerCode'=>array(self::HAS_ONE, 'ExerciseCodeAnswer', 'question_id'),
                              
		);
	}

	
}