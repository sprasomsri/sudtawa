<?php

/**
 * This is the model class for table "content_info".
 *
 * The followings are the available columns in table 'content_info':
 * @property integer $course_id
 * @property string $course_name
 * @property string $number_of_content
 */
class ContentInfo extends ContentInfoBase
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ContentInfoBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	
}