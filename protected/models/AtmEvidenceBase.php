
<?php

/**
 * This is the model class for table "atm_evidence".
 *
 * The followings are the available columns in table 'atm_evidence':
 * @property integer $id
 * @property string $ref_no
 * @property integer $user_id
 * @property integer $course_id
 * @property string $tranfer_date
 * @property string $create_date
 * @property string $update_date
 * @property integer $is_approve
 * @property string $evidence_file
 * @property string $from_bank
 * @property string $from_account
 * @property string $account_name
 * @property integer $qty_money
 * @property integer $active_status
 */
class AtmEvidenceBase extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'atm_evidence';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ref_no, user_id, course_id, tranfer_date, create_date, update_date, evidence_file, from_bank, from_account, account_name, qty_money', 'required'),
            array('user_id, course_id, is_approve, qty_money, active_status', 'numerical', 'integerOnly'=>true),
            array('ref_no, evidence_file, from_bank, from_account, account_name', 'length', 'max'=>500),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, ref_no, user_id, course_id, tranfer_date, create_date, update_date, is_approve, evidence_file, from_bank, from_account, account_name, qty_money, active_status', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'ref_no' => 'Ref No',
            'user_id' => 'User',
            'course_id' => 'Course',
            'tranfer_date' => 'Tranfer Date',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
            'is_approve' => 'Is Approve',
            'evidence_file' => 'Evidence File',
            'from_bank' => 'From Bank',
            'from_account' => 'From Account',
            'account_name' => 'Account Name',
            'qty_money' => 'Qty Money',
            'active_status' => 'Active Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('ref_no',$this->ref_no,true);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('course_id',$this->course_id);
        $criteria->compare('tranfer_date',$this->tranfer_date,true);
        $criteria->compare('create_date',$this->create_date,true);
        $criteria->compare('update_date',$this->update_date,true);
        $criteria->compare('is_approve',$this->is_approve);
        $criteria->compare('evidence_file',$this->evidence_file,true);
        $criteria->compare('from_bank',$this->from_bank,true);
        $criteria->compare('from_account',$this->from_account,true);
        $criteria->compare('account_name',$this->account_name,true);
        $criteria->compare('qty_money',$this->qty_money);
        $criteria->compare('active_status',$this->active_status);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AtmEvidenceBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}