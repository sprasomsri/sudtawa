<?php

/**
 * This is the model class for table "exercise_code_score".
 *
 * The followings are the available columns in table 'exercise_code_score':
 * @property integer $id
 * @property integer $question_id
 * @property string $code
 * @property string $answer
 * @property integer $user_id
 * @property string $do_time
 * @property integer $is_true
 */
class ExerciseCodeScore extends ExerciseCodeScoreBase
{
	/**
	 * @return string the associated database table name
	 */
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
