<?php

/**
 * This is the model class for table "file".
 *
 * The followings are the available columns in table 'file':
 * @property integer $id
 * @property string $name
 * @property string $original_name
 * @property integer $user_id
 * @property string $type
 */
class FileVdo extends FileBase
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FileBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        

        
        
//        	public function rules()
//	{
//		// NOTE: you should only define rules for those attributes that
//		// will receive user inputs.
//		return array(
//			array('name, original_name, user_id, type', 'required'),
//			array('user_id', 'numerical', 'integerOnly'=>true),
//                        array('original_name', 'file','types'=>'webm, mp4, 3gpp, mov, avi, wmv, flv, mpeg ', 'allowEmpty'=>false),
//			array('name, original_name', 'length', 'max'=>1000),
//			array('type', 'length', 'max'=>100),
//			// The following rule is used by search().
//			// Please remove those attributes that should not be searched.
//			array('id, name, original_name, user_id, type', 'safe', 'on'=>'search'),
//		);
//	}
        public function rules()
                {
                        // NOTE: you should only define rules for those attributes that
                        // will receive user inputs.
                        return array(
                                array('name, user_id, type', 'required'),
                                array('user_id', 'numerical', 'integerOnly'=>true),
                                array('name, original_name', 'length', 'max'=>1000),
                                array('type, duration', 'length', 'max'=>100),
                                array('original_name', 'checkVdoType'),
                                // The following rule is used by search().
                                // Please remove those attributes that should not be searched.
                                array('id, name, original_name, user_id, type, duration', 'safe', 'on'=>'search'),
                        );
                }
                
                
                
       public function checkVdoType($attribute, $params) {
        if (!empty($this->$attribute)) {
            list($name, $type) = explode(".", $this->$attribute);
            if ($type != "webm" && $type != "mp4" && $type != "3gpp" &&  $type != "mov" && $type != "avi" && $type != "wmv" && $type != "flv" && $type != "mpeg") {
                $this->addError('photo', ' **Worng File Type >> must be webm,mp4,3gpp,mov,avi,wmv,flv,mpeg');
            }
        }
    }
	
}