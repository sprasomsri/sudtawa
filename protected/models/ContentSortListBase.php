<?php

/**
 * This is the model class for table "content_sort_list".
 *
 * The followings are the available columns in table 'content_sort_list':
 * @property integer $show_order
 * @property string $title
 * @property integer $id
 * @property integer $modify_id
 * @property integer $is_change_content
 * @property integer $is_change_title
 * @property integer $is_change_order
 * @property integer $is_change_file
 * @property string $date_approve
 * @property string $update_date
 * @property integer $course_id
 * @property integer $parent_id
 * @property integer $is_exercise
 * @property integer $is_code
 * @property integer $is_public
 * @property integer $is_approve
 */
class ContentSortListBase extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'content_sort_list';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, course_id', 'required'),
            array('show_order, id, modify_id, is_change_content, is_change_title, is_change_order, is_change_file, course_id, parent_id, is_exercise, is_code, is_public, is_approve', 'numerical', 'integerOnly'=>true),
            array('title', 'length', 'max'=>500),
            array('date_approve, update_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('show_order, title, id, modify_id, is_change_content, is_change_title, is_change_order, is_change_file, date_approve, update_date, course_id, parent_id, is_exercise, is_code, is_public, is_approve', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'show_order' => 'Show Order',
            'title' => 'Title',
            'id' => 'ID',
            'modify_id' => 'Modify',
            'is_change_content' => 'Is Change Content',
            'is_change_title' => 'Is Change Title',
            'is_change_order' => 'Is Change Order',
            'is_change_file' => 'Is Change File',
            'date_approve' => 'Date Approve',
            'update_date' => 'Update Date',
            'course_id' => 'Course',
            'parent_id' => 'Parent',
            'is_exercise' => 'Is Exercise',
            'is_code' => 'Is Code',
            'is_public' => 'Is Public',
            'is_approve' => 'Is Approve',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('show_order',$this->show_order);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('id',$this->id);
        $criteria->compare('modify_id',$this->modify_id);
        $criteria->compare('is_change_content',$this->is_change_content);
        $criteria->compare('is_change_title',$this->is_change_title);
        $criteria->compare('is_change_order',$this->is_change_order);
        $criteria->compare('is_change_file',$this->is_change_file);
        $criteria->compare('date_approve',$this->date_approve,true);
        $criteria->compare('update_date',$this->update_date,true);
        $criteria->compare('course_id',$this->course_id);
        $criteria->compare('parent_id',$this->parent_id);
        $criteria->compare('is_exercise',$this->is_exercise);
        $criteria->compare('is_code',$this->is_code);
        $criteria->compare('is_public',$this->is_public);
        $criteria->compare('is_approve',$this->is_approve);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ContentSortListBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}