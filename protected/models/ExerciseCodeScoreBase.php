<?php

/**
 * This is the model class for table "exercise_code_score".
 *
 * The followings are the available columns in table 'exercise_code_score':
 * @property integer $id
 * @property integer $question_id
 * @property string $code
 * @property string $answer
 * @property integer $user_id
 * @property string $do_time
 * @property integer $is_true
 */
class ExerciseCodeScoreBase extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'exercise_code_score';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('question_id, user_id, do_time', 'required'),
			array('question_id, user_id, is_true', 'numerical', 'integerOnly'=>true),
			array('code, answer', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, question_id, code, answer, user_id, do_time, is_true', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'question_id' => 'Question',
			'code' => 'Code',
			'answer' => 'Answer',
			'user_id' => 'User',
			'do_time' => 'Do Time',
			'is_true' => 'Is True',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('question_id',$this->question_id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('answer',$this->answer,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('do_time',$this->do_time,true);
		$criteria->compare('is_true',$this->is_true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ExerciseCodeScoreBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
