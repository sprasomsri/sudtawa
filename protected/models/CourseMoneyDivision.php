<?php

/**
 * This is the model class for table "course_money_division".
 *
 * The followings are the available columns in table 'course_money_division':
 * @property integer $id
 * @property string $teacher_share_percent
 * @property string $teacher_share_price
 * @property string $company_share_percent
 * @property string $company_share_price
 * @property integer $course_id
 * @property integer $user_id
 * @property string $create_date
 */
class CourseMoneyDivision extends CourseMoneyDivisionBase
{
    
    public $affiliate_share_price_sum;


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'course_money_division';
    }


    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
      public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'profile'=>array(self::BELONGS_TO, 'Profile', 'user_id'),
            'user'=>array(self::BELONGS_TO, 'UsersFaceBook', 'id'),
            'course'=>array(self::BELONGS_TO, 'Course', 'course_id')
        );
        
        
    }
}