<?php

/**
 * This is the model class for table "user_affiliate_course".
 *
 * The followings are the available columns in table 'user_affiliate_course':
 * @property integer $id
 * @property integer $user_id
 * @property integer $course_id
 * @property integer $affiliate_id
 */
class UserAffiliateCourse extends UserAffiliateCourseBase
{
    /**
     * @return string the associated database table name
     */
    
    public $affiliate_share_price_sum;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    
     public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'profile'=>array(self::BELONGS_TO, 'Profile', 'user_id'),
            'user'=>array(self::BELONGS_TO, 'UsersFaceBook', 'id'),
            'course'=>array(self::BELONGS_TO, 'Course', 'course_id'),
            'affiliate'=>array(self::BELONGS_TO, 'CourseMoneyDivision', 'affiliate_id')
        );
        
        
    }
}