<?php

class UserContentLog extends UserContentLogBase
{
        public $user_email;
        public $user_name;
        public $user_lastname;
        public $qty_lesson_studied;
        public $progress;

        public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
        
        public function relations(){
                $user_id = Yii::app()->user->id;
		return array(

                        'course'=>array(self::BELONGS_TO, 'Course', 'course_id'),                    
                        'usr_profile'=>array(self::BELONGS_TO, 'Profile', 'user_id'),
                        'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
                                          
		);
	}
        
        
        
        public function getStatInLesson($lesson_id) {
        $criteria = new CDbCriteria;
        $criteria->select="t.qty_times as qty_times,
                           t.study_time as study_time,
                           tbl_users.email as user_email,
                           tbl_profiles.firstname as user_name,
                           tbl_profiles.lastname as user_lastname";
        $criteria->join= "INNER JOIN tbl_profiles on t.user_id = tbl_profiles.user_id
                          INNER JOIN tbl_users on t.user_id = tbl_users.id";
        $criteria->condition = "t.content_id ='$lesson_id'";
        $criteria->order = "qty_times DESC";
       
        return new CActiveDataProvider('UserContentLog', array(
            'pagination' => array('pagesize' => 20),
            'criteria' => $criteria,
        ));
    }
	
}