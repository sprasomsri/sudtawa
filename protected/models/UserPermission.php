<?php

/**
 * This is the model class for table "user_permission".
 *
 * The followings are the available columns in table 'user_permission':
 * @property integer $id
 * @property integer $user_id
 * @property integer $can_create_course
 */
class UserPermission extends UserPermissionBase
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserPermissionBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

        
           public function relations()
            {
                // NOTE: you may need to adjust the relation name and the related
                // class name for the relations automatically generated below.
                return array(             
                    'user'=>array(self::BELONGS_TO, 'Users', 'user_id'),   
                    'profile'=>array(self::BELONGS_TO, 'Profile', 'user_id'),   
                );
            }
	
}