<?php

/**
 * This is the model class for table "course_payment".
 *
 * The followings are the available columns in table 'course_payment':
 * @property integer $id
 * @property integer $user_id
 * @property integer $course_id
 * @property string $create_date
 * @property string $update_date
 * @property string $invoice_number
 * @property string $approveCode_first
 * @property string $approveCode_next
 * @property integer $qty_coin
 * @property string $result
 * @property string $result_next
 * @property string $apCode
 * @property string $amt
 * @property string $fee
 * @property string $method
 * @property string $method_name
 * @property string $confirm_cs
 * @property string $remark
 * @property integer $is_active
 */
class CoursePaymentBase extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'course_payment';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, course_id, create_date, invoice_number, approveCode_first', 'required'),
            array('user_id, course_id, qty_coin, is_active', 'numerical', 'integerOnly'=>true),
            array('invoice_number, approveCode_first, approveCode_next, result, result_next, apCode, amt, fee, method, method_name', 'length', 'max'=>500),
            array('confirm_cs', 'length', 'max'=>10),
            array('remark', 'length', 'max'=>1000),
            array('update_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, course_id, create_date, update_date, invoice_number, approveCode_first, approveCode_next, qty_coin, result, result_next, apCode, amt, fee, method, method_name, confirm_cs, remark, is_active', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'course_id' => 'Course',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
            'invoice_number' => 'Invoice Number',
            'approveCode_first' => 'Approve Code First',
            'approveCode_next' => 'Approve Code Next',
            'qty_coin' => 'Qty Coin',
            'result' => 'Result',
            'result_next' => 'Result Next',
            'apCode' => 'Ap Code',
            'amt' => 'Amt',
            'fee' => 'Fee',
            'method' => 'Method',
            'method_name' => 'Method Name',
            'confirm_cs' => 'Confirm Cs',
            'remark' => 'Remark',
            'is_active' => 'Is Active',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('course_id',$this->course_id);
        $criteria->compare('create_date',$this->create_date,true);
        $criteria->compare('update_date',$this->update_date,true);
        $criteria->compare('invoice_number',$this->invoice_number,true);
        $criteria->compare('approveCode_first',$this->approveCode_first,true);
        $criteria->compare('approveCode_next',$this->approveCode_next,true);
        $criteria->compare('qty_coin',$this->qty_coin);
        $criteria->compare('result',$this->result,true);
        $criteria->compare('result_next',$this->result_next,true);
        $criteria->compare('apCode',$this->apCode,true);
        $criteria->compare('amt',$this->amt,true);
        $criteria->compare('fee',$this->fee,true);
        $criteria->compare('method',$this->method,true);
        $criteria->compare('method_name',$this->method_name,true);
        $criteria->compare('confirm_cs',$this->confirm_cs,true);
        $criteria->compare('remark',$this->remark,true);
        $criteria->compare('is_active',$this->is_active);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CoursePaymentBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}