<?php 
/**
 * This is the model class for table "stat_study_in_lesson".
 *
 * The followings are the available columns in table 'stat_study_in_lesson':
 * @property string $qty_study_lesson
 * @property string $total_time_view
 * @property integer $content_id
 * @property string $title
 * @property integer $show_order
 * @property integer $parent_id
 * @property integer $course_id
 */
class StatStudyInLesson extends StatStudyInLessonBase
{
    /**
     * @return string the associated database table name
     */

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}


?>