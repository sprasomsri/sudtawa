<?php

/**
 * This is the model class for table "exercise_answer_choice".
 *
 * The followings are the available columns in table 'exercise_answer_choice':
 * @property integer $id
 * @property string $answer
 * @property integer $is_true
 * @property integer $question_id
 * @property string $hint
 */
class ExerciseAnswerChoice extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ExerciseAnswerChoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'exercise_answer_choice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('answer, is_true, question_id', 'required'),
			array('is_true, question_id', 'numerical', 'integerOnly'=>true),
			array('answer', 'length', 'max'=>1000),
			array('hint', 'length', 'max'=>700),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, answer, is_true, question_id, hint', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'answer' => 'Answer',
			'is_true' => 'Is True',
			'question_id' => 'Question',
			'hint' => 'Hint',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('answer',$this->answer,true);
		$criteria->compare('is_true',$this->is_true);
		$criteria->compare('question_id',$this->question_id);
		$criteria->compare('hint',$this->hint,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}