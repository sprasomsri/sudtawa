<?php

/**
 * This is the model class for table "course".
 *
 * The followings are the available columns in table 'course':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $price
 * @property string $course_img
 * @property integer $user_id
 * @property integer $duration
 * @property integer $is_public
 * @property integer $is_affiliate
 * @property integer $demo_video
 * @property string $theme_photo
 * @property string $youtube_demo
 * @property string $date_start
 * @property string $date_end
 * @property integer $course_option
 */

class CourseBase extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'course';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, price, user_id', 'required'),
            array('price, user_id, duration, is_public, is_affiliate, demo_video, course_option', 'numerical', 'integerOnly'=>true),
            array('name, theme_photo, youtube_demo', 'length', 'max'=>500),
            array('course_img', 'length', 'max'=>1000),
            array('description, date_start, date_end', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, description, price, course_img, user_id, duration, is_public, is_affiliate, demo_video, theme_photo, youtube_demo, date_start, date_end, course_option', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'price' => 'Price',
            'course_img' => 'Course Img',
            'user_id' => 'User',
            'duration' => 'Duration',
            'is_public' => 'Is Public',
            'is_affiliate' => 'Is Affiliate',
            'demo_video' => 'if not exit show course img',
            'theme_photo' => 'Theme Photo',
            'youtube_demo' => 'Youtube Demo',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'course_option' => 'Course Option',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('price',$this->price);
        $criteria->compare('course_img',$this->course_img,true);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('duration',$this->duration);
        $criteria->compare('is_public',$this->is_public);
        $criteria->compare('is_affiliate',$this->is_affiliate);
        $criteria->compare('demo_video',$this->demo_video);
        $criteria->compare('theme_photo',$this->theme_photo,true);
        $criteria->compare('youtube_demo',$this->youtube_demo,true);
        $criteria->compare('date_start',$this->date_start,true);
        $criteria->compare('date_end',$this->date_end,true);
        $criteria->compare('course_option',$this->course_option);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CourseBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}