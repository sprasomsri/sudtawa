<?php

/**
 * This is the model class for table "coin_setting".
 *
 * The followings are the available columns in table 'coin_setting':
 * @property integer $id
 * @property integer $coin_amount
 * @property integer $money_amount
 * @property integer $is_promotion
 * @property string $code
 * @property string $description
 * @property string $create_date
 * @property string $update_date
 * @property integer $active_status
 */
class CoinSettingBase extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'coin_setting';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('coin_amount, money_amount, create_date, update_date', 'required'),
            array('coin_amount, money_amount, is_promotion, active_status', 'numerical', 'integerOnly'=>true),
            array('code', 'length', 'max'=>100),
            array('description', 'length', 'max'=>500),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, coin_amount, money_amount, is_promotion, code, description, create_date, update_date, active_status', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'coin_amount' => 'Coin Amount',
            'money_amount' => 'Money Amount',
            'is_promotion' => 'Is Promotion',
            'code' => 'Code',
            'description' => 'Description',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
            'active_status' => 'Active Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('coin_amount',$this->coin_amount);
        $criteria->compare('money_amount',$this->money_amount);
        $criteria->compare('is_promotion',$this->is_promotion);
        $criteria->compare('code',$this->code,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('create_date',$this->create_date,true);
        $criteria->compare('update_date',$this->update_date,true);
        $criteria->compare('active_status',$this->active_status);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CoinSettingBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}