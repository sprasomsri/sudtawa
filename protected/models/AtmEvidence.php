
<?php

/**
 * This is the model class for table "atm_evidence".
 *
 * The followings are the available columns in table 'atm_evidence':
 * @property integer $id
 * @property string $ref_no
 * @property integer $user_id
 * @property integer $course_id
 * @property string $tranfer_date
 * @property string $create_date
 * @property string $update_date
 * @property integer $is_approve
 * @property string $evidence_file
 * @property string $from_bank
 * @property string $from_account
 * @property string $account_name
 * @property integer $qty_money
 * @property integer $active_status
 */
class AtmEvidence extends AtmEvidenceBase
{
    /**
     * @return string the associated database table name
     */

    /**
     * @return array relational rules.
     */


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
        public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'course'=>array(self::BELONGS_TO, 'Course', 'course_id'),    
            'user'=>array(self::BELONGS_TO, 'Users', 'user_id'),   
            'profile'=>array(self::BELONGS_TO, 'Profile', 'user_id'),   
        );
    }
}