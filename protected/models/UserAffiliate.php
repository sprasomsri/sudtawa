<?php

/**
 * This is the model class for table "user_affiliate".
 *
 * The followings are the available columns in table 'user_affiliate':
 * @property integer $id
 * @property integer $user_id
 * @property string $affiliate_code
 * @property string $bankaccount
 * @property string $subbranch
 * @property string $telephone
 * @property string $detail
 * @property integer $active_status
 * @property string $create_date
 */
class UserAffiliate extends UserAffiliateBase
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_affiliate';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, telephone, create_date', 'required'),
            array('user_id, active_status', 'numerical', 'integerOnly'=>true),
            array('affiliate_code', 'length', 'max'=>500),
            array('bankaccount, subbranch', 'length', 'max'=>200),
            array('telephone', 'length', 'max'=>100),
            array('detail', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, affiliate_code, bankaccount, subbranch, telephone, detail, active_status, create_date', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'profile'=>array(self::BELONGS_TO, 'Profile', 'user_id'),
            'user'=>array(self::BELONGS_TO, 'UsersFaceBook', 'id'),
           
        );
        
        
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'affiliate_code' => 'Affiliate Code',
            'bankaccount' => 'Bankaccount',
            'subbranch' => 'Subbranch',
            'telephone' => 'Telephone',
            'detail' => 'Detail',
            'active_status' => 'Active Status',
            'create_date' => 'Create Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('affiliate_code',$this->affiliate_code,true);
        $criteria->compare('bankaccount',$this->bankaccount,true);
        $criteria->compare('subbranch',$this->subbranch,true);
        $criteria->compare('telephone',$this->telephone,true);
        $criteria->compare('detail',$this->detail,true);
        $criteria->compare('active_status',$this->active_status);
        $criteria->compare('create_date',$this->create_date,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserAffiliateBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}