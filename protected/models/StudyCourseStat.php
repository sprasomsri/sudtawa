<?php

/**
 * This is the model class for table "study_course_stat".
 *
 * The followings are the available columns in table 'study_course_stat':
 * @property integer $id
 * @property integer $user_id
 * @property integer $course_id
 * @property string $view_date
 */
class StudyCourseStat extends StudyCourseStatBase
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StudyCourseStatBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}