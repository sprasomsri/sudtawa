<?php
/**
 * RegistrationForm class.
 * RegistrationForm is the data structure for keeping
 * user registration form data. It is used by the 'registration' action of 'UserController'.
 */
class UsersStudent extends Users {
	public $verifyPassword;
	public $verifyCode;
	
     public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }    
        
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('username, password', 'required'),
            array('username', 'unique',"message"=>"username นี้ถูกใช้งานแล้ว"),
           
        );
    }
        
        
        
	
}