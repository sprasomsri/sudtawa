<?php

/**
 * This is the model class for table "user_course".
 *
 * The followings are the available columns in table 'user_course':
 * @property integer $user_id
 * @property integer $course_id
 * @property integer $role
 */
class UserCourse extends UserCourseBase
{
        public $name;
        public $lastname;
        public $email;
        public $photo;
        public $username;
        public $detail;
        public $position;
        public $number_of_content;
        public $content_has_studied;
        public $facebook_id;
        public $is_head,$is_show;


        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserCourseBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function relations(){
                
		return array(
			'profile'=>array(self::BELONGS_TO, 'Profile', 'user_id',
                                                'order'=>'firstname ASC'),
                        'user'=>array(self::BELONGS_TO, 'UsersFaceBook', 'id'),
                        'content'=>array(self::BELONGS_TO, 'Content', 'course_id')
//		        'study'=>array(self::BELONGS_TO, 'StatUserStudy', 'user_id','condition'),
		);
	}
        
           
        public function getRelatePeople($course,$user_type1,$user_type2="",$type_query="dataProvider",$is_random="",$limit="",$already_list="") {
            $criteria = new CDbCriteria;
            $criteria->join = "INNER JOIN tbl_users on t.user_id = tbl_users.id
                               INNER JOIN tbl_profiles on tbl_users.id= tbl_profiles.user_id";
            $criteria->group = "tbl_users.id";
                        
            $criteria->select="t.id as id,
                               t.role as role,
                               t.is_head as is_head,
                               t.is_show as is_show,
                               t.create_date as create_date,
                               tbl_profiles.firstname as name,
                               tbl_profiles.lastname as lastname,
                               tbl_profiles.detail as detail,
                               tbl_users.username as username,
                               tbl_profiles.photo as photo,
                               tbl_profiles.position as position,
                               tbl_users.id as user_id,
                               tbl_users.email as email,
                               tbl_users.facebook_id as facebook_id";

            $criteria->condition = "course_id='$course' AND t.role='$user_type1'";
            
            if(!empty($user_type2)){
                  $criteria->condition = "course_id='$course' AND (t.role='$user_type1' OR t.role='$user_type2')";
            }
            
            if(!empty($limit)){
                $criteria->limit=$limit;
            }
            
            if(!empty($is_random)){
                $criteria->order="RAND()";
            }else{
                 $criteria->order="is_head DESC,role ASC";
            }
            
            if(!empty($already_list)){
                $criteria->condition.=" AND tbl_users.id NOT IN ($already_list)";
            }
            
            
            if($type_query=="dataProvider"){
            
            return new CActiveDataProvider(get_class($this), array(
                'pagination' => array(
                    'pageSize' => 9,
                ),
                'criteria' => $criteria,
            ));
            
            }else{                
//                print_r($criteria);
//                echo "<br/>";
                
                return $criteria;
            }
    }
    
    public function getQtyStudent($course_id){
        $qty_student=  UserCourse::model()->count("course_id='$course_id' AND role='3'");
        return $qty_student;
    }
    
    
    
    public function getListUserInformationInCourse($course,$key_word){
            $criteria = new CDbCriteria;
            $criteria->select= "tbl_profiles.firstname as name,
                                tbl_profiles.firstname as lastname,
                                tbl_profiles.photo as photo,
                                tbl_users.email as email,
                                tbl_users.facebook_id as facebook_id,
                                t.course_id as course_id,
                                t.user_id as user_id";
            $criteria->condition="t.course_id='$course' AND t.role='3'";
            $criteria->join=" INNER JOIN tbl_users ON t.user_id = tbl_users.id
                              INNER JOIN tbl_profiles ON t.user_id = tbl_profiles.user_id";
           
            
            if(!empty($key_word)){
               $criteria->condition .= " AND (tbl_users.email like '%$key_word%' OR tbl_profiles.firstname like '%$key_word%' OR tbl_profiles.lastname like '%$key_word%')";
            }
            
             return new CActiveDataProvider('UserCourse', array(
            'pagination' => array('pagesize' => 20),
            'criteria' => $criteria,
        ));     
            
        }
        
      
    
        

}