<?php

/**
 * This is the model class for table "content_modify".
 *
 * The followings are the available columns in table 'content_modify':
 * @property integer $id
 * @property integer $original_id
 * @property integer $parent_id
 * @property string $title
 * @property string $detail
 * @property string $youtube_link
 * @property integer $show_order
 * @property integer $file_id
 * @property integer $is_public
 * @property integer $is_exercise
 * @property integer $is_code
 * @property integer $is_free
 * @property integer $is_change_content
 * @property integer $is_change_title
 * @property integer $is_change_order
 * @property integer $is_change_file
 * @property integer $is_approve
 * @property string $date_approve
 */
class ContentModifyBase extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'content_modify';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('original_id, title', 'required'),
            array('original_id, parent_id, show_order, file_id, is_public, is_exercise, is_code, is_free, is_change_content, is_change_title, is_change_order, is_change_file, is_approve', 'numerical', 'integerOnly'=>true),
            array('title, youtube_link', 'length', 'max'=>500),
            array('detail, date_approve', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, original_id, parent_id, title, detail, youtube_link, show_order, file_id, is_public, is_exercise, is_code, is_free, is_change_content, is_change_title, is_change_order, is_change_file, is_approve, date_approve', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
        public function relations(){
                $user_id = Yii::app()->user->id;
              
		return array(
			'childContents'=>array(self::HAS_MANY, 'Content', 'parent_id',
                                                'order'=>'show_order ASC,id ASC','condition'=>"is_public='1'"),
                        'childContentsManage'=>array(self::HAS_MANY, 'Content', 'parent_id',
                                                'order'=>'show_order ASC,id ASC'),
			'videoFile'=>array(self::BELONGS_TO, 'File', 'file_id'),                    
                        'codeBox'=>array(self::HAS_ONE, 'CodeEditorInContent', 'content_id'),
                        'course'=>array(self::BELONGS_TO, 'Course', 'course_id'),                    
                        'log'=>array(self::HAS_MANY, 'UserContentLog', 'user_id',),   
                        'approve'=>array(self::HAS_ONE, 'ContentModifyBase', 'original_id'),
                    
//                        'log_study'=>array(self::HAS_MANY, 'UserContentLog', 'id','condition'=>"user_id='$user_id'"),     
                    
                        //  $relations['profile'] = array(self::HAS_ONE, 'Profile', 'user_id');   
                      
                        
		);
	}

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'original_id' => 'Original',
            'parent_id' => 'Parent',
            'title' => 'Title',
            'detail' => 'Detail',
            'youtube_link' => 'Youtube Link',
            'show_order' => 'Show Order',
            'file_id' => 'File',
            'is_public' => 'Is Public',
            'is_exercise' => 'Is Exercise',
            'is_code' => 'Is Code',
            'is_free' => 'Is Free',
            'is_change_content' => 'Is Change Content',
            'is_change_title' => 'Is Change Title',
            'is_change_order' => 'Is Change Order',
            'is_change_file' => 'Is Change File',
            'is_approve' => 'Is Approve',
            'date_approve' => 'Date Approve',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('original_id',$this->original_id);
        $criteria->compare('parent_id',$this->parent_id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('detail',$this->detail,true);
        $criteria->compare('youtube_link',$this->youtube_link,true);
        $criteria->compare('show_order',$this->show_order);
        $criteria->compare('file_id',$this->file_id);
        $criteria->compare('is_public',$this->is_public);
        $criteria->compare('is_exercise',$this->is_exercise);
        $criteria->compare('is_code',$this->is_code);
        $criteria->compare('is_free',$this->is_free);
        $criteria->compare('is_change_content',$this->is_change_content);
        $criteria->compare('is_change_title',$this->is_change_title);
        $criteria->compare('is_change_order',$this->is_change_order);
        $criteria->compare('is_change_file',$this->is_change_file);
        $criteria->compare('is_approve',$this->is_approve);
        $criteria->compare('date_approve',$this->date_approve,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ContentModifyBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}