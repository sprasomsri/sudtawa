<?php
/**
 * RegistrationForm class.
 * RegistrationForm is the data structure for keeping
 * user registration form data. It is used by the 'registration' action of 'UserController'.
 */
class RegistrationFormDoctor extends User {
	public $verifyPassword;
	public $verifyCode;
	
	public function rules() {
		$rules = array(
			array('username, password, verifyPassword, email', 'required'),
			array('username', 'length', 'max'=>100, 'min' => 3,'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('password', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
			array('email', 'email'),
			array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
			array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
                        array('username', 'checkavailable'),
			//array('verifyPassword', 'compare', 'compareAttribute'=>'password', 'message' => UserModule::t("Retype Password is incorrect.")),
//			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9).")),
		);
//		if (!(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')) {
//			array_push($rules,array('verifyCode', 'captcha', 'allowEmpty'=>!UserModule::doCaptcha('registration')));
//		}
		
		array_push($rules,array('verifyPassword', 'compare', 'compareAttribute'=>'password', 'message' => UserModule::t("Retype Password is incorrect.")));
		return $rules;
	}
        
        
    public function checkavailable($attribute, $params){
            //if id card 
            if (is_numeric($this->$attribute)) {
               
                   if ((strlen($this->$attribute)) != 13) {
                       $this->addError($attribute, 'กรุณาป้อนเลขบัตรประชาชน 13 หลัก หรือ เลขที่สมาชิกสัตวแพทยสภา');
                   }else{
                       //number 13 digit
                       //so check is real id number
                        $id = $this->$attribute;                        
                        $sum = 0;
                        for ($i = 0; $i < 12; $i++) {
                            if(empty($id{$i})){
                                $id{$i}="0";
                            }
                            $sum += (int) ($id{$i}) * (13 - $i);
                        }
                        
                        if ((11 - ($sum % 11)) % 10 != (int) ($id{12})) {
                             $this->addError($attribute, 'กรอกเลขบัตรประชาชนไม่ถูกต้อง');
                        }              
                   }
                
             }else{
            //else -- number of sudtawa sapa  
                  if ((strlen($this->$attribute)) != 12) {
                       $this->addError($attribute, 'กรุณาป้อนเลขบัตรประชาชน 13 หลัก หรือ เลขที่สมาชิกสัตวแพทยสภา');
                  }else{
                   
                       $number = $this->$attribute;
    
                        $list_id = explode("/", $number);                      

                        if(count($list_id)!=2){
                             $this->addError($attribute, 'เลขที่สมาชิกตัวแทนแพทย์สภาไม่ถูกต้อง ควรมีรูปแบบดังนี้ xx-xxxx/xxxx');
                        }else{
                            
                            $state_check = false;
                            if (preg_match("/^[0-9]{2}-[0-9]{4}$/",$list_id[0])){
                                $state_check = TRUE;
                            } 
                            
                            if (preg_match("/^[0-9]{4}$/",$list_id[1])){
                                $state_check = $state_check && TRUE;
                            }
                            if($state_check == FALSE){
                                 $this->addError($attribute, 'เลขที่สมาชิกตัวแทนแพทย์สภาไม่ถูกต้อง ควรมีรูปแบบดังนี้ xx-xxxx/xxxx');
                            }

                        }           
                      
                  }         
             }             
    }

 
        
        
        
	
}