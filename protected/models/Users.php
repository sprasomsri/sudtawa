<?php
/**
 * This is the model class for table "{{users}}".
 *
 * The followings are the available columns in table '{{users}}':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $facebook_id
 * @property string $activkey
 * @property string $create_at
 * @property string $lastvisit
 * @property integer $superuser
 * @property integer $is_inside
 * @property integer $is_student
 * @property integer $status
 */
class Users extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{users}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('username, password, email, activkey, create_at, superuser, status', 'required'),
            array('superuser, is_inside, is_student, status', 'numerical', 'integerOnly'=>true),
            array('username, password, email, activkey', 'length', 'max'=>128),
            array('facebook_id', 'length', 'max'=>500),
            array('lastvisit', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, username, password, email, facebook_id, activkey, create_at, lastvisit, superuser, is_inside, is_student, status', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
              'permission'=>array(self::HAS_ONE, 'UserPermission', 'user_id'),
              'profile'=>array(self::HAS_ONE, 'Profile', 'user_id'),
              'profile_edit'=>array(self::HAS_ONE, 'ProfilesEdit', 'user_id'),
              
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'facebook_id' => 'Facebook',
            'activkey' => 'Activkey',
            'create_at' => 'Create At',
            'lastvisit' => 'Lastvisit',
            'superuser' => 'Superuser',
            'is_inside' => 'Is Inside',
            'is_student' => 'Is Student',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('password',$this->password,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('facebook_id',$this->facebook_id,true);
        $criteria->compare('activkey',$this->activkey,true);
        $criteria->compare('create_at',$this->create_at,true);
        $criteria->compare('lastvisit',$this->lastvisit,true);
        $criteria->compare('superuser',$this->superuser);
        $criteria->compare('is_inside',$this->is_inside);
        $criteria->compare('is_student',$this->is_student);
        $criteria->compare('status',$this->status);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}