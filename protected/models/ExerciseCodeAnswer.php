<?php

/**
 * This is the model class for table "exercise_code_answer".
 *
 * The followings are the available columns in table 'exercise_code_answer':
 * @property integer $id
 * @property integer $question_id
 * @property string $code
 * @property string $answer
 */
class ExerciseCodeAnswer extends ExerciseCodeAnswerBase
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
