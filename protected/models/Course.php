<?php

/**
 * This is the model class for table "course".
 *
 * The followings are the available columns in table 'course':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $teacher
 * @property integer $price
 * @property string $course_img
 * @property integer $course_status
 */
class Course extends CourseBase
{
        public $qty_student;
        public $number_of_content;
        public $course_id;
        public $affiliate_code;


        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CourseBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
//        
//
//          public function rules()
//            {
//                // NOTE: you should only define rules for those attributes that
//                // will receive user inputs.
//                return array(
//                    array('name, price, user_id', 'required'),
//                    array('price, user_id, commission, duration, is_public, is_affiliate, demo_video, course_option', 'numerical', 'integerOnly'=>true),
//                    array('name, theme_photo, youtube_demo', 'length', 'max'=>500),
//                    array('course_img', 'length', 'max'=>1000),
//                    array('description, date_start, date_end', 'safe'),
//                    // The following rule is used by search().
//                    // @todo Please remove those attributes that should not be searched.
//                    array('id, name, description, price, course_img, user_id, commission, duration, is_public, is_affiliate, demo_video, theme_photo, youtube_demo, date_start, date_end, course_option', 'safe', 'on'=>'search'),
//                );
//            }
            
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, price, user_id', 'required'),
            array('price, user_id, duration, is_public, is_affiliate, demo_video, course_option', 'numerical', 'integerOnly'=>true),
            array('name, theme_photo, youtube_demo', 'length', 'max'=>500),
            array('course_img', 'length', 'max'=>1000),
            array('description, date_start, date_end', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, description, price, course_img, user_id, duration, is_public, is_affiliate, demo_video, theme_photo, youtube_demo, date_start, date_end, course_option', 'safe', 'on'=>'search'),
        );
    }
            
        
        
        public function relations(){
              
		return array(
			'owner'=>array(self::HAS_MANY, 'UserCourse', 'course_id',
                                                'condition'=>"role='1'","limit"=>"1"),
                        'info'=>array(self::HAS_ONE, 'ContentInfo', 'course_id'),
                    
		);
	}
        
         public function getQtyStudent(){
             $qty_students=  UserCourse::model()->Count("course_id='$this->id' AND role='3'");
             return $qty_students;
        }
        
         public function getTotalMoney(){
             $sql = "SELECT SUM(amt) as amt FROM course_payment WHERE course_id ='$this->id' AND approveCode_next='00'";
             
             $model_price= CoursePaymentBase::model()->findBySql($sql);
             return $model_price->amt;
        }
        
        
        
        public function isModifyContent(){
            $model_modify = ContentModifyBase::model()->find("original_id IN (SELECT id FROM content WHERE course_id='$this->id')");
            if(!empty($model_modify)){
                return TRUE;
            }else{
                return FALSE;
            }  
        }
        
         public function getMoneyStat($type="day"){
            $today = date("Y-m-d");
           
            
            if($type=="day"){
                $start = $today." 00:00:01";
                $end = $today." 23:59:59";
            }
            
            if($type=="week"){
                $period = SiteHelper::getWeekFromDate($today);
                $start = $period['start'];
                $end = $period['end'];
            }
            
            
            if($type=="month"){
                $period = SiteHelper::getMonthFromDate($today);
                $start = $period['start'];
                $end = $period['end'];
            }
            
            $sql_money = "SELECT SUM(amt) as amt FROM course_payment WHERE course_id='$this->id' AND approveCode_next='00' AND create_date BETWEEN '$start' AND '$end'";
            $model_price= CoursePaymentBase::model()->findBySql($sql_money);
            return $model_price->amt;
            
        }
        
        
        
        public function getStudentAccessStat($type="day"){
            $today = date("Y-m-d");
           
            
            if($type=="day"){
                $start = $today." 00:00:01";
                $end = $today." 23:59:59";
            }
            
            if($type=="week"){
                $period = SiteHelper::getWeekFromDate($today);
                $start = $period['start'];
                $end = $period['end'];
            }
            
            
            if($type=="month"){
                $period = SiteHelper::getMonthFromDate($today);
                $start = $period['start'];
                $end = $period['end'];
            }
            
            $sql_std = "SELECT COUNT(id) as qty FROM user_course WHERE course_id='$this->id' AND role='3' AND create_date BETWEEN '$start' AND '$end'";
            $stat=Yii::app()->db->createCommand($sql_std)->queryScalar();
            return $stat;
            
        }

        



        public function searchData()
            {
                // @todo Please modify the following code to remove attributes that should not be searched.

                $criteria=new CDbCriteria;

                $criteria->compare('id',$this->id);
                $criteria->compare('name',$this->name,true);
                $criteria->compare('name_th',$this->name_th,true);
  
                return $criteria;
            }
        
        public function getAffiliateCourse () {            
            $user_id = Yii::app()->user->id;
            $criteria = new CDbCriteria; 
            $criteria->select="t.*,user.affiliate_code as affiliate_code";
            $criteria->join="LEFT JOIN user_affiliate_course as affiliate ON (t.id = affiliate.course_id)
                             LEFT JOIN user_affiliate as user ON (affiliate.user_id = user.user_id)";
            
            $criteria->condition="t.id NOT IN (SELECT course_id FROM user_affiliate_course WHERE user_id='$user_id') AND t.is_affiliate=1 AND t.is_public='1'";
            $criteria->order="id DESC";
     
            $division_stat = new CActiveDataProvider('Course', array(
                'pagination' => array('pageSize' => "10"),
                'criteria' => $criteria,
            ));            
            
   
            $searchData=  $this->searchData();
            $criteria->mergeWith($searchData);
       
            return new CActiveDataProvider(get_class($this), array(
                        'pagination' => array(
                            'pageSize' => 10,
                        ),
                        'criteria' => $criteria,
                    ));
        }
        

}