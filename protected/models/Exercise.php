<?php

/**
 * This is the model class for table "exercise".
 *
 * The followings are the available columns in table 'exercise':
 * @property integer $id
 * @property integer $content_id
 * @property string $name
 */
class Exercise extends ExerciseBase
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ExerciseBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    
                   'question'=>array(self::HAS_MANY, 'ExerciseQuestion', 'exercise_id',
                                                'order'=>"show_order ASC"),
                    
                   'content'=>array(self::BELONGS_TO, 'content', 'content_id'),
                    
		);
	}
        

}