<?php

/**
 * This is the model class for table "external_provider".
 *
 * The followings are the available columns in table 'external_provider':
 * @property integer $id
 * @property integer $user_id
 * @property string $provider
 * @property string $user_provide_name
 * @property string $email
 * @property string $provider_id
 * @property string $create_date
 */
class ExternalProviderBase extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'external_provider';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, email, provider_id, create_date', 'required'),
            array('user_id', 'numerical', 'integerOnly'=>true),
            array('provider', 'length', 'max'=>100),
            array('user_provide_name, email, provider_id', 'length', 'max'=>500),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, provider, user_provide_name, email, provider_id, create_date', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'provider' => 'Provider',
            'user_provide_name' => 'User Provide Name',
            'email' => 'Email',
            'provider_id' => 'เช่น Facebook_id',
            'create_date' => 'Create Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('provider',$this->provider,true);
        $criteria->compare('user_provide_name',$this->user_provide_name,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('provider_id',$this->provider_id,true);
        $criteria->compare('create_date',$this->create_date,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ExternalProviderBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}

?>