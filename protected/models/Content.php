<?php

class Content extends ContentBase
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ContentBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'course_id' => 'Course',
			'parent_id' => 'Parent',
			'detail' => 'Detail',
			'show_order' => 'Show Order',
			'file_id' => 'File',
			'is_exercise' => 'Is Exercise',
		);
	}
        
        public function relations(){
                $user_id = Yii::app()->user->id;
              
		return array(
			'childContents'=>array(self::HAS_MANY, 'Content', 'parent_id',
                                                'order'=>'show_order ASC,id ASC','condition'=>"is_public='1'"),
                        'childContentsManage'=>array(self::HAS_MANY, 'Content', 'parent_id',
                                                'order'=>'show_order ASC,id ASC'),
			'videoFile'=>array(self::BELONGS_TO, 'File', 'file_id'),                    
                        'codeBox'=>array(self::HAS_ONE, 'CodeEditorInContent', 'content_id'),
                        'course'=>array(self::BELONGS_TO, 'Course', 'course_id'),                    
                        'log'=>array(self::HAS_MANY, 'UserContentLog', 'user_id',),   
                        'approve'=>array(self::HAS_ONE, 'ContentModifyBase', 'original_id'),
                    
//                        'log_study'=>array(self::HAS_MANY, 'UserContentLog', 'id','condition'=>"user_id='$user_id'"),     
                    
                        //  $relations['profile'] = array(self::HAS_ONE, 'Profile', 'user_id');   
                      
                        
		);
	}
        
        public function getLessonLogBullet(){
            $content_id = $this->id;
            $user_id = Yii::app()->user->id;
            $model_bullet = UserContentLogBase::model()->find("user_id='$user_id' AND content_id='$content_id'");
            
            $stat_img = "normal.png";
            
            if(!empty($model_bullet)){
                if($model_bullet->is_end==1){
                    $stat_img= "end.png";
                }else{
                    $stat_img="study.png";
                }
            }
            
            return $stat_img;
        }

        

        public function getLessonViews($id){
            $views=0;
            $sql="SELECT SUM( qty_times ) as qty_times
                  FROM user_content_log 
                  WHERE content_id='$id'";
            
            $model= UserContentLog::model()->findBySql($sql);
            if(!empty($model)){
                $views=$model->qty_times;
            }
            return number_format($views);
            
        }
        
        public function getNextLessonLink(){

            $sql="SELECT id,is_exercise,title
                  FROM content 
                  WHERE show_order > '$this->show_order' AND parent_id='$this->parent_id' AND course_id='$this->course_id'
                  ORDER BY show_order ASC
                  LIMIT 1";
            
            
           $html="";
            
            $content_next=  Content::model()->findBySql($sql);
            if(!empty($content_next)){
                if($content_next->is_exercise!= 1){
                    $link=Yii::app()->createUrl("course/default/getContent/id/$content_next->id");
                }else{
                    $link=Yii::app()->createUrl("course/default/doExerciseChoice/id/$content_next->id");
                }
//                $html="<i class='icon-step-forward'></i>";                
//                $html=CHtml::ajaxLink("Next Lesson $html", $link,array('update'=>'#content-place-holder'),array("id"=>"bar-next-$content_next->id","title"=>"Next"));                
            
                $html = "<a href='#' id='bar-next-$content_next->id' class='history-link' 
                        onclick='viewNextContent($content_next->id)' 
                        data-url='$link'>
                        Next Lesson <i class='icon-step-forward'></i>
                        </a>";   
                
             }
            
            return $html;  
                       
        }
        
        public function getPreviousLessonLink(){
           $sql="SELECT id,is_exercise,title
                  FROM content 
                  WHERE show_order < '$this->show_order' AND parent_id='$this->parent_id' AND course_id='$this->course_id'
                  ORDER BY show_order DESC
                  LIMIT 1";      
           $html="";
            
            $content_before=  Content::model()->findBySql($sql);
            if(!empty($content_before)){
                if($content_before->is_exercise!= 1){
                    $link=Yii::app()->createUrl("course/default/getContent/id/$content_before->id");
                }else{
                    $link=Yii::app()->createUrl("course/default/doExerciseChoice/id/$content_before->id");
                }
//                $html="<i class='icon-step-backward'></i>";                
//                $html=CHtml::ajaxLink("$html  Previous Lesson", $link,array('update'=>'#content-place-holder'),array("id"=>"bar-pev-$content_before->id","title"=>"Next"));                
//           viewPreviousContent
                 $html = "<a href='#' id='bar-previous-$content_before->id' class='history-link' 
                        onclick='viewPreviousContent($content_before->id)' 
                        data-url='$link'>
                        <i class='icon-step-backward'></i> Previous Lesson 
                        </a>";      
                
              }     
            
            return $html;  
            
        }
        
         public function getChildContent(){
            $model_children = Content::model()->findAll("parent_id='$this->id' AND course_id='$this->course_id'");
            return $model_children;
        }
        
        
       
        
	
}