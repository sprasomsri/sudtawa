
<?php

/**
 * This is the model class for table "content_suggestion".
 *
 * The followings are the available columns in table 'content_suggestion':
 * @property integer $id
 * @property integer $content_id
 * @property integer $user_id
 * @property string $message
 * @property string $create_date
 */
class ContentSuggestion extends ContentSuggestionBase
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'content_suggestion';
    }

 
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'content'=>array(self::BELONGS_TO, 'Content', 'content_id'),
            'user'=>array(self::BELONGS_TO, 'Users', 'user_id'),
            'profile'=>array(self::BELONGS_TO, 'Profile', 'user_id'),
            
        );
    }

 
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}