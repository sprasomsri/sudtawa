public function up()
{
    $this->createTable('CAMPAIN', array(
        'campain_id'=>'pk',
        'campain_name'=>'varchar(50) NOT NULL',
        'campain_desc'=>'varchar(200) NOT NULL',
        'campaint_slot'=>'int(11) NOT NULL',
        'campain_type'=>'int(11) NOT NULL',
        'date_start'=>'date NOT NULL',
        'date_end'=>'date NOT NULL',
        'date_added'=>'date NOT NULL',
        'status'=>'int(11) NOT NULL',
        'item_desc'=>'text NOT NULL',
        'url_img'=>'varchar(200) NOT NULL',
        'category_id'=>'int(11) NOT NULL',
        'website'=>'varchar(200) DEFAULT NULL',
        'fb_link'=>'varchar(200) DEFAULT NULL',
        'fb_name'=>'varchar(200) DEFAULT NULL',
        'fb_caption'=>'varchar(200) DEFAULT NULL',
        'fb_description'=>'varchar(200) DEFAULT NULL',
        'fb_picture'=>'varchar(200) DEFAULT NULL',
        'fb_media_source'=>'varchar(200) DEFAULT NULL',
    ), '');

    $this->createTable('CAMPAIN_TYPE', array(
        'campain_type_id'=>'pk',
        'type_name'=>'varchar(30) NOT NULL',
    ), '');

    $this->createTable('CATEGORY', array(
        'category_id'=>'pk',
        'category_name'=>'varchar(50) NOT NULL',
    ), '');

    $this->createTable('GIFT', array(
        'gift_id'=>'pk',
        'send_by'=>'int(11) NOT NULL',
        'send_to'=>'int(11) NOT NULL',
        'item_id'=>'int(11) NOT NULL',
        'status'=>'int(11) NOT NULL',
        'date_time'=>'date NOT NULL',
    ), '');

    $this->createTable('ITEM', array(
        'item_id'=>'pk',
        'c_id'=>'int(11) NOT NULL',
        'item_name'=>'varchar(50) NOT NULL',
        'item_des'=>'text NOT NULL',
        'number'=>'int(11) NOT NULL',
    ), '');

    $this->createTable('RELATED', array(
        'user_id'=>'int(11) NOT NULL',
        'item_id'=>'int(11) NOT NULL',
        'campain_id'=>'int(11) NOT NULL',
        'extra_info'=>'varchar(200) DEFAULT NULL',
    ), '');

    $this->createTable('USERS', array(
        'user_id'=>'pk',
        'email'=>'varchar(30) DEFAULT NULL',
        'fb_id'=>'varchar(50) NOT NULL',
        'regis_date'=>'date NOT NULL',
        'last_date'=>'date NOT NULL',
        'user_info'=>'text NOT NULL',
    ), '');

}


public function down()
{
    $this->dropTable('CAMPAIN');
    $this->dropTable('CAMPAIN_TYPE');
    $this->dropTable('CATEGORY');
    $this->dropTable('GIFT');
    $this->dropTable('ITEM');
    $this->dropTable('RELATED');
    $this->dropTable('USERS');
}
