<?php
class SiteHelper
{

    public static function setMetaTag($tag,$value)
   {
        //For Set Meta data in thai and english

       Yii::app()->clientScript->registerMetaTag($value, $tag);


    }

    public static function setTitle($value,$controller){
       $controller->pageTitle = $value;
    }

    public static function getImgBgOpacity($img=""){
        if(!empty($img)){
        return "
        color:black;
        background: -moz-linear-gradient(top, rgba(255,255,255,0.7) 0%, rgba(255,255,255,0.7) 100%), url(images/courseBg/$img) center center repeat;
        background: -webkit-linear-gradient(top, rgba(255,255,255,0.7) 0%,rgba(255,255,255,0.7) 100%), url(images/courseBg/$img) center center repeat;
        background: -o-linear-gradient(top, rgba(255,255,255,0.7) 0%,rgba(255,255,255,0.7) 100%), url(images/courseBg/$img) center center repeat;
        ";
        }
    }



    public static function getcontentStatus($status = "") {
        $status_name = "";
        if ($status == "1") {
            $status_name = "featured";
        }

        if ($status == 2) {
            $status_name = "New";
        }

        if (!empty($status_name)) {
            return "<span class='label label-warning label-noradius'>" . $status_name . "</span>";
        }
    }

    public static function getRibbonStatus($status=""){
        $html = "";
        if ($status == "1") {
           $html="<div class='ribbon-featured'></div>";
        }

        if ($status == 2) {
           $html="<div class='ribbon-new'></div>";
        }

        if (!empty($html)) {
            return $html;
        }
    }

    public static function dateFormat($datetime,$type="datetime",$show_time ="1") {

        $lang = Yii::app()->user->getState('lang');
        if (empty($lang)) {
            $lang = Yii::app()->language;
        }

        if($type=="datetime"){
            list($date, $time) = explode(' ', $datetime); // แยกวันที่ กับ เวลาออกจากกัน
            list($H, $i, $s) = explode(':', $time); // แยกเวลา ออกเป็น ชั่วโมง นาที วินาที
            list($Y, $m, $d) = explode('-', $date); // แยกวันเป็น ปี เดือน วัน
        }else{
             list($Y, $m, $d) = explode('-', $datetime); // แยกวันเป็น ปี เดือน วัน
        }



        if ($lang == "th") {
            $Y = $Y + 543;
            switch ($m) {
                case "01":$m = "มกราคม";
                    break;
                case "02":$m = "กุมพาพันธ์";
                    break;
                case "03":$m = "มีนาคม";
                    break;
                case "04":$m = "เมษายน";
                    break;
                case "05":$m = "พฤษภาคม";
                    break;
                case "06":$m = "มิถุนายน";
                    break;
                case "07":$m = "กรกฏาคม";
                    break;
                case "08":$m = "สิงหาคม";
                    break;
                case "09":$m = "กันยายน";
                    break;
                case "10":$m = "ตุลาคม";
                    break;
                case "11":$m = "พฤศจิกายน";
                    break;
                case "12":$m = "ธันวาคม";
                    break;
            }
        } else {
            switch ($m) {
                case "01":$m = "January";
                    break;
                case "02":$m = "February";
                    break;
                case "03":$m = "March";
                    break;
                case "04":$m = "April";
                    break;
                case "05":$m = "May";
                    break;
                case "06":$m = "June";
                    break;
                case "07":$m = "July";
                    break;
                case "08":$m = "August";
                    break;
                case "09":$m = "September";
                    break;
                case "10":$m = "October";
                    break;
                case "11":$m = "November";
                    break;
                case "12":$m = "December";
                    break;
            }
        }

        if($show_time==0){
             $datetime = $d . " " . $m . " " . $Y;
        }else{

            if(!empty($time)){
                 $datetime = $d . " " . $m . " " . $Y." ".$time;
            }else{
                 $datetime = $d . " " . $m . " " . $Y;
            }
        }

        return $datetime;
    }

    public static function getShortMonthName($month){
        $count_str = strlen($month);
        if($count_str==1){
            if($count_str <10){
                $month="0".$month;
            }
        }


        switch ($month) {
                case "01":$m = "Jan";
                    break;
                case "02":$m = "Feb";
                    break;
                case "03":$m = "Mar";
                    break;
                case "04":$m = "Apr";
                    break;
                case "05":$m = "May";
                    break;
                case "06":$m = "June";
                    break;
                case "07":$m = "July";
                    break;
                case "08":$m = "Aug";
                    break;
                case "09":$m = "Sep";
                    break;
                case "10":$m = "Oct";
                    break;
                case "11":$m = "Nov";
                    break;
                case "12":$m = "Dec";
                    break;
            }

            return $m;

    }

    public static function dateDMY($date="",$delimeter="/"){
        if(!empty($date)){
            $year=$month=$day="";
            list($year,$month,$day) = explode("-",$date);
            $new_date_formath= $day."$delimeter".$month."$delimeter".$year;
            return $new_date_formath;
        }

    }

    public static function datetimeDMY($datetime="",$delimeter="/"){
        if(!empty($datetime)){
            $year=$month=$day="";
            list($date,$time) = explode(" ",$datetime);
            list($year,$month,$day) = explode("-",$date);

            $new_date_formath= $day."$delimeter".$month."$delimeter".$year." ".$time;
            return $new_date_formath;
        }

    }

     public static function dateFromDatetime($datetime="",$delimeter="/"){
        if(!empty($datetime)){
            $year=$month=$day="";
            list($date,$time) = explode(" ",$datetime);
            list($year,$month,$day) = explode("-",$date);

            $new_date_formath= $day."$delimeter".$month."$delimeter".$year;
            return $new_date_formath;
        }

    }


    #formath fulllist | user_id => name lastname
    public static function getTeacherList($course_id = "", $formath = "fulllist") {
        $user_id = Yii::app()->user->id;
        $sql = " SELECT tbl_profiles.firstname as course_id,tbl_profiles.lastname as role,user_permission.user_id as user_id
                FROM `user_permission`
                INNER JOIN tbl_profiles ON user_permission.user_id = tbl_profiles.user_id
                WHERE user_permission.can_create_course='1'";
            $model_teachers = UserPermission::model()->findAllBySql($sql);

        if (!empty($course_id)) {
            // $criteria->condition.=" AND t.course_id='$course_id'";
            $sql = " SELECT tbl_profiles.firstname as course_id,tbl_profiles.lastname as role,tbl_profiles.user_id as user_id
                FROM `user_course`
                INNER JOIN tbl_profiles ON user_course.user_id = tbl_profiles.user_id
                WHERE user_course.role = '2' AND course_id='$course_id'";
             $model_teachers = UserCourse::model()->findAllBySql($sql);
        }


        $model_teachers = UserCourse::model()->findAllBySql($sql);

        $teacher_list = array();

        if (!empty($model_teachers)) {

            foreach ($model_teachers as $teacher) {
                if ($formath == "fulllist") {
                    $teacher_list[$teacher->user_id] = $teacher->course_id . " " . $teacher->role;
                } else {
                    $teacher_list[$teacher->user_id] = $teacher->user_id;
                }
            }
        }

        return $teacher_list;
    }


    #get user not in course to add

    public static function getPersonNotInCourse($course_id){
          $array_data=array();

          //find user from user's email
          $criteria_user_email = new CDbCriteria;
          $criteria_user_email->condition = "id NOT IN (SELECT user_id FROM user_course WHERE course_id='$course_id')";
          $model_users=  User::model()->findAll($criteria_user_email);

          //find user from user's profile
          $criteria_user_name=new CDbCriteria;
          $criteria_user_name->condition = "t.user_id NOT IN (SELECT user_id FROM user_course WHERE course_id='$course_id')";
          $model_profiles=  Profile::model()->findAll($criteria_user_name);

          if(!empty($model_profiles)){
              foreach($model_profiles as $profile){
                  if(!empty($profile->firstname)){
                    $array_data[]=$profile->firstname." ".$profile->lastname;
                  }
              }
          }

           if(!empty($model_users)){
              foreach($model_users as $user){
                  $array_data[]=$user->email;
              }
          }

          return $array_data;
    }

           public static function getPersonInCourse($course_id,$role=""){
          $array_data=array();

          //find user from user's email
          $criteria_user_email = new CDbCriteria;
          $criteria_user_email->condition = "id IN (SELECT user_id FROM user_course WHERE course_id='$course_id')";
          if(!empty($role)){
             $criteria_user_email->condition = "id IN (SELECT user_id FROM user_course WHERE course_id='$course_id' AND role='$role')";
          }
          $model_users=  User::model()->findAll($criteria_user_email);

          //find user from user's profile
          $criteria_user_name=new CDbCriteria;
          $criteria_user_name->condition = "t.user_id IN (SELECT user_id FROM user_course WHERE course_id='$course_id')";
          if(!empty($role)){
             $criteria_user_name->condition = "t.user_id IN (SELECT user_id FROM user_course WHERE course_id='$course_id' AND role='$role')";
          }
          $model_profiles=  Profile::model()->findAll($criteria_user_name);

          if(!empty($model_profiles)){
              foreach($model_profiles as $profile){
                  if(!empty($profile->firstname)){
                    $array_data[]=$profile->firstname." ".$profile->lastname;
                  }
              }
          }

           if(!empty($model_users)){
              foreach($model_users as $user){
                  $array_data[]=$user->email;
              }
          }

          return $array_data;
    }




    public static function getImgFileType($type){
        $icon="general.gif";
       if($type=="doc"||$type=="docx"){
           $icon="icon_word.png";
       }

       if($type=="xls"||$type=="xlsx"){
           $icon="icon_excel.png";
       }

       if($type=="pdf"){
           $icon="icon_pdf.png";
       }


       if($type=="ppt"||$type=="pptx"){
            $icon="icon_ppt.png";
       }

       if ($type == "webm" || $type == "mp4" || $type == "3gpp" ||  $type == "mov" || $type == "avi" || $type == "wmv" || $type == "flv" || $type == "mpeg"){
           $icon="media.png";
       }

       $html="<img src='images/file_icon/$icon'>";
       return $html;


     }

     public static function getFacbook(){
          return $facebook = new Facebook(array(
            'appId' => '407397162716127',
            'secret' => '7e84a67fa39d10df7ff6be4755ac3baf',
            'cookie' => true
          ));
          $facebook->CURL_OPTS['CURLOPT_CONNECTTIMEOUT'] = 100;
     }

     public static function getStatHasStudy($user_id,$course_id){
         $model_stat=  StatUserStudy::model()->find("course_id='$course_id' AND user_id ='$user_id'");
         if(empty($model_stat)){
             return 0;
         }
         return $model_stat->content_has_studied;
     }

     public static function convertDatedb($date){
         list($day,$month,$year)=  explode("/", $date);
         return $year."-".$month."-".$day;
     }


     public static function convertDatetimedb($datetime){
         list($date,$time)= explode(" ", $datetime);
         list($day,$month,$year)=  explode("/", $date);
         return $year."-".$month."-".$day." ".$time;
     }

//     public static function convertDatetStart($datetime){
//         list($date,$time)= explode(" ", $datetime);
//         list($day,$month,$year)=  explode("/", $date);
//         return $year."-".$month."-".$day." ".$time;
//     }



    public static function checkRating($was_check,$value){
         if($was_check==$value){
             echo "checked='checked'";
         }
     }


      public static function roundRate($number = "") {
         if (!empty($number)) {
            if ((int)$number== $number) {
                //decimal
                return $number;
            } else {
                $number = round($number, 2);
                list($int, $decimal) = explode(".", $number);
                $check=$int+0.5;
                if ($number > $check) {
                    $number = $int + 1;
                } else {
                    $number = $int+".5";
                }
                return $number;
            }
        }
    }

     public static function convertTimeMinute($time){

         $time_format=  explode(".", $time);

         if(count($time_format)==2){
             list($time,$option)=explode(".", $time);
         }

         $parsed = date_parse($time);
         $seconds = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];

         $minute = $seconds/60;
         return $minute;
     }

     public static function CheckReference(){
         if(!empty($_SERVER['HTTP_REFERER'])){
         $site= $_SERVER['HTTP_REFERER'];
         if(!preg_match('/\bcoursecreek.com\b/', $site)) {
             $model_ref= ReferenceSiteBase::model()->find("website='$site'");
             $today=date("Y-m-d H:i:s");

             if(!empty($model_ref)){
                 $time = $model_ref->times+1;
             }else{
                 $model_ref= new ReferenceSiteBase();
                 $time=1;
                 $model_ref->create_date=$today;
             }

             $model_ref->website=$site;
             $model_ref->times = $time;
             $model_ref->lasted_date=$today;
             $model_ref->save(false);

         }
         }
     }

     public static function CheckavailableCourseTime($model_course){
         $today_time = date("Y-m-d H:i:s");
         $user_id=Yii::app()->user->id;
         $is_available = TRUE;


         if ($model_course->course_option == 1) {  //ตามวันเปิดปิดคอร์ส
            if (!empty($model_course->date_start)) {
                if ($today_time < $model_course->date_start) {
                    $is_available = $is_available && FALSE;
                }
            }

            if (!empty($model_course->date_end)) {
                if ($today_time > $model_course->date_end) {
                    $is_available = $is_available && FALSE;
                }
            }
        }else{ //ตามระยะเวลาที่กำหนด
            $model_usercourse = UserCourse::model()->find("user_id='$user_id' AND course_id='$model_course->id'");
            if($model_usercourse->role ==3){
                $model_usercourse->is_active = 0;
                $model_usercourse->save(false);
                $model_payment = CoursePaymentBase::model()->find("user_id='$user_id' AND course_id='$model_course->id' AND is_active='1'");
                if(!empty($model_payment)){
                    $model_payment->is_active= 0;
                    $model_payment->save(false);
                }


                if(!empty($model_usercourse->expire_date))
                if($today_time > $model_usercourse->expire_date){
                     $is_available = $is_available && FALSE;
                }
            }
         }
         return  $is_available;
     }

     public static function getWeekFromDate($date, $weekStartSunday = false){

       $timestamp = strtotime($date);

            // Week starts on Sunday
            if($weekStartSunday){
                $start = (date("D", $timestamp) == 'Sun') ? date('Y-m-d', $timestamp) : date('Y-m-d', strtotime('Last Sunday', $timestamp));
                $end = (date("D", $timestamp) == 'Sat') ? date('Y-m-d', $timestamp) : date('Y-m-d', strtotime('Next Saturday', $timestamp));
            } else { // Week starts on Monday
                $start = (date("D", $timestamp) == 'Mon') ? date('Y-m-d', $timestamp) : date('Y-m-d', strtotime('Last Monday', $timestamp));
                $end = (date("D", $timestamp) == 'Sun') ? date('Y-m-d', $timestamp) : date('Y-m-d', strtotime('Next Sunday', $timestamp));
            }

            return array('start' => $start, 'end' => $end);
        }

        public static function getMonthFromDate($date){

            list($year,$month,$day) = explode("-", $date);
            $start = $year."-".$month."-"."01";

            $end = date("Y-m-t", strtotime($date));


             return array('start' => $start, 'end' => $end);


        }
}
?>
