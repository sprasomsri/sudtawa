//initial code
class Singleton {
    private static Singleton instance=null;
    
    private Singleton() {
    }
    
    // Add code so that getInstance will return new Singleton object if the class has never been
    // instantiated before. Return previously instantiated object otherwise
    public static Singleton getInstance() {
        
        // An instance has never been instantiated before
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}

public class TestSingletonClass {
    public static void main (String[] args) {
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();
        System.out.println((s1 == s2) && (s1 != null) && (s2 != null));
    }
}