//initial code
public class PrintDaysInMontha {
    public static int getDays(int month) {
        int daysInMonth=0;
        
        // Add code to assigned dayInMonth to appropriate value according to month variable
        switch(month) {
            case 1:
            case 3: 
            case 5:
            case 7: 
            case 8:
            case 10:
            case 12:
                daysInMonth = 31;
                break;
                
            case 2:
                daysInMonth = 28;
                break;
                
            case 4:
            case 6:
            case 9:
            case 11:
                daysInMonth = 30;
                break;
       }
        return daysInMonth;
    }
    
    public static void main (String[] args) {
        String[] month={"January", "Febuary", "March", "April", "May", "June", "July", "August",
        "September", "October", "November", "December"};
        
         for (int i=0;i < 12;i++) {
             System.out.println(month[i]+" has "+getDays(i+1)+" days");
         }
    }
}
