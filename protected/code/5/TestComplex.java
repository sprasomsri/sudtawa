//initial code
class Complex {
    public double re;
    public double im;
    
    // Add code to return re + (im)i
    public String toString(){
        return this.re + " + " + "(" + this.im + ")i";
    }
}

// Create new class inherit from Complex class and add double getMagnitude method
class ComplexChild extends Complex {
    // Add constructor ComplexChild(double, double) which init re, im values
    ComplexChild(double re, double im) {
        this.re = re;
        this.im = im;
    }
    
    // Add double getMagnitude method which calculate magnitude of this complex number
    public double getMagnitude() {
        return Math.sqrt(this.re*this.re + this.im*this.im);
    }
}

public class TestComplex {
    public static void main (String[] args) {
        ComplexChild c = new ComplexChild(-2.569, 3.13);
        
        System.out.println(c.toString());
        System.out.println("Magnitude of c = " + c.getMagnitude());
    }
}