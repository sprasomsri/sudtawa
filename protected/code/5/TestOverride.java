//initial code
class A {
    public String getClassName() {
        return "A";
    }
}

// Create class B inherit from class A and override method getClassName
class B extends A {
    public String getClassName() {
        return "B";
    }
}

public class TestOverride {
    public static void main (String[] args) {
        A a = new A();
        A b = new B();
        System.out.println("Class of a is " + a.getClassName());
        System.out.println("Class of b is " + b.getClassName());
    }
}