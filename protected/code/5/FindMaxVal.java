//initial code
public class FindMaxVal {
    
    // Add method that return max value between two variables
    public static int max(int a, int b) {
        if (a > b) {
            return a;
        }
        else {
            return b;
        }
    }
    
    // Add method that return max value between three variables
    public static int max(int a, int b, int c) {
         if (a > b) {
            if (a > c) {
                return a;
            }
            else {
                return c;
            }
        }
        else {
             if (b > c) {
                return b;
            }
            else {
                return c;
            }
        }       
    }
    
    public static void main (String[] args) {
        System.out.println(max(100, -100));
        System.out.println(max(0, 1000));
        System.out.println(max(-5, -4));
    }
}