//initial code
public class CircleArea {
    public static void main(String[] args) {
        double radius = 2.0;
        double pi = 3.14;
        
        // Write an instruction to calculate circle area here
        double area = pi*radius*radius;
        
        // Writ an instruction to print out the calculated area to console
        System.out.println(area);
    }
}