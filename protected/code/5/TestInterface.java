//initial code
// Create SportInfo interface
interface SportInfo {
    public int getNumberOfPlayersPerTeam();
}

// Create FootballInfo class by implementing SportInfo interface
class FootballInfo {
    // Implement int getNumberOfPlayersPerTeam
    public int getNumberOfPlayersPerTeam() {
        return 11;
    }
    
    // Add method String getSportName
    public String getSportName() {
        return "Football";
    }
}

public class TestInterface {
    public static void main (String[] args) {
        FootballInfo footballInfo = new FootballInfo();
        System.out.println("Sport Name = " + footballInfo.getSportName());
        System.out.println("Players per team  =" + footballInfo.getNumberOfPlayersPerTeam());
    }
}