//initial code
public class Factorial {
    public static int f(int n) {
        int i=1;
        int factorial=1;
        do {
            // Add code to calculate factorial of n
            factorial = i*factorial;
            i++;
        } while (i <= n);
        return factorial;
    }
    
    public static void main (String[] args) {
        for (int i = 0;i < 10;i++) {
            System.out.print(f(i)+" ");
        }
    }
}