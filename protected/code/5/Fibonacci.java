//initial code
public class Fibonacci {
    public static int f(int n) {
        // Init f(0)=0, f(1)=1
        if (n == 0) {
            return 0;
        }
        else if (n == 1) {
            return 1;
        }
        else {
            int i=2;
            int fn=0, fn_1=1, fn_2=0;
            while (i <= n) {
                // Add code to calculate f(n) when n >= 2
                fn = fn_1 + fn_2;
                fn_2 = fn_1;
                fn_1 = fn;
                
                i++;
            }
            return fn;
        }
    }
    
    public static void main (String[] args) {
        for (int i=0;i < 20;i++) {
            System.out.print(f(i)+" ");
        }
    }
}

