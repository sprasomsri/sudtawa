//initial code
abstract class Shape {
    public String color;
    public abstract double area();
}

class Circle extends Shape {
    private double pi = 3.14;
    public double radius = 0.0;
    public Circle(double radius){
        this.radius = radius;
    }
    public double area(){
        return pi*radius*radius;
    }
}

class Square extends Shape {
    double length = 0.0;
    public Square(double length){
        this.length = length;
    }
    public double area(){
        return length*length;
    }
}

public class TestAbstract {
    public static void main (String[] args) {
        Shape c = new Circle(3.7);
        c.color = "red";
        
        Shape sq = new Square(5.5);
        sq.color = "green";
        
        System.out.println("c is "+c.color+" and area="+c.area());
        System.out.println("sq is "+sq.color+" and area="+sq.area());
    }
}