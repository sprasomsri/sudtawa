//initial code
public class PowerMofN {
    
    // Add method pow(int n, int m) which calculate power m of n
    public static int pow(int n, int m) {
        int result = 1;
        for (int i=0;i < m;i++) {
            result = result*n;
        }
        return result;
    }
    
    public static void main (String[] args) {
        System.out.println(pow(5, 0));
        System.out.println(pow(2, 10));
        System.out.println(pow(7, 5));
        System.out.println(pow(9, 3));
    }
}