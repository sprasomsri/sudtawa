//initial code

public class PrimeNumber {

    public static void printPrime(int n) {
 		boolean isPrime=true;
		for(int i = 2;i < n;i++){
			isPrime = true;
			for (int j = 2;j < i;j++){
				int result = i%j;
				if (result == 0 && i!=j){
					isPrime=false;
				}
			}
			if (isPrime){
				System.out.print(i+" ");
			}
		}       
    }
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		printPrime(100);
	}
}