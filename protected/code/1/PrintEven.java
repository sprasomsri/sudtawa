//initial code
public class PrintEven {
    public static void print(int inputNumber) {
        // Add code to check and print inputNumber if inputNumber is an even number
        // Don't forget to add newline after print inputNumber
        
        if (inputNumber % 2 == 0) {
            System.out.println(inputNumber);
        }
    }    
    
    public static void main (String[] args) {
        PrintEven.print(5);
        PrintEven.print(2);
        PrintEven.print(10);
        PrintEven.print(7);
        PrintEven.print(120);
    }
}