  <style type="text/css" title="currentStyle">
          @import "js/datatable/css/demo_page.css";
          @import "js/datatable/css/demo_table.css";
      </style>
      <script type="text/javascript" language="javascript" src="js/datatable/js/jquery.js"></script>
      <script type="text/javascript" language="javascript" src="js/datatable/js/jquery.dataTables.js"></script>
      <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {

              $('#project-list').dataTable({
//                  "bServerSide": true,
//                   "bJQueryUI": true,
                   "aaSorting": [],
                   "sDom": '<"H"lfrp>t<"F"ip>',
                  "sPaginationType": "full_numbers",
                  "iDisplayLength": 25,
                  "aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]]
              });
          });
      </script>

  
      
      
<div class="w-hidden-small w-hidden-tiny hero-unit-section">
    <div class="w-container">
        <div class="w-row">


            <div class="w-col w-col-12">
                <h1> ยืนยันการจ่ายเงินทาง ATM</h1>
                <hr/>
                
                       
            <table id="project-list" width="100%" style="">
            <thead>
                <tr>
                    <th class="text-center">  </th>
             
                    <th class="text-center"> ชื่อ - สกุล</th>
                    <th class="text-center"> คอร์สที่สมัครเรียน </th>
                    <th class="text-center"> เลขใบเสร็จ </th>
                    <th class="text-center"> File </th>               
                    <th class="text-center">จัดการ</th>
                 
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($model_atm_all)){ 
                    $count_row =1;
                    foreach ($model_atm_all as $atm){
                ?>
                    <tr id="slip-<?php echo $atm->course->id.$atm->user->id; ?>" >
                        <td> <?php echo $count_row; ?> </td>
                        <td> <?php echo $atm->profile->firstname." ". $atm->profile->lastname; ?> </td>
                        <td> <?php echo $atm->course->name; ?> </td>
                        <td> <?php echo $atm->ref_no; ?> </td>
                        <td> 
                            <a target="_blank" href="atmSlip/<?php echo $atm->evidence_file; ?>" class="theme-select fancybox.ajax slip-file"> Slip </a>
                        </td>
                        <td class="text-center">
                            <?php if($atm->is_approve!=1){ ?>
                            <a class="accept-slip" onclick="acceptAtmEvidence(<?php echo $atm->course->id; ?>,<?php echo $atm->user->id; ?>)" href="#">ยอมรับ </a>
                             |
                            <a class="accept-slip" onclick="cancelAtmEvidence(<?php echo $atm->course->id; ?>,<?php echo $atm->user->id; ?>)" href="#">ปฏิเสธ </a>
                            <?php }else { echo "ยอมรับแล้ว"; } ?>
                        </td>                         
                        
                    </tr>
                <?php 
                    $count_row++;                
                    }
                    
                } 
                    ?>
            </tbody>
            </table>  
            </div> 
        </div>
    </div>
</div>
      
      

<script type="text/javascript">

$(document).ready(function() {
    $(".accept-slip").on("click",function(event){
         event.preventDefault();
      
    });
 
 
    $(".slip-file").fancybox({
          helpers: {
              title : {
                  type : 'float'
              }
          }
      });
  
});


 function acceptAtmEvidence(course,user){
     
        $.ajax({
                  url: '<?php echo Yii::app()->createUrl("payment/acceptAtmSlip"); ?>',
                  type: 'POST',
                  data:{course:course,user:user},
                  dataType: 'html',                        
                  success: function(result) {          
                       if(result==1){                          
                           $("#slip-"+course+user).hide();
                       }
                   }
                });  
   }

   function cancelAtmEvidence(course,user) {
        var msg = confirm('Are you sure?');
        if (msg == false) {
            return false;
        }else{
            $.ajax({
                  url: '<?php echo Yii::app()->createUrl("payment/canceltAtmSlip"); ?>',
                  type: 'POST',
                  data:{course:course,user:user},
                  dataType: 'html',                        
                  success: function(result) {          
                       if(result==1){                          
                           $("#slip-"+course+user).hide();
                       }
                   }
                });  
        }
    }
  
 </script>
