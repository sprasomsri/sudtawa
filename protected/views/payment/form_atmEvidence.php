<div class="gray-row">
  <div class="container-full">
        <div class="w-hidden-small w-hidden-tiny hero-unit-section form-box">
            <div class="w-container">
                <div class="w-row">


                    <div class="w-col w-col-12">
                        <h1> ยืนยันการจ่ายเงินทาง ATM</h1>
                        <hr/>

                      <?php
                       $form = $this->beginWidget('CActiveForm', array(
                           'id' => 'atm-form',
                           'htmlOptions' => array('enctype' => 'multipart/form-data',"role"=>"form"),
                           'clientOptions' => array(
                               'validateOnSubmit' => true,
                           ),
                       ));
                       ?>


                         <?php echo $form->errorSummary($model_atm); ?>
                    
                    <?php if(Yii::app()->user->hasFlash('saved_atm')):?>
                    <div class="bg-success">
                        <?php echo Yii::app()->user->getFlash('saved_atm'); ?>
                    </div>
                    <?php endif; ?> 
                        
                       
                       <div class="form-group">
                                    <label>Course</label>             
                                    <?php
                                    if(!empty($array_buy)){
                                        echo $form->dropDownList($model_atm, 'course_id',$array_buy,array("class"=>"form-control")); 
                                    }else{
                                        echo "<span class='red-text'> ไม่มีรายการ คอร์สที่ คุณได้ซื้อทาง ATM ไว้    </span>" ;
                                    }

                                    ?>
                         </div>  

                        <div class="form-group">
                                    <label>จำนวรนเงิน</label>             
                                    <?php echo $form->textField($model_atm, 'qty_money',array("class"=>"form-control")); ?>
                         </div>  


                        <div class="form-group">

                           <label>Tranfer Date</label>   
                            <?php
                                                    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                                                    $this->widget('CJuiDateTimePicker', array(
                                                    'name' => "tranfer_date",                                          
                                                    'mode' => 'date', //use "time","date" or "datetime" (default)
                                                    'htmlOptions'=>array("class"=>"form-control",),    
                                                    'options' => array(
                                                        //'dateFormat'=>'yy-mm-dd',                                                
                                                    ), // jquery plugin options
                                                    ));
                                  ?>

                        </div>



                        <br/>

                        <h3> ข้อมูล บัญชีของคุณ</h3>
                        <div class="form-group">
                                    <label>จากธนาคาร</label>       
                                      <?php echo $form->textField($model_atm, 'from_bank',array("class"=>"form-control")); ?>

                         </div> 

                        <div class="form-group">
                                    <label>จากบัญชี</label>  
                                    <?php echo $form->textField($model_atm, 'from_account',array("class"=>"form-control")); ?>
                        </div>          

                       <div class="form-group">
                                    <label>ชื่อบัญชี</label>  
                                    <?php echo $form->textField($model_atm, 'account_name',array("class"=>"form-control")); ?>
                        </div>          



                        <div class="form-group">
                                    <label>Slip</label>                            
                                    <?php echo CHtml::activeFileField($model_atm, 'evidence_file',array("class"=>"input-block-level")) ?>
                        </div>


                         <button type="submit" class="btn btn-warning"> บันทึก</button>


                        <?php $this->endWidget(); ?>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>

