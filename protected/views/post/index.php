<?php if(empty($user_id)){ ?>
<div id="precontent">
	<div class="container-fluid" id="slider">
		<a class="carousel-control left visible-phone" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                <a class="carousel-control right visible-phone" href="#myCarousel" data-slide="next">&rsaquo;</a>
                    <div class="container shadow-bottom">
                        <div class="row-fluid shadow-top">

                            <div class="span8">
                                <a class="carousel-control left hidden-phone" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                                <a class="carousel-control right hidden-phone" href="#myCarousel" data-slide="next">&rsaquo;</a>
                                <div class="bgslider">
                                    <img class="bgslider-img" src="images/slider.png"/>
                                    <div id="myCarousel" class="carousel slide">
                                        <!-- Carousel items -->
                                        <div class="carousel-inner">
                                            <div class="active item"><img src="images/slider-1.png"></div>
                                            <div class="item"><img src="images/slider-2.png"></div>
                                        </div>
                                        <ol class="carousel-indicators">
                                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                            <li data-target="#myCarousel" data-slide-to="1"></li>
                                            <li data-target="#myCarousel" data-slide-to="2"></li>
                                        </ol>
                                    </div><!-- myCarousel-->
                                </div> <!-- span7 -->
                            </div>
                                 <?php $this->widget('UserLoginBox', array()); ?>                                
                               
			</div> <!-- row-fluid -->	
		</div> <!-- container -->
	</div><!-- container-fluid-->
</div><!--precontent-->
<?php } ?> 

<div id="maincontent">
	<div class="container-fluid">
		<div class="container">
			<div class="row-fluid line-lightgrey-wrapper">
				<div class="heading-wrapper">
					<div class="one hidden-phone"><?php echo Yii::t("site","Let 's study")?></div> <br />
					<div class="two hidden-phone"><?php echo Yii::t("site","Chosse Appropriate course and start now")?></div>
<!--					<div class="one visible-phone">เริ่มเรียนกันเลย</div> 
					<div class="two visible-phone">เลือกคอร์สที่เหมาะสมกับคุณ แล้วเริ่มต้นเรียนได้ทันที</div>-->
				</div> <!-- /heading wrapper -->
			</div> <!-- /row-fluid -->
                        
                        
                        
                        
                        <!--- Course Box --> 
                       <?php $count_box=0;?>
                       <?php  if(!empty($model_courses)){ ?>
                       <?php foreach($model_courses as $course){
                           if ($count_box % 3 == 0 || $count_box == 0) {  
                               $state = "new_div";?>
                               <div class="row-fluid row-course"> 
                           <?php }else{
                               $state="not_new_div";
                           } ?>
                                    
                           <?php $this->widget('CourseBox', array("data" => $course)); ?>     
                                    
                       <?php if ($state == "not_new_div" && (($count_box + 1) % 3 == 0)) { ?>
                                </div>
                          
                       <?php }$count_box++;}} ?>
                       <?php if($count_box % 3 !=0){
                           echo "</div>"; //last number cannot % by3 close div
                  
                       }?>
                                                                          
			<div class="row-fluid line-lightgrey-wrapper">
				<div class="heading-wrapper">
					<div class="btn-center-wrapper">
						<a class="btn btn-green btn-big" href="<?php echo Yii::app()->createUrl('/course/default/courseAll'); ?>">
                                                    <?php echo Yii::t("site","View All Course")?>
                                                </a>
					</div>
				</div> <!-- /heading wrapper -->
			</div>
		</div>
	</div> <!-- container-fluid -->
</div> <!-- content -->
