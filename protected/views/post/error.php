

<div id="maincontent">
    <div class="container-fluid">
        <div class="container">
            <div class="img-error">
                <div class="img-error-links">
                    <a href="<?php echo Yii::app()->createUrl("/site/index"); ?>">กลับไปหน้าแรก</a>
                    <span> | </span>
                    <a href="<?php echo Yii::app()->createUrl("/course/default/courseAll"); ?>">ค้นหาคอร์ส</a>
                </div>
                <?php 

                    if(!empty($message)){
                         echo CHtml::encode($message); 
                    }
                   


                ?>
                <img src="images/error404.png"/>
            </div>
        </div>
    </div> <!-- container-fluid -->
</div> <!-- maincontent -->