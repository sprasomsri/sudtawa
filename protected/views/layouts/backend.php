<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>CourseCreek</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
  <link rel="stylesheet" type="text/css" href="css/normalize.css">
  <link rel="stylesheet" type="text/css" href="css/webflow.css">
  <link rel="stylesheet" type="text/css" href="css/coursecreek.webflow.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="cssv1/content-style.css">
  <link rel="stylesheet" type="text/css" href="cssv1/combined.css"> 

  <script src="js/jquery-1.9.0.js" type="text/javascript"></script> 
  <link rel="stylesheet" href="plugin/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" /> 
  
  
  
  <!--- bootstrapt dropdown --->
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

  <script type="text/javascript" src="plugin/fancybox/source/jquery.fancybox.pack.js"></script>
  <script src="plugin/jquery.formplugin.js" type="text/javascript"></script>
  <link rel="icon" href="images/coursecreek.ico" type="image/x-icon" />  
  
 
  
  
  
  <script src="plugin/ace/ace.js" type="text/javascript" charset="utf-8"></script>
  
  <script>
    if (/mobile/i.test(navigator.userAgent)) document.documentElement.className += ' w-mobile';
    
  </script>
  
  
</head>

       
	<body class="front"> 
          
            <?php $this->renderPartial('//layouts/_header'); ?>   
            <?php echo $content; ?>
            <?php $this->renderPartial('//layouts/_footer'); ?>   
	
            

        
	</body>
</html>