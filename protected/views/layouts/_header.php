<header class="navbar">
    <div>
        <div class="w-row">
            <div class="w-col w-col-6">
                <div class="logo-web">
                    <a href="<?php echo Yii::app()->createUrl("site/index"); ?>">
                        <img src="images/VET-CU-logo.png" alt="54000400ca44dcce7cb6d15a_VET-CU-logo.png">
                    </a>
                </div>
            </div>
            <div class="w-col w-col-6 header-right-colum">
                <!--<a class="header-link" href="#">All-Course</a>-->
                <?php
                $user_id = Yii::app()->user->id;

                if (!empty($user_id)) {
                    $is_owner = Yii::app()->user->getState('owner');
                    $is_super = Yii::app()->user->getState('superadmin');
                    ?>


                    <a class="w-nav-link menu dropdown-toggle nav-link header-link" data-toggle="dropdown"href="#">  
                        <?php echo Yii::app()->user->getState('username'); ?> 
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="btnGroupVerticalDrop2">
                        <li role="presentation"><a role="menuitem" href="<?php echo Yii::app()->createUrl("user/profile/edit"); ?>"><?php echo Yii::t('menu', 'Profile'); ?></a></li>

                        <?php if ($is_owner == 1) { ?>
                            <li role="presentation">
                                <a role="menuitem" href="<?php echo Yii::app()->createUrl("user/userFile/picture") ?>"><?php echo Yii::t('menu', 'File Manager'); ?></a>
                            </li>
                        <?php } ?>

                        <?php /*if ($is_super == 1) { ?>
                            <li role="presentation">
                                <a role="menuitem" href="<?php echo Yii::app()->createUrl("admin/affiliate/index") ?>">Admin Affiliate Manage</a>
                            </li>
                        <?php } else { ?>    
                            <li role="presentation"><a role="menuitem" href="<?php echo Yii::app()->createUrl("course/affiliate/index"); ?>">Affiliate</a></li>
                        <?php } */?>

                        <li role="presentation"><a role="menuitem" href="<?php echo Yii::app()->createUrl("user/dashboard/index"); ?>"><?php echo Yii::t('menu', 'Dashboard'); ?></a></li>
                       


                        <?php
                        $is_super_admin = Yii::app()->user->getState("superadmin");
                        if (!empty($is_super_admin)) {
                            ?>
                            <!--<li role="presentation"><a role="menuitem" href="<?php echo Yii::app()->createUrl("admin/permission"); ?>">จัดการสิทธิ์ผู้ใช้</a></li>-->
                            <li role="presentation"><a role="menuitem" href="<?php echo Yii::app()->createUrl("payment/acceptAtmEvidenceList"); ?>"><?php echo Yii::t('menu', 'Approve ATM slip'); ?></a></li>
                              <li role="presentation"><a role="menuitem" href="<?php echo Yii::app()->createUrl("owner/default/ApproveTeacherRequest"); ?>"><?php echo Yii::t('menu', 'Approve teacher'); ?></a></li>
                            <li role="presentation"><a role="menuitem" href="<?php echo Yii::app()->createUrl("admin/permission/stdAccount"); ?>"><?php echo Yii::t('menu', 'Create student account');?></a></li>
                       <?php }else{ ?>
                            <li role="presentation"><a role="menuitem" href="<?php echo Yii::app()->createUrl("payment/atmEvidence"); ?>"><?php echo Yii::t('menu', 'Upload ATM slip'); ?></a></li>
                       <?php } ?>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem" href="<?php echo Yii::app()->createUrl("user/logout") ?>"><?php echo Yii::t('menu', 'Logout'); ?></a></li>

                    </ul>



            <?php } ?>
            </div>

        </div>
    </div>
</header>

