<!DOCTYPE html>
<!-- This site was created in Webflow. http://www.webflow.com-->
<!-- Last Published: Mon Mar 17 2014 06:50:21 GMT+0000 (UTC) -->
<html data-wf-site="53154e4558335e9d6f00009b">
<head>
  <meta charset="utf-8">
  <title>CourseCreek</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
  <link rel="stylesheet" type="text/css" href="css/normalize.css">
  <link rel="stylesheet" type="text/css" href="css/webflow.css">
  <link rel="stylesheet" type="text/css" href="css/coursecreek.webflow.css">
  
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" href="plugin/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />  
  <script src="js/jquery-1.9.0.js" type="text/javascript"></script>    
  <script src="js/bootstrap.js" type="text/javascript"></script>    

  <script type="text/javascript" src="plugin/fancybox/source/jquery.fancybox.pack.js"></script>
  <script src="plugin/jquery.formplugin.js" type="text/javascript"></script>
  <link rel="icon" href="images/coursecreek.ico" type="image/x-icon" />  
  
  <script>
    if (/mobile/i.test(navigator.userAgent)) document.documentElement.className += ' w-mobile';
  </script>
  

  <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script><![endif]-->
</head>

<body>
 
    
 <?php echo $content; ?> 


</body>
</html>