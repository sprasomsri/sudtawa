<div class="w-hidden-small w-hidden-tiny footer">
    <div>
        <div>
            <div>
                <div class="w-row">
                    <div class="w-col w-col-2"></div>
                    <div class="w-col w-col-8 logo-footer logo-footer-first">
                        <div class="w-row">
                            <div class="w-col w-col-3">
                                <a href="http://www.chula.ac.th" target="_blank">
                                    <img src="images/linkwebchula.jpg" alt="540008b7ca44dcce7cb6d183_linkwebchula.jpg">
                                </a>
                            </div>
                            <div class="w-col w-col-3">
                                <img src="images/logo.png" alt="54000a634e9e0dcc7c8be64d_logo.png">
                            </div>
                            <div class="w-col w-col-3">
                                <a href="http://161.200.35.61/" target="_blank">
                                    <img src="images/Lib-3.jpg" alt="54000913ca44dcce7cb6d186_Lib-3.jpg">
                                </a>
                            </div>
                            <div class="w-col w-col-3">
                                <a href="http://www.chulapedia.chula.ac.th/" target="_blank">
                                    <img src="images/pedia-banner.jpg" alt="54000c5c4e9e0dcc7c8be65f_pedia-banner.jpg">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="w-col w-col-2"></div>
                </div>
                <div class="w-row">
                    <div class="w-col w-col-2"></div>
                    <div class="w-col w-col-8 logo-footer logo-footer-last">
                        <div class="w-row">
                            <div class="w-col w-col-3">
                                <a href="http://www.scholarbank.chula.ac.th/" target="_blank">
                                    <img src="images/schola-banner.jpg" alt="54000e46a584c663122b8144_schola-banner.jpg">
                                </a>
                            </div>
                            <div class="w-col w-col-3">
                                <a href="http://www.vet.chula.ac.th/vet2014/dept/sah" target="_blank">
                                    <img src="images/sah-1.jpg" alt="540008e15aa47f6512b66e9b_sah-1.jpg">
                                </a>
                            </div>
                            <div class="w-col w-col-3">
                                <a href="http://www.vet.chula.ac.th/vet2014/Ebook/ebook.html" target="_blank">
                                    <img src="images/e-book-3.jpg" alt="54000e644e9e0dcc7c8be67b_e-book-3.jpg">
                                </a>
                            </div>
                            <div class="w-col w-col-3">
                                <a href="http://cuir.car.chula.ac.th/" target="_blank">
                                    <img src="images/curesearch-main-banner.gif" alt="54000e6ea584c663122b8146_curesearch-main-banner.gif">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="w-col w-col-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer-icon">
    <div>
        <div class="w-row">
            <div class="w-col w-col-8">
                <div class="address"><?php echo Yii::t('site', 'Faculty of Veterinary Science, Chulalongkorn University, Henri-Dunant Rd., Pathumwan, Bangkok 10330, Thailand. Tel.+66(0) 2218 9771,3 Fax.+66(0) 2255 3910'); ?></div>
            </div>
            <div class="w-col w-col-4 all-icon">
                <a href="https://www.facebook.com/pages/Faculty-of-Veterinary-Science-Chulalongkorn-University/140381195974480" target="_blank">
                    <div class="logo-facebook icon-social"></div>
                </a>
                <a href="http://twitter.com/#!/CU_VET" target="_blank">
                    <div class="icon-social icon-twitter"></div>
                </a>

                <div class="icon-social icon-youtube"></div>
            </div>
        </div>
    </div>
</div>
