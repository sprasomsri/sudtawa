<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                <link rel="stylesheet" type="text/css" href="cssv1/bootstrap.css">
                <link rel="stylesheet" type="text/css" href="cssv1/bootstrap-responsive.css">
                <link rel="stylesheet" type="text/css" href="cssv1/bootstrap-responsive.min.css">
                <link rel="stylesheet" type="text/css" href="cssv1/combined.css">
                <!--<link rel="stylesheet" type="text/css" href="cssv1/content-style.css">-->
                <!--<link rel="stylesheet" type="text/css" href="cssv1/header-style.css">-->
                <!--<link rel="stylesheet" type="text/css" href="cssv1/video-style.css">-->
                <link rel="stylesheet" type="text/css" href="cssv1/style.css">
                <link rel="stylesheet" type="text/css" href="cssv1/temp.css">
                <link rel="stylesheet" href="plugin/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />
                           
               <script src="js/jquery-1.9.0.js" type="text/javascript"></script>    
               <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
               <script type="text/javascript" src="plugin/fancybox/source/jquery.fancybox.pack.js"></script>
               <script src="plugin/ace/ace.js" type="text/javascript" charset="utf-8"></script>
               <title> CourseCreek &vrtri; Course Online </title>
                
	</head>
                
	<body class="front"> 
          
		<?php $this->renderPartial('//layouts/_headerv1'); ?>
		<?php echo $content; ?>
		<?php $this->renderPartial('//layouts/_footerv1'); ?>
            
            
                <script src="js/bootstrap.js" type="text/javascript"></script>
                <script src="plugin/jquery.formplugin.js" type="text/javascript"></script>
                <script src="js/site.js" type="text/javascript"></script>
<!--                <script src="js/jwplayer.html5.js" type="text/javascript"></script>           -->
                <script src="http://jwpsrv.com/library/aoLspqDSEeKQuyIACpYGxA.js"></script>                
                <script type="text/javascript">
//                   var man = {
//                        hands: 2, 
//                        legs: 2, 
//                        heads: 1,
//
//                     };
//                     var i;
//                     for(i in man){
//                         alert(i);
//                     }
//             
             
             
                  
			$(document).ready(function(){
                 
			  $("#member").click(function(){
			    $("#member-login").css("display", "none");
			    $("#member-regis").css("display", "block");
			    
			  });
			  $("#login").click(function(){
			    $("#member-regis").css("display", "none");
			    $("#member-login").css("display", "block");
			  });                          
			});
                        
                      $("#login_button").click(function(){
                         $("#regis").submit();
                    });
		</script>
                
<!-- Piwik -->



<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u=(("https:" == document.location.protocol) ? "https" : "http") + "://localhost/piwik/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 1]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
    g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();

</script>
<noscript><p><img src="http://localhost/piwik/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->
                
	</body>
</html>