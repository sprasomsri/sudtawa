<!DOCTYPE html>
<!-- This site was created in Webflow. http://www.webflow.com-->
<!-- Last Published: Mon Mar 17 2014 06:50:21 GMT+0000 (UTC) -->
<html data-wf-site="53154e4558335e9d6f00009b">
<head>
  <meta charset="utf-8">
  <title>Chula</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
  
  <link rel="stylesheet" type="text/css" href="css/normalize.css">
  <link rel="stylesheet" type="text/css" href="css/webflow.css">
  <link rel="stylesheet" type="text/css" href="css/clickclass3.webflow.css">
  <!--<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>-->
  
  <link rel="stylesheet" type="text/css" href="css/style.css">
  
   <script>
    WebFont.load({
      google: {
        families: ["Montserrat:400,700","Bitter:400,700,400italic"]
      }
    });
  </script>
  
  <link rel="stylesheet" href="plugin/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />  
  <script src="js/jquery-1.9.0.js" type="text/javascript"></script>    
  <script src="js/bootstrap.js" type="text/javascript"></script>    

  <script type="text/javascript" src="plugin/fancybox/source/jquery.fancybox.pack.js"></script>
  <script src="plugin/jquery.formplugin.js" type="text/javascript"></script>
  <link rel="icon" href="images/coursecreek.ico" type="image/x-icon" />  
  
</head>

<body>
  <?php // $this->renderPartial('//layouts/_header'); ?>    
    
    
 <?php echo $content; ?> 
  
<?php $this->renderPartial('//layouts/_footer'); ?>   
  <!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
  <script type="text/javascript" src="js/webflow.js"></script>
  



</body>
</html>