<div class="w-nav header" data-collapse="small" data-animation="default" data-duration="400">
    <a class="w-nav-brand brand-position" href="<?php echo Yii::app()->createUrl("site/index"); ?>" title="home">    
            <img src="images/VET-CU-logo.png" width="150" alt="53155bd95cf6834766000144_logo.png">       
           
    </a>
  
    
    <nav class="w-nav-menu nav-sidebar" role="navigation">
        <!--<a class="w-nav-link teach-menu" href="#">เปิดคอร์สสอน</a>-->
        <a class="w-nav-link menu" href="<?php echo Yii::app()->createUrl("course/default/courseAll");?>">คอร์สทั้งหมด</a>
        <?php 
              $user_id=Yii::app()->user->id;
              if(empty($user_id)){
        ?>
        
        <a class="w-nav-link menu" href="<?php echo Yii::app()->createUrl("user/login/login"); ?>">เข้าสู่ระบบ</a>
        <a class="w-nav-link menu" href="<?php echo Yii::app()->createUrl("user/registration/registration");?>">สมัครสมาชิก</a>
        <?php }else{ ?>
           <a class="w-nav-link menu dropdown-toggle nav-link" data-toggle="dropdown" href="#"> 
                <?php echo Yii::app()->user->getState('username'); ?>
                <span class="caret"></span>
           </a>
        
            <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="btnGroupVerticalDrop2">
                    <li role="presentation" class="dropdown-header">ตั้งค่า</li>

                    <li role="presentation"><a role="menuitem" href="<?php echo Yii::app()->createUrl("user/profile/edit"); ?>">ข้อมูลผู้ใช้</a></li>

                    <?php $is_owner = Yii::app()->user->getState('owner'); 
                          $is_super = Yii::app()->user->getState('superadmin');                     
                    ?>

                    <?php if ($is_owner == 1) { ?>
                        <li role="presentation">
                            <a role="menuitem" href="<?php echo Yii::app()->createUrl("user/userFile/picture") ?>">File ManageMent</a>
                        </li>
                    <?php } ?>
                        
                     <?php if ($is_super == 1) { ?>
<!--                        <li role="presentation">
                            <a role="menuitem" href="<?php echo Yii::app()->createUrl("admin/affiliate/index") ?>">Admin Affiliate Manage</a>
                        </li>-->
                    <?php }else{ ?>    
                         <!--<li role="presentation"><a role="menuitem" href="<?php  echo Yii::app()->createUrl("course/affiliate/index"); ?>">Affiliate</a></li>-->
                    <?php }?>
                        
                    <li role="presentation"><a role="menuitem" href="<?php echo Yii::app()->createUrl("user/dashboard/index"); ?>">แดชบอร์ด</a></li>
                   
                    <li role="presentation" class="divider"></li>
                    <li role="presentation"><a role="menuitem" href="<?php echo Yii::app()->createUrl("user/logout") ?>">ออกจากระบบ</a></li>

              </ul>
       
        
        
        
        <?php } ?>
    </nav>
    <div class="w-nav-button">
        <div class="w-icon-nav-menu"></div>
    </div>
</div>

