<?php $rand=rand(1, 2); 
      $src="error".$rand.".png";
?>
<div class="section section-course w-container error-box">
   
<div class="w-row snippet-row text-center">
       <img src="images/error/<?php echo $src;?>" class="img-responsive error-pic"/>
       <div class="pull-right">
       <a class="btn btn-danger" href="javascript:history.back();">Go Back</a>
       <a class="btn btn-warning btn-lg" href="<?php echo Yii::app()->createUrl("site/index");?>" title="home"><b>CourseCreek</b></a>
       </div>
</div>
  
</div>

<script>
    $(document).ready(function(){
       document.title = "404 | CourseCreek"; 
    });
</script>