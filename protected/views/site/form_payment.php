  <div class="gray-row">
    <div class="container-full">
        <div id="payment-box">
            <h3>Payment Option</h3>
            <hr/>

            <?php 
            if (!empty($course->course_img)) {
                        $course_nav_media = "<img src='course/original/$course->course_img' class='img-thumbnail' width='100%' title='$course->name'>";
                    } else {
                        $course_nav_media = "<img src='images/default_course.png' width='90%' class='img-thumbnail'>";
                    }
                    //echo $course_nav_media;

            ?>

            <div class="row">
                  <div class="col-sm-5"> <?php echo $course_nav_media; ?> </div>
                   <div class="col-sm-7">  <h3> <?php echo $model_course->name; ?> </h3>
                   <label for="inputEmail3" class="col-sm-12 control-label detail-lesson"> รายละเอียด:</label>
                   <?php echo $model_course->description; ?>

                    <label for="inputEmail3" class="col-sm-12 control-label detail-lesson"> ราคา:</label>
                    <p> <?php echo $model_course->price;?> บาท</p>

                    <input type="hidden" class="form-control" id="coin" placeholder="Coin amount" value="<?php echo $model_course->price;?>">
                    <input type="hidden" id="money" value="<?php echo $model_course->price;?>">
                    <hr/>     

                     <label class="col-sm-12 control-label"> การชาระเงิน:</label>
                     <input type="radio" name="source" id="optionsRadios6" value="6" checked>  เคาน์เตอร์เซอร์วิส <br/>
                     <input type="radio" name="source" id="optionsRadios2" value="2"> บัตรเครดิต<br/>
                     <input type="radio" name="source" id="optionsRadios5" value="5"> ออนไลน์ แบงค์กิ้ง <br/>
                     <input type="radio" name="source" id="optionsRadios1" value="1"> บัญชี PAYSBUY  <br/>
                     <input type="radio" name="source" id="optionsRadios3" value="3"> PayPal <br/>
                     <input type="radio" name="source" id="optionsRadios100" value="100"> โอนผ่าน ATM <br/>
                     <br/>
                     <button type="button" id="submit-payment" class="btn btn-default"><span class="glyphicon-class glyphicon glyphicon-tags"></span>&nbsp;&nbsp; Buy</button>
                   </div>
            </div>


        </div>
     </div>
 </div>



<?php if (!empty($user_id)) { ?>
    <script type="text/javascript">        
         $(document).ready(function() {        
                                      
             $("#Add-money").on("click",function(){
                $("#payment-box").slideToggle();
    //                       'input[name="genderS"]:checked').val();
             });
                        
             $("#submit-payment").on("click",function(){
                 var payment = $('input[name="source"]:checked').val();
                 var coin = $("#coin").val();
                 var money = $("#money").val();                       
                 var link="index.php?r=payment/buyCoin&coin="+coin+"&method="+payment+"&course=<?php echo $course;?>";  
                 window.location=link;
                            
             });                    
                        
                                            
             
                                            
         });
                    
         function calmoney(){
              var coin = $("#coin").val();              
              if(Math.round(coin) != coin) {
                 $("#show-payment").html("Not a number");
                  $("#submit-payment").attr("disabled","disabled");
                         } else {
                             $("#submit-payment").removeAttr("disabled");
                             var rate = "<?php //echo $model_coin_setting_default->money_amount; ?>";
                             var price = coin * rate;
                             $("#show-payment").html("  Price is " + price + " bath");
                             $("#money").val(price);



                         }
                     }


    </script>
<?php } ?>