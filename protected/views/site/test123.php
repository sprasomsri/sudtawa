<div class="section">
    <div class="w-container">
        <h1 class="head-center">Center aligned text.</h1>

        <div class="w-row">
            <div class="w-col w-col-8 content-column">
                <h3>Leson Name</h3>
                
                
                <div class="image-example" style="background-color: graytext;"> VDO</div>
                <div class="content-block">
                  
                    <p>Section description. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus
                        nibh et justo cursus id rutrum lorem imperdiet. Nunc ut sem vitae risus tristique posuere.
                        
                    </p>
                </div>
            </div>
            <div class="w-col w-col-4">
                
                <div class="fixed-nav">
                    
                    <div class="study-img">
                        <img class="logo" src="images/default_course.png" width="268px" class="img-responsive" alt="51e1a871bceaf5b67b000052_logo-img.png">
                    </div>
                    
                    
                    <h4>Unit | Lesson</h4>
                    <a class="nav-link-study" href="#" target="_self">NAV Link 1</a>
                    <a class="nav-link-study" href="#" target="_self">NAV Link 2</a>
                    <a class="nav-link-study" href="#" target="_self">NAV Link 3</a>
                    <a class="nav-link-study" href="#">NAV Link 4</a>
                    <a class="nav-link-study" href="#">sNav Link 5</a>
                    
                    <a class="nav-link-study" href="#">NAV Link 4</a>
                    <a class="nav-link-study" href="#">sNav Link 5</a>
                    <a class="nav-link-study" href="#">NAV Link 4</a>
                    <a class="nav-link-study" href="#">sNav Link 5</a>
                    <a class="nav-link-study" href="#">NAV Link 4</a>
                    <a class="nav-link-study" href="#">sNav Link 5</a>


                    <div class="italic-text"> ©&nbsp;Streaming. All Rights Reserved.</div>

                </div>
            </div>
            
        </div><!--w-row -->
    </div> <!--w-container -->
</div>



