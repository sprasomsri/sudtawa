<?php if(empty($user_id)){ ?>

  <div class="section hero">
    <div>
      <div class="w-row">
        <div class="w-col w-col-8"></div>
        <div class="w-col w-col-3">
           <?php $this->widget('UserLoginBox'); ?> 
        </div>
        <div class="w-col w-col-1"></div>
      </div>
    </div>
  </div>


<!--- suggestion ---->
  <div class="white-row">
    <div class="container-full">
      <div class="how-row">
        <div class="w-row how-row" id="how-row">
          <div class="w-col w-col-4">
            <div class="how-box">
              <div>
                <img src="images/icon-1.png" alt="53fc3e10b61a9eb86e9b96ff_icon-1.png">
              </div>
              <h2 class="suggest-1">24/7</h2>
              <p>Lorem ipsum dolor sitametconsectetur adipiscing elit. Suspendisse varius enim in eros elementutristique.&nbsp;Suspendisse varius enim in eros tristique.&nbsp;</p>
            </div>
          </div>
          <div class="w-col w-col-4">
            <div class="how-box">
              <div>
                <img src="images/icon-2.png" alt="53fc3eb1f30aa4b00373eea0_icon-2.png">
                <h2 class="course-free suggest-2">คอร์สเรียน Free!!</h2>
                <p>Lorem ipsum dolor sit ametconsectetur adipiscing elit. Suspendisse varius enim in eros elementutristique.&nbsp;Suspendisse varius enim in eros tristique.&nbsp;</p>
              </div>
            </div>
          </div>
          <div class="w-col w-col-4">
            <div class="how-box">
              <div>
                <img src="images/icon-3.png" alt="53fc3ee8c4c606b2038a6022_icon-3.png">
                <h2 class="icon-pay suggest-3">การชำระเงิน</h2>
                <p>Lorem ipsum dolor sit ametconsectetur adipiscing elit. Suspendisse varius enim in eros elementutristique.&nbsp;Suspendisse varius enim in eros tristique.&nbsp;</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--- end suggestion---->
<?php } ?>


<!---- course box ---->
  <div class="gray-row">
    <div class="container-full">
      <h1 class="course-head"><?php echo Yii::t('site', 'Start learning today') ?></h1>
      <h3 class="text-study"><?php echo Yii::t('site', 'All courses are provided by Center for Veterinary Continuing Education ') ?></h3>
      
      
      <div class="w-row "> 

              <?php $count_box = 0; ?>
              <?php if (!empty($model_courses)) { ?>
                  <?php
                  foreach ($model_courses as $course) {
                      if ($count_box % 4 == 0 || $count_box == 0) {
                          $state = "new_div";
                          ?>
                          <div class="w-row">
                          <?php
                          } else {
                              $state = "not_new_div";
                          }
                          ?>

                      <?php $this->widget('CourseBox', array("data" => $course)); ?>     

                      <?php if ($state == "not_new_div" && (($count_box + 1) % 4 == 0)) { ?>
                          </div>

                    <?php }$count_box++;
                }
            } ?>
          
            <?php
            if ($count_box % 4 != 0) {
                echo "</div>"; //last number cannot % by3 close div                  
            }
            ?>                  


          </div> 
    </div>
    <div class="w-container button-study"><a class="button-course" href="<?php echo Yii::app()->createUrl("course/default/courseAll"); ?>"><?php echo Yii::t('site', 'All courses'); ?></a>
    </div>
  </div>
<!---- end course box --->






