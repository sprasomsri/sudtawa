 <div class="w-container" style="padding: 5% 1%;">
          
          
          <h2 class="course-overview-title" style="letter-spacing: 2px;">ใครอยากเรียนคอร์สแนวไหนแนะนำเราได้นะครับ </h2>
          
          <div class="w-row" id="save-state" style="display: none;">
              <div class="w-col w-col-2"></div>
              <div class="w-col w-col-8">
                  <div class="alert alert-success"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp; ได้รับคำแนะนำแล้วครับ ขอบคุณมากครับ</div>
              </div>
              <div class="w-col w-col-2"></div>
           </div>
            <br/>  
           <div class="w-row">





               <div class="w-col w-col-2"></div>
                 <div class="w-col w-col-8 text-center">
                     
                      <?php
                     $form = $this->beginWidget('CActiveForm', array(
                         'id' => 'poll',
                         'clientOptions' => array(
                             'validateOnSubmit' => true,
                         ),
                     ));
                     ?>                     
                     
                     <div class="w-row">
                         <div class="w-col w-col-4 text-left"><strong> ประเภทคอร์ส </strong></div>
                         <div class="w-col w-col-8  form-group text-left">
                             
                              <label class="checkbox-inline">
                                    <input name="category" type="radio"  value="IT" checked="checked"> IT
                              </label>
                                    <label class="checkbox-inline">
                                        <input  name="category" type="radio"  value="ภาษา"> ภาษา
                                    </label>
                                    <label class="checkbox-inline">
                                        <input  name="category" type="radio"  value="การตลาด"> การตลาด
                                    </label>

                                    <label class="checkbox-inline">
                                        <input  name="category" type="radio"  value="การลงทุน"> การลงทุน
                                    </label>

                                    <label class="checkbox-inline">
                                        <input  name="category" type="radio"  value="other"> อื่นๆ

                                    </label>
                                    
                             
                         </div>
                         
                     </div>
                     
                     <div class="w-row">
                         <div class="w-col w-col-4 text-left"><strong> แนะนำชื่อคอร์สที่อยากเรียน</strong></div>
                         <div id="div-coursename" class="w-col w-col-8 form-group has-feedback">                        
                                <input id="poll-coursename" class="form-control" name="name" type="text" placeholder="">                          
                         </div>
                         
                     </div>

                    
                     
                     <div class="w-row">
                         <div class="w-col w-col-4 text-left"><strong> ราคาคอร์สที่จ่ายได้</strong></div>
                         <div class="w-col w-col-8 form-group">
                          <select id="poll-price" name="price" class="form-control form-dropdown" >
                                <option value="100">ต่ำกว่า 100 บาท</option>
                                <option value="100-299">100 - 299 บาท</option>
                                <option value="300-599">300 - 599 บาท</option>
                                <option value="600-999">600 - 999 บาท</option>
                                <option value="1000-2499">1000 - 2499 บาท</option>
                                <option value="2500">2500 บาทขึ้นไปก็จ่ายได้ถ้าดีจริง</option>
                          </select>                        
                         </div>                         
                     </div>
                     
                     
                     <br/>
                     <label class="checkbox-inline">
                        <a id="submit-button" type="button" name="btn-submit" class="all-course-button">ส่งคำแนะนำ</a>
                     </label>
                     
                      <?php $this->endWidget(); ?>
                     
                     
                 </div>               
                <div class="w-col w-col-2"></div>
               
           </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
      $("#save-state").click(function(){
          $("#save-state").hide();
      });
            
      $("#submit-button").on("click",function(){         
         var category = $("input:radio[name=category]:checked").val();
         var name = $("#poll-coursename").val();
         var price=$("#poll-price").val();
         
         if(name==""){
             $("#div-coursename").addClass("has-error");
             $("#poll-coursename").attr("placeHolder","ระบุคอร์สเรียนหน่อยครับ");
             $("#save-state").hide();
         }else{           
            $.ajax({
                                        url: '<?php echo Yii::app()->createUrl("site/savePoll"); ?>',
                                        type: 'POST',
                                        data:{                                    
                                              'category':category,
                                              'name':name,
                                              'price':price
                                             },
                                        dataType: 'html',

                                        success: function(result) {                                          
                                              $("#save-state").show();  
                                              $("#div-coursename").removeClass("has-error");
                                          }
                                      }); 
                     }        
         
      });

      
  });
  
</script>