//_list video ---page
$(document).ready(function() {
    $(".vdo-cap").click(function(event) {
        event.preventDefault();
        var id = $(this).data("id");
        var name = $(this).data("name");
        var showname = "<i class='icon-share'></i> " + name;
        $("#demo-video-id").val(id);
        $("#demo-video-name").html(showname);
        $("#demo-preview").hide();
        $.fancybox.close();
    })
});    


//_list photo  ---page
$(document).ready(function() {
    jQuery.noConflict();
    $(".photo-cap").click(function(event) {
        event.preventDefault();

        var name = $(this).data("name");
        var showname = "<i class='icon-share'></i> " + name;


        $("#theme-phto-id").val(name);
        $("#theme-photo-name").html(showname);

        $.fancybox.close();
    })
}); 



