﻿/* footer Banner */
function viewhistory(id){
	var user=$('#view-history-exercise-'+id).data('user');
	var exercise=$('#view-history-exercise-'+id).data('exercise');
	var url=$('#view-history-exercise-'+id).data('url');
	showhistory(id,user,exercise,url);

}

function showhistory(id,user,exercise,url){	
	$.ajax({
        url: url,
        type: 'POST',
        data:{
            id:id,
            user_id:user,
            exercise:exercise,
        },

        dataType: 'html',
        beforeSend : function(message) {
            $("#loading").slideDown()
                       
        },


        complete : function(message) {
            $("#loading").hide()
        },

        success: function(message) {
            $("#exercise-content").html(message);
            $("#result").slideDown();
        }
    });

}


function showHint(question){
	$("#hint-div-"+question).slideDown();
}
