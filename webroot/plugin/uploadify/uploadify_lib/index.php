
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>UploadiFive Test</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="jquery.uploadify.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="uploadify.css">
<style type="text/css">
body {
	font: 13px Arial, Helvetica, Sans-serif;
}
#some_file_queue {
		    background-color: #FFF;
		    border-radius: 3px;
		    box-shadow: 0 1px 3px rgba(0,0,0,0.25);
		    height: 150px;
		    margin-bottom: 10px;
		    overflow: auto;
		    padding: 5px 10px;
		    width: 350px;

}
.uploadify-queue-item{
	margin :0 auto;
}
</style>
<script type="text/javascript">
		<?php $timestamp = time();?>
		$(function() {
			$('#file_upload').uploadify({
				'formData'     : {
					// 'timestamp' : '<?php echo $timestamp;?>',
					// 'token'     : '<?php echo md5('unique_hash' . $timestamp);?>',
					'value_a' : 1
				},
				'swf'      : 'uploadify.swf',
				'uploader' : 'uploadify.php',
				//add 
				'height'   : 50, //defualt 30
				'width'    : 300, //defualt 120
				'auto'     : false,
				'multi'    : true,
				'buttonText' : 'แนบไฟล์',
				'method'   : 'post',
				// 'fileObjName' : 'the_files',//defualt 'Filedata'
				// 'checkExisting' : 'check-exists.php', // return 1 if has file / 0 not has
				'fileSizeLimit' : '15MB',
				'fileTypeDesc' : 'Image File', // file of type in browse.. Defualt  'All Files'
				 'fileTypeExts' : '*.gif; *.jpg; *.jpeg; *.png', // defualt '*.*'
				// 'debug'    : true,
				 // 'itemTemplate' : '<div id="${fileID}" class="uploadify-queue-item">\
     //                <div class="cancel">\
     //                    <a href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')">X</a>\
     //                </div>\
     //                <span class="fileName">${fileName} (${fileSize})</span><span class="data"></span>\
     //            </div>' ,
                // 'preventCaching' : false,//defualt true random swf cache
                // 'progressData' : 'speed',//defualt 'percentage'
                'queueID'  : 'some_file_queue',
                // 'queueSizeLimit' : 5, // defualt 999
                // 'removeTimeout' : 10,//3
                // 'requeueErrors' : true,
                // 'successTimeout' : 5, //30
                // 'uploadLimit' : 3,//999
                // 'removeCompleted' : false,//defualt true
                'overrideEvents' : ['onDialogClose'],
				//events
				'onUploadStart' : function(file) {
					 // console.log('Attempting to upload: ' + file.name);
		            $("#file_upload").uploadify("settings", "value_a", 2);
		        } ,
				'onUploadSuccess' : function(file, data, response) {
		            alert(file.name + ' was successfully uploaded with a response of ' + response + ':' + data);
		        } ,
		        'onUploadComplete' : function(file) {
		            alert('The file ' + file.name + ' finished processing.');
		        } ,
		        'onProgress' : function(event,ID,fileObj,data) 
				{
				  var bytes = Math.round(data.bytesLoaded / 1024);
				  $('#' + $(event.target).attr('id') + ID).find('.percentage').text(' - ' + bytes + 'KB Uploaded');
				  return false;
				},
				 'onUploadError' : function(file, errorCode, errorMsg, errorString) {
		            alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
		        },
		        'onDialogOpen' : function() {
		            $('#message_box').html('The file dialog box was opened...');
		        },
		        'onDialogClose'  : function(queueData) {
		            alert(queueData.filesQueued + ' files were queued of ' + queueData.filesSelected + ' selected files. There are ' + queueData.queueLength + ' total files in the queue.');
		        	$("#total_file").html("จำนวนไฟล์ที่เลือกทั้งหมด "+queueData.queueLength + " ไฟล์");
		        },
		        'onCancel' : function(file) {
		            alert('The file ' + file.name + ' was cancelled.');
		        },
		        'onSelect' : function(file) {
		            alert('The file ' + file.name + ' was added to the queue.');
		        }    
			});
		});

		function changeBtnText() {
		    $('#file_upload').uploadify('settings','buttonText','BROWSE');
		}

		function returnBtnText() {
		    alert('The button says ' + $('#file_upload').uploadify('settings','buttonText'));
		}
	</script>
</head>

<body>
	<h1>Uploadify</h1>
	<form>
                
		<div id="queue"></div>
		<!-- <input id="file_upload" name="file_upload" type="file" multiple="true"> -->
		<p><input type="file" name="file_upload" id="file_upload" /></p>
		
		<div id="some_file_queue"></div>
		<div id="total_file">จำนวนไฟล์ที่เลือกทั้งหมด 0 ไฟล์</div>
		
		<!-- <p><a href="javascript:$('#file_upload').uploadify('upload')">Upload Files</a></p> -->
		
		<a href="javascript:$('#file_upload').uploadify('cancel')">ยกเลิกไฟล์แรก</a> |
		<a href="javascript:$('#file_upload').uploadify('cancel', '*')">ยกเลิกไฟล์ทั้งหมด</a> | 
		<a href="javascript:$('#file_upload').uploadify('upload', '*')">เริ่มอัพโหลดไฟล์</a>
		<a href="javascript:$('#file_upload').uploadify('stop')">Stop the Uploads!</a>
		<div id="message_box"></div>

		<a href="javascript:changeBtnText()">Change the Button Text</a> | <a href="javascript:returnBtnText();">Read the Button</a>

		<a href="javascript:$('#file_upload').uploadify('disable', true)">Disable the Button</a> | 
		<a href="javascript:$('#file_upload').uploadify('disable', false)">Enable the Button</a>


			
	</form>

	
		<img src="<?php echo '/uploadify/uploads/cesc.gif';?>"/>
		<img src="<?php echo '/uploadify/uploads/th_sanjivszoro.gif';?>"/>
		<img src="<?php echo '/uploadify/uploads/th_luffyhancockgif.gif';?>"/>
		<img src="<?php echo '/uploadify/uploads/cute (2).jpg';?>"/>
</body>
</html>